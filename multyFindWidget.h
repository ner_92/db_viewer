#ifndef MULTYFINDWIDGET_H
#define MULTYFINDWIDGET_H
#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include <QListWidget>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QString>
#include <QLabel>
#include <QComboBox>
#include <QTableView>
#include <QStandardItemModel>
//#include "DBThread.h"
#include <QScrollBar>
#include <QWheelEvent>
#include <QCoreApplication>
#include <QToolTip>
#include <QMenu>
#include <QMenuBar>
#include <QStringListModel>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QGroupBox>
#include <QCheckBox>
#include <QSettings>
#include <QFileDialog>
#include <QDialog>
#ifdef WIN32
#include <Windows.h>
#endif
#include <QDateTime>

/**
*	\class multyFindWidget
*	\brief -- ����� multyFindWidget;
*/
class multyFindWidget : public QDialog
{
	Q_OBJECT

public:
	
	/**
	*	\brief -- �����������;
	*	\param parent -- ��������.
	*/
	multyFindWidget(QWidget *parent = 0);
	
	/**
	*	\brief -- ����������;
	*/
	~multyFindWidget();

private:
	/// -- ������;
	QWidget* widg;
public:
	/// -- ���� ���;
	QLineEdit *MSK1Edit;
	/// -- ���� ���;
	QLineEdit *MSK2Edit;
	/// -- ���� �����;
	QLineEdit *ADREdit;
	/// -- ���� ��������;
	QLineEdit *PADREdit;
	/// -- ���� ���������� ��;
	QLineEdit *CountSDEdit;
	/// -- ���� ��;
	QTextEdit *SDEdit;
	/// -- ���� ��;
	QLineEdit *BT1Edit;
	/// -- ���� ��;
	QLineEdit *BT2Edit;
	/// -- ���� ��;
	QLineEdit *OKEdit;
	/// -- ���� ��;
	QLineEdit *CWEdit;
	/// -- ���� ��;
	QLineEdit *AWEdit;
	/// -- ���� �� �����;
	QLineEdit *KU_SPOBUEdit;
	/// -- ���� ���2;
	QLineEdit *KS_KPI2Edit;
	/// -- ���� ���;
	QLineEdit *UTCEdit;
	/// -- ���� ���;
	QLineEdit *UTCEdit2;
	/// -- ���� ����������� ��;
	QLineEdit *diffBTEdit;
	/// -- ����  ����������� ��;
	QLineEdit *diffBTEdit2;
	/// -- ���� �� ���;
	QLineEdit *MD_CBKEdit;
	/// -- ���� �����;
	QLineEdit *metkaEdit;

	/// -- ����� ���;
	QLabel *MSKLabel;
	/// -- ����� �����;
	QLabel *ADRLabel;
	/// -- ����� ��������;
	QLabel *PADRLabel;
	/// -- ����� ���������� ��;
	QLabel *CountSDLabel;
	/// -- ����� ��;
	QLabel *SDLabel;
	/// -- ����� ��;
	QLabel *BTLabel;
	/// -- ����� ��;
	QLabel *OKLabel;
	/// -- ����� ��;
	QLabel *CWLabel;
	/// -- ����� ��;
	QLabel *AWLabel;
	/// -- ����� �� ��� ��;
	QLabel *KU_SPOBULabel;
	/// -- ����� ���2;
	QLabel *KS_KPI2Label;
	/// -- ����� ���;
	QLabel *UTCLabel;
	/// -- ����� ����������� ��;
	QLabel *diffBTLabel;
	/// -- ����� �� ���;
	QLabel *MD_CBKLabel;
	/// -- ����� �����;
	QLabel *metkaLabel;
	//difference

	/// -- ������� ���;
	QGroupBox *MMKOGroupBox;
	/// -- ������� ���;
	QGroupBox *LMKOGroupBox;
	/// -- ������� ������;
	QGroupBox *FormatGroupBox;

	/// -- ������� ;
	QCheckBox *MMKOCheckBox1;
	/// -- ������� ;
	QCheckBox *MMKOCheckBox2;
	/// -- ������� ;
	QCheckBox *LMKOCheckBox1;
	/// -- ������� ;
	QCheckBox *LMKOCheckBox2;
	/// -- ������� ;
	QCheckBox *FormatCheckBox1;
	/// -- ������� ;
	QCheckBox *FormatCheckBox2;
	/// -- ������� ;
	QCheckBox *FormatCheckBox3;

	/// -- ������ ��;
	QPushButton *okButton;
	/// -- ������ ������;
	QPushButton *cancelButton;

	/// -- ������� ;
	QGroupBox *filter1MKOGroupBox;
	/// -- ������� ;
	QGroupBox *filter2MKOGroupBox;
	/// -- ������� ;
	QGroupBox *filter3MKOGroupBox;
	/// -- ������� ;
	QGroupBox *filter4MKOGroupBox;
	/// -- ������� ;
	QGroupBox *filter5MKOGroupBox;
	/// -- �������; 
	QGroupBox *filter6MKOGroupBox;
	/// -- ������� ;
	QGroupBox *filter7MKOGroupBox;

	/// -- ����������;
	QGridLayout *DB_MKOmini1GridLayout;
	/// -- ����������;
	QGridLayout *DB_MKOmini2GridLayout;
	/// -- ����������;
	QGridLayout *DB_MKOmini3GridLayout;
	/// -- ����������;
	QGridLayout *DB_MKOmini4GridLayout;
	/// -- ����������; 
	QGridLayout *DB_MKOmini5GridLayout;
	/// -- ����������; 
	QGridLayout *DB_MKOmini6GridLayout;
	/// -- ����������; 
	QGridLayout *DB_MKOmini7GridLayout;

	/// -- ����������; 
	QHBoxLayout *h_filterMKOLayout;

signals:
	
	/**
	*	\brief -- ������ ������ ��;
	*/
	void okButtonSignal();

public slots:
	
	/**
	*	\brief -- ���� ������ ��;
	*/
	void pushOkButton();

	/**
	*	\brief -- ���� ��������;
	*/
	void del();

	/**
	*	\brief -- �������� ��;
	*/
	void SDTextChange();
	
	/**
	*	\brief -- �������� ���
	*/
	void MSKTextCheck(QString str);
};

#endif // MULTYFINDWIDGET_H
