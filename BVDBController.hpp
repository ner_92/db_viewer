#ifndef BVDBCONTROLLER_H
#define BVDBCONTROLLER_H

#include "CommonDBController.h"

#include <QString>
#include <QObject>
#include <exception>
#include <list>
#include <string>
#include <functional>

/// -- Структура сообщений для БД БВ.
struct MsgFromBVDB
{
	/**
	*	\brief -- Конструктор;
	*	\param _bv -- бортовое время;
	*	\param _utc -- время UTC.
	*/
	MsgFromBVDB(long long _time_stamp = 0, unsigned short _bv = 0, long long _utc = 0)
	: time_stamp(_time_stamp), bv(_bv), utc(_utc)
	{}
	/// -- Метка времени;
	long long time_stamp = 0;
	/// -- Бортовое время;
	unsigned short bv = 0;
	/// -- Метка времени.
	long long utc = 0;
};
Q_DECLARE_METATYPE(MsgFromBVDB);


/**
*!\class BVDBController
*	\brief -- Управление БД БВ.
*
*/
class BVDBController : public CommonDBController
{
public:
	/**
	*	\brief -- Конструктор;
	*	\param _read_delay -- период опроса.
	*/
	BVDBController(int _read_delay) : CommonDBController(_read_delay) {}

	/**
	*	\brief -- Запись значения БВ в базу
	*	\param time_stamp -- метка времени прихода БВ
	*	\param bv -- значение БВ в числе дискретов
	*	\param utc -- значение UTC в формате time_t
	*/
	virtual bool insertBVData(long long time_stamp, unsigned short bv, long long utc) = 0;
	/**
	*	\brief -- Запрос сообщения из базы БВ по МСК времени;
	*	\param dt -- МСК время, для которого нужно получить запись;
	*	\return -- запись из БД БВ, соответствующую dt.
	*/
	virtual MsgFromBVDB getBVbyDT(long long dt) = 0;
	/**
	*	\brief -- Запрос списка МСК времен, соответствующих запросу;
	*	\param query -- запрос к БД БВ;
	*	\return -- список сообщений из БД БВ.
	*/
	virtual QStringList getDTbyBV(QString query) = 0;
	/**
	*	\brief -- Запрос списка МСК времен, соответствующих диапазону БВ.
	*	\param BV1 -- начальное БВ;
	*	\param BV2 -- конечное БВ;
	*	\return -- список МСК времен.
	*/
	virtual QStringList getDTbyBV(int BV1, int BV2) = 0;

	virtual std::vector<MsgFromBVDB> getBVbyDT_12(long long dt1, long long dt2) = 0;
};


#include "mongo_v3/MongoBVDBController_v3.h"

#endif // MAINSERVERDBCONTROLLER_H