#include "multyFindForm.h"


multyFindForm::multyFindForm(QWidget *parent)
: QWidget(parent)
{
	widg = new QWidget(this);
	//widg->setFixedSize(30, 300);

	findEdit = new QTextEdit(widg);
	findEdit->setMaximumHeight(25);
	findEdit->setMaximumWidth(145);
	findEdit->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	findEdit->setLineWrapMode(QTextEdit::NoWrap);
//	setButton = new QPushButton("sett", widg);
	setButton = new QPushButton(widg);
	setButton->setFlat(true);
	QPixmap pix(":img/settings.png");
	setButton->setIcon(pix);
	setButton->setIconSize(QSize(32, 34));

	//setButton->setIcon(QIcon(":img/settings.png"));
	//setButton->setFixedSize(setButton->iconSize());
	//setButton->raise();

	addButton = new QPushButton(widg);
	addButton->setFlat(true);
	QPixmap pix2(":/img/add.png");
	addButton->setIcon(pix2);
	addButton->setIconSize(QSize(32, 34));


	delButton = new QPushButton(widg);
	delButton->setFlat(true);
	QPixmap pix3(":img/delete.png");
	delButton->setIcon(pix3);
	delButton->setIconSize(QSize(32, 34));
	
	hLayout = new QHBoxLayout(widg);
	hLayout->addWidget(findEdit);
	hLayout->addWidget(setButton);
	hLayout->addWidget(delButton);
	hLayout->addWidget(addButton);
	hLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));

	connect(addButton, &QPushButton::clicked, this, &multyFindForm::addNewForm);
	connect(delButton, &QPushButton::clicked, this, &multyFindForm::delForm);
	connect(setButton, &QPushButton::clicked, this, &multyFindForm::showDialog);
}

multyFindForm::~multyFindForm()
{

}

void multyFindForm::hideDelButton()
{
	delButton->hide();
}

void multyFindForm::hideAddButton()
{
	addButton->hide();
}

void multyFindForm::showAddButton()
{
	addButton->show();
}

void multyFindForm::delForm()
{
	emit delFormSignal(index);
}

void multyFindForm::showDialog()
{
	emit showDialogSignal(index);
}

void multyFindForm::setTextToEdit(QString str)
{
	findEdit->setText(str);
}