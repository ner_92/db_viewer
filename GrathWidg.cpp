#include "GrathWidg.h"
#include <QApplication>
#include "instruments.h"

GrathWidg::GrathWidg(QWidget *parent)
: QWidget(parent)
{
	thr_map.insert("MKO", new DBReaderThread("MKO"));
	thr_map.insert("AIK", new DBReaderThread("ТМ АИК aik"));
	thr_map.insert("MCA", new DBReaderThread("ТМ МЦА mca"));
	for (QMap<QString, DBReaderThread*>::iterator itr = thr_map.begin(); itr != thr_map.end(); itr++)
		(*itr)->start();
	cur_thr = NULL;
	//connectToDB("AIK");

	queryValueList = new QList<QMap<QString, QVariant> >();

	MSKLabel = new QLabel("MSK");
	MSK1Edit = new QLineEdit();
	MSK1Edit->setMaximumHeight(25);
	MSK1Edit->setMaximumWidth(135);
	MSK1Edit->setInputMask("99.99.9999 99:99:99.999");

	MSK2Edit = new QLineEdit();
	MSK2Edit->setMaximumHeight(25);
	MSK2Edit->setMaximumWidth(135);
	MSK2Edit->setInputMask("99.99.9999 99:99:99.999");

	BTLabel = new QLabel("БВ");
	BT1Edit = new QLineEdit();
	BT1Edit->setMaximumHeight(25);
	BT1Edit->setMaximumWidth(135);
	BT1Edit->setInputMask("99:99:99");

	BT2Edit = new QLineEdit();
	BT2Edit->setMaximumHeight(25);
	BT2Edit->setMaximumWidth(135);
	BT2Edit->setInputMask("99:99:99");

	TLSignanLabel = new QLabel("Номер сигнала ТМ");
	TLSignanEdit = new SearchBox();
	TLSignanEdit->setMaximumHeight(25);
	TLSignanEdit->setMaximumWidth(185);
	QStringList oksIdList;
	oksIdList << "" << "АИК_103" << "АИК_130" << "АИК_131" << "АИК_132" << "АИК_133" << "АИК_134" << "АИК_135" << "АИК_136";
	oksIdList << "МЦА_1" << "МЦА_2" << "МЦА_3" << "МЦА_4" << "МЦА_5";
	
	oksIdList << "МЦА_101" << "МЦА_102" << "МЦА_103" << "МЦА_104" << "МЦА_105" << "МЦА_106" << "МЦА_107" << "МЦА_108" << "МЦА_109" << "МЦА_110";

	buildButton = new QPushButton("Построить график");
	zoomInButton = new QPushButton("+");
	zoomOutButton = new QPushButton("-");
	zoomLeftButton = new QPushButton("<<");
	zoomRightButton = new QPushButton(">>");

	QGridLayout *sceneLayout = new QGridLayout();

	scene = new QGraphicsScene();
	view = new QGraphicsView(scene);
	view->setGeometry(0, 0, scene->width(), scene->height());
	view->setScene(scene);
	view->viewport()->installEventFilter(this);
	view->viewport()->setMouseTracking(true);
	sceneLine = new QGraphicsLineItem();
	view->setStyleSheet("QGraphicsView { border-style: none; }");

	scene->setSceneRect(0, 0, view->width(), view->height());

	leftScene = new QGraphicsScene();
	leftView = new QGraphicsView(leftScene);
	leftView->setGeometry(0, 0, leftView->width(), leftView->height());
	leftView->setScene(leftScene);
	leftScene->setSceneRect(0, 0, leftView->width(), leftView->height());
	leftView->setMaximumWidth(80);
	leftScene->setBackgroundBrush(QBrush(QColor(240, 240, 240)));
	leftView->setStyleSheet("QGraphicsView { border-style: none; }");
	leftView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	leftView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	leftView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	botScene = new QGraphicsScene();
	botView = new QGraphicsView(botScene);
	botView->setGeometry(0, 0, botScene->width(), botScene->height());
	botView->setScene(botScene);
	leftScene->setSceneRect(0, 0, botView->width(), botView->height());
	botView->setMaximumHeight(80);
	botScene->setBackgroundBrush(QBrush(QColor(240, 240, 240)));
	botView->setStyleSheet("QGraphicsView { border-style: none; }");
	botView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	botView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	botView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	//QGraphicsRectItem* item = scene->addRect(0, 0, 100, 100, QPen(Qt::green));
	view->setAlignment(Qt::AlignTop | Qt::AlignLeft);

	sceneLayout->addWidget(leftView, 0, 0);
	sceneLayout->addWidget(view, 0, 1);
	sceneLayout->addWidget(botView, 1, 1);
	sceneLayout->setSpacing(0);
	sceneLayout->setMargin(0);

	text = new QGraphicsTextItem();
	leftText = new QGraphicsTextItem();
	leftLine = new QGraphicsLineItem();
	botText = new QGraphicsTextItem();
	botLine = new QGraphicsLineItem();

	QGridLayout *filtrLayout = new QGridLayout();
	filtrLayout->addWidget(MSKLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	filtrLayout->addWidget(MSK1Edit, 1, 0);
	filtrLayout->addWidget(MSK2Edit, 1, 1);

	filtrLayout->addWidget(BTLabel, 2, 0, 1, 2, Qt::AlignHCenter);
	filtrLayout->addWidget(BT1Edit, 3, 0);
	filtrLayout->addWidget(BT2Edit, 3, 1);

	filtrLayout->addWidget(TLSignanLabel, 1, 2, Qt::AlignHCenter);
	filtrLayout->addWidget(TLSignanEdit, 2, 2);
	filtrLayout->addWidget(buildButton, 3, 2);
	filtrLayout->addWidget(zoomInButton, 0, 3);
	filtrLayout->addWidget(zoomOutButton, 0, 4);
	filtrLayout->addWidget(zoomLeftButton, 1, 3);
	filtrLayout->addWidget(zoomRightButton, 1, 4);

	filtrLayout->setSpacing(7);
	//filtrLayout->addWidget(view);
	QHBoxLayout *h_lay = new QHBoxLayout();
	h_lay->addLayout(filtrLayout);
	h_lay->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));

	QVBoxLayout *v_lay = new QVBoxLayout(this);
	//v_lay->addLayout(filtrLayout);
	v_lay->addLayout(h_lay);
	//v_lay->addWidget(view);
	v_lay->addLayout(sceneLayout);
	
	progressBar = new QProgressBar();
	progressBar->setMinimum(0);
	progressBar->setMaximum(230);
	progressBar->setValue(0);
	progressLabel = new QLabel("Начало работы");
	progressLabel->setMinimumWidth(250);
	QFont font = progressLabel->font();
	font.setPointSize(12);
	progressLabel->setFont(font);
	QHBoxLayout *progressbarLayout = new QHBoxLayout();
	progressbarLayout->addWidget(progressLabel);
	progressbarLayout->addWidget(progressBar);

	v_lay->addLayout(progressbarLayout);

	connect(buildButton, &QPushButton::clicked, this, &GrathWidg::query);
	connect(zoomInButton, &QPushButton::clicked, this, &GrathWidg::zoomIn);
	connect(zoomOutButton, &QPushButton::clicked, this, &GrathWidg::zoomOut);
	connect(zoomLeftButton, &QPushButton::clicked, this, &GrathWidg::zoomLeft);
	connect(zoomRightButton, &QPushButton::clicked, this, &GrathWidg::zoomRight);


	//grathStructValues = new grathValues;

	valuesList = new QList<QList<PointData>>;
	okList = new QList<okValues*>;
	TMList = new QList<TMValues *>;
	
	
}

GrathWidg::~GrathWidg()
{
	
}

void GrathWidg::receiptData()
{
	globalVect = cur_thr->getDataBlock();
	vectLeftValue = 0;
	vectRightValue = 0;
	data(globalVect);
}

void GrathWidg::zoomIn()
{
	if (globalVect.empty())
		return;
	vectLeftValue += 250;
	vectRightValue += 250;
	if (vectLeftValue + vectRightValue >= globalVect.size())
	{
		vectLeftValue -= 250;
		vectRightValue -= 250;
		return;
	}
	auto first = globalVect.begin() + vectLeftValue;
	auto last = globalVect.end() - vectRightValue;
	std::vector<QVariant> subVect(first, last);
	data(subVect);
}

void GrathWidg::zoomOut()
{
	if (globalVect.empty())
		return;
	vectLeftValue -= 1000;
	vectRightValue -= 1000;
	if (vectLeftValue < 0)
	{
		vectLeftValue = 0;
	}
	if (vectRightValue < 0)
	{
		vectRightValue = 0;
	}
	auto first = globalVect.begin() + vectLeftValue;
	auto last = globalVect.end() - vectRightValue;
	std::vector<QVariant> subVect(first, last);
	data(subVect);
}

void GrathWidg::zoomLeft()
{
	if (globalVect.empty())
		return;

	vectLeftValue -= 250;
	vectRightValue += 250;
	if ((vectLeftValue < 0) || (vectRightValue < 0))
	{
		vectLeftValue += 250;
		vectRightValue -= 250;
		return;
	}
	auto first = globalVect.begin() + vectLeftValue;
	auto last = globalVect.end() - vectRightValue;
	std::vector<QVariant> subVect(first, last);
	data(subVect);
}

void GrathWidg::zoomRight()
{
	if (globalVect.empty())
		return;
	vectLeftValue += 250;
	vectRightValue -= 250;
	if ((vectLeftValue > globalVect.size()) || (vectRightValue > globalVect.size()))
	{
		vectLeftValue -= 250;
		vectRightValue += 250;
		return;
	}
	auto first = globalVect.begin() + vectLeftValue;
	auto last = globalVect.end() - vectRightValue;
	std::vector<QVariant> subVect(first, last);
	data(subVect);
}

void GrathWidg::data(std::vector<QVariant> vect)
{

	grathMinY = 0;
	grathMaxY = 0;

	valuesList->clear();
	if (vect.empty())
	{
		buildButton->setEnabled(true);
		changeProgressLabel(0, "Запрос не выдал результатов");
		return;
	}
	changeProgressLabel(100, "Анализ результатов");
	long long ND;
	long long KD;
	if (currentDB == "MCA")
	{
		ND = qvariant_cast<MsgFromTMDB>(vect.front()).dt;
		KD = qvariant_cast<MsgFromTMDB>(vect.back()).dt;
	}
	if (currentDB == "AIK")
	{
		ND = qvariant_cast<MsgFromTMDB>(vect.front()).dt;
		KD = qvariant_cast<MsgFromTMDB>(vect.back()).dt;
	}
	if (currentDB == "MKO")
	{
		ND = qvariant_cast<MsgFromMonitorDB>(vect.front()).dt;
		KD = qvariant_cast<MsgFromMonitorDB>(vect.back()).dt;
	}
	//17.07.2015 12:35 : 00.000
	long long interval = KD - ND;
	double norm = (double)interval / (view->width() - shift * 3/*80*/);
	//qDebug() << "norm " << norm  << " " << ND << " " << KD;
	if (currentDB == "MKO")
	{
		for (int i = 0; i < okList->size(); ++i)
		{
			points = new QList<PointData>;
		
			int addr = okList->at(i)->addr;
			int paddr = okList->at(i)->paddr;
			//points->clear();
			//auto& ok_itr = okList->begin();
			foreach(const QVariant& q_obj, vect)
			{
				MsgFromMonitorDB obj = qvariant_cast<MsgFromMonitorDB>(q_obj);
				if (addr == obj.addr && paddr == obj.saddr)
				{
					int pos = (obj.dt - ND) / norm;
					//int word = (*ok_itr)->word;
					//auto& bs = (*ok_itr)->mask;
					int word = okList->at(i)->word;
					auto& bs = okList->at(i)->mask;
					//QString tmp_str = obj.words;
					//tmp_str.remove(" ");
					//uint hex = tmp_str.mid(word * 4 - 4, 4).toUInt(new bool, 16);

					if ((addr == 27) && (obj.data.size() > 28))
						continue;

					if (obj.data.size() < word)
						continue;
					int hex = obj.data.at(okList->at(i)->word - 1);
					//int hex = obj.words.mid(okList->at(i)->word * 4 - 4, okList->at(i)->word * 4).toInt();
					int tmp_mask = bs.to_ulong();
					hex &= bs.to_ulong();
					int size = bs.size();
					for (int i = 0; i < bs.size(); ++i)
					{
						if (bs.test(i))
							break;
						hex = hex >> 1;
					}
					///////////////////////////////////////////////////////////
					//if (hex != 0)
					//	hex = hex / 4 - 273;
					//if (hex < 0)
					//	hex = 0;
					///////////////////////////////////////////////////////////
					hex = hex + 2 * i;
					points->push_back(PointData(obj.dt, pos, hex));
					if (grathMaxY < hex)
						grathMaxY = hex;
					if (grathMinY > hex)
						grathMinY = hex;
				}
			}
			valuesList->push_back(*points);
		}
	}

	if (currentDB == "AIK")
	{
		for (int i = 0; i < TMList->size(); ++i)
		{
			points = new QList<PointData>;

			int tm_code = TMList->at(i)->tm_code;
			double value = TMList->at(i)->value;
			//points->clear();
			//auto& ok_itr = TMList->begin();
			foreach(const QVariant& q_obj, vect)
			{
				MsgFromTMDB obj = qvariant_cast<MsgFromTMDB>(q_obj);
				if (tm_code == obj.tm_code)
				{
					int pos = (obj.dt - ND) / norm;
					
					points->push_back(PointData(obj.dt, pos, obj.value.toDouble()));
					if (grathMaxY < obj.value.toDouble())
						grathMaxY = obj.value.toDouble();
					if (grathMinY > obj.value.toDouble())
						grathMinY = obj.value.toDouble();
				}
			}
			valuesList->push_back(*points);
		}
	}

	if (currentDB == "MCA")
	{
		for (int i = 0; i < TMList->size(); ++i)
		{
			points = new QList<PointData>;

			int tm_code = TMList->at(i)->tm_code;
			double value = TMList->at(i)->value;
			//points->clear();
			//auto& ok_itr = TMList->begin();
			foreach(const QVariant& q_obj, vect)
			{
				MsgFromTMDB obj = qvariant_cast<MsgFromTMDB>(q_obj);
				if (tm_code == obj.tm_code)
				{
					int pos = (obj.dt - ND) / norm;

					points->push_back(PointData(obj.dt, pos, obj.value.toDouble()));	// TODO: Разные типы
					if (grathMaxY < obj.value.toDouble())
						grathMaxY = obj.value.toDouble();
					if (grathMinY > obj.value.toDouble())
						grathMinY = obj.value.toDouble();
				}
			}
			valuesList->push_back(*points);
		}
	}

	changeProgressLabel(200, "Рисую");
	drawBack(grathMaxY, grathMinY);
	changeProgressLabel(230, "Отрисовка");
	drawGrath(vect, grathMaxY);
	changeProgressLabel(0, "Завершено");
}

void GrathWidg::drawBack(double maxY, double minY)
{
	removeLeftBotLine();
	scene->clear();
	draw = true;
	leftScene->clear();
	botScene->clear();
	QPen pen(Qt::black);
	pen.setWidth(1);
	x = view->width();
	y = view->height() ;

	// Линии для оси ординат
	pen.setWidth(1);

	double delta_bv = (maxY - minY) / 10;
	//	Что бы опустить график пониже "-10" заменить на "-20". Но отображение leftText от перемещения мыши становится не корректным
	double deltaY = (y - 10) / 10;

	for (int i = 0; i <= 10; i++)
	{
		int tmp = y - deltaY * i;
		if (i == 10)
		{
			// На последнем шаге y = 0. Но на самом деле на несколько стотысячных(на моём примере) меньше нуля и само рисоваться не желает. Рисую принудительно.
			scene->addLine(0, 0,  x - shift * 3, 0, pen);
			leftScene->addLine(40, 1, leftView->width() - 4, 1, pen);
		}
		else
		{
			scene->addLine(0, y - deltaY * i - shiftY, x - shift * 3, y - deltaY * i - shiftY, pen);
			leftScene->addLine(40, y - deltaY * i - shiftY, leftView->width() - 4, y - deltaY * i - shiftY, pen);
		}
		text = leftScene->addText(QString::number(minY + delta_bv * i));
		QFont font2 = text->font();
		font2.setPixelSize(14);
		text->setFont(font2);
		if (i == 0)
			text->setPos(10, leftView->height() - deltaY * i - 30);
		else
			text->setPos(10, leftView->height() - deltaY * i );
	}
}

void GrathWidg::drawGrath(std::vector<QVariant> vect, double maxY)
{

//	view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//	view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	qDebug() << "vl size " << valuesList->size();
	qDebug() << "vect size " << vect.size();
	for (int j = 0; j < valuesList->size(); ++j)
	{
		if (valuesList->at(j).size() < 1)	/// < 2 ???
			continue;
		long long ND = valuesList->at(j).front().dt;
		long long KD = valuesList->at(j).back().dt;
		double deltaY = 0;
		if (maxY != 0)
			//	Что бы опустить график пониже "-10" заменить на "-20". Но отображение leftText от перемещения мыши становится не корректным
			deltaY = (y - 10) / maxY;
		
		for (int i = 1; i < valuesList->at(j).size(); ++i)
		{
			QPen pen;
			if (currentDB == "MKO")
				pen = okList->at(j)->pen;
			if ((currentDB == "AIK" )|| (currentDB == "MCA"))
				pen = TMList->at(j)->pen;
			const PointData& prev_pd = valuesList->at(j).at(i - 1);

			//const PointData& prev_pd = points[i - 1];
			const PointData& cur_pd = valuesList->at(j).at(i);
			if ((prev_pd.x != cur_pd.x) || (prev_pd.y != cur_pd.y))
				scene->addLine(/*shift +*/ prev_pd.x, y - prev_pd.y * deltaY - shiftY, /*shift +*/ cur_pd.x, y - cur_pd.y * deltaY - shiftY, pen);
		}
	}
	//ND = qvariant_cast<MsgFromMonitorDB>(vect.front()).dt;
	//KD = qvariant_cast<MsgFromMonitorDB>(vect.back()).dt;

	if (currentDB == "AIK")
	{
		ND = qvariant_cast<MsgFromTMDB>(vect.front()).dt;
		KD = qvariant_cast<MsgFromTMDB>(vect.back()).dt;
	}
	if (currentDB == "MCA")
	{
		ND = qvariant_cast<MsgFromTMDB>(vect.front()).dt;
		KD = qvariant_cast<MsgFromTMDB>(vect.back()).dt;
	}
	if (currentDB == "MKO")
	{
		ND = qvariant_cast<MsgFromMonitorDB>(vect.front()).dt;
		KD = qvariant_cast<MsgFromMonitorDB>(vect.back()).dt;
		ND = ND / 1000;
		KD = KD / 1000;
	}

	long long interval = KD - ND;

	norm = interval / (view->width() - shift * 3);
	int a = view->width();

	for (float x = 0; x <= view->width() - shift * 3; x += ((view->width() - shift * 3) / 24) /*100*/)
	{
		QPen pen;
		pen.setColor(Qt::black);
		pen.setWidth(1);
		botScene->addLine(x , 0, x , 10, pen);
		long long tmpX = x * norm; //	tmpX - потому что
		long long val = ND + tmpX; //	long long число + 0.000000 * long long = другое long long число, а по отдельности - ок
		text = botScene->addText(QDateTime::fromMSecsSinceEpoch(val).toString("hh:mm:ss"));
		QString d_time = QDateTime::fromMSecsSinceEpoch(val).toString("hh:mm:ss");
		if (x == 0)
			text->setPos(25, 8);
		else
			text->setPos(x+15, 8);
		text->setRotation(90);
		QFont font = text->font();
		font.setPixelSize(14);
		text->setFont(font);
	}
	buildButton->setEnabled(true);
}

void GrathWidg::query()
{
	buildButton->setEnabled(false);
	okList->clear();
	TMList->clear();
	queryValueList->clear();

	

	int ok_count = 0;

	QStringList all_list = TLSignanEdit->currentText().split(',');
	foreach(auto& _signal, all_list)
	{
		QStringList list = _signal.split('_');

		if (list.at(0) == "АИК")
		{
			connectToDB("AIK");
			queryDB = "AIK";
			//queryValueList->push_back(QMap<QString, QVariant>());
			//queryValueList->back()["signalTM1"] = 133;
			TMStruckValues = new TMValues;
			TMStruckValues->tm_code = list.at(1).toInt();
			TMStruckValues->textValue = new QGraphicsTextItem();
			TMStruckValues->pen.setColor(Qt::blue);
			TMStruckValues->pen.setWidth(1);
			appendQueryAIK(TMStruckValues);

		}

		if (list.at(0) == "МЦА")
		{
			connectToDB("MCA");
			queryDB = "MCA";
			TMStruckValues = new TMValues;
			TMStruckValues->tm_code = list.at(1).toInt();
			TMStruckValues->textValue = new QGraphicsTextItem();
			TMStruckValues->pen.setColor(instr::get_aik_colour(ok_count));
			TMStruckValues->pen.setWidth(1);
			appendQueryAIK(TMStruckValues);
			ok_count++;
		}

		if (list.at(0) == "OK")
		{
			connectToDB("MKO");
			queryDB = "MKO";
			foreach(OK ok, ok_list)
			{
				if (ok.ok_id == list.at(1))
				{
					okStruckValues = new okValues;
					okStruckValues->addr = ok.ok_mkoAddr.toInt();
					okStruckValues->paddr = ok.ok_mkoSAddr.toInt();
					okStruckValues->word = ok.ok_mkoWord.toInt();

					for (int i = 0; i < ok.ok_mkoBitCount.toInt(); i++)
					{
						okStruckValues->mask.set(i + ok.ok_mkoBitStart.toInt());
					}
					okStruckValues->pen.setColor(instr::get_aik_colour(ok_count));
					okStruckValues->pen.setWidth(2);
					okStruckValues->textValue = new QGraphicsTextItem();
					if (ok_count == 0)////////////
						appendQueryMKO(okStruckValues);

					okList->append(okStruckValues);
					ok_count++;
				}
			}
		}
	}

	cur_thr->needDataBlock(0, -1, queryValueList, 1, 0);


}

void GrathWidg::appendQueryMKO(okValues *okStruckValues)
{
	queryValueList->push_back(QMap<QString, QVariant>());

	if (MSK1Edit->text() != ".. ::.")
		queryValueList->back()["MSK1"] = MSK1Edit->text();
	if (MSK2Edit->text() != ".. ::.")
		queryValueList->back()["MSK2"] = MSK2Edit->text();

	if (BT1Edit->text() != "::")
		queryValueList->back()["BT1"] = BT1Edit->text();
	if (BT2Edit->text() != "::")
		queryValueList->back()["BT2"] = BT2Edit->text();


	queryValueList->back()["addr"] = QString::number(okStruckValues->addr);
	queryValueList->back()["paddr"] = QString::number(okStruckValues->paddr);
	if (okStruckValues->addr == 27)
		queryValueList->back()["countSD"] = QString::number(28);
	

	
}

void GrathWidg::appendQueryAIK(TMValues *TMStruckValues)
{
	queryValueList->push_back(QVariantMap());

	if (MSK1Edit->text() != ".. ::.")
		queryValueList->back()["MSK1"] = MSK1Edit->text();
	if (MSK2Edit->text() != ".. ::.")
		queryValueList->back()["MSK2"] = MSK2Edit->text();

	if (BT1Edit->text() != "::")
		queryValueList->back()["BT1"] = BT1Edit->text();
	if (BT2Edit->text() != "::")
		queryValueList->back()["BT2"] = BT2Edit->text();


	queryValueList->back()["signalTM1"] = QString::number(TMStruckValues->tm_code);
	queryValueList->back()["signalTM2"] = QString::number(TMStruckValues->tm_code);
	
	TMList->append(TMStruckValues);
}


void GrathWidg::removeLeftBotLine()
{
	if (!draw)
	{
		leftScene->removeItem(leftLine);
		leftScene->removeItem(leftText);
		//leftText->deleteLater();

		botScene->removeItem(botLine);
		botScene->removeItem(botText);
		botText->deleteLater();
		scene->removeItem(sceneLine);
		removeGrathValue();
		draw = !draw;
	}
}


void GrathWidg::drawLeftBotLine(int x, int y)
{
	if (draw)
	{
		float maxValueY = grathMaxY;
		float minvalueY = grathMinY;
		leftLine->setPen(QPen(Qt::darkGreen));
		leftLine->setLine(10, y, leftView->width(), y);
		leftScene->addItem(leftLine);
		int test = leftView->height();
		float delta_dt = (maxValueY - minvalueY) / (leftView->height() - shiftY);
		leftText = leftScene->addText(QString::number(maxValueY - delta_dt * y));
		leftText->setDefaultTextColor(QColor(Qt::darkGreen));
		if (y + 30 > leftView->height())
			leftText->setPos(10, y - 20);
		else
			leftText->setPos(10, y);

		botLine->setLine(x, 0, x, botView->height() - 10);
		botLine->setPen(QPen(Qt::darkGreen));
		botScene->addItem(botLine);
		long long val = ND + x * norm;
		botText = botScene->addText(QDateTime::fromMSecsSinceEpoch(val).toString("hh:mm:ss"));
		botText->setDefaultTextColor(QColor(Qt::darkGreen));
		if (x - 30 < 0 )
			botText->setPos(x + 25, 8);
		else
			botText->setPos(x, 8);
		botText->setRotation(90);
		QFont font = botText->font();
		font.setPixelSize(14);
		font.setWeight(5);
		botText->setFont(font);
		drawGrathValue(x);

		sceneLine->setPen(QPen(Qt::darkGreen));
		sceneLine->setLine(x, 1, x, view->height() - 3);
		scene->addItem(sceneLine);

		draw = !draw;
	}
}
void GrathWidg::drawGrathValue(int x)
{
	int grathMaxX = scene->height();
	for (int i = 0; i < valuesList->size(); ++i)
	{
		int aaa = valuesList->at(i).size();
		int tmpX = 0;// valuesList->at(i).at(0).x;
		double tmpY = 0;// valuesList->at(i).at(0).y;
		for (int j = 0; j < valuesList->at(i).size(); ++j)
		{
			if (valuesList->at(i).at(j).x > x)
				break;
			 tmpX = valuesList->at(i).at(j).x;
			 tmpY = valuesList->at(i).at(j).y;
		}
		if (currentDB == "MKO")
			okList->at(i)->textValue = scene->addText(QString::number(tmpY));
		if ((currentDB == "AIK") || (currentDB == "MCA"))
			TMList->at(i)->textValue = scene->addText(QString::number(tmpY));
		//okList->at(i)->textValue->setDefaultTextColor(okList->at(i)->pen.color());
		double deltaY = 0;
		if (grathMaxY != 0)
			deltaY = (y - 10) / grathMaxY;
		if (y - tmpY * deltaY - shiftY > y - 30)
		{
			if (x + 30 > grathMaxX)
				currentDB == "MKO" ? okList->at(i)->textValue->setPos(tmpX - 30, y - tmpY * deltaY - shiftY - 20) : TMList->at(i)->textValue->setPos(tmpX - 30, y - tmpY * deltaY - shiftY - 20);
			else
				currentDB == "MKO" ? okList->at(i)->textValue->setPos(tmpX, y - tmpY * deltaY - shiftY - 20) : TMList->at(i)->textValue->setPos(tmpX, y - tmpY * deltaY - shiftY - 20);
			
		}
		else
		{
			if (x + 30 > grathMaxX)
				currentDB == "MKO" ? okList->at(i)->textValue->setPos(tmpX - 30, y - tmpY * deltaY - shiftY) : TMList->at(i)->textValue->setPos(tmpX - 30, y - tmpY * deltaY - shiftY);
			else
				currentDB == "MKO" ? okList->at(i)->textValue->setPos(tmpX, y - tmpY * deltaY - shiftY) : TMList->at(i)->textValue->setPos(tmpX, y - tmpY * deltaY - shiftY);
		}

	}
}

void GrathWidg::removeGrathValue()
{
	for (int i = 0; i < valuesList->size(); ++i)
	{

		if (!okList->isEmpty() && i < okList->size())
			scene->removeItem(okList->at(i)->textValue);
		if (!TMList->isEmpty() && i < TMList->size())
			scene->removeItem(TMList->at(i)->textValue);
	}
}

bool GrathWidg::eventFilter(QObject* object, QEvent* event)
{

	if (event->type() == QEvent::MouseMove)
	{
		try
		{
			
			QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);
			if (mouse_event->pos().y() + shiftY >= leftView->height() || mouse_event->pos().x() > botView->width() - 10)
				return false;
			if (queryDB != currentDB)
				connectToDB(queryDB);
			removeLeftBotLine();
			drawLeftBotLine(mouse_event->pos().x(), mouse_event->pos().y());
			return true;
		}
		catch (std::exception err)
		{
			int a = 1;
		}
		catch (...)
		{
			int a = 1;
		}
	}
	return false;
}

void GrathWidg::connectWithCollection(QString collName, QString mon_ip)
{
	cur_thr->connectWithCollection(collName, mon_ip);
}

void GrathWidg::connectToDB(QString DB)
{
	if (cur_thr)
	{
		disconnect(cur_thr, &DBReaderThread::finishedRead, this, &GrathWidg::receiptData);
		disconnect(cur_thr, &DBReaderThread::changeProgressMsg, this, &GrathWidg::changeProgressLabel);
	}

	currentDB = DB;
	cur_thr = thr_map[DB];

	connect(cur_thr, &DBReaderThread::finishedRead, this, &GrathWidg::receiptData);
	connect(cur_thr, &DBReaderThread::changeProgressMsg, this, &GrathWidg::changeProgressLabel);

}

void GrathWidg::changeProgressLabel(int value, QString msg)
{
	if (progressBar == nullptr)
		return;
	progressBar->setValue(value);
	progressLabel->setText(msg);
	//QApplication::processEvents();
}