#include "DBThread.h"
#include "instruments.h"
#include <qcoreapplication.h>

DBReaderThread::DBReaderThread(QString _BD, QString _ip, QObject* parent) : QThread(parent), currentDB(_BD), connected(false), ip(_ip)
{

}

void DBReaderThread::run()
{
	this->setObjectName(currentDB + "_thr");
	if (currentDB.contains("ТМ"))
	{
		QStringList tmp_list = currentDB.split(" ");
		currentDB = "tm_"+tmp_list[2];
		DB_controller = std::dynamic_pointer_cast<CommonDBController>(std::shared_ptr<MongoTMDBController_v3>(new MongoTMDBController_v3(currentDB + "_db", "ТМ "+tmp_list[1])));
	}

	if (currentDB == "MKO")
		DB_controller = std::dynamic_pointer_cast<CommonDBController>(std::shared_ptr<MongoMonitorDBController_v3>(new MongoMonitorDBController_v3)); 
	if (currentDB == "BV")
		DB_controller = std::dynamic_pointer_cast<CommonDBController>(std::shared_ptr<MongoBVDBController_v3>(new MongoBVDBController_v3));
	if (currentDB == "Protocol")
		DB_controller = std::dynamic_pointer_cast<CommonDBController>(std::shared_ptr<MongoProtocolDBController_v3>(new MongoProtocolDBController_v3));
	if (currentDB == "Frame")
		DB_controller = std::dynamic_pointer_cast<CommonDBController>(std::shared_ptr<MongoFrameDBController_v3>(new MongoFrameDBController_v3));

	connected = false;
	connect(DB_controller.get(),
		&CommonDBController::error, this, &DBReaderThread::error);

	//connect(this, &DBReaderThread::needDataBlock,
	//	DB_controller.get(), &MongoMonitorDBReader::readBlockData);//del
	connect(this, &DBReaderThread::needDataBlockSignal,
		DB_controller.get(), &CommonDBController::readBlockData);

	//connect(this, &DBReaderThread::needQueryStringSignal,
	//	DB_controller.get(), &CommonDBController::getQueryString);

	connect(this, &DBReaderThread::needIntervalDataBlock,
		DB_controller.get(), &CommonDBController::readDataFromDBQuery);

	connect(DB_controller.get(), &CommonDBController::finishedReadDataBlock,
								this, &DBReaderThread::finishedRead, Qt::DirectConnection);

	//connect(this, &DBReaderThread::needCount,
	//	DB_controller.get(), &MongoMonitorDBReader::getCountRec/*, Qt::DirectConnection*/);

	connect(this, &DBReaderThread::needSynchronizeSignal,
		DB_controller.get(), &CommonDBController::synchronize);

	connect(DB_controller.get(), &CommonDBController::synchronizeFinished,
		this, &DBReaderThread::synchronizeFinished);

	connect(this, &DBReaderThread::needCollectionNames,
		DB_controller.get(), &CommonDBController::getCollectionNames, Qt::DirectConnection);

	if (ip == "")
	{
		QSettings ipSettings("Cometa", "СПО АИК");
		ip = ipSettings.value(QString("%1_db").arg(currentDB), "localhost").toString();
	}

	if (!DB_controller->tryConnect("", ip))
		/*return*/ emit error("Ошибка подключения к БД "+currentDB);

	timer = new QTimer(); // this);
	connect(timer, &QTimer::timeout, this, &DBReaderThread::interval, Qt::DirectConnection);
	connect(this, SIGNAL(startIntervalSignal(int)), timer, SLOT(start(int)), Qt::QueuedConnection);
	connect(this, SIGNAL(stopIntervalSignal()), timer, SLOT(stop()), Qt::QueuedConnection);
	connect(DB_controller.get(), &CommonDBController::changeProgressMsg,
		this, &DBReaderThread::changeProgressMsg);
	
	foreach(DBCallback cb, cb_list)
		DB_controller->addCallBack(cb);


	conn_mutex.lock();
	connected = true;

	conn_mutex.unlock();

	exec();

	disconnect(DB_controller.get(),
		&CommonDBController::error, this, &DBReaderThread::error);

	//disconnect(this, &DBReaderThread::needDataBlock,
	//	DB_controller.get(), &MongoMonitorDBReader::readDataFromDB);

	disconnect(DB_controller.get(), &CommonDBController::finishedReadDataBlock, this, &DBReaderThread::finishedRead);
}

/*const std::shared_ptr<MongoMonitorDBReader>& DBThread::getReader() const
{
	return reader_DB_MKO;
}*/

void DBReaderThread::waitForConnection()
{

	while (true)
	{

		QCoreApplication::processEvents();
		QMutexLocker lock(&conn_mutex);
		if (connected)
			return;
		QThread::currentThread()->msleep(50);


	}
}

void DBReaderThread::setIp(QString _ip)
{
	ip = _ip;
}

void DBReaderThread::addCallBackFunction(DBCallback& func)
{
	cb_list.push_back(func);
}

void DBReaderThread::startRead()
{
	DB_controller->startRead();
}

std::vector<QVariant> DBReaderThread::getDataBlock() const //
{
	return DB_controller->getDataBlock();
}

std::vector<QVariant> DBReaderThread::getIntervalDataBlock() const //
{
	return DB_controller->getIntervalDataBlock();
}

int DBReaderThread::getCount()
{
	return DB_controller->getCount();
}

QString DBReaderThread::getQueryString(QList<QMap<QString, QVariant> > *list)
{
	return DB_controller->getQueryString(list);
}

std::list<std::string> DBReaderThread::getCollList()
{
	return DB_controller->getCollList();
}

bool DBReaderThread::connectWithCollection(QString collectionName, QString mon_ip)
{
	return DB_controller->tryConnect(collectionName, mon_ip);
}

void DBReaderThread::interval()
{
	timer->stop();
	emit needIntervalDataBlock(&queryList);
	timer->start();
}

void DBReaderThread::runInterval(int ms)
{
	timer->setInterval(ms);
	emit startIntervalSignal(ms);
}

void DBReaderThread::offInterval()
{
	emit stopIntervalSignal();
}

void DBReaderThread::setQueryString(QString query_string)
{
	query_str = query_string;
}

void DBReaderThread::setQueryList(QList<QMap<QString, QVariant> > list)
{
	queryList.clear();
	queryList = list;
}

void DBReaderThread::tryConnectWithMonIp(QString ip)
{
	if (DB_controller->tryConnect("", ip))
		//return emit error("Ошибка подключения к БД");
		DB_controller.get()->changeProgressMsg(0, QString("Успешно подключился к %1").arg(ip));
}


void DBReaderThread::needDataBlock(int start_pos, int n, QList<QMap<QString, QVariant> > *queryList, bool needCount, int rezh)
{
	emit needDataBlockSignal(start_pos, n, queryList, needCount, rezh);
}

void DBReaderThread::needCount(QList<QMap<QString, QVariant> > *queryList)
{
	DB_controller->getCountRec(queryList);
}

void DBReaderThread::synchronize(QList<QMap<QString, QVariant> > *queryList)
{
	emit needSynchronizeSignal(queryList);
}