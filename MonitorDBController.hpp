#ifndef MONITORDBCONTROLLER_H
#define MONITORDBCONTROLLER_H

#include "CommonDBController.h"


/// -- Структура командного слова сообщения МКО.
typedef union _COM_WORD{
	/// -- Командное слово целиком;
	unsigned short cw;				 
	struct{
	/// -- Число сл.данных / команда;
	unsigned short count : 5,    
	/// -- Подадрес;
	subAdr : 5,   
	/// -- Направление передачи(1-чт.ОУ);
	tr : 1,       
	/// -- Адрес.
	adr : 5;
	};  
} CW_MKO;

/// -- Структура сообщений для БД МКО.
struct MsgFromMonitorDB
{
	/**
	*	\brief -- Конструктор;
	*	\param _dt -- метка времени;
	*	\param _addr -- адрес абонента;
	*	\param _saddr -- подадрес абонента;
	*	\param _chan -- номер МКО;
	*	\param _nWords -- число слов данных;
	*	\param _cwd -- командное слово;
	*	\param _receive -- прием/передача;
	*	\param _swd -- ответное слово;
	*	\param _reserveBus -- основная/резервная линия;
	*	\param _data -- слова данных в двоичном виде;
	*	\param _words -- слова данных в текстовом виде;
	*	\param _bv -- бортовое время.
	*	\param _utc -- вреям UTC;
	*/
	MsgFromMonitorDB(qint64 _dt = 0, int _addr = 0, int _saddr = 0, int _chan = 0,
	int _nWords = 0, int _cwd = 0, bool _receive = true,
	int _swd = 0, bool _reserveBus = false,
	const std::vector<unsigned short>& _data = std::vector<unsigned short>(), QString _words = "", unsigned short _bv = 0, qint64 _utc = 0, int _bv_dev = 0)
		: dt(_dt), addr(_addr), saddr(_saddr), chan(_chan),
		nWords(_nWords), cwd(_cwd), receive(_receive),
		swd(_swd), reserveBus(_reserveBus), data(_data), words(_words), bv(_bv), utc(_utc), bv_dev(_bv_dev)
	{}
	/// -- Метка времени;
	qint64 dt = 0;
	/// -- Адрес абонента;
	int addr = 0;
	/// -- Подадрес абонента;
	int saddr = 0;
	/// -- Номер МКО;
	int chan = 0;
	/// -- Число слов данных;
	int nWords = 0;
	/// -- Командное слово;
	int cwd = 0;
	/// -- Прием/передача;
	bool receive = true;
	/// -- Ответное слово;
	int swd = 0;
	/// -- Основная/резервная линия;
	bool reserveBus = false;
	/// -- Слова данных в двоичном виде;
	std::vector<unsigned short> data;
	/// -- Слова данных в текстовом виде;
	QString words = "";
	/// -- Бортовое время;
	unsigned short bv = 0;
	/// -- Время UTC;
	qint64 utc = 0;
	/// -- Разбежка БВ.
	int bv_dev = 0;
};


Q_DECLARE_METATYPE(MsgFromMonitorDB);


/**
*!\class MonitorDBCtrl
*	\brief -- Управление БД монитора.
*
*/
class MonitorDBController : public CommonDBController
{
public:
	/**
	*	\brief -- Конструктор;
	*	\param _read_delay -- период опроса.
	*/
	MonitorDBController(int _read_delay) : CommonDBController(_read_delay) {}
	/**
	*	\brief -- Добавление сообщений в БД.
	*	\param msg_list -- сообщения;
	*	\return -- флаг успешности добавления.
	*/
	virtual bool insertMKOData(const std::vector<MsgFromMonitorDB>& msg_list) = 0;
};


#include "mongo_v3/MongoMonitorDBController_v3.h"
#endif // MONITORDBCONTROLLER_H