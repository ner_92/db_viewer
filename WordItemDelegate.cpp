#include "WordItemDelegate.h"
#include <QPainter>
#include <QTextDocument>
#include <QAbstractTextDocumentLayout>


WordItemDelegate::WordItemDelegate(QObject *parent) : QStyledItemDelegate(parent)
{}

void WordItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
	auto options = option;
	initStyleOption(&options, index);

	painter->save();

	QTextDocument doc;
	doc.setHtml(options.text);

	options.text = "";
	options.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);

	painter->translate(options.rect.left(), options.rect.top());
	QRect clip(0, 0, options.rect.width(), options.rect.height());
	doc.drawContents(painter, clip);

	painter->restore();
}

QSize WordItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const {
	QStyleOptionViewItemV4 options = option;
	initStyleOption(&options, index);
	QTextDocument doc;
	doc.setHtml(options.text);
//	doc.setTextWidth(options.rect.width());
	return QSize(doc.idealWidth(), doc.size().height());
//	return QSize(doc.size().width(), doc.size().height());
}