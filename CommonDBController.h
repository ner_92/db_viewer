#ifndef COMMONDBCONTROLLER_H
#define COMMONDBCONTROLLER_H
#include <QTimer>
#include <QObject>
#include <QMutexLocker>
#include <QList>
#include <QObject>
#include <QVariant>
#include <functional>
#include <memory>

/// -- Тип обработчика новых сообщений.
typedef std::function<void(const std::vector<QVariant>& msg_list)> DBCallback;

/**
*!class CommonDBInterface
*	\brief -- Класс интерфейса доступа к БД.
*/
class CommonDBInterface
{
public:
	/// -- Конструктор;
	CommonDBInterface(){}
	static const int INTERVAL_DATA_SIZE = 200;

	/**
	*	\brief -- Подключение к БД;
	*	\return -- флаг успешности подключения
	*/
	virtual bool tryConnect(QString col_name = "", QString db_ip = "", bool create = false) = 0;
	/// -- Запрос состояния соединения с БД;
	virtual bool isConnected() = 0;
	/// -- Получение блока данных;
	virtual std::vector<QVariant> getDataBlock() = 0;
	virtual std::vector<QVariant> getIntervalDataBlock();
	virtual void addIntervalDataBlock(std::vector<QVariant> add_list);
	/**
	*	\brief -- Добавление функции обратного вызова в список;
	*	\param func -- функция обратного вызова.
	*/
	void addCallBack(DBCallback& func);
	/// -- Очистка списка функций обратного вызова;
	void clearCallBackList();
	/**
	*	\brief -- Обработка списка сообщений;
	*	\param msg_list -- список сообщений для обработки.
	*/
	void process(const std::vector<QVariant>& msg_list);
	/// -- Слот запроса и обработки новых сообщений(вызывается по таймеру);
	virtual void readDataFromDB() = 0;


	/// -- Слот запроса и обработки новых сообщений(вызывается по таймеру);
	virtual void readDataFromDBQuery(QList<QMap<QString, QVariant> > *queryList) = 0;
	/**
	*	brief -- Слот запроса и обработки сообщений;
	*	\param _start_pos -- стартовая позиция блока;
	*	\param _block_size -- количество блоков;
	*	\param queryList -- структура с данными для запроса;
	*	\param needCount -- признак необходимости запроса числа записей;
	*	\param rezh -- режим работы с _id для скроллинга. 0 - не работаем, 1 - скроллинг вниз.
	*/
	virtual void readBlockData(int _start_pos, int _block_size, QList<QMap<QString, QVariant> > *queryList, bool needCount, int rezh = 0) = 0;
	/**
	*	\brief -- Слот запроса количесва записей;
	*	\param queryList -- список условий для поиска.
	*/
	virtual void getCountRec(QList<QMap<QString, QVariant> > *queryList) = 0;
	/// -- Слот запроса коллекций;
	virtual void getCollectionNames() = 0;
	/// -- Сохраняет информации о последней прочитанной записи;
	virtual void saveLastData() = 0;
	/**
	* \brief -- Получение списка коллекций;
	* \return -- список коллекций.
	*/
	virtual std::list<std::string> getCollList() = 0;
	/// -- Получение числа записей, соответствующих последнему запросу;
	virtual int getCount() = 0;
	/**
	*	\brief -- Преобразование списка условий в строку запроса.
	*	\param queryList -- список условий для поиска;
	*	\return -- строка запроса.
	*/
	virtual QString getQueryString(QList<QMap<QString, QVariant> > *queryList) = 0;
	///Синхронизация с запросом
	virtual void synchronize(QList<QMap<QString, QVariant> > *queryList) = 0;


	/**
	*	\brief -- Чтение поля из БД МЦА;
	*	\param query -- запрос;
	*	\param field -- Имя запрашиваемого поля;
	*	\param orderBy -- Имя поля, по которому происходит сортировка;
	*	\param sort -- Сортировка. 1 - ASC(по возрастанию), -1 - DESC(по убыванию), 0 - игнорировать сортировку;
	*	\return -- значение поля.
	*/
	virtual QVariant selectFieldOrderBy(QString query, QString field, QString orderBy, int sort) = 0;

	/**
	*	\brief -- Чтение строки из БД МЦА;
	*	\param query -- запрос;
	*	\param field -- Имя запрашиваемого поля;
	*	\param orderBy -- Имя поля, по которому происходит сортировка;
	*	\param sort -- Сортировка. 1 - ASC(по возрастанию), -1 - DESC(по убыванию), 0 - игнорировать сортировку;
	*	\return -- значения полей.
	*/
	virtual QVariantMap selectMapOrderBy(QString query, QString field, QString orderBy, int sort) = 0;
private:
	/// -- Список функций обратного вызова;
	QList<DBCallback> callback_list;
	/// -- Мьютекс обработчиков.
	QMutex callback_mtx;
	QMutex interval_list_mtx;
	std::vector<QVariant> interval_msg_list;
};


/**
*!\class CommonDBController
*	\brief -- Шаблонный класс управления БД.
*
*/
class CommonDBController : public QObject
{
	Q_OBJECT
public:
	/**
	*	\brief -- Конструктор;
	*	\param _read_delay -- период для таймера опроса последних данных.
	*/
	CommonDBController(int _read_delay) : db_read_delay(_read_delay)
	{
		connect(&timer, &QTimer::timeout, this, &CommonDBController::readDataFromDB);
		connect(this, SIGNAL(startIntervalSignal(int)), &timer, SLOT(start(int)));
		connect(this, &CommonDBController::stopIntervalSignal, &timer, &QTimer::stop);
	}

	/**
	*	\brief -- Подключение к БД;
	*	\return -- флаг успешности подключения.
	*/
	virtual bool tryConnect(QString col_name = "", QString db_ip = "", bool create = false);
	/// -- Запрос состояния соединения с БД;
	virtual bool isConnected();
	/**
	*	\brief -- Получение блока данных;
	*	\return -- последний блок данных.
	*/
	virtual std::vector<QVariant> getDataBlock();
	virtual std::vector<QVariant> getIntervalDataBlock();
	/// -- Запуск таймера запроса новых сообщений;
	virtual void startRead();
	/// -- Остановка таймера запроса новых сообщений;
	virtual void stopRead();
	/**
	*	\brief -- Добавление функции обратного вызова в список;
	*	\param func -- функция обратного вызова.
	*/
	void addCallBack(DBCallback& func);
	/// -- Очистка списка функций обратного вызова;
	void clearCallBackList();
	/// -- Получение списка коллекций;
	virtual std::list<std::string> getCollList();
	/// -- Получение числа записей, соответствующих последнему запросу;
	virtual int getCount();
	/**
	*	\brief -- Преобразование списка условий в строку запроса.
	*	\param queryList -- список условий для поиска;
	*	\return -- строка запроса.
	*/
	virtual QString getQueryString(QList<QMap<QString, QVariant> > *list);


	/**
	*	\brief -- Чтение поля из БД МЦА;
	*	\param query -- запрос;
	*	\param field -- Имя запрашиваемого поля;
	*	\param orderBy -- Имя поля, по которому происходит сортировка;
	*	\param sort -- Сортировка. 1 - ASC(по возрастанию), -1 - DESC(по убыванию), 0 - игнорировать сортировку;
	*	\return -- значение поля.
	*/
	virtual QVariant selectFieldOrderBy(QString query, QString field, QString orderBy, int sort);

	/**
	*	\brief -- Чтение строки из БД МЦА;
	*	\param query -- запрос;
	*	\param field -- Имя запрашиваемого поля;
	*	\param orderBy -- Имя поля, по которому происходит сортировка;
	*	\param sort -- Сортировка. 1 - ASC(по возрастанию), -1 - DESC(по убыванию), 0 - игнорировать сортировку;
	*	\return -- значения полей.
	*/
	virtual QVariantMap selectMapOrderBy(QString query, QString field, QString orderBy, int sort);

public:
	/// -- Указатель на интерфейс доступа к БД.
	std::shared_ptr<CommonDBInterface> DBInterface;
private slots:
	/// -- Слот запроса и обработки новых сообщений(вызывается по таймеру).
	void readDataFromDB();
private:
	/// -- Период тайера опорса последних сообщений;
	int db_read_delay;
	/// -- Таймер запроса и обработки новых сообщений.
	QTimer timer;

public slots:
	/// -- Слот запроса и обработки новых сообщений(вызывается по таймеру);
	void readDataFromDBQuery(QList<QMap<QString, QVariant> > *queryList);
	/**
	*	brief -- Слот запроса и обработки сообщений;
	*	\param _start_pos -- стартовая позиция блока;
	*	\param _block_size -- количество блоков;
	*	\param queryList -- структура с данными для запроса;
	*	\param needCount -- признак необходимости запроса числа записей;
	*	\param rezh -- режим работы с _id для скроллинга. 0 - не работаем, 1 - скроллинг вниз.
	*/
	void readBlockData(int _start_pos, int _block_size, QList<QMap<QString, QVariant> > *queryList, bool needCount, int rezh = 0);
	/**
	*	\brief -- Слот запроса количесва записей;
	*	\param queryList -- список условий для поиска.
	*/
	void getCountRec(QList<QMap<QString, QVariant> > *queryList);
	/// -- Слот запроса коллекций;
	void getCollectionNames();
	/// -- Сохраняет информации о последней прочитанной записи.
	void saveLastData();
	///Синхронизация с запросом
	void synchronize(QList<QMap<QString, QVariant> > *queryList);
signals:
	/// -- Сигнал о завершении чтения блока;
	void finishedReadDataBlock();
	/// -- Сигнал для изменения строки состояния;
	void changeProgressMsg(int value, QString msg);
	/**
	*	\brief -- Функция сигнализации об ошибках БД;
	*	\param err_str -- информация об ошибке.
	*/
	void error(QString err_str);
	/**
	*	\brief -- Сигнал для запуска интервального поиска;
	*	\param ms -- интервал поиска в мс.
	*/
	void startIntervalSignal(int ms);
	/// -- Сигнал для останова интервального поиска.
	void stopIntervalSignal();
	///Сигнал окончания синхронизации
	void synchronizeFinished(int count);
};


#endif // MONITORDBCONTROLLER_H