#include "myxmlreader.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/exception/all.hpp>
#include "instruments.h"
#include <qdebug.h>

void OperatorsXmlReader::parse(const QString& text, QList<const Operator*>& op_list, QList<const OpGroup*>& group_list)
{
	try
	{
		//op_list.clear();
		//group_list.clear();
		using namespace boost::property_tree;
		std::wstringstream data_stream;
		data_stream.write(text.toStdWString().c_str(), text.size());
		read_xml(data_stream, pt);

		foreach(const wptree::value_type& mod, pt.get_child(L"document"))
		{
			if (mod.first.compare(L"module"))
				continue;
			OpGroup* tmp_group = parseGroup(mod, 0);
			op_list << tmp_group->op_list;
			group_list << tmp_group;
		}
	}
	catch (std::exception& e)
	{
		qDebug() << QString(e.what());
	}
	catch (...)
	{

	}
}

OpGroup* OperatorsXmlReader::parseGroup(const boost::property_tree::wptree::value_type& group, OpGroup* parent)
{
	using namespace boost::property_tree;
	OpGroup* cur_group = new OpGroup(parent);

	cur_group->group_name = QString().fromStdWString(group.second.get<std::wstring>(L"<xmlattr>.name", L""));

	foreach(const wptree::value_type& com, group.second)
	{
		if (com.first.compare(L"module") == 0)
		{
			OpGroup* tmp_group = parseGroup(com, cur_group);
			cur_group->op_list << tmp_group->op_list;
			cur_group->sub_groups << tmp_group;
		}

		if (com.first.compare(L"command") == 0)
		{
			QString name = QString().fromStdWString(
				com.second.get<std::wstring>(L"<xmlattr>.name", L""));
			QString header = QString().fromStdWString(
				com.second.get<std::wstring>(L"<xmlattr>.header", L""));
			QString short_description = QString().fromStdWString(
				com.second.get<std::wstring>(L"<xmlattr>.short_description", L""));
			QString description = QString().fromStdWString(
				com.second.get<std::wstring>(L"<xmlattr>.description", L""));


			Operator* op = new Operator(name, header, short_description, description, cur_group);

			foreach(const wptree::value_type& param, com.second)
			{
				if (param.first.compare(L"param"))
					continue;

				Param* m_par = parseParam(param);
				if (m_par)
					op->addParam(m_par);
			}

			cur_group->op_list.push_back(op);
		}
	}
	return cur_group;
}

Param* OperatorsXmlReader::parseParam(const boost::property_tree::wptree::value_type& param)
{
	if (param.first.compare(L"param") && param.first.compare(L"matrix_param"))
		return 0;

	QString name = QString().fromStdWString(
		param.second.get<std::wstring>(L"<xmlattr>.name", L""));
	QString type = QString().fromStdWString(
		param.second.get<std::wstring>(L"<xmlattr>.type", L""));
	QString desc = QString().fromStdWString(
		param.second.get<std::wstring>(L"<xmlattr>.description", L""));

	Param* par = new Param(name, type, getTypeFromName(type), desc);
	if (type.contains("запись"))
		par->is_ret = true;

	foreach(const boost::property_tree::wptree::value_type& param_data, param.second)
	{
		if (!param_data.first.compare(L"limit"))
		{
			QString left = QString().fromStdWString(
				param_data.second.get<std::wstring>(L"<xmlattr>.left", L""));
			QString right = QString().fromStdWString(
				param_data.second.get<std::wstring>(L"<xmlattr>.right", L""));
			QString step = QString().fromStdWString(
				param_data.second.get<std::wstring>(L"<xmlattr>.step", L""));

			par->inc_l = !param_data.second.get_optional<bool>(L"<xmlattr>.l_except");
			par->inc_r = !param_data.second.get_optional<bool>(L"<xmlattr>.r_except");

			par->leftlimit = instr::convertToVariant(left, par->type, &eng);
			par->rightlimit = instr::convertToVariant(right, par->type, &eng);
			par->step = instr::convertToVariant(step, par->type, &eng);
		}

		if (!param_data.first.compare(L"default"))
		{
			QString def_val = QString().fromStdWString(
				param_data.second.get_value<std::wstring>());
			par->def_value = instr::convertToVariant(def_val, par->type, &eng);
		}
		
		if (!param_data.first.compare(L"matrix_param"))
		{
			Param* m_par = parseParam(param_data);
			if (m_par)
				par->children.push_back(m_par);
		}

		if (!param_data.first.compare(L"value"))
		{
			QString val = QString().fromStdWString(param_data.second.get_value<std::wstring>());
			par->fix_values.push_back(instr::convertToVariant(val, par->type, &eng));
		}
	}
	if (!par->children.isEmpty())
	{
		par->def_value = QVariant();
		par->fix_values = QVariantList();
	}
	return par;
}

QVariant::Type OperatorsXmlReader::getTypeFromName(const QString& type)
{
	if (type.contains("массив"))
		return QVariant::List;

	if (type.contains("целое_без_знака"))
		return QVariant::ULongLong;

	if (type.contains("целое"))
		return QVariant::LongLong;

	if (type.contains("вещественное"))
		return QVariant::Double;

	if (type.contains("булево"))
		return QVariant::Bool;

	if (type.contains("строка"))
		return QVariant::String;

	return QVariant::UserType;
}

		/*if (fileName.isEmpty())
			throw std::exception("Имя файла незадано!");

		QFile file(fileName);
		if (!file.open(QFile::ReadOnly | QFile::Text))
			 throw std::exception(QString("Невозможно открыть файл %1:\\n%2.")
								  .arg(fileName).arg(file.errorString()).toStdString().c_str()
								 );

		err_str="";
		if(!xmlValidate(fileName, err_str))
			throw std::exception(err_str.toStdString().c_str());

		if (!read(&file))
			 throw std::exception(QString("Ошибка при чтении файла %1:\n\n%2")
									.arg(fileName).arg(errorString()).toStdString().c_str()
								 );
		return true;*/
