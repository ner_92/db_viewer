#include "TM_name_widget.h"

TM_name_widget::TM_name_widget(QString DB_name, QWidget *parent)
	: QWidget(parent)
{
	widg = new QWidget(this);
	QVBoxLayout *v_layout = new QVBoxLayout(this);
	//setFixedSize(100, 100);
	setMinimumWidth(1600);
	MSKLabel = new QLabel("MSK", widg);
	MSK1Edit = new QLineEdit(widg);
	MSK1Edit->setMinimumHeight(25);
	MSK1Edit->setMaximumWidth(135);
	MSK1Edit->setInputMask("99.99.9999 99:99:99.999");

	MSK2Edit = new QLineEdit(widg);
	MSK2Edit->setMaximumHeight(25);
	MSK2Edit->setMaximumWidth(135);
	MSK2Edit->setInputMask("99.99.9999 99:99:99.999");

	BTLabel = new QLabel("БВ", widg);
	BT1Edit = new QLineEdit(widg);
	BT1Edit->setMaximumHeight(25);
	BT1Edit->setMaximumWidth(135);
	BT1Edit->setInputMask("99:99:99");

	BT2Edit = new QLineEdit(widg);
	BT2Edit->setMaximumHeight(25);
	BT2Edit->setMaximumWidth(135);
	BT2Edit->setInputMask("99:99:99");

	numberTMLabel = new QLabel("Номер сигнала ТМ", widg);
	numberTMEdit1 = new QLineEdit(widg);
	numberTMEdit1->setMinimumHeight(25);
	numberTMEdit1->setMaximumWidth(160);
	numberTMEdit2 = new QLineEdit(widg);
	numberTMEdit2->setMinimumHeight(25);
	numberTMEdit2->setMaximumWidth(160);

	nameTMLabel = new QLabel("Функциональное назначение сигнала ТМ", widg);
	nameTMEdit = new QLineEdit(widg);
	nameTMEdit->setMinimumHeight(25);
	nameTMEdit->setMinimumWidth(350);

	measuredValueLabel = new QLabel("Полученное значение", widg);
	measuredValueEdit1 = new QLineEdit(widg);
	measuredValueEdit1->setMinimumHeight(25);
	measuredValueEdit1->setMaximumWidth(100);
	measuredValueEdit2 = new QLineEdit(widg);
	measuredValueEdit2->setMinimumHeight(25);
	measuredValueEdit2->setMaximumWidth(100);

	metkaLabel = new QLabel("Метка", widg);
	metkaEdit = new QLineEdit(widg);
	metkaEdit->setMinimumHeight(25);
	metkaEdit->setMinimumWidth(206);

	//DB_TM_MCAGridLayout = new QGridLayout(DB_TM_MCAwidg);
	h_filterTMMCALayout = new QHBoxLayout();
	DB_TM_MCAmini1GridLayout = new QGridLayout();
	DB_TM_MCAmini2GridLayout = new QGridLayout();
	DB_TM_MCAmini3GridLayout = new QGridLayout();
	DB_TM_MCAmini4GridLayout = new QGridLayout();
	filter1TMMCAGroupBox = new QGroupBox();
	filter2TMMCAGroupBox = new QGroupBox();
	filter3TMMCAGroupBox = new QGroupBox();
	filter4TMMCAGroupBox = new QGroupBox();

	if (h_filterTMMCALayout->count() != 0)
		h_filterTMMCALayout->removeItem(h_filterTMMCALayout->takeAt(4));	// Удаляю SpaceItem, что бы лайаут не уезжал
	MSKLabel->show();
	MSK1Edit->show();
	DB_TM_MCAmini1GridLayout->addWidget(MSKLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini1GridLayout->addWidget(MSK1Edit, 1, 0);
	DB_TM_MCAmini1GridLayout->addWidget(MSK2Edit, 1, 1);
	DB_TM_MCAmini1GridLayout->addWidget(BTLabel, 2, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini1GridLayout->addWidget(BT1Edit, 3, 0);
	DB_TM_MCAmini1GridLayout->addWidget(BT2Edit, 3, 1);
	DB_TM_MCAmini1GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	DB_TM_MCAmini2GridLayout->addWidget(numberTMLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini2GridLayout->addWidget(numberTMEdit1, 1, 0);
	DB_TM_MCAmini2GridLayout->addWidget(numberTMEdit2, 1, 1);
	DB_TM_MCAmini2GridLayout->addWidget(nameTMLabel, 2, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini2GridLayout->addWidget(nameTMEdit, 3, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini2GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	DB_TM_MCAmini3GridLayout->addWidget(measuredValueLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini3GridLayout->addWidget(measuredValueEdit1, 1, 0);
	DB_TM_MCAmini3GridLayout->addWidget(measuredValueEdit2, 1, 1);
	/*DB_TM_MCAmini3GridLayout->addWidget(calculatedValueLabel, 2, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini3GridLayout->addWidget(calculatedValueEdit1, 3, 0);
	DB_TM_MCAmini3GridLayout->addWidget(calculatedValueEdit2, 3, 1);*/
	DB_TM_MCAmini3GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	DB_TM_MCAmini4GridLayout->addWidget(metkaLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	DB_TM_MCAmini4GridLayout->addWidget(metkaEdit, 1, 0);
	DB_TM_MCAmini4GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	filter1TMMCAGroupBox->setLayout(DB_TM_MCAmini1GridLayout);
	filter2TMMCAGroupBox->setLayout(DB_TM_MCAmini2GridLayout);
	filter3TMMCAGroupBox->setLayout(DB_TM_MCAmini3GridLayout);
	filter4TMMCAGroupBox->setLayout(DB_TM_MCAmini4GridLayout);

	h_filterTMMCALayout->addWidget(filter1TMMCAGroupBox);
	h_filterTMMCALayout->addWidget(filter2TMMCAGroupBox);
	h_filterTMMCALayout->addWidget(filter3TMMCAGroupBox);
	h_filterTMMCALayout->addWidget(filter4TMMCAGroupBox);
	h_filterTMMCALayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));

	v_layout->setMargin(11);
	v_layout->setSpacing(8);
	v_layout->addLayout(h_filterTMMCALayout);
}

TM_name_widget::~TM_name_widget()
{

}


QList<QMap<QString, QVariant> > TM_name_widget::getQueryStruct()
{
	QList<QMap<QString, QVariant> > list;
	QMap<QString, QVariant> tmp_query;
	list.append(tmp_query);
	if (MSK1Edit->text() != ".. ::.")
		list[0].insert("MSK1", MSK1Edit->text());
	if (MSK2Edit->text() != ".. ::.")
		list[0].insert("MSK2", MSK2Edit->text());
	if (BT1Edit->text() != "::")
		list[0].insert("BT1", BT1Edit->text());
	if (BT2Edit->text() != "::")
		list[0].insert("BT2", BT2Edit->text());
	if (!(numberTMEdit1->text().isEmpty()))
		list[0].insert("signalTM1", numberTMEdit1->text());
	if (!(numberTMEdit2->text().isEmpty()))
		list[0].insert("signalTM2", numberTMEdit2->text());
	if (!(nameTMEdit->text().isEmpty()))
		list[0].insert("nameTM", nameTMEdit->text());
	//if (!(measuredValueEdit1->text().isEmpty()))
	//	list[0].insert("signalValue1", measuredValueEdit1->text());
	//if (!(measuredValueEdit2->text().isEmpty()))
	//	list[0].insert("signalValue2", measuredValueEdit2->text());
	
	return list;
}

void TM_name_widget::clearFields()
{
	MSK1Edit->clear();
	MSK2Edit->clear();
	BT1Edit->clear();
	BT2Edit->clear();
	numberTMEdit1->clear();
	numberTMEdit2->clear();
	nameTMEdit->clear();
	measuredValueEdit1->clear();
	measuredValueEdit2->clear();
	metkaEdit->clear();
}

