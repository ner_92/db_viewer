#include "SearchBox.h"
#include <qlayout.h>
#include <qevent.h>
#include "functions.h"

SearchBox::SearchBox(QWidget *parent) : QWidget(parent), popup(this)
{
	QHBoxLayout* h_layout = new QHBoxLayout(this);

	editor.installEventFilter(this);
	editor.setFixedHeight(24);
	editor.setToolTip("Поле поиска.");
	editor.setPlaceholderText("Введите название оператора");

	sfp_model.setFilterKeyColumn(0);
	sfp_model.setFilterRegExp(".*");
	sfp_model.setSourceModel(&search_model);
	sfp_model.setFilterCaseSensitivity(Qt::CaseInsensitive);

	popup.setModel(&sfp_model);
	popup.setWindowFlags(Qt::Popup);
	popup.setFocusProxy(parent);
	popup.setMouseTracking(true);
	popup.setFrameStyle(QFrame::Box | QFrame::Plain);
	popup.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	popup.installEventFilter(this);
	popup.setToolTip("Результат поиска.");

	timer.setSingleShot(true);
	timer.setInterval(250);
	editor.setFocus();

	h_layout->addWidget(&editor);
	h_layout->setSpacing(0);
	h_layout->setMargin(0);

	connect(&editor, SIGNAL(textEdited(QString)), &timer, SLOT(start()));
	connect(&timer,  SIGNAL(timeout()), this, SLOT(renewSearchList()));
	connect(&editor, SIGNAL(returnPressed()), this, SLOT(agreeWithOutList()));
	connect(&popup,	 SIGNAL(clicked(QModelIndex)), this, SLOT(doneCompletion()));
}

bool SearchBox::eventFilter(QObject *obj, QEvent *ev)
{
	try
	{
		if ((obj == &editor) && (ev->type() == QEvent::MouseButtonPress) &&
			(static_cast<QMouseEvent*>(ev)->button() == Qt::LeftButton))
			return true;

		if (obj != &popup)
			return false;

		if (ev->type() == QEvent::MouseButtonPress)
		{
			popup.hide();
			editor.setFocus();
			return true;
		}

		if (ev->type() == QEvent::KeyPress)
		{
			bool consumed = false;
			int key = static_cast<QKeyEvent*>(ev)->key();
			switch (key)
			{
			case Qt::Key_Enter:
			case Qt::Key_Return:
				doneCompletion();
				consumed = true;

			case Qt::Key_Escape:
				popup.hide();
				consumed = true;
			case Qt::Key_Down:
			case Qt::Key_Up:
			case Qt::Key_Home:
			case Qt::Key_End:
			case Qt::Key_PageUp:
			case Qt::Key_PageDown:
				break;

			default:
				editor.setFocus();
				editor.event(ev);
				popup.hide();
				break;
			}
			return consumed;
		}
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}
	return false;
}

void SearchBox::doneCompletion()
{
	try
	{
		popup.hide();
		editor.setFocus();
		editor.setText(popup.currentIndex().data().toString());
		emit finishSearch(editor.text());
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}
}

void SearchBox::agreeWithOutList()
{
	try
	{
		renewSearchList();
		int rows = popup.model()->rowCount();
		switch (rows)
		{
			case 0:
			{
				popup.hide();
				editor.clear();
				editor.setFocus();
			}
			break;
			case 1:
			{
				popup.setCurrentIndex(sfp_model.index(0, 0));
				doneCompletion();
			}
			break;
		default:
			renewSearchList();
			break;
		}
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}
}

void SearchBox::renewSearchList()
{
	try
	{
		sfp_model.setFilterRegExp(editor.text());
		
		popup.adjustSize();
		popup.setUpdatesEnabled(true);

		int h = popup.sizeHintForRow(0) * qMin(7, sfp_model.rowCount()) + 3;
		popup.resize(editor.width(), h);

		popup.move(editor.mapToGlobal(QPoint(0, editor.height())));
		popup.setFocus();
		popup.show();
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}
}
void SearchBox::setListData(const QStringList &processes)
{
	try
	{
		search_model.clear();
		foreach(QString line, processes)
		{
			QStandardItem* std_item = new QStandardItem(line);
			std_item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			search_model.appendRow(std_item);
		}
		sfp_model.sort(0);
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}
}

void SearchBox::setPlaceholderText(const QString& _str)
{
	editor.setPlaceholderText(_str);
}

QString SearchBox::currentText() const
{
	return editor.text();
}

void SearchBox::setText(const QString& text)
{
	editor.setText(text);
	//agreeWithOutList();
}

void SearchBox::clearText()
{
	editor.clear();
}

void SearchBox::setWidgetFont(const QFont& font)
{
	setFont(font);
	popup.setFont(font);
}