#ifndef FRAMEDBCONTROLLER_H
#define FRAMEDBCONTROLLER_H

#include <QString>
#include <QMutexLocker>
#include <QObject>
#include <qvariant.h>
#include <exception>
#include <list>
#include <string>
#include <functional>
#include "CommonDBController.h"

/**
*	\brief -- Перечисление битовых флагов нормы сигнала.
*/
enum FRAME_FLAGS
{
	/// -- Флаг ККС;
	KKS_FLAG = 1,
	/// -- Флаг ДТК;
	DTK_FLAG = 2,
	/// -- Флаг КС СП.
	KS_FLAG = 4
};


/**
*	@struct MsgFromFrameDB
*	\brief -- Структура сообщений БД ИК.
*/
struct MsgFromFrameDB
{
	/// -- Конструктор;
	MsgFromFrameDB() {}
	/// -- Метка времени;
	qint64 dt = 0;
	/// -- Метка текущего участка процесса;
	QString label = "";
	/// -- Папка для хранения кадров;
	QString folder = "";
	/// -- Номер кадра в папке;
	qint32 frame_num = 0;
	/// -- Режим кадра;
	QString mode = "";
	/// -- Список обнаруженных массивов;
	QString arrays = "";
	/// -- Обнаруженные квитанции;
	qint32 kvit;
	/// -- Признак нормы ККС;
	bool kks = false;
	/// -- Признак нормы ДТК;
	bool dtk = false;
	/// -- Признак нормы КС СП;
	bool ks = false;
	/// -- ЧБН КФК;
	qint32 kfk = 0;
	/// -- Путь к хранимому кадру;
	QString path = "";
	/// -- Признак наличия сформированного q10;
	bool q10_exist = false;
	/// -- Соответствующее БВ;
	unsigned short bv = 0;
	/// -- Получение строки с обнаруженными квитанциями.
	QString kvit_string()
	{
		QString ret_str = "";
		if (kvit & 1)
			ret_str += "НЕТ КПИ ";

		if (kvit & 2)
			ret_str += "ДА КПИ ";

		if (kvit & 4)
			ret_str += "НЕТ СЧЗК ";

		if (kvit & 8)
			ret_str += "ДА СЧЗК ";
		return ret_str;
	}
};
Q_DECLARE_METATYPE(MsgFromFrameDB);


/**
*!\class FrameDBController
*	\brief -- Управление БД ИК.
*
*/
class FrameDBController : public CommonDBController
{
public:
	/**
	*	\brief -- Конструктор класса FrameDBController;
	*	\param _read_delay -- задержка чтения.
	*/
	FrameDBController(int _read_delay) : CommonDBController(_read_delay) {}
	/**
	*	\brief -- Добавление новых данных в БД ИК.
	*	\param _msg -- данные для добавления в виде структуры типа MsgFromFrameDB;
	*	\param dt -- метка времени;
	*	\return -- флаг успешности добавления новых данных.
	*/
	virtual bool insertFrameData(MsgFromFrameDB _msg, qint64 dt = 0) = 0;

};


#include "mongo_v3/MongoFrameDBController_v3.h"

#endif // FRAMEDBCONTROLLER_H