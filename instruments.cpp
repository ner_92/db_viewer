#include "instruments.h"
#include <qfile.h>
#include <qcryptographichash.h>
#include <qmessagebox.h>
#include <qpushbutton.h>
#include <qdatetime.h>
#include <qcoreapplication.h>
#include <qsettings.h>
#include <qendian.h>
#include <qdebug.h>

static QSettings *inSettings, *ipSettings;
static QString inSettingsFile, ipSettingsFile;


QString instr::displayString(const QString& str, bool hexformat)
{	
	try
	{
		if(str.isEmpty())
			return str;

		if(QRegExp("^[0][x][0-9a-fA-F]+$").exactMatch(str) 
			  || QRegExp("^\\d+\\.\\d+$").exactMatch(str) 
			  || QRegExp("^\\d+\\.\\d+e-?\\d+$").exactMatch(str)  
			  || QRegExp("^\\[.*\\]$").exactMatch(str) 
			  || QRegExp("^\".*\"$").exactMatch(str) 
			  || QRegExp("^\'.*\'$").exactMatch(str)
		  )
			return str;
		bool isValid = false;
		long long int_val = str.toLongLong(&isValid);
		
		if(isValid)
		{
			unsigned short len = (int_val > 0xFFFF) ? 8 : ((int_val > 0xFF) ? 4 : 0);
			if (hexformat && (int_val > 0))
				return QString("0x")+QString("%1").arg(int_val, len, 16, QChar('0')).toUpper();
			else
				return str;
		}
		else
			return ("\""+str+"\"");
	}
	catch (std::exception& e)
	{
	}
	catch (...)
	{

	}
	return "";
}

QString instr::variantToString(const QVariant& val, bool hexformat)
{
	try
	{
		QVariantList list = val.toList();
		if(val.isNull())
			return "0";

		if(val.canConvert<QVariantList>())
			return variantListToString(list, hexformat);
		else
		{
			if(val.canConvert<QVariantMap>())
			{
				QVariantMap v_map = val.toMap();
			
				QString ret_str = "[";
				for (auto itr = v_map.begin(), end = v_map.end(); itr != end; ++itr)
				{
					ret_str += QString("%1 : %2, ").arg(itr.key())
						.arg(variantToString(itr.value(), hexformat));
				}
				ret_str.chop(2);
				ret_str += "]";

				return ret_str;
			}
			else
				return displayString(val.toString(), hexformat);
		}
	}
	catch (std::exception& e)
	{
	}
	catch (...)
	{

	}
	return "";
};

QString instr::variantListToString(const QVariantList& list, bool hexformat)
{
	try
	{
		QString ret_str;
		ret_str = "[";
		for (QVariantList::const_iterator itr(list.begin()), end(list.end()); itr < end; ++itr)
		{
			ret_str += QString("%1,").arg(variantToString(*itr, hexformat)
										.replace("\n", "\\n").replace("\f", "\\f"));
		}
		if(list.count() > 0)
			ret_str.chop(1);

		return (ret_str + "]");
	}
	catch (std::exception& e)
	{
	}
	catch (...)
	{

	}
	return "";
};


QVariant instr::convertToVariant(const QString& str, 
								 const QVariant::Type type,
								 QScriptEngine* eng)
{
	if (str.isEmpty())
		return QVariant();

	int base = (str.contains("0x")) ? 16 : 10;
	switch (type)
	{
		case QVariant::Int: return str.toLongLong(0, base);
		case QVariant::UInt: return str.toULongLong(0, base);
		case QVariant::Double: return str.toDouble();
		case QVariant::String: return str;
		case QVariant::Bool: return str.toInt();
		case QVariant::UserType:
		case QVariant::List: 
		default: 
			if (eng == 0)
			{
				QScriptEngine tmp_eng;
				QScriptValue val = tmp_eng.evaluate(str);
				return val.toVariant();
			}
			else
				return eng->evaluate(str).toVariant();
	}
}
QString instr::CalcHash(const QString& path)
{
	QFile fin(path);
	fin.open(QIODevice::ReadOnly);
	QByteArray fcont = fin.readAll();
	fin.close();

	QByteArray ret = QCryptographicHash::hash(fcont, QCryptographicHash::Md5);

	QString crc;
	for (int i = 0; i < ret.count(); ++i)
		crc += QString("%1").arg((unsigned char)(ret.at(i)), 2, 16, QChar('0'));

	return crc;
}

QString instr::CalcFolderHash(const QString& path)
{
	QStringList tmp_paths;
	QDirIterator it(path, QDir::Files, QDirIterator::Subdirectories);
	while (it.hasNext()) {
		tmp_paths << it.next();
	}
	tmp_paths.sort();
	QByteArray tmp_files;
	for (int i = 0; i < tmp_paths.size(); i++){
		QFile fin(tmp_paths[i]);
		fin.open(QIODevice::ReadOnly);
		tmp_files.append(fin.readAll());
		fin.close();
	}
	QString hash_sum = QCryptographicHash::hash(tmp_files, QCryptographicHash::Md5).toHex();
	return hash_sum;
}

QString instr::readVersion()
{
	QString str = "";
	QFile f(":/version.txt");
	if (f.open(QFile::ReadOnly | QFile::Text))
	{
		str = f.readAll();
		f.close();
	}

	return str;
}

QFont instr::mainFont()
{
	//Trace T("instruments::mainFont");
	//QVariant val = LOGSMODEL::Instance().optionFromDB(ClientOptions::LN_MAIN_FONT_SIZE);

	QFont font;
	//font.setPointSize(val.isNull() ? 11 : val.toInt());
	font.setPointSize(11);
	font.setBold(true);
	font.setFamily(QString::fromUtf8("Calibri"));
	return font;
}

QString instr::get_table_style()
{
	return QString("alternate-background-color: rgb(206, 227, 255);background-color: rgb(239, 239, 239);");
}

QScriptValue instr::QVariantToScriptValue(QVariant val, QScriptEngine* eng)
{
	try
	{
		//Trace T("instruments::QVariantToScriptValue");
		QScriptValue result;

		if (val.canConvert<QVariantList>())
		{
			QVariantList tmp_list = val.toList();
			result = eng->newArray(tmp_list.count());
			for (int i = 0, end = tmp_list.count(); i < end; ++i)
				result.setProperty(i, QVariantToScriptValue(tmp_list[i], eng));

			return result;
		}

		if (val.canConvert<QVariantMap>())
		{
			result = eng->newObject();
			QVariantMap v_map = val.toMap();
			for (QVariantMap::const_iterator itr = v_map.begin(), end = v_map.end(); itr != end; ++itr)
				result.setProperty(itr.key(), instr::QVariantToScriptValue(itr.value(), eng));

			return result;
		}
		if (val.type() == QVariant::DateTime)
			return eng->newDate(val.toDateTime());

		bool isValid = false;

		QString str_val = val.toString();
		if (QRegExp("^[0][x][0-9a-fA-F]+$").exactMatch(str_val) || QRegExp("^\\d+\\.\\d+$").exactMatch(str_val)
			|| QRegExp("^\\d+\\.\\d+e-?\\d+$").exactMatch(str_val) || QRegExp("^\\d+$").exactMatch(str_val))

		{
			result = str_val.toDouble(&isValid);
			if (!isValid)
			{
				result = str_val.toInt();
				if (!isValid)
					return (val.type() == QVariant::String) ? str_val : eng->newVariant(val);
			}
		}
		else
			return (val.type() == QVariant::String) ? str_val : eng->newVariant(val);

		return result;
	}
	//CATCH_EXCEPTION("instruments::QVariantToScriptValue()");
	catch (...)
	{

	}
	return QScriptValue();
}

QVariant instr::QScriptValueToVariant(QScriptValue val, QScriptEngine* /*eng*/)
{
	//Trace T("instruments::QScriptValueToVariant");
	if (val.isString())
		return val.toString();

	if (val.isNumber())
		return val.toNumber();

	if (val.isUndefined() || val.isNull() || !val.isValid())
		return "";

	return val.toVariant();
}

bool instr::showSimpleMsg(const QString& title, const QString& text)
{
	//Trace T(QString("instruments::setFilterItemIcon title %1 text %2").arg(title).arg(text).toStdString());
	QMessageBox msg(QMessageBox::Information, title, text);
	msg.setFont(instr::mainFont());
	msg.addButton(new QPushButton("Да"), QMessageBox::AcceptRole);
	msg.addButton(new QPushButton("Нет"), QMessageBox::RejectRole);
	return !msg.exec();
}

int instr::showSimpleMsgButtons(const QString& title, const QString& text, const QStringList& btns)
{
	QMessageBox msg;
	msg.setIcon(QMessageBox::Information);
	msg.setWindowTitle(title);
	msg.setText(text);
	msg.setFont(instr::mainFont());
	QList<QPushButton> btn_list;
	foreach(QString btn_text, btns)
		msg.addButton(new QPushButton(btn_text), QMessageBox::NoRole);

	msg.exec();

	return btns.indexOf(msg.clickedButton()->text());
}



QString instr::get_aik_colour(int _ind)
{
	QString _colour;
	switch (_ind)
	{
	case 0:
		_colour = "DarkBlue";
		break;
	case 1:
		_colour = "LimeGreen";
		break;
	case 2:
		_colour = "Orange";
		break;
	case 3:
		_colour = "DarkGoldenRod";
		break;
	case 4:
		_colour = "OliveDrab";
		break;
	case 5:
		_colour = "DarkSlateGray";
		break;
	case 6:
		_colour = "LightSeaGreen";
		break;
	case 7:
		_colour = "Indigo";
		break;
	case 8:
		_colour = "Purple";
		break;
	case 9:
		_colour = "SlateGray";
		break;
	case 10:
		_colour = "Gold";
		break;
	case 11:
		_colour = "Sienna";
		break;
	case 12:
		_colour = "SeaGreen";
		break;
	case 13:
		_colour = "RosyBrown";
		break;
	case 14:
		_colour = "Tomato";
		break;
	case 15:
		_colour = "Gray";
		break;
	case 16:
		_colour = "Goldenrod";
		break;
	case 17:
		_colour = "DarkKhaki";
		break;
	case 18:
		_colour = "DarkCyan";
		break;
	case 19:
		_colour = "MediumOrchid";
		break;
	case 20:
		_colour = "DarkSlateBlue";
		break;
	case 21:
		_colour = "Salmon";
		break;
	case 22:
		_colour = "MediumVioletRed";
		break;
	case 23:
		_colour = "SlateGray";
		break;
	case 24:
		_colour = "PaleVioletRed";
		break;
	case 25:
		_colour = "MediumSeaGreen";
		break;
	case 26:
		_colour = "Maroon";
		break;
	case 27:
		_colour = "Chocolate";
		break;
	case 28:
		_colour = "MediumPurple";
		break;
	case 29:
		_colour = "IndianRed";
		break;
	case 30:
		_colour = "DarkGreen";
		break;

	default: 
		_colour = "black";
	};
	return _colour;
}


QList<TMDBs> instr::getTMDBs()
{
	QList<TMDBs> db_names;
	QString settings_dir_path = QCoreApplication::applicationDirPath() + "/" + "db_names.ini";
	QSettings db_names_settings(settings_dir_path, QSettings::IniFormat);
	db_names_settings.setIniCodec("UTF-8");
	int size = db_names_settings.beginReadArray("db_names");
	for (int i = 0; i < size; ++i)
	{
		TMDBs tmdbs;
		db_names_settings.setArrayIndex(i);
		tmdbs.DBType = db_names_settings.value("DBType").toString();
		tmdbs.rusName = db_names_settings.value("rusName").toString();
		tmdbs.engName = db_names_settings.value("engName").toString();
		db_names.push_back(tmdbs);
	}
	db_names_settings.endArray();
	return db_names;
}


QString instr::GetPathFromSettings(QString applname)
{
	QString tmp_frame_path;
	//if (frame_settings_dir_path.isEmpty())
	QString inSettingsFile = QString(QCoreApplication::applicationDirPath() + "/" + "%1.ini").arg(applname);
	QSettings inSettings(inSettingsFile, QSettings::IniFormat, NULL);
	inSettings.beginGroup("PATH");
	tmp_frame_path = inSettings.value("FRAME_PATH", "").toString();
	inSettings.endGroup();
	return tmp_frame_path;


}

QString instr::GetIpFromSettings(QString srver)
{
	QString tmp_srvr_ip;
	QSettings ipSettings("Cometa", "СПО АИК");
	tmp_srvr_ip = ipSettings.value(QString("%1").arg(srver), "localhost").toString();
	return tmp_srvr_ip;


}

bool instr::checkTemplate(QByteArray data, QString templ)
{
	bool res = true;
	if (templ.isEmpty())
		return res;
	QStringList sd_list = templ.split(" ");
	for (int i = 0; i < sd_list.size(); ++i)
	{
		QString tmp_sd = sd_list.at(i);
		int index = i;
		bool eq = true;
		if (tmp_sd.contains(":"))
		{
			QStringList num_list = tmp_sd.split(":");
			index = num_list.at(0).toInt()-1;
			tmp_sd = num_list.at(1);
		}
		if (index >= data.size())
			return false;
		quint8 value = data.at(index);
		quint8 mask = 0xFF;

		if (tmp_sd.contains("!"))
		{
			eq = false;
			tmp_sd.remove(0, 1);
		}

		if (tmp_sd.contains("m"))
		{
            QStringList num_list = tmp_sd.split("m");
            mask = num_list.at(1).toInt(0, 16);
            tmp_sd = num_list.at(0);
		}

		value &= mask;
		if (tmp_sd.contains("-"))
		{
			QStringList range_list = tmp_sd.split("-");
			int first_et = range_list.at(0).toInt(0, 16) & mask;
			int second_et = range_list.at(1).toInt(0, 16) & mask;
			res &= (eq == ((value >= first_et) && (value <= second_et)));
		}
		else
		{
			if (tmp_sd == "xx")
				continue;
			quint8 et = tmp_sd.toInt(0, 16) & mask;
			res &= (eq == (value == et));
		}
	
	}
	return res;
}


QVariantList instr::arrToList(const QByteArray& _arr)
{
	QVariantList _list;
	foreach (auto _b, _arr)
	{
		_list << (quint8)_b;
	}
	return _list;
}

std::vector<bool> instr::byteToBits(const QByteArray ba)
{
	// 0x7A7 -> 11110100111
	std::vector<bool> res;

	for (auto const &byte : ba)
	{
		for (int posBit = 7; posBit >= 0; --posBit)		// старший бит первый
			(byte >> posBit) & true ? res.push_back(true) : res.push_back(false); // Выделил соответствующий бит
	}

	/*while (true && !res.empty())
	if (!res[0])
	res.erase(res.begin());
	else
	break;*/

	return res;
}

QByteArray instr::bitsToByte(std::vector<bool> bits)
{
	// 011110100111 -> 0x7A7
	QByteArray res;
	int count = 7;
	quint8 byte = 0;
	while (bits.size() % 8 != 0)
		bits.insert(bits.begin(), false);

	for (auto bit : bits)
	{
		if (bit)
			byte = byte | (bit << count);

		if (count == 0)
		{
			res.push_back(byte);
			byte = 0;
			count = 7;
		}
		else
			--count;
	}
	return res;
}

QVariantList instr::arrToShortList(const QByteArray& _arr)
{
	QString short_str = "short str ";
	QVariantList _list;
	QByteArray local_arr = _arr;
	while (!local_arr.isEmpty())
	{
		QByteArray tmp_arr = local_arr.left(2);
		local_arr.remove(0, 2);
		_list << tmp_arr.toHex().toUShort(0, 16);
		short_str += QString("%1 ").arg((unsigned short)_list.back().toInt(), 4, 16);
	}
	qDebug() << short_str;
	return _list;
}


QByteArray instr::listToArr(const QVariantList& _list)
{
	QByteArray _arr;
	foreach(auto _b, _list)
	{
		_arr.push_back((unsigned char)_b.toInt());
	}
	return _arr;
}

QByteArray instr::shortListToArr(const QVariantList& _list)
{
	QByteArray _arr;
	foreach(auto _b, _list)
	{
		_arr.push_back((unsigned char)((_b.toInt()>>8)&0xFF));
		_arr.push_back((unsigned char)(_b.toInt()&0xFF));
	}
	return _arr;
}


quint16 instr::KSM_msg(QByteArray new_msg)
{
	if (new_msg.size() % 2 != 0) // проверка на нечет
	{
		QByteArray new_bit;
		new_bit = QByteArray::fromHex("55");
		new_msg.append(new_bit);
	}

	// расчет КСМ
	quint32 words = 0;
	for (int i = 0; !new_msg.isEmpty(); i++)
	{
		quint16 q;
		q = 0;
		q = qFromBigEndian<quint16>((const uchar*)new_msg.left(2).constData());
		new_msg.remove(0, 2);
		words += q;

		if (words > 0xFFFF)
		{
			words &= 0xFFFF;
			words++;
		}
	}
	return words;
}

bool instr::controlUnParity(const quint16 spo)
{
	quint16 tmp_spo = spo;

	tmp_spo >>= 1;	// выкинуть сам бит нечётности
	tmp_spo <<= 1;

	for (int pos = 15; pos > 0; --pos)
	{
		bool r = (tmp_spo >> pos) & true; // старший бит spo
		tmp_spo <<= 16 - pos; // выкидываем старший из основного числа
		tmp_spo >>= 16 - pos;
		if (r)
			tmp_spo ^= 1 << (pos - 1);
	}

	return (bool)!tmp_spo;		// Если нечётное, то 1, а нужно наоборот
}

int instr::gen_crc5(const std::vector<bool> data)
{
	int crc = 0;
	size_t i, j;

	std::vector<bool> ksm = data;
	//ksm.resize(ksm.size() + 4, false);

	bool poly[] = { 1, 0, 0, 1, 1 };

	for (i = 0; i <= (ksm.size() - 5); i++)
	{
		if (ksm[i] == 0)
			continue;

		for (j = 0; j < 5; j++)
			ksm[j + i] = ksm[j + i] ^ poly[j];
	}

	uint8_t boolToUint[] = { 0 };

	for (int j = 0; j < 4; j++)
	{
		uint8_t tmp = ksm[11 + j];
		tmp <<= (7 - j);
		boolToUint[0] |= tmp;
	}

	return (boolToUint[0] >> 4);
}

QByteArray instr::gen_crc6(std::vector<bool> data)
{
	int crc = 0;
	size_t i, j;

	std::vector<bool> ksm = data;
	ksm.resize(ksm.size() + 6, false);

	bool poly[] = { 1, 1, 0, 0, 0, 0, 1 };

	for (i = 0; i <= (ksm.size() - 7); i++)
	{
		if (ksm[i] == 0)
			continue;

		for (j = 0; j < 7; j++)
			ksm[j + i] = ksm[j + i] ^ poly[j];
	}

	uint8_t boolToUint[] = { 0 };

	for (int j = 0; j < 6; j++)
	{
		uint8_t tmp = ksm[29 + j];
		tmp <<= (7 - j);
		boolToUint[0] |= tmp;
	}

	QByteArray CRC6;
	CRC6.push_back((unsigned char)(boolToUint[0] >> 2));
	return CRC6;
}

int instr::gen_crc16(const QByteArray data)
{
	unsigned short tmp = 0x0;
	unsigned short crc = 0x0;
	static const unsigned short CRC_CCITT_TABLE[256] =
	{
		0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
		0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
		0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
		0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
		0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
		0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
		0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
		0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
		0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
		0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
		0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
		0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
		0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
		0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
		0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
		0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
		0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
		0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
		0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
		0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
		0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
		0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
		0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
		0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
		0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
		0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
		0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
		0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
		0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
		0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
		0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
		0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
	};

	for (char i : data)
	{
		tmp = (crc >> 8) ^ (i & 0xFF);
		crc = (crc << 8) ^ CRC_CCITT_TABLE[tmp];
	}

	return (~crc) & 0xFFFF;
}