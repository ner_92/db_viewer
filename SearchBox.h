#ifndef SEARCHBOX_H
#define SEARCHBOX_H

#include <qwidget.h>
#include <qmenu.h>
#include <qstandarditemmodel.h>
#include <qlineedit.h>
#include <qlistview.h>
#include <qsortfilterproxymodel.h>
#include <qtoolbutton.h>
#include <qtimer.h>

/**
*!\class SearchBox
* \brief Меню поиска директив
*/
class SearchBox : public QWidget
{
	Q_OBJECT

public:
	///Конструктор класса SearchBox
	SearchBox(QWidget *parent = 0);
	/**
	*	\brief Загрузка модели элементов списка
	*	\param processes - список строк
	*/
	void setListData(const QStringList& processes);
	/**
	*	\brief Выдача текущего запроса в поле поиска
	*	\return Текущий запрос поля поиска
	*/
	QString currentText() const;
	/**
	*	\brief Задание запроса для поля поиска
	*	\param text - запрос для поля поиска
	*/
	void setText(const QString& text);
	/**
	*	\brief Очистка поля поиска
	*/
	void clearText();
	void setWidgetFont(const QFont& font);
	void setPlaceholderText(const QString& _str);
protected:
	/**
	*	\brief Фильтр событий
	*	\param obj - источник события
	*	\param ev - событие
	*	\return Флаг обработки события
	*/
	bool eventFilter(QObject *obj, QEvent *ev);
private slots:
	/*
	*	\brief Подтверждение выбора директивы из меню поиска
	*/
	void doneCompletion();
	/*
	*	\brief Обновление модели элементов списка
	*/
	void renewSearchList();
	/*
	*	\brief Подтверждение выбора при нажатии клавишы Enter в поле ввода
	*	При отсутствии вариантов очищает поле вводаи скрывает список.
	*	При наличие одниночного варианта- выбрает его.
	*	При наличии от 2-х и более вариантов выводит список
	*/
	void agreeWithOutList();
private:
	///Таймер задержки отображения списка
	QTimer timer;
	///Модель элементов списка
	QStandardItemModel search_model;
	///Модель фильтрации элементов списка
	QSortFilterProxyModel sfp_model;
	///Поле ввода
	QLineEdit editor;
	///Список доступных директив
	QListView popup;
signals:
	/*
	*	\brief Сигнал подтверждения выбора директивы из меню поиска
	*	\param text - выбранная директива
	*/
	void finishSearch(QString text);
};


#endif //SEARCHBOX_H
