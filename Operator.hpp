#ifndef OPERATOR_HPP
#define OPERATOR_HPP

#include <qstring.h>
#include <qvariant.h>
#include "instruments.h"

/**
*!\class Param
*	\brief Параметр
*
*/
struct Param
{
	/**
	*	\brief Конструктор класса Param(Параметр)
	*	\param _name - название
	*	\param _type_name - название типа параметра
	*	\param _type - тип параметра в QVariant
	*	\param _desc - описание
	*/
	Param(const QString& _name, const QString& _type_name, QVariant::Type _type, 
			const QString& _desc = QString())
		: name(_name), type_name(_type_name), type(_type), 
			desc(_desc), inc_l(true), inc_r(true), is_ret(false)
	{
	}
	/**
	*	\brief Деструктор объекта параметра
	*/
	~Param()
	{
		foreach(const Param* par, children)
			delete par;
		
		children.clear();
	}
	///Название типа параметра
	QString type_name;
	///Тип параметра в QVariant
	QVariant::Type type;
	///Флаг включения левой границы интервала допустимых значений
	bool inc_l;
	///Флаг включения правой границы интервала допустимых значений
	bool inc_r;
	///Значение правой границы интервала допустимых значений
	QVariant rightlimit;
	///Значение левой границы интервала допустимых значений
	QVariant leftlimit;
	///Шаг значения параметра в пределах интервала допустимых значений
	QVariant step;
	///Значение по умолчанию
	QVariant def_value;
	///Список фиксированных значений
	QVariantList fix_values;
	///Название
	QString name;
	///Описание
	QString desc;
	///Список детей-параметров
	QList<const Param*> children;
	///Флаг возвращаемого значения
	bool is_ret;
};

class Operator;

/**
*!\class OpGroup
*	\brief Группа операторов
*
*/
class OpGroup
{
public:
	OpGroup(OpGroup* _parent) : parent(_parent) {}
	OpGroup* parent;
	QList<const OpGroup*> sub_groups;
	QList<const Operator*> op_list;
	QString group_name;
};

/**
*!\class Operator
*	\brief Оператор
*
*/
class Operator
{
public:
	/**
	*	\brief Конструктор класса Operator(Оператор)
	*	\param _name - название
	*	\param _header - заголовок
	*	\param _group - группа
	*/
	Operator(QString _name = "", QString _header = "", QString _short_description = "", QString _description = "", OpGroup* _parent = 0)
		: name(_name), header(_header), short_description(_short_description), description(_description), parent(_parent)
	{}
	/**
	*	\brief Деструктор объекта оператора
	*/
	~Operator()
	{
		foreach(const Param* par, par_list)
			delete par;

		par_list.clear();
	}
	/**
	*	\brief Добавление параметра
	*	\param par - параметр
	*/
	void addParam(const Param* par)
	{
		par_list.push_back(par);
	}
	/**
	*	\brief Выдача списка параметров
	*	\return Заголовок
	*/
	const QList<const Param*>& getParamList() const
	{
		return par_list;
	}
	/**
	*	\brief Выдача названия оператора
	*	\return Название
	*/
	QString getName() const
	{
		return name;
	}
	/**
	*	\brief Выдача заголовка оператора
	*	\return Заголовок
	*/
	QString getHeader() const
	{
		return header;
	}
	/**
	*	\brief Выдача краткого описания оператора
	*	\return Краткое описание
	*/
	QString getShortDescription() const
	{
		return short_description;
	}
	/**
	*	\brief Выдача подробного описания оператора
	*	\return Подробное описание
	*/
	QString getDescription() const
	{
		return description;
	}
	OpGroup* getGroup() const
	{
		return parent;
	}
	QMap<int, QString> getDefaultMAP() const
	{
		QMap<int, QString> tmp_map;
		for (int i = 0; i < par_list.count(); i++)
		{
			if (par_list.at(i)->def_value != QVariant())
				tmp_map.insert(i, instr::variantToString(par_list.at(i)->def_value));
		}
		return tmp_map;
	}
private:
	///Список параметров
	QList<const Param*> par_list;
	///Название
	QString name;
	///Заголовок
	QString header;
	///Краткое описание
	QString short_description;
	///Полное описание
	QString description;
	///Группа, в которую входит оператор
	OpGroup* parent;

	
};



#endif//OPERATOR_HPP
