#include "NarabotkaWidg.h"

NarabotkaWidg::NarabotkaWidg(QWidget *parent) :QWidget(parent)
{
	inicial();
	inicialNarabotka();
	connect(_14R732Label, &MyLabel::clicked, this, &NarabotkaWidg::_14R732lick);
	connect(MBK02Label, &MyLabel::clicked, this, &NarabotkaWidg::MBK02Click);
	connect(ASNLabel, &MyLabel::clicked, this, &NarabotkaWidg::ASNClick);
	connect(MBK04Label, &MyLabel::clicked, this, &NarabotkaWidg::MBK04Click);
	connect(LKA05Label, &MyLabel::clicked, this, &NarabotkaWidg::LKA05Click);
	connect(BECHLabel, &MyLabel::clicked, this, &NarabotkaWidg::BECHClick);
	connect(_14R733Label, &MyLabel::clicked, this, &NarabotkaWidg::_14R733Click);
	connect(MBK07Label, &MyLabel::clicked, this, &NarabotkaWidg::MBK07Click);
	connect(BUP_AFS_BKUPILabel, &MyLabel::clicked, this, &NarabotkaWidg::BUP_AFS_BKUPIClick);
	connect(BAO_VLabel, &MyLabel::clicked, this, &NarabotkaWidg::BAO_VClick);
	connect(BKOILabel, &MyLabel::clicked, this, &NarabotkaWidg::BKOIClick);
	connect(BRTK_BAULabel, &MyLabel::clicked, this, &NarabotkaWidg::BRTK_BAUClick);
	connect(BRTK_BALOILabel, &MyLabel::clicked, this, &NarabotkaWidg::BRTK_BALOIClick);
	connect(BAO_SHPKLabel, &MyLabel::clicked, this, &NarabotkaWidg::BAO_SHPKClick);
	connect(BAO_UPKLabel, &MyLabel::clicked, this, &NarabotkaWidg::BAO_UPKClick);
	connect(BAO_OSLabel, &MyLabel::clicked, this, &NarabotkaWidg::BAO_OSClick);
	connect(BAO_SOTRLabel, &MyLabel::clicked, this, &NarabotkaWidg::BAO_SOTRClick);
	connect(BRTK_AFARLabel, &MyLabel::clicked, this, &NarabotkaWidg::BRTK_AFARClick);
	connect(BRTK_AFS1Label, &MyLabel::clicked, this, &NarabotkaWidg::BRTK_AFS1Click);
	connect(BRTK_AFS2Label, &MyLabel::clicked, this, &NarabotkaWidg::BRTK_AFS2Click);
	connect(BRTK_AOSLabel, &MyLabel::clicked, this, &NarabotkaWidg::BRTK_AOSClick);
	connect(SOTR_MCALabel, &MyLabel::clicked, this, &NarabotkaWidg::SOTR_MCAClick);
	connect(UKPLabel, &MyLabel::clicked, this, &NarabotkaWidg::UKPClick);
	connect(CBKLabel, &MyLabel::clicked, this, &NarabotkaWidg::CBKClick);
	connect(time_MCALabel, &MyLabel::clicked, this, &NarabotkaWidg::timeMCAClick);
	connect(time_AIKLabel, &MyLabel::clicked, this, &NarabotkaWidg::timeAIKClick);

}
NarabotkaWidg::~NarabotkaWidg()
{

}

void NarabotkaWidg::_14R732lick()
{
	hideAll();
	groupBox->setTitle("Наработка 14Р732");
}

void NarabotkaWidg::MBK02Click()
{
	hideAll();
	groupBox->setTitle("Наработка МБК02");
}

void NarabotkaWidg::ASNClick()
{
	hideAll();
	groupBox->setTitle("Наработка АСН");
}

void NarabotkaWidg::MBK04Click()
{
	hideAll();
	groupBox->setTitle("Наработка МБК04");
}

void NarabotkaWidg::LKA05Click()
{
	hideAll();
	groupBox->setTitle("Наработка ЛКА05");
}

void NarabotkaWidg::BECHClick()
{
	hideAll();
	groupBox->setTitle("Наработка БЭЧ");
}

void NarabotkaWidg::_14R733Click()
{
	hideAll();
	groupBox->setTitle("Наработка 14Р733");
}

void NarabotkaWidg::MBK07Click()
{
	hideAll();
	groupBox->setTitle("Наработка МБК07");
}

void NarabotkaWidg::BUP_AFS_BKUPIClick()
{
	hideAll();
	groupBox->setTitle("Наработка БУП АФС БКУПИ");
}

void NarabotkaWidg::BAO_VClick()
{
	hideAll();
	groupBox->setTitle("Наработка БАО В");
}

void NarabotkaWidg::BKOIClick()
{
	hideAll();
	groupBox->setTitle("Наработка БКОИ");
}

void NarabotkaWidg::BRTK_BAUClick()
{
	hideAll();
	groupBox->setTitle("Наработка БРТК БАУ");
}

void NarabotkaWidg::BRTK_BALOIClick()
{
	hideAll();
	groupBox->setTitle("Наработка БРТК БАЛОИ");
}

void NarabotkaWidg::BAO_SHPKClick()
{
	hideAll();
	groupBox->setTitle("Наработка БАО ШПК");
}

void NarabotkaWidg::BAO_UPKClick()
{
	hideAll();
	groupBox->setTitle("Наработка БАО УПК");
}

void NarabotkaWidg::BAO_OSClick()
{
	hideAll();
	groupBox->setTitle("Наработка БАО ОС");
}

void NarabotkaWidg::BAO_SOTRClick()
{
	hideAll();
	groupBox->setTitle("Наработка БАО СОТР");
}

void NarabotkaWidg::BRTK_AFARClick()
{
	hideAll();
	groupBox->setTitle("Наработка БРТК АФАР");
}

void NarabotkaWidg::BRTK_AFS1Click()
{
	hideAll();
	groupBox->setTitle("Наработка БРТК АФС1");
}

void NarabotkaWidg::BRTK_AFS2Click()
{
	hideAll();
	groupBox->setTitle("Наработка БРТК АФС2");
}

void NarabotkaWidg::BRTK_AOSClick()
{
	hideAll();
	groupBox->setTitle("Наработка БРТК АОС");
}

void NarabotkaWidg::SOTR_MCAClick()
{
	hideAll();
	groupBox->setTitle("Наработка СОТР МЦА");
}

void NarabotkaWidg::UKPClick()
{
	hideAll();
	groupBox->setTitle("Наработка УКП");
}

void NarabotkaWidg::CBKClick()
{
	hideAll();
	VM1Label->show();
	VM2Label->show();
	VM3Label->show();
	VM4Label->show();
	VM1Edit->show();
	VM2Edit->show();
	VM3Edit->show();
	VM4Edit->show();
	groupBox->setTitle("Наработка ЦБК");
}
void NarabotkaWidg::timeMCAClick()
{
	hideAll();
	groupBox->setTitle("Время наработки МЦА");
}

void NarabotkaWidg::timeAIKClick()
{
	hideAll();
	groupBox->setTitle("Время наработки АИК");
}


void NarabotkaWidg::hideAll()
{
	VM1Label->hide();
	VM2Label->hide();
	VM3Label->hide();
	VM4Label->hide();
	VM1Edit->hide();
	VM2Edit->hide();
	VM3Edit->hide();
	VM4Edit->hide();
}

void NarabotkaWidg::inicialNarabotka()
{
	////////////////////////////////////////////////////////////////////////// NARABOTKA CBK
	VM1Label = new QLabel("ВМ1");
	VM2Label = new QLabel("ВМ2");
	VM3Label = new QLabel("ВМ3");
	VM4Label = new QLabel("ВМ4");
	VM1Edit = new QLineEdit();
	VM2Edit = new QLineEdit();
	VM3Edit = new QLineEdit();
	VM4Edit = new QLineEdit();
	mini3GridLayout->addWidget(VM1Label, 0, 0);
	mini3GridLayout->addWidget(VM1Edit, 0, 1);
	mini3GridLayout->addWidget(VM2Label, 1, 0);
	mini3GridLayout->addWidget(VM2Edit, 1, 1);
	mini3GridLayout->addWidget(VM3Label, 2, 0);
	mini3GridLayout->addWidget(VM3Edit, 2, 1);
	mini3GridLayout->addWidget(VM4Label, 3, 0);
	mini3GridLayout->addWidget(VM4Edit, 3, 1);
	////////////////////////////////////////////////////////////////////////// 



	mini3GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 2);
	hideAll();
}

void NarabotkaWidg::inicial()
{
	grandGridLayout = new QGridLayout(this);
	mini1GridLayout = new QGridLayout;
	mini2GridLayout = new QGridLayout;
	mini3GridLayout = new QGridLayout;

	groupBox = new QGroupBox;

	_14R732Label = new MyLabel("14Р732");
	MBK02Label = new MyLabel("МБК02");
	ASNLabel = new MyLabel("АСН");
	MBK04Label = new MyLabel("МБК04");
	LKA05Label = new MyLabel("ЛКА05");
	BECHLabel = new MyLabel("БЭЧ");
	_14R733Label = new MyLabel("14Р733");
	MBK07Label = new MyLabel("МБК07");
	BUP_AFS_BKUPILabel = new MyLabel("БУП АФС БКУПИ");
	BAO_VLabel = new MyLabel("БАО В");
	BKOILabel = new MyLabel("БКОИ");
	BRTK_BAULabel = new MyLabel("БРТК БАУ");
	BRTK_BALOILabel = new MyLabel("БРТК БАЛОИ");
	BAO_SHPKLabel = new MyLabel("БАО ШПК");
	BAO_UPKLabel = new MyLabel("БАО УПК");
	BAO_OSLabel = new MyLabel("БАО ОС");
	BAO_SOTRLabel = new MyLabel("БАО СОТР");
	BRTK_AFARLabel = new MyLabel("БРТК АФАР");
	BRTK_AFS1Label = new MyLabel("БРТК АФС1");
	BRTK_AFS2Label = new MyLabel("БРТК АФС2");
	BRTK_AOSLabel = new MyLabel("БРТК АОС");
	SOTR_MCALabel = new MyLabel("СОТР МЦА");
	UKPLabel = new MyLabel("УКП");
	CBKLabel = new MyLabel("ЦБК");
	time_MCALabel = new MyLabel("Время наработки МЦА");
	time_AIKLabel = new MyLabel("Время наработки АИК");


	_14R732Edit = new QLineEdit;
	MBK02Edit = new QLineEdit;
	ASNEdit = new QLineEdit;
	MBK04Edit = new QLineEdit;
	LKA05Edit = new QLineEdit;
	BECHEdit = new QLineEdit;
	_14R733Edit = new QLineEdit;
	MBK07Edit = new QLineEdit;
	BUP_AFS_BKUPIEdit = new QLineEdit;
	BAO_VEdit = new QLineEdit;
	BKOIEdit = new QLineEdit;
	BRTK_BAUEdit = new QLineEdit;
	BRTK_BALOIEdit = new QLineEdit;
	BAO_SHPKEdit = new QLineEdit;
	BAO_UPKEdit = new QLineEdit;
	BAO_OSEdit = new QLineEdit;
	BAO_SOTREdit = new QLineEdit;
	BRTK_AFAREdit = new QLineEdit;
	BRTK_AFS1Edit = new QLineEdit;
	BRTK_AFS2Edit = new QLineEdit;
	BRTK_AOSEdit = new QLineEdit;
	SOTR_MCAEdit = new QLineEdit;
	UKPEdit = new QLineEdit;
	CBKEdit = new QLineEdit;
	time_MCAEdit = new QLineEdit;
	time_AIKEdit = new QLineEdit;

	mini1GridLayout->addWidget(_14R732Label, 0, 0);
	mini1GridLayout->addWidget(_14R732Edit, 0, 1);
	mini1GridLayout->addWidget(MBK02Label, 1, 0);
	mini1GridLayout->addWidget(MBK02Edit, 1, 1);
	mini1GridLayout->addWidget(ASNLabel, 2, 0);
	mini1GridLayout->addWidget(ASNEdit, 2, 1);
	mini1GridLayout->addWidget(MBK04Label, 3, 0);
	mini1GridLayout->addWidget(MBK04Edit, 3, 1);
	mini1GridLayout->addWidget(LKA05Label, 4, 0);
	mini1GridLayout->addWidget(LKA05Edit, 4, 1);
	mini1GridLayout->addWidget(BECHLabel, 5, 0);
	mini1GridLayout->addWidget(BECHEdit, 5, 1);
	mini1GridLayout->addWidget(_14R733Label, 6, 0);
	mini1GridLayout->addWidget(_14R733Edit, 6, 1);
	mini1GridLayout->addWidget(MBK07Label, 7, 0);
	mini1GridLayout->addWidget(MBK07Edit, 7, 1);
	mini1GridLayout->addWidget(BUP_AFS_BKUPILabel, 8, 0);
	mini1GridLayout->addWidget(BUP_AFS_BKUPIEdit, 8, 1);
	mini1GridLayout->addWidget(BAO_VLabel, 9, 0);
	mini1GridLayout->addWidget(BAO_VEdit, 9, 1);
	mini1GridLayout->addWidget(BKOILabel, 10, 0);
	mini1GridLayout->addWidget(BKOIEdit, 10, 1);
	mini1GridLayout->addWidget(BRTK_BAULabel, 11, 0);
	mini1GridLayout->addWidget(BRTK_BAUEdit, 11, 1);
	mini1GridLayout->addWidget(BRTK_BALOILabel, 12, 0);
	mini1GridLayout->addWidget(BRTK_BALOIEdit, 12, 1);
	mini1GridLayout->addWidget(BAO_SHPKLabel, 13, 0);
	mini1GridLayout->addWidget(BAO_SHPKEdit, 13, 1);
	mini1GridLayout->addWidget(BAO_UPKLabel, 14, 0);
	mini1GridLayout->addWidget(BAO_UPKEdit, 14, 1);
	mini1GridLayout->addWidget(BAO_OSLabel, 15, 0);
	mini1GridLayout->addWidget(BAO_OSEdit, 15, 1);
	mini1GridLayout->addWidget(BAO_SOTRLabel, 16, 0);
	mini1GridLayout->addWidget(BAO_SOTREdit, 16, 1);
	mini1GridLayout->addWidget(BRTK_AFARLabel, 17, 0);
	mini1GridLayout->addWidget(BRTK_AFAREdit, 17, 1);
	mini1GridLayout->addWidget(BRTK_AFS1Label, 18, 0);
	mini1GridLayout->addWidget(BRTK_AFS1Edit, 18, 1);
	mini1GridLayout->addWidget(BRTK_AFS2Label, 19, 0);
	mini1GridLayout->addWidget(BRTK_AFS2Edit, 19, 1);
	mini1GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Fixed, QSizePolicy::Expanding), 20, 2);
	mini1GridLayout->setSpacing(25);

	mini2GridLayout->addWidget(BRTK_AOSLabel, 0, 0);
	mini2GridLayout->addWidget(BRTK_AOSEdit, 0, 1);
	mini2GridLayout->addWidget(SOTR_MCALabel, 1, 0);
	mini2GridLayout->addWidget(SOTR_MCAEdit, 1, 1);
	mini2GridLayout->addWidget(UKPLabel, 2, 0);
	mini2GridLayout->addWidget(UKPEdit, 2, 1);
	mini2GridLayout->addWidget(CBKLabel, 3, 0);
	mini2GridLayout->addWidget(CBKEdit, 3, 1);
	mini2GridLayout->addWidget(time_MCALabel, 0, 2);
	mini2GridLayout->addWidget(time_MCAEdit, 0, 3);
	mini2GridLayout->addWidget(time_AIKLabel, 1, 2);
	mini2GridLayout->addWidget(time_AIKEdit, 1, 3);
	//mini2GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Fixed, QSizePolicy::Expanding), 4, 4);
	mini2GridLayout->setSpacing(25);


	mini3GridLayout->setSpacing(25);
	groupBox->setLayout(mini3GridLayout);
	groupBox->setTitle("Выберите устройство для того чтобы увидеть детальную наработку");

	grandGridLayout->addLayout(mini1GridLayout, 0, 0, 2, 1);
	grandGridLayout->addLayout(mini2GridLayout, 0, 1);
	grandGridLayout->addWidget(groupBox, 1, 1);
	grandGridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 2, 2);
}