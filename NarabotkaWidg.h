#ifndef NARABOTKAWIDG_H
#define NARABOTKAWIDG_H

#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QGroupBox>

/**
*	\class MyLabel
*	\brief -- класс MyLabel;
*/
class MyLabel : public QLabel
{
	Q_OBJECT
public:
	
	/**
	*	\brief -- Конструктор;
	*	\param text -- Текст.
	*	\param parent -- Родитель.
	*/
	MyLabel(const QString &text = "", QWidget *parent = 0)
	{
		setText(text);
	}
	
	/**
	*	\brief -- Деструктор;
	*/
	~MyLabel(){}
signals:
	
	/**
	*	\brief -- Сигнал клика;
	*/
	void clicked();
protected:

	/**
	*	\brief -- event;
	*	\param event -- event
	*/
	void mousePressEvent(QMouseEvent *event)
	{
		emit clicked();
	}
};

/**
*	\class NarabotkaWidg
*	\brief -- класс NarabotkaWidg;
*/
class NarabotkaWidg : public QWidget
{
	Q_OBJECT
public:
	/**
	*	\brief -- Конструктор;
	*	\param parent -- Родитель.
	*/
	NarabotkaWidg(QWidget *parent = 0);

	/**
	*	\brief -- Деструктор;
	*/
	~NarabotkaWidg();
private:

	/// -- Метка;
	MyLabel *_14R732Label;
	/// -- Метка; 
	MyLabel *MBK02Label;
	/// -- Метка; 
	MyLabel *ASNLabel;
	/// -- Метка; 
	MyLabel *MBK04Label;
	/// -- Метка; 
	MyLabel *LKA05Label;
	/// -- Метка; 
	MyLabel *BECHLabel;
	/// -- Метка; 
	MyLabel *_14R733Label;
	/// -- Метка; 
	MyLabel *MBK07Label;
	/// -- Метка; 
	MyLabel *BUP_AFS_BKUPILabel;
	/// -- Метка; 
	MyLabel *BAO_VLabel;
	/// -- Метка; 
	MyLabel *BKOILabel;
	/// -- Метка; 
	MyLabel *BRTK_BAULabel;
	/// -- Метка; 
	MyLabel *BRTK_BALOILabel;
	/// -- Метка; 
	MyLabel *BAO_SHPKLabel;
	/// -- Метка; 
	MyLabel *BAO_UPKLabel;
	/// -- Метка; 
	MyLabel *BAO_OSLabel;
	/// -- Метка; 
	MyLabel *BAO_SOTRLabel;
	/// -- Метка; 
	MyLabel *BRTK_AFARLabel;
	/// -- Метка; 
	MyLabel *BRTK_AFS1Label;
	/// -- Метка; 
	MyLabel *BRTK_AFS2Label;
	/// -- Метка; 
	MyLabel *BRTK_AOSLabel;
	/// -- Метка; 
	MyLabel *SOTR_MCALabel;
	/// -- Метка; 
	MyLabel *UKPLabel;
	/// -- Метка; 
	MyLabel *CBKLabel;
	/// -- Метка; 
	MyLabel *time_MCALabel;
	/// -- Метка;
	MyLabel *time_AIKLabel;

	/// -- Поле;
	QLineEdit *_14R732Edit;
	/// -- Поле; 
	QLineEdit *MBK02Edit;
	/// -- Поле; 
	QLineEdit *ASNEdit;
	/// -- Поле; 
	QLineEdit *MBK04Edit;
	/// -- Поле; 
	QLineEdit *LKA05Edit;
	/// -- Поле; 
	QLineEdit *BECHEdit;
	/// -- Поле; 
	QLineEdit *_14R733Edit;
	/// -- Поле; 
	QLineEdit *MBK07Edit;
	/// -- Поле; 
	QLineEdit *BUP_AFS_BKUPIEdit;
	/// -- Поле; 
	QLineEdit *BAO_VEdit;
	/// -- Поле; 
	QLineEdit *BKOIEdit;
	/// -- Поле; 
	QLineEdit*BRTK_BAUEdit;
	/// -- Поле; 
	QLineEdit *BRTK_BALOIEdit;
	/// -- Поле 
	QLineEdit *BAO_SHPKEdit;
	/// -- Поле; 
	QLineEdit *BAO_UPKEdit;
	/// -- Поле; 
	QLineEdit *BAO_OSEdit;
	/// -- Поле; 
	QLineEdit *BAO_SOTREdit;
	/// -- Поле; 
	QLineEdit *BRTK_AFAREdit;
	/// -- Поле; 
	QLineEdit *BRTK_AFS1Edit;
	/// -- Поле; 
	QLineEdit *BRTK_AFS2Edit;
	/// -- Поле; 
	QLineEdit *BRTK_AOSEdit;
	/// -- Поле; 
	QLineEdit *SOTR_MCAEdit;
	/// -- Поле; 
	QLineEdit *UKPEdit;
	/// -- Поле; 
	QLineEdit *CBKEdit;
	/// -- Поле; 
	QLineEdit *time_MCAEdit;
	/// -- Поле; 
	QLineEdit *time_AIKEdit;

	/// -- Компановка;
	QGridLayout *grandGridLayout;
	/// -- Компановка;
	QGridLayout *mini1GridLayout;
	/// -- Компановка;
	QGridLayout *mini2GridLayout;
	/// -- Компановка;
	QGridLayout *mini3GridLayout;
	/// -- Компановка;
	QGroupBox *groupBox;

	/// -- Метка; 
	QLabel *VM1Label;
	/// -- Метка; 
	QLabel *VM2Label;
	/// -- Метка; 
	QLabel *VM3Label;
	/// -- Метка; 
	QLabel *VM4Label;
	/// -- Поле; 
	QLineEdit *VM1Edit;
	/// -- Поле; 
	QLineEdit *VM2Edit;
	/// -- Поле; 
	QLineEdit *VM3Edit;
	/// -- Поле; 
	QLineEdit *VM4Edit;

	/**
	*	\brief -- Инициация;
	*/
	void inicial();
	
	/**
	*	\brief -- Инициация;
	*/
	void inicialNarabotka();

	/**
	*	\brief -- Скрыть все;
	*/
	void hideAll();

	/**
	*	\brief -- Клик;
	*/
	void _14R732lick();

	/**
	*	\brief -- Клик
	*/
	void MBK02Click();
	
	/**
	*	\brief -- Клик;
	*/
	void ASNClick();

	/**
	*	\brief -- Клик;
	*/
	void MBK04Click();

	/**
	*	\brief -- Клик;
	*/
	void LKA05Click();

	/**
	*	\brief -- Клик;
	*/
	void BECHClick();
	
	/**
	*	\brief -- Клик;
	*/
	void _14R733Click();

	/**
	*	\brief -- Клик;
	*/
	void MBK07Click();

	/**
	*	\brief -- Клик;
	*/
	void BUP_AFS_BKUPIClick();

	/**
	*	\brief -- Клик;
	*/
	void BAO_VClick();

	/**
	*	\brief -- Клик;
	*/
	void BKOIClick();

	/**
	*	\brief -- Клик;
	*/
	void BRTK_BAUClick();

	/**
	*	\brief -- Клик;
	*/
	void BRTK_BALOIClick();

	/**
	*	\brief -- Клик;
	*/
	void BAO_SHPKClick();

	/**
	*	\brief -- Клик;
	*/
	void BAO_UPKClick();

	/**
	*	\brief -- Клик;
	*/
	void BAO_OSClick();

	/**
	*	\brief -- Клик;
	*/
	void BAO_SOTRClick();

	/**
	*	\brief -- Клик;
	*/
	void BRTK_AFARClick();

	/**
	*	\brief -- Клик;
	*/
	void BRTK_AFS1Click();

	/**
	*	\brief -- Клик
	*/
	void BRTK_AFS2Click();

	/**
	*	\brief -- Клик;
	*/
	void BRTK_AOSClick();

	/**
	*	\brief -- Клик;
	*/
	void SOTR_MCAClick();
	
	/**
	*	\brief -- Клик
	*/
	void UKPClick();

	/**
	*	\brief -- Клик;
	*/
	void CBKClick();

	/**
	*	\brief -- Клик;
	*/
	void timeMCAClick();

	/**
	*	\brief -- Клик;
	*/
	void timeAIKClick();
	
};
#endif