#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QScriptEngine>
#include "myxmlreader.h"

class Functions : public QObject
{
	Q_OBJECT
public:
	/**
	*	Список параметров проверки: 
	*	UNDEF - неиспользуемые переменные
	*	UNUSED - необъявленные переменные 
	*/
	enum CheckOptions { UNDEF = 1, UNUSED = 2, SEMANTIC = 4 };
	Functions(QObject* parent = 0) : QObject(parent)
	{
		eng_names.insert("string", "строка");
		eng_names.insert("return", "запись");
		eng_names.insert("int", "целое");
		eng_names.insert("double", "вещественное");
		eng_names.insert("arr_begin", "массив_откр");
		eng_names.insert("arr_end", "массив_закр");
	}
	/**
	*	\brief Проверка интерпретируемой программы на языке JS на ошибки
	*	\param fileName - имя файла
	*	\param flags - флаги параметров проверки
	*	\return Результат проверки
	*/
	QStringList checkFile(const QString& fileName, int flags = 0, 
							const QList<const Operator*>& op_list = QList<const Operator*>());
	/**
	*	\brief Проверка операторов и параметров на ошибки
	*	\param fileName - имя файла
	*	\param errorList - ошибки
	*	\return Результат проверки
	*/
	QStringList checkSemantic(const QString& fileName, QScriptEngine* eng, 
		const QList<const Operator*>& op_list, QStringList& errorList);
	/**
	*	\brief Проверка параметров массива
	*	\param vec - вектор с массивом
	*	\param iteratorVec - указатель на массив
	*	\param SymbolTable - указатель на таблицу символов
	*	\param errorList - ошибки
	*	\param err - флаг наличия ошибки
	*	\param CountRow - номер строки
	*	\param name - имя оператора
	*	\param etalon - эталон аргументов
	*	\return количество литералов. Нужно для смещения в векторе
	*/
	int checkMas(std::vector<std::wstring> vec, int iteratorVec, 
		QStringList& errorList, bool& err, int CountRow, QString name, const Param* etalon);
	/**
	*	\brief Проверка элемента(массива) в массиве-массивов. Так же подходит для проверки параметров обчного массива.
	*	\param vec - вектора с оператором
	*	\param iteratorVec - указатель вектора с оператором
	*	\param SymbolTable - указатель на таблицу символов
	*	\param ParamNum - номер неверного параметра
	*	\param errorList - ошибки
	*	\param CountRow - номер строки
	*	\param name - имя оператора
	*	\param etalon - эталон аргументов
	*	\return смещённый указатель вектора с оператором
	*/
	int checkMasInMas(std::vector<std::wstring>& vec, int iteratorVec, 
		int ParamNum, QStringList& errorList, int CountRow, QString name, const Param* etalon);
	/**
	*	\brief Проверка аргументов оператора на граничные значения
	*	\param ParamType - тип аргумента
	*	\param ParamVal - значение аргумента
	*	\param etalon - эталон аргумента
	*	\return Результат проверки
	*/
	bool limitsValue(QString ParamType, QString ParamVal, const Param* etalon);
		/**
	*	\brief Обрезание "this." у идентификатора
	*	\param str - идентификатор
	*	\return Результат проверки
	*/
	QString noThis(QString str);
	/**
	*	\brief Чтение файла
	*	\param fileName - имя файла
	*	\return Содержимое файла
	*/
	static QString readFile(const QString& fileName);
	/**
	*	\brief Вывод упращенного диалога с алтернативами "Да","Нет"
	*	\param title - титульная строка
	*	\param text - текст сообщения
	*	\return Результат выбора
	*/
	static bool showSimpleMsg(QString title, QString text, const QFont& font);
	/**
	*	\brief Возвращает полный путь
	*	\param path - исходный путь
	*/
	static QString fullPath(const QString& path);
		/**
	*	\brief Ищет включения других файлов и исполняет их
	*	\param fileName - имя файла
	*	\param cur_dir - текущая директория
	*	\param files - список найденных на текущий момент файлов
	*	\param errors - список ошибок
	*	\param eng - указатель на исполнитель(по умолчанию 0), если задан, 
					файлы не только будут проверяться, но еще и исполняться
	*/
	bool fullFileCheck(const QString& fileName, const QString& cur_dir, QStringList& files, 
						QMap<QString, QStringList>& errors_map, const QList<const Operator*>& op_list,
						QScriptEngine* eng = 0, int flags = 0);
signals:
	/**
	*	\brief Сигнал выдачи сообщения в жжурнал
	*	\param msg - сообщение
	*/
	void toLog(QString msg);
	/**
	*	\brief Сигнал отображения диалогового окна ожидания
	*	\param msg - сообщение
	*/
	void showWaitMsg(QString msg);
	/**
	*	\brief Сигнал закрытия диалогового окна ожидания
	*/
	void hideWaitMsg();
private:
	QMap <QString, QString> eng_names;
};

#endif //FUNCTIONS_H