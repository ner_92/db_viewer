#define BOOST_SPIRIT_DEBUG
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_statement.hpp>
#include <boost/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/spirit/repository/include/qi_confix.hpp>
#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <iostream>
#include <string>
#include <vector>
#include <QDebug>

namespace fusion = boost::fusion;
namespace phoenix = boost::phoenix;
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;



  

//Не ходите сюда

template <typename Iterator>
struct skip_grammar : public qi::grammar<Iterator>
{
	skip_grammar()
		: skip_grammar::base_type(skip)
	{
			skip = boost::spirit::qi::space |//; //| boost::spirit::repository::confix("/*", "*/")[*(qi::char_ - "*/")]

				//| boost::spirit::repository::confix("//", qi::eol)[*(qi::char_ - qi::eol)];
				qi::omit[ ("//" > *(qi::char_ - qi::eol) > qi::eol)];

	}

	qi::rule<Iterator> skip;
};

template<typename Iterator, typename skip_grammar>
struct sparta_grammar:qi::grammar<Iterator, std::vector<std::wstring>(), skip_grammar>
{
	sparta_grammar():sparta_grammar::base_type(commands, "sspg")
		{
			using qi::lit;
			using qi::lexeme;
			using qi::on_error;
			using qi::fail;
			using qi::omit;
			using ascii::char_;
			using ascii::string;
			using phoenix::construct;
			using phoenix::val;
			using phoenix::push_back;
			//commands = id;
			//id = *( lexeme[ (qi::standard_wide::alpha) >> *( qi::standard_wide::alnum | qi::lit( '_' ) ) ] );
		// /*
			       
// 			commands	%= (  ( paramVar[push_back(qi::_val, L"VAR")] > id[push_back(qi::_val, L"identificator")] > -( paramEq[push_back(qi::_val, L"EQ")] > params ) > ';' > -(*qi::omit[qi::standard_wide::char_]) )  // ПЕРЕМЕННАЯ var
// 								| ( ( id[push_back(qi::_val, L"identificator")] > '(' > -params > ')' ) > lit(';') > -( *qi::omit[qi::standard_wide::char_] ) )	);	//ОПЕРАТОР
			
			commands %= (commOp[push_back(qi::_val, L"commOp")] > -(*qi::omit[qi::standard_wide::char_ - "*/"]) > -commClos[push_back(qi::_val, L"commClos")][push_back(qi::_val, L"*/")]) 
						//| (-(*qi::omit[qi::standard_wide::char_ - "*/"]) > commClos[push_back(qi::_val, L"commClos")][push_back(qi::_val, L"*/")])
						| (commClos[push_back(qi::_val, L"commClos")][push_back(qi::_val, L"*/")])
						| ("//" > -(*qi::omit[qi::standard_wide::char_]))
						| (oblOp[push_back(qi::_val, L"oblOp")] > -(*qi::omit[qi::standard_wide::char_] ))
						| (oblEnd[push_back(qi::_val, L"oblEnd")] > -(*qi::omit[qi::standard_wide::char_])) 
						| (-paramVar[push_back(qi::_val, L"VAR")][push_back(qi::_val, qi::_1)] > id[push_back(qi::_val, L"identificator")] > -(-(paramEq[push_back(qi::_val, L"EQ")][push_back(qi::_val, qi::_1)] | qi::lit('(')) > -params > -qi::lit('(') > -params > -qi::lit('(') > -qi::lit(')')) > (qi::lit(';') | (commClos[push_back(qi::_val, L"commClos")][push_back(qi::_val, L"*/")])) >-(*qi::omit[qi::standard_wide::char_]));
							  
			params	= (																										
								( 
								  paramStr	[push_back(qi::_val, L"string")]		[push_back(qi::_val, qi::_1)]
								| ( id		[push_back(qi::_val, L"return")]		[push_back(qi::_val, qi::_1)] > -( MasOp /*[push_back(qi::_val, L"эл_массив_откр")] [push_back(qi::_val, qi::_1)]*/ > paramInt /*[push_back(qi::_val, L"целое")] [push_back(qi::_val, qi::_1)]*/ >MasClos/*[push_back(qi::_val, L"эл_массив_закр")]	[push_back(qi::_val, qi::_1)]*/))
								| paramHex	[push_back(qi::_val, L"int")]			[push_back(qi::_val, qi::_1)]
								| paramInt	[push_back(qi::_val, L"int")]			[push_back(qi::_val, qi::_1)]
								| paramDoub	[push_back(qi::_val, L"double")]	[push_back(qi::_val, qi::_1)]
								|	*( MasOp		[push_back(qi::_val, L"arr_begin")]	[push_back(qi::_val, qi::_1)] > -MasOp [push_back(qi::_val, L"arr_begin")]	[push_back(qi::_val, qi::_1)]
									> *(( paramStr	[push_back(qi::_val, L"string")]			[push_back(qi::_val, qi::_1)]
										| id			[push_back(qi::_val, L"return")]			[push_back(qi::_val, qi::_1)]
										| paramHex	[push_back(qi::_val, L"int")]			[push_back(qi::_val, qi::_1)]
										| paramInt	[push_back(qi::_val, L"int")]			[push_back(qi::_val, qi::_1)]
										| paramDoub	[push_back(qi::_val, L"double")]	[push_back(qi::_val, qi::_1)]
										) % qi::lit(','))
										> MasClos	[push_back(qi::_val, L"arr_end")]	[push_back(qi::_val, qi::_1)] > -MasClos [push_back(qi::_val, L"arr_end")]	[push_back(qi::_val, qi::_1)]
									) 
								) % qi::lit(',')
							);

			paramMas	= ( qi::lit('[') > paramHex > qi::lit(']') );
			paramStr	= (qi::lit('"') > *(qi::standard_wide::char_ - "\"") > qi::lit('"'));
			paramInt	= ( lexeme[ (+qi::standard_wide::digit >> !qi::double_) ] );
			paramDoub	= ( lexeme[ qi::raw[ +(qi::double_ ) ] ] );
			paramHex	= ( lexeme[ (qi::standard_wide::string("0x") | qi::standard_wide::string("0X")) > +qi::standard_wide::char_("a-fA-F0-9") ] );
			id			= ( lexeme[ (qi::standard_wide::alpha | qi::standard_wide::char_( '_' )) > *( qi::standard_wide::alnum | qi::standard_wide::char_( '_' ) | qi::standard_wide::char_( '.' ) ) ] ); 
			MasOp		= qi::standard_wide::char_('[');
			MasClos		= qi::standard_wide::char_(']');
			paramVar	= ( qi::standard_wide::string("var") | qi::standard_wide::string("VAR") );
			paramEq		= ( qi::standard_wide::char_('=') );
			oblOp		= ( qi::lit('{') );
			oblEnd		= ( qi::standard_wide::char_('}') );
			commOp		= ( qi::standard_wide::string("/*") );
			commClos	= ( qi::standard_wide::string("*/") );

			
			commands.name	("Commands");
			params.name		("Params");
			paramStr.name	("Str");
			paramInt.name	("Int");
			paramDoub.name	("Doub");
			paramHex.name	("Hex");
			paramVar.name	("Var");
			paramMas.name	("Mas");
			id.name			("Id");

			on_error<fail>
			(
				commands,
					std::cout << val("Error! Expected ")
					<< qi::_4
					<< val(" here: \"")
					<< construct<std::string>(qi::_3, qi::_2)
					<< val("\"")
					<< std::endl
			);
			BOOST_SPIRIT_DEBUG_NODE(commands);
			BOOST_SPIRIT_DEBUG_NODE(params);
			BOOST_SPIRIT_DEBUG_NODE(id);
			BOOST_SPIRIT_DEBUG_NODE(MasOp);
			BOOST_SPIRIT_DEBUG_NODE(paramInt);
			BOOST_SPIRIT_DEBUG_NODE(MasClos);

		}
	
/*
    qi::rule<Iterator, std::vector<std::string>(), qi::locals<std::string>, ascii::space_type> root;
    qi::rule<Iterator, std::vector<std::string>(), ascii::space_type> names_and_types, name_and_type, names,
        create, values, insert, select, where, conditions, condition, delete_, names_and_values, name_and_value,
        update, alter_add, alter_drop, alter_alter, alter, truncate, drop, desc, show, quit, help;
    qi::rule<Iterator, std::string(), ascii::space_type> table_name_for_create, type, name, not_null_spec, value,
        oper, cond;
 
    std::vector<std::string> tokens;
*/

	 std::vector<std::wstring> vec_test_str;

	 qi::rule<Iterator, std::vector<std::wstring>(), skip_grammar> commands, params, paramMas;
	 qi::rule<Iterator, std::wstring(), skip_grammar> id, paramStr, paramInt, paramDoub, paramHex, paramVar, MasOp, MasClos, paramEq, oblOp, oblEnd, commOp, commClos;
	
	 //std::vector<std::pair<std::string, std::vector<std::string>>> vec_command; // Вектор пар идентификатор + пара тип и значение
	 

};

