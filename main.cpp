#include <QTextCodec>
#include <QtWidgets/QApplication>
#include <QtCore/QCoreApplication>
//#include "core/OperatorsController.h"
#include "db_viewer.h"


#ifdef WIN32

int __stdcall WinMain(HINSTANCE, HINSTANCE, LPSTR cmdLine, int)
{
	int argc = __argc;
	char** argv = __argv;

#else
int main(int argc, char **argv)
{
#endif
	try
	{
		//mongo::client::GlobalInstance glob_inst;
		QApplication app(argc, argv);

		QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
		SMongoSingleton_v3::Instance();
		//instr::connectUsersDB();

		//if (argc == 1)
		//{
		//	PasswordWindow pass_window("Панель управления СУБД");
		//	if (pass_window.exec() == QDialog::Rejected)
		//		return 0;
		//}
		//else
		//{
		//	instr::setCurrentUser(instr::getUsersCtrl()->getUserById(QString(argv[1]).toInt()));
		//}

		DBView db_view;
		QFont newFont("Consolas", 13/*, QFont::Bold*/);
	//	bool ok;
	//	QFont newFont = QFontDialog::getFont(&ok);
		//newFont.setPointSize(13);
		QApplication::setFont(newFont);

		db_view.showMaximized();
		db_view.setWindowTitle("Панель управления СУБД - Менеджер поиска");
		return app.exec();

	}
	catch(const std::exception& err)
	{
	}
	catch(...)
	{
	}
	return 0;
}
