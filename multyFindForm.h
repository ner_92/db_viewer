#ifndef MULTYFINDFORM_H
#define MULTYFINDFORM_H
#include <QtWidgets>
#include <QTextEdit>
#include <QPushButton>
#include <QHBoxLayout>

/**
*	\class multyFindForm
*	\brief -- класс multyFindForm;
*/
class multyFindForm : public QWidget
{
	Q_OBJECT

public:
	
	/**
	*	\brief -- Конструктор;
	*	\param parent -- Родитель.
	*/
	multyFindForm(QWidget *parent = 0);
	
	/**
	*	\brief -- Деструктор;
	*/
	~multyFindForm();

	/**
	*	\brief -- Скрыть кнопку удаления;
	*/
	void hideDelButton();

	/**
	*	\brief -- Скрыть кнопку добавления;
	*/
	void hideAddButton();

	/**
	*	\brief -- Отобразить кнопку добавления;
	*/
	void showAddButton();

	/**
	*	\brief -- Удалить форму;
	*/
	void delForm();

	/**
	*	\brief -- Вывести диалог;
	*/
	void showDialog();
	
	/// -- индекс в QList;
	int index;

	/**
	*	\brief -- Фильтры в поле;
	*/
	void setTextToEdit(QString str);
private:
	/// -- Виджет;
	QWidget *widg;
	/// -- Поле поиска;
	QTextEdit *findEdit;
	/// -- Кнопка настроек фильтра;
	QPushButton *setButton;
	/// -- Кнопка добавления;
	QPushButton *addButton;
	/// -- Кнопка удаления;
	QPushButton *delButton;
	/// -- Компановка;
	QHBoxLayout *hLayout;
	
signals:
	
	/**
	*	\brief -- Сигнал доавления новой формы;
	*/
	void addNewForm();

	/**
	*	\brief -- Сигнал удаления формы;
	*	\param curIndex -- индекс.
	*/
	void delFormSignal(int curIndex);

	/**
	*	\brief -- Сигнал вывода диалога;
	*	\param curIndex -- индекс.
	*/
	void showDialogSignal(int curIndex);
public slots:


};

#endif // MULTYFINDFORM_H