#include "functions.h"
#include <qfile.h>
#include <qpushbutton.h>
#include <qmessagebox.h>
#include "Grammar.h"
#include <QTextCodec>
#include <qcoreapplication.h>
#include <qfileinfo.h>

QStringList Functions::checkFile(const QString& fileName, int flags, 
										const QList<const Operator*>& op_list)
{
	try
	{
		QScriptEngine engine;
		engine.setProcessEventsInterval(10);
		/*QScriptEngineDebugger deb;
		deb.attachTo(&engine);
		deb.action(QScriptEngineDebugger::InterruptAction)->trigger();*/
		QFile file(fileName);

		if (!file.open(QIODevice::ReadOnly))
		{
			return (QStringList()<< "Ошибка: невозможно открыть файл.");
		}
		QTextStream in(&file);
		in.setCodec("utf-8");

		QFile v_file(":/jslint.js");
		//QFile v_file(QCoreApplication::applicationDirPath()+ "/jslint.js");
		if(!v_file.open(QIODevice::ReadOnly))
		{
			QTextCodec* codec = QTextCodec::codecForName("CP1251");
			QMessageBox::critical(0, "Ошибка", "Проверка синтаксиса невозможна. "\
							"Отсутствует файл проверки. Убедитесь в наличии папки "\
							"JSLint в папке с клиентом");
			return (QStringList() << "Проверка синтаксиса невозможна. Отсутствует файл проверки");
		}
		QTextStream v_in(&v_file);
		v_in.setCodec("utf-8");
		engine.evaluate(v_in.readAll(), v_file.fileName());
		QString extra_options = "";
		if(!(flags & UNDEF))
			extra_options += "undef: true, ";
		if(!(flags & UNUSED))
			extra_options += "unparam: true, ";

		QString options = QString("{%1devel: false, \"continue\": true, forin: true, "\
			"bitwise: true, regexp: true, confusion: true, node: false,  debug: true, "\
			"sloppy: true, eqeq: true, sub: true, vars: true, evil: true, white: true, "\
			"passfail: true, newcap: true, nomen: true, on: true, plusplus: true}")
																		.arg(extra_options);
		engine.globalObject().setProperty("my_program",in.readAll());
		QString program = QString("JSLINT(my_program, %1);\n JSLINT.report(true)"\
								".replace(/(<([^>]+)>)/ig,\"\");").arg(options);

		QScriptValue val = engine.evaluate(program).toString();
		QStringList errors;
		errors = val.toString().split("Problem at line ");

		//Если синтаксических ошибок нет, то можно проверять семантику операторов
		if ((errors.first() != "Error:") && (flags & SEMANTIC))	
			checkSemantic(fileName, &engine, op_list, errors);

		errors.removeFirst();
		v_file.close();
		file.close();
		return errors;
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}

	emit hideWaitMsg();

	return QStringList();
}

QStringList Functions::checkSemantic(const QString& fileName, QScriptEngine* eng, 
							const QList<const Operator*>& op_list, QStringList& errorList)
{
	try
	{
		emit showWaitMsg("Завершение проверки файла \n" + fileName);




		//QList<ComDataForEval> tmp_arr = XMLReader::Instance().getCommands();
		QFile file(fileName);
		if (!file.open(QIODevice::ReadOnly))
			return errorList;

		QTextStream in(&file);
		in.setCodec("utf-8");

		int CountRow = 0;
		int ind;

		bool comments = false;
		while (!in.atEnd())
		{
			++CountRow;
			qDebug() << CountRow;
			QString qComStr = in.readLine();
			// boost spirit давится
			qComStr.remove("«");
			qComStr.remove("»");
			qComStr.remove("µ");
			qComStr.remove("§");
			qComStr.remove("©");
			qComStr.remove("®");
			//
			QString name2;
			bool for_lish_paran = false;
			std::wstring wComStr = qComStr.toStdWString();
			auto wbegin = wComStr.begin();
			auto wend = wComStr.end();
			int iteratorVec = 0;
			std::vector<std::wstring> vec;
			bool err = false;
			//qi::rule<decltype(wbegin)> Skipper = boost::spirit::qi::space | boost::spirit::repository::confix("/*", "*/")[*(qi::char_ - "*/")] | boost::spirit::repository::confix("//", qi::eol)[*(qi::char_ - qi::eol)];
			//sparta_grammar<decltype(wbegin), decltype(Skipper)> spar_gram;
			
			skip_grammar<decltype(wbegin)> Skipper;
			sparta_grammar<decltype(wbegin), decltype(Skipper)> spar_gram;
			bool r = phrase_parse(wbegin, wend, spar_gram, Skipper, vec);
			if (r && wbegin == wend)
			{
				if (vec.size() == 0)
					continue;

				if (QString::fromStdWString(vec[0]) == "commOp")
				{
					comments = true;
					if (iteratorVec + 2 < vec.size())
						if (QString::fromStdWString(vec[iteratorVec+2]) == "commClos")
								comments = false;
					continue;
				}

				if (QString::fromStdWString(vec[vec.size() - 2]) == "commClos")
				{
					comments = false;
					continue;
				}

				if (QString::fromStdWString(vec[0]) == "commClos")
				{
					comments = false;
					continue;
				}

				if (comments)	// если закомментированная строка
					continue;	// пропускаем

				if (QString::fromStdWString(vec[0]) == "oblOp")
				{
				
				}

				if (QString::fromStdWString(vec[0]) == "oblEnd")
				{
				
				}
				if (QString::fromStdWString(vec[0]) == "VAR") // ОПЕРАТОР(); проверить такой вариант, перед тем как "переменная =" т.к. возможен выход за пределы вектора vec
				{

					iteratorVec = iteratorVec + 2;		// Указываем на идентификатор
					if ((iteratorVec + 2) >= vec.size())	//если просто объявление
					{
						continue;
					}
					if (QString::fromStdWString(vec[iteratorVec + 2]) == "EQ")	// Если объявление с инициализацией
					{
						continue;
					}

				}		//if var
				QString name = noThis(QString::fromStdWString(vec[1]));
				name2 = name;
				if (iteratorVec + 2 < vec.size())
				if (QString::fromStdWString(vec[2]) == "EQ")	//Если присвоение идентификатору
				{
					continue;
				}	// if присвоение идентификатору

				/*foreach(const Operator* op, op_list)
				{
					if(op->getName() == name)
				}*/
				auto itr = std::find_if(op_list.begin(), op_list.end(), 
							[name](const Operator* op){return op->getName() == name;});

				// ВЫВЕСТИ - исключение, не проверяем.
				if ((name == "ВЫВЕСТИ")	 || (itr == op_list.end()))
					continue;
	
					
				auto oper = *itr;
				const QList<const Param*> params = oper->getParamList();
				for_lish_paran = true;
				if (params.isEmpty())
				{
					if (vec.size() > 2)	// если в оператор без параментр передаётся что либо
					{
						err = true;
						errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2 НЕ ПРИНИМАЕТ ПАРАМЕТРОВ").arg(CountRow).arg(name));
						continue;
					}
				}
				else
				{
					iteratorVec = 2; //указываем на третий элемент вектора(тип первого параметра)
					for (int i = 0, end = params.count(); i < end; ++i)	//проверяем параметры оператора
					{
						err = false;
						if (params.value(i)->type_name == "массив")
						{
							if (iteratorVec >= vec.size())
							{
								err = true;
								errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. %3-Й ПАРАМЕТР ОЖИДАЕТСЯ %5").arg(CountRow).arg(name).arg(iteratorVec / 2).arg(params.value(i)->type_name));
								break;
							}
							if (QString::fromStdWString(vec[iteratorVec]) == "arr_begin")
							{
								iteratorVec = checkMas(vec, iteratorVec, errorList, err, CountRow, name, params.value(i));
								if (err)
									break;
								//iteratorVec = iteratorVec + 2;
								continue;
							}
							if (QString::fromStdWString(vec[iteratorVec]) == "return")
							{
								iteratorVec += 2;
								continue;
							}
							if (QString::fromStdWString(vec[iteratorVec]) != "arr_begin" && QString::fromStdWString(vec[iteratorVec]) != "return")
							{
								err = true;
								errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. %3-Й "\
									"ПАРАМЕТР %4. ОЖИДАЕТСЯ %5")
									.arg(CountRow)
									.arg(name)
									.arg(iteratorVec / 2)
									.arg(QString::fromStdWString(vec[iteratorVec]))
									.arg(params.value(i)->type_name));
								break;
							}

						}
						else //if массив
						{
							if (err)
								break;
							if (iteratorVec >= vec.size()) //если токены закончились раньше, чем параметры оператора
							{
								if (params.value(i)->def_value.isNull())	// если параметры по умолчанию - всё ок, заканчиваем
									break;
								errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. ОЖИДАЕТСЯ %3-Й ПАРАМЕТР: %4").arg(CountRow).arg(name).arg(i + 1).arg(params.value(i)->type_name));
								break;
							}
							if (eng_names[QString::fromStdWString(vec[iteratorVec])] != params.value(i)->type_name)
							{
								if (QString::fromStdWString(vec[iteratorVec]) == "return")		//Если переменная
								{
									iteratorVec = iteratorVec + 2;
									continue;
								}		// if запись

								if (QString::fromStdWString(vec[iteratorVec]) == "string" &&  params.value(i)->type_name.section("_", 0, 0/*, QString::SectionIncludeTrailingSep*/) == "запись")	//так же для остальных типов
								{
									// Если пришла "строка", а ждём запись - ок
									iteratorVec = iteratorVec + 2;
									continue;
								}

								if (QString::fromStdWString(vec[iteratorVec]) == "int" && params.value(i)->type_name == "вещественное")
								{
									if (!limitsValue(QString::fromStdWString(vec[iteratorVec]), QString::fromStdWString(vec[iteratorVec + 1]), params.value(i)))	//проверка на граничные значения
									{
										errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. %3-Й АРГУМЕНТ НЕ СООТВЕТСТВУЕТ ДОПУСТИМЫМ ЗНАЧЕНИЯМ").arg(CountRow).arg(name).arg(iteratorVec / 2));
										break;
									}
									iteratorVec = iteratorVec + 2;
									continue;
								}
								if (QString::fromStdWString(vec[iteratorVec]) == "arr_begin")	//Фикс вывода ошибки
									errorList.append(QString("%1 СТРОКА : НЕСООТВЕТСТВИЕ ПАРАМЕТРОВ ОПЕРАТОРА: %2. %3-Й ПАРАМЕТР %4. ОЖИДАЕТСЯ %5").arg(CountRow).arg(name).arg(iteratorVec / 2).arg("массив").arg(params.value(i)->type_name));
								else
									if (QString::fromStdWString(vec[iteratorVec]) != "arr_end")	// Фикс масств_закр перед дефолтным значением
										errorList.append(QString("%1 СТРОКА : НЕСООТВЕТСТВИЕ "\
										"ПАРАМЕТРОВ ОПЕРАТОРА : %2. %3-Й ПАРАМЕТР %4. "\
										"ОЖИДАЕТСЯ %5")
											.arg(CountRow)
											.arg(name)
											.arg(iteratorVec / 2)
											.arg(QString::fromStdWString(vec[iteratorVec]))
											.arg(params.value(i)->type_name));

								iteratorVec = iteratorVec + 2;
								break;
							}


							if (!limitsValue(QString::fromStdWString(vec[iteratorVec]),
									QString::fromStdWString(vec[iteratorVec + 1]), 
											params.value(i)))	//проверка на граничные значения
							{
								errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. %3-Й АРГУМЕНТ НЕ СООТВЕТСТВУЕТ ДОПУСТИМЫМ ЗНАЧЕНИЯМ").arg(CountRow).arg(name).arg(iteratorVec / 2));
								break;
							}
							iteratorVec = iteratorVec + 2;

						}//if массив else
					}//for params.count
					//if ((iteratorVec + 2 < vec.size()) && (for_lish_paran))
					//{
					//	errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2 НЕ ПРИНИМАЕТ СТОЛЬКО ПАРАМЕТРОВ").arg(CountRow).arg(name2));
					//}
				}// if !params.isEmpty()
			}
		}
		file.close();
		return errorList;
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}
	emit hideWaitMsg();
	return QStringList();
}

int Functions::checkMas(std::vector<std::wstring> vec, int iteratorVec, QStringList& errorList, bool& err, int CountRow, QString name, const Param* etalon)
{
	int ParamNum = 0;
	if ( QString::fromStdWString(vec[iteratorVec]) != "arr_begin" )
	{
		if  (  ( QString::fromStdWString(vec[iteratorVec]) == "arr_end" ) )		// Массив массивов закрылся,
			iteratorVec = iteratorVec + 2;	// смещаем если дальше что-то есть
		else
		{
			err = true;
			errorList.append(QString("%1 СТРОКА :  НЕСООТВЕТСТВИЕ ПАРАМЕТРОВ МАССИВА В "\
			"ОПЕРАТОРЕ: %2. ОЖИДАЕТСЯ: %3")
						.arg(CountRow).arg(name).arg(etalon->type_name));
			return 0;
		}
	}
	iteratorVec = iteratorVec + 2; // переврыгиваем открытие массива
	if ( QString::fromStdWString(vec[iteratorVec]) == "arr_begin" )	//если массив массивов
	{
		iteratorVec = iteratorVec + 2;	// переврыгиваем открытие массива-аргумента
		while ( QString::fromStdWString(vec[iteratorVec]) != "arr_end" )	//пока не закрылся массив массивов
		{
			if ( QString::fromStdWString(vec[iteratorVec]) == "arr_begin" )	// Если открытие массива-аргумента
				iteratorVec = iteratorVec + 2;	// перепрыгиваем

			iteratorVec = checkMasInMas(vec, iteratorVec, ParamNum, errorList, CountRow, name, etalon);		// проверка подмассива
			if ( iteratorVec == -1 )
			{
				err = true;
				break;
			}
			iteratorVec = iteratorVec + 2;	// Смещаем с закрытия массива-аргумента, либо на закрытие массива-массивов, либо на следующий массив-аргумент
		}
		if  ( iteratorVec + 2 < vec.size() )		// Массив массивов закрылся,
			iteratorVec = iteratorVec + 2;	// смещаем если дальше что-то есть
	}
	else	//если просто массив
	{
		iteratorVec = checkMasInMas(vec, iteratorVec, ParamNum, errorList, CountRow, name, etalon);		// проверка массива
		if ( iteratorVec == -1 )
			err = true;
		if  ( iteratorVec + 2 < vec.size() )		// Массив массивов закрылся,
			iteratorVec = iteratorVec + 2;	// смещаем если дальше что-то есть
	}
	return iteratorVec;
}

int Functions::checkMasInMas(std::vector<std::wstring>& vec, int iteratorVec, int ParamNum, QStringList& errorList, int CountRow, QString name, const Param* etalon)
{
	while ( QString::fromStdWString(vec[iteratorVec]) != "arr_end" )	//	Пока не закрылся массив
	{
		++ParamNum;
		int ind;
		for (int j = 0, jend = etalon->children.count(); j < jend; ++j)	//проверяем параметры массива
		{
			if (eng_names[QString::fromStdWString(vec[iteratorVec])] != etalon->children.value(j)->type_name) // если параметры не совпадают
			{
				bool err = true;
				if ((QString::fromStdWString(vec[iteratorVec]) == "return" )) // для переменных можно с любым типом
				{
					iteratorVec = iteratorVec + 2;
					continue;
				}//if запись
				if (QString::fromStdWString(vec[iteratorVec]) == "int" && etalon->children.value(j)->type_name == "вещественное")	// для целых можно с вещественным
				{
					if (!limitsValue(QString::fromStdWString(vec[iteratorVec]), QString::fromStdWString(vec[iteratorVec + 1]), etalon->children.value(j)))	//проверка на граничные значения
					{
						errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. %3 - НЕ СООТВЕТСТВУЕТ ДОПУСТИМЫМ ЗНАЧЕНИЯМ. %4-Й ПАРАМЕТР В МАССИВЕ").arg(CountRow).arg(name).arg( QString::fromStdWString(vec[iteratorVec+1])).arg(j+1));
						return -1;
					}
					iteratorVec = iteratorVec + 2;
					continue;
				}
				errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. %3 %4 - НЕДОПУСТИМЫЙ %5-Й ЭЛЕМЕНТ МАССИВА. ОЖИДАЕТСЯ %6").arg(CountRow).arg(name).arg(QString::fromStdWString(vec[iteratorVec])).arg(QString::fromStdWString(vec[iteratorVec + 1])).arg(ParamNum).arg(etalon->children.value(j)->type_name));
				return -1;
				
			}//if параметры не совпадают

			if (!limitsValue(QString::fromStdWString(vec[iteratorVec]), QString::fromStdWString(vec[iteratorVec + 1]), etalon->children.value(j)))	//проверка на граничные значения
			{
				errorList.append(QString("%1 СТРОКА : ОПЕРАТОР: %2. %3 - НЕ СООТВЕТСТВУЕТ ДОПУСТИМЫМ ЗНАЧЕНИЯМ. %4-Й ЭЛЕМЕНТ МАССИВА").arg(CountRow).arg(name).arg( QString::fromStdWString(vec[iteratorVec+1])).arg(j+1));
				return -1;
			}
			iteratorVec = iteratorVec + 2;	//	На следующий токен
		}//	for matrix_params	
	}
	return iteratorVec;
}

bool Functions::limitsValue(QString ParamType, QString ParamVal, const Param* etalon)
{
	
	if ( ParamType == "int" ) 
	{
		if (!etalon->fix_values.isEmpty())
		{
			bool ok;
			uint a = ParamVal.toUInt(&ok, 0);	//если "целое" == hex
			if (ok)										//
				ParamVal = QString::number(a);	//

			if (etalon->fix_values.indexOf(ParamVal) == -1)
				return false;
		}
		else
		if ( ( etalon->leftlimit.isValid() ) && ( etalon->rightlimit.isValid() ) )
		{
			int left = etalon->leftlimit.toInt();
			int right = etalon->rightlimit.toInt();
			int val = ParamVal.toInt(0,0);

			if ( etalon->inc_l)
			{
				if ( val < left )
					return false;
			}

			if (!etalon->inc_l)
			{
				if ( val <= left )
					return false;
			}
			
			if (etalon->inc_r)
			{
				if ( val > right )
					return false;
			}

			if (!etalon->inc_r)
			{
				if ( val >= right )
					return false;
			}
		}
	}

	if ( ParamType == "double" ) 
	{
		if (!etalon->fix_values.isEmpty())
		{
			if (etalon->fix_values.indexOf(ParamVal) == -1)
				return false;
		}
		else
		if ( ( etalon->leftlimit.isValid()) && ( etalon->rightlimit.isValid()) )
		{
			double left = etalon->leftlimit.toDouble();
			double right = etalon->rightlimit.toDouble();
			double val = ParamVal.toDouble();

			if (etalon->inc_l)
			{
				if ( val < left )
					return false;
			}

			if (!etalon->inc_l)
			{
				if ( val <= left )
					return false;
			}

			if (etalon->inc_r)
			{
				if ( val > right )
					return false;
			}

			if (!etalon->inc_r)
			{
				if ( val >= right )
					return false;
			}
		}
	}

	if ( ParamType == "string" ) 
	{
		if (!etalon->fix_values.isEmpty())
		{
			if (etalon->fix_values.indexOf(ParamVal) == -1)
				return false;
		}
	}
	return true;
}

QString Functions::noThis(QString str)
{
	if (str.section(".", 0, 0, QString::SectionIncludeTrailingSep) == "this.")
	{
		str = str.section(".", 1);
	}
	return str;
}

QString Functions::readFile(const QString& fileName)
{
	try
	{
		QFile file(fileName);
		if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
			return "";

		QTextStream in(&file);
		in.setCodec("UTF-8");
		QString fileText = in.readAll();
		file.close();
		return fileText;
	}
	catch (std::exception& e)
	{}
	catch (...)
	{}
	return "";
}


bool Functions::showSimpleMsg(QString title, QString text, const QFont& font)
{
	QMessageBox msg(QMessageBox::Information, title, text);
	msg.setFont(font);
	msg.addButton(new QPushButton("Да"), QMessageBox::AcceptRole);
	msg.addButton(new QPushButton("Нет"), QMessageBox::RejectRole);
	return !msg.exec();
}

QString Functions::fullPath(const QString& path)
{
	return path.contains(':') ? path 
			: QString("%1/%2").arg(QCoreApplication::applicationDirPath()).arg(path);
}

bool Functions::fullFileCheck(const QString& fname, const QString& cur_dir, 
					QStringList& files, QMap<QString, QStringList>& errors_map, 
					const QList<const Operator*>& op_list,  QScriptEngine* eng, int flags)
{
	QString fistFile = fname;
	bool ret_val = true, is_main_file = files.isEmpty();
	try
	{
		QRegExp rx("@include[\\t\\s]+(\"?[а-яА-ЯЁёa-zA-Z0-9\\+\\=\\-_(\\s)/\\(\\.\\.)]+\\."\
					"[a-zA-Z]{1,5}\"?)\\W");

		if(!QFile::exists(fname))
		{
			emit toLog(QString("Ошибка проверки файла %1. Файл не найден").arg(fname));
			return false;
		}
		QFile cur_file(fname);
		QString program = "";
		if(cur_file.open(QFile::ReadOnly | QFile::Text))
			program = cur_file.readAll();

		int pos = 0;
		while ((pos = rx.indexIn(program, pos)) != -1)
		{
			QString format = rx.cap(0);
			QString f_name = rx.cap(1);
			if(f_name.contains(QRegExp("^[\"|\']")))
			{
				QString tmp_fname = QString("%1/%2").arg(cur_dir).arg(f_name).replace(QRegExp("[\"\']"), "");
				QString fileName = QFileInfo(tmp_fname).canonicalFilePath();
				if(!QFile::exists(fileName))
				{
					emit toLog(QString("Ошибка добавления описаний из файла %1."\
																	" Файл не найден")
																	.arg(tmp_fname));
					ret_val = false;
				}
				if(!files.contains(fileName))
				{
					files.push_back(fileName);
					ret_val &= fullFileCheck(fileName, fileName.left(fileName.lastIndexOf('/')), files, errors_map, op_list, eng, flags);
				}
			}
			else
			{
				emit toLog(QString("Ошибка добавления описаний из файла %1. "\
							"Неправильный формат добавления. Ожидается @include "\
							"имя файла\"")
						.arg(format)); 
				ret_val = false;
			}
			pos += rx.matchedLength();
		}

		QStringList err_list = Functions::checkFile(fname, flags, op_list);
		if(err_list.count()!= 0)
		{
			emit toLog(QString("Невозможно выполнение файла %1"\
								"\nСинтаксические ошибки:").arg(fname));
			
			errors_map.insert(fname, err_list);
			ret_val = false;
		}
		else if(eng)
		{
			if(is_main_file)
				program = QString("try{\n%1\n}\ncatch(e)\n{\n if(e!=='exit')\n  "\
														"throw e;\n}").arg(program);

			eng->evaluate(program, fname);
		}
	}
	catch(...)
	{
		emit toLog("Отладочная информация: неизвестное в "\
								"Functions::fullFileCheck");
		ret_val = false;
		emit hideWaitMsg();
	}
	//	if (ret_val)
	// checkThisFile(firstFile);	// Семантическая проверка файла
	return ret_val;
}
