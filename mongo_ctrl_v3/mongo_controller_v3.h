#ifndef MONGO_CONTROLLERV3_H
#define MONGO_CONTROLLERV3_H
#ifdef WIN32
#include <winsock2.h>
#endif
#pragma warning(push)
#pragma warning(disable:4351)
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/types.hpp>
#pragma warning(pop)
#include <qstring.h>
#include <qobject.h>
#include <qmutex.h>
#include <qsettings.h>

enum AggregateCodes
{
	AGG_MATCH,
	AGG_SORT,
	AGG_GROUP
};

class MongoSingleton_v3
{
public:
	//static MongoSingleton_v3* Instance();
	//QMutex mtx;
	//static QMutex mutex;


	static MongoSingleton_v3& Instance()
	{
		static MongoSingleton_v3 inst;
		return inst;
	}
	MongoSingleton_v3(MongoSingleton_v3 const&) = delete;
	MongoSingleton_v3& operator= (MongoSingleton_v3 const&) = delete;

private:
	QMutex mutex;
	MongoSingleton_v3()
	{
		QMutexLocker lock(&mutex);
		if (glob_inst == NULL)
			glob_inst = new mongocxx::instance();
	}
	~MongoSingleton_v3();
	mongocxx::instance* glob_inst = NULL;

	//static MongoSingleton_v3* _instance;
};

typedef MongoSingleton_v3 SMongoSingleton_v3;

/**
*!\class MongoController
*	\brief Управление БД
*
*/
class MongoController_v3 : public QObject
{
	Q_OBJECT
public:
	/**
	*	\brief Конструктор класса MongoController_v3
	*	\param parent - родительский объект
	*/
	MongoController_v3(QString _settings_path, QObject* parent = 0);
	/// Деструктор класса MongoController
	~MongoController_v3();

	/**
	*	\brief Подключение к БД
	*	\param _db - имя БД
	*	\return Результат подключения
	*/
	bool connectToDB(const QString& database_name, const QString& socket = QString());
	/**
	*	\brief Вставляет данные в БД
	*	\param data - данные
	*/
	bool insertData(const bsoncxx::document::view_or_value& 	document);
	/**
	*	\brief Вставляет данные в БД
	*	\param data - данные
	*/
	bool insertData(const std::vector<bsoncxx::document::view_or_value>& data);
	/**
	*	\brief Вставляет данные в БД
	*	\param data - данные
	*/
	bool insertData(const QString& data);
	bool removeData(QString query_str, int flags = 0, const mongocxx::write_concern* wc = NULL);
	bool updateData(const bsoncxx::document::view_or_value& obj, QString query_str, int flags = 0,const mongocxx::write_concern* wc = NULL);
	/**
	*	\brief Удаление коллекции(таблицы) БД
	*	\param col_name - имя коллекции(таблицы)
	*  \return Флаг успешности создания коллекции(таблицы)
	*/
	mongocxx::collection createCollection(QString col_name);
	/**
	*	\brief Создание коллекции(таблицы) БД
	*	\param col_name - имя коллекции(таблицы)
	*  \return Флаг успешности удаления файла
	*/
	bool removeCollection(QString col_name);
	/**
	*	\brief Проверка на наличие коллекции(таблицы)
	*	\param col_name - имя коллекции
	*  \return Флаг успешности наличия коллекции(таблицы)
	*/
	bool hasCollection(const QString& col_name);
	/**
	*	\brief Установка текущую коллекцию(таблицу)
	*	\param col_name - имя коллекции(таблицы)
	*  \return Флаг успешности установки текущей коллекции(таблицы)
	*/
	bool setCurrentCollection(const QString& col_name);
	/**
	*	\brief Получение новых записей текущей коллекции(таблицы) с последнего вызова этой функции
	*	\param ret_vect - новые записи
	*	\param query_str - запрос
	*	\param chop - признак обрезки пачки сообщений
	*  \return Флаг успешности получения новых записей 
	*/
	bool getNewRecords(std::vector<QVariantMap>& ret_vect, QString query_str = "{}", int chop = 0);
	/**
	*	\brief Выдача запроса БД
	*	\param query - запрос
	*	\param ret_vect - записи по запросу
	*  \return Флаг успешности выполнения запроса
	*/
	bool query(const bsoncxx::document::view_or_value& query, std::vector<QVariantMap>& ret_vect);
	/**
	*	\brief Запрос флага состояния подключения к БД
	*  \return Флаг состояния подключения к БД
	*/
	bool isConnected() const;
	/// Запрос состояния соединения с БД
	bool isStillConnected() const;
	/**
	*	\brief Генератор имени коллекции(таблицы)
	*  \return Сгенерированное имя коллекции(таблицы)
	*/
	QString genColNameForCurDB();
	/**
	*	\brief Экспорт БД
	*	\param db_name - имя БД
	*	\param from_host - сокет сервера БД
	*	\param to - новое имя БД
	*  \return Сгенерированное имя коллекции(таблицы)
	*/
	bool copyDB(const QString& db_name, const QString& from_host, const QString& new_db_name);
	/**
	*	\brief Экспорт коллекции(таблицы)
	*	\param col_name - имя коллекции(таблицы)
	*	\param db_name - имя БД
	*	\param from_host - сокет сервера БД
	*  \return Сгенерированное имя коллекции(таблицы)
	*/
	bool copyCollection(const QString& col_name, const QString& db_name,  const QString& from_host);
	/**
	*	\brief Остановка сервера БД
	*/
	void shutdownServer();
	/**
	*	\brief Получение имён коллекций заданной БД
	*	\param db_name - имя БД
	*  \return Лист с именами коллекций заданной БД
	*/
	std::list<std::string> getCollectionList(const QString& name);
	/**
	*	\brief Получение имён БД
	*  \return Лист с именами БД
	*/
	std::list<std::string> getDatabasesList() const;
	/**
	*	\brief Возвращает путь к БД
	*  \return Путь к БД
	*/
	QString getDBPath();
	/**
	*	\brief Возвращает последний _id
	*  \return последний _id
	*/
	QString getLastId();
	/// -- Получение списка индексов;
	QStringList getIndexesList();
	/// -- Создание индекса.
	void createIndex(const bsoncxx::document::view_or_value& keys);
	/**
	*	\brief Получение n записей текущей коллекции(таблицы)
	*	\param res - результативный объект
	*	\param start_pos - начальная позиция
	*	\param n - количество записей
	*	\param dt - дата
	*	\param sort - сортировка. 1 - обычная, -1 - обратная
	*  \return Флаг успешности получения n записей
	*/
	bool get1Record(QVariantMap &res,
		const int start_pos, QString query_str = "{}", int sort = 0, QString sort_field = "_id");
protected:
	/**
	*	\brief Возвращает индентификатор последней считанной записи из БД
	*  \return Индентификатор последней считанной записи из БД
	*/
	QString lastReadID();
	/**
	*	\brief Устанавливает индентификатор последней считанной записи из БД
	*/
	void setLastID(const QString& last_id);
	/**
	*	\brief Возвращает указатель на объект-контроллер БД
	*  \return Указатель на объект-контроллер БД
	*/
	mongocxx::client* getConnectPtr() const;
	bool getAggregateRecords(std::vector<QVariantMap>& ret_vect, QList<QPair<int, QString>>);


	/**
	*	\brief Получение n записей текущей коллекции(таблицы)
	*	\param ret_vect - последние добавленные записи
	*	\param start_pos - начальная позиция
	*	\param n - количество записей
	*	\param QString query_str - строка запроса
	*  \return Флаг успешности получения n записей
	*/
	bool getNRecords(std::vector<QVariantMap>& ret_vect,
		const int start_pos, const int n, QString query_str = "{}", int sort = 1, QString sort_field = "dt");
	/**
	*	\brief Получение новых записей текущей коллекции(таблицы) с последнего вызова этой функции
	*	\param ret_vect - новые записи
	*  \return Флаг успешности получения новых записей
	*/
	int getCountRecords(QString query_str = "{}");

private:
	///Количество новых записей в текущей коллекции(таблице)
	int new_records_count;
	///Количество прочитанных из текущей коллекции(таблицы)
	quint64 cur_rec_readed;
	///Адрес(сокет) сервера БД
	std::string cur_server;
	///Индентификатор последней прочитанной записи из текущей коллекции(таблицы)
	std::string last_id;
	///Текущая коллекция(таблица)
	mongocxx::collection cur_col;
	///Путь к директории с БД
	QString db_path;
	///Имя БД
	QString db_name;
	///Объект подключения к СУБД
	mongocxx::client* client_connect;
	///Объект подключения к БД
	mongocxx::database db_connect;
	///Мьютекс
	QMutex mtx;
private:
	/**
	*	\brief Формирователь полного имени коллекции(таблицы)
	*	\param col_name - имя коллекции(таблицы)
	*  \return Полное имя коллекции(таблицы)
	*/
	std::string fullColName(const QString& col_name);
	///Отключение от БД
	void disconnectFromDB();
	QVariantMap convertObjToMap(const bsoncxx::document::view& obj);

protected:
	std::vector<QVariantMap> last_block;
	int count;
	QString QueryString;
	///Название записи в настройках для индентификатора последнего прочитанного сообщения
	static const QString last_id_key;
	///Название записи в настройках для имени коллекции на которой завершилась работа
	static const QString col_name_key;
public:
	///Вспомогательные данные (настройки)
	QSettings settings;
	///Время задержки опроса БД на наличие новых записей
	static const int read_delay;

signals:
	/**
	*	\brief Функция сигнализации об ошибках БД
	*	\param err_str - информация об ошибке
	*/
	void error(QString err_str);
};

#endif // MONGO_CONTROLLER_V3_H
