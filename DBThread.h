#ifndef DBTHREAD_H
#define DBTHREAD_H

#include <qthread.h>
#include <qmutex.h>
#include <QEventLoop>

#include "MonitorDBController.hpp"
#include "TMDBController.hpp"
#include "BVDBController.hpp"
#include "ProtocolDBController.hpp"
#include "FrameDBController.hpp"

/// -- �����, ����������� ����� ��� ����� � ��.
class DBReaderThread : public QThread
{
	Q_OBJECT
public:
	/**
	*	\brief -- �����������;
	*	\param _DB -- �������� ��;
	*	\param parent -- ������������ ������.
	*/
	DBReaderThread(QString _DB, QString _ip = "", QObject* parent = 0);
	/**
	*	\brief -- ��������� ���������� ����� ������;
	*	\return -- ��������� ���� ������.
	*/
	std::vector<QVariant> getDataBlock() const;
	/**
	*	\brief -- ��������� ���������� ����� ������ ������������� ������;
	*	\return -- ��������� ���� ������.
	*/
	std::vector<QVariant> getIntervalDataBlock() const;
	/// -- ��������� ����� �������, ��������������� ���������� �������;
	int getCount();
	/**
	*	\brief -- �������������� ��������� ������� � ������;
	*	\param list ��������� �������;
	*	\return -- ������ � ���� ������.
	*/
	QString getQueryString(QList<QMap<QString, QVariant> > *list);
	/**
	*	\brief -- ��������� ������ ���������;
	*	\return -- ������ ���������.
	*/
	std::list<std::string> getCollList();
	/**
	*	\brief -- ���������� � �������� ����������;
	*	\param collectionName -- ��� ���������;
	*	\mon_ip -- IP ������� ��.
	*/
	bool connectWithCollection(QString collectionName, QString mon_ip = "");

	/// -- �������;
	//int i = 0;
	//void runInterval();
	//void offInterval();
	/**
	*	\brief -- ��������� �������� ������� � ���� ������;
	*	\param query_string -- ������ � ���� ������.
	*/
	void setQueryString(QString query_string);
	/**
	*	\brief -- ��������� ������� � ���� ���������;
	*	\param list -- ������ � ���� �������.
	*/
	void setQueryList(QList<QMap<QString, QVariant> > list);
	/// -- ������ ������;
	QTimer* timer;
	/**
	*	\brief -- ��������� ����� ��;
	*	\param DB -- ��� ��.
	*/
	void setDB(QString DB);
	/**
	*	\brief -- ���������� �����������;
	*	\param func -- ���������� ����� ������.
	*/
	void addCallBackFunction(DBCallback& func);
	/**
	*	\brief -- ������ ������ ����� ������;
	*/
	void startRead();
	/**
	*	\brief -- �������� ����������.
	*/
	void waitForConnection();
	void setIp(QString _ip);
	std::shared_ptr<CommonDBController> get_DB_controller()
	{
		return DB_controller;
	}
protected:
	/// -- �������� ������� ������;
	void run();
	//QTimer* timer;
//private:
	/// -- ��������� �� ���������� ������� � ��;
	std::shared_ptr<CommonDBController> DB_controller;
	/// -- ������� ����� �������;
	QString query_str;
	/// -- ������� ��������� �������;
	QList<QMap<QString, QVariant> > queryList;
	/// -- ��� ��;
	QString currentDB;
	QString ip;
	/// -- ������ ������������;
	QList<DBCallback> cb_list;
	/// -- ������� ���������� ���������� � ����;
	bool connected;
	/// -- �������, ���������� �����������.
	QMutex conn_mutex;
signals:
	/**
	*	\brief -- ������ �� ������ ������ � ����;
	*	\param msg -- ��������� �� ������.
	*/
	void error(QString msg);
	/// -- ������ �� ��������� ������ ������;
	void finishedRead();
	///������ ��������� �������������
	void synchronizeFinished(int count);
	//void needCount(QString query_str = "");
	
	void needSynchronizeSignal(QList<QMap<QString, QVariant> > *queryList);
	/**
	*	\brief -- ������ �� ��������� ����� �������, ��������������� �������;
	*	\param queryList -- ������ � ���� ���������.
	*/
	void needCountNow(QList<QMap<QString, QVariant> > *queryList);
	/// -- ������ �� ��������� ���� ���������;
	void needCollectionNames();
	/**
	*	\brief -- ������ �� ��������� ����� ������ �� �������;
	*	\param queryList -- ������ � ���� ���������.
	*/
	void needIntervalDataBlock(QList<QMap<QString, QVariant> > *queryList);
	/**
	*	\brief -- ������ �� ������ ������� ������;
	*	\param ms -- �������� ��� ������� ������.
	*/
	void startIntervalSignal(int ms);
	/// --������ �� ��������� ������� ������;
	void stopIntervalSignal();
	/**
	*	\brief -- ������ ���������� � ���� �������;
	*	\param value -- �������� ����������;
	*	\param msg -- ��������� � ���� �������.
	*/
	void changeProgressMsg(int value, QString msg);
	/**
	*	\brief -- ������ �� ��������� ����� ������.
	*	\param start_pos -- ����� ������ ������;
	*	\param n -- ����� ������� ��� �������;
	*	\param queryList -- ������ � ���� ���������;
	*	\param rezh -- ����� �������;
	*	\param BV -- ������� ������������� ��������� ��.
	*/
	void needDataBlockSignal(int start_pos, int n, QList<QMap<QString, QVariant> > *queryList, int rezh, bool BV);
public slots:
	/// -- ���������� ������� ������;
	void interval();
	/**
	*	\brief -- ������ ������� ������;
	*	\param ms -- �������� ��� ������� ������.
	*/
	void runInterval(int ms = 4000);
	/// -- ��������� ������� ������;
	void offInterval();
	/**
	*	\brief -- ���������� � �������� �� �� ������ IP ������;
	*	\param ip -- ����� IP �����.
	*/
	void tryConnectWithMonIp(QString ip);
	/**
	*	\brief -- ��������� ����� ������.
	*	\param start_pos -- ����� ������ ������;
	*	\param n -- ����� ������� ��� �������;
	*	\param queryList -- ������ � ���� ���������;
	*	\param needCount -- ������� ������������� ������� ����� �������;
	*	\param rezh -- ����� �������;
	*/
	void needDataBlock(int start_pos, int n, QList<QMap<QString, QVariant> > *queryList, bool needCount, int rezh = 0);
	/**
	*	\brief -- ��������� ����� �������, ��������������� �������;
	*	\param queryList -- ������ � ���� ���������.
	*/
	void needCount(QList<QMap<QString, QVariant> > *queryList);
	void synchronize(QList<QMap<QString, QVariant> > *queryList);
};


#endif //DBTHREAD_H