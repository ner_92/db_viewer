#ifndef XMLREADER_H
#define XMLREADER_H

#include <QXmlStreamReader>
#include <QList>

/// -- Структура значения ОКов;
struct okState
{
	okState(QString _value = "", QString _descr = "") : value(_value), descr(_descr)
	{}
	/// -- Значение;
	QString value;
	/// -- Описание.
	QString descr;
};

/// -- Структура значения ОКов;
struct analiz_OK
{
	analiz_OK(int _value, QList<okState> _state = QList<okState>()) : value(_value), state(_state) {}
	/// -- Значение;
	int value = 0;
	/// -- Контейнер значений ОКов.
	QList<okState> state = QList<okState>();
};

// Структура ОКов;
struct okFormula
{
	okFormula(bool _available = false, QString _form = "", QString _ci = "", QString _cc = "", QString _ogran_poluch_min = "", QString _ogran_poluch_max = "",
	QString _ogran_res_min = "", QString _ogran_res_max = "", QList<okState> _const_poluch_list = QList<okState>(), QList<okState> _const_res_list = QList<okState>()):
	available(_available), form(_form), ci(_ci), cc(_cc), ogran_poluch_min(_ogran_poluch_min), ogran_poluch_max(_ogran_poluch_max),
	ogran_res_min(_ogran_res_min), ogran_res_max(_ogran_res_max), const_poluch_list(_const_poluch_list), const_res_list(_const_res_list)
	{}
	/// -- Наличие формулы;
	bool available = false;									
	/// -- Формула;
	QString form = "";										
	/// -- Система единиц (°С);
	QString ci = "";										
	/// -- Система счисления (десятичная);
	QString cc = "";										
	/// -- Ограничение полученного значения снизу;
	QString ogran_poluch_min = "";							
	/// -- Ограничение полученного значения сверху;
	QString ogran_poluch_max = "";							
	/// -- Ограничение результата снизу;
	QString ogran_res_min = "";								
	/// -- Ограничение результата сверху;
	QString ogran_res_max = "";								
	/// -- Константа полученного значения;
	QList<okState> const_poluch_list = QList<okState>();	
	/// -- Константа полученного результата.
	QList<okState> const_res_list = QList<okState>();		
};

/// -- ОК;
struct OK
{
	OK(QString _ok_id = "",	QString _ok_name = "", QString _ok_descr = "",	QString _ok_mkoAddr = "", QString _ok_mkoSAddr = "", QString _ok_mkoWord = "",
	QString _ok_mkoBitStart = "", QString _ok_mkoBitCount = "", QString _ok_cbkMsg = "", QString _ok_cbkWordNum = "", QList<okState> _okStateList = QList<okState>(), okFormula _okForm = { 0 }, 
	QString _analiz_ok_id = "", QList<analiz_OK> _analiz_ok = QList<analiz_OK>(), QString _device = "", QString _ok_cbkWordNumStart = "", QString _ok_cbkWordNumCount = "")
		: ok_id(_ok_id), ok_name(_ok_name), ok_descr(_ok_descr), ok_mkoAddr(_ok_mkoAddr),
		ok_mkoSAddr(_ok_mkoSAddr), ok_mkoWord(_ok_mkoWord), ok_mkoBitStart(_ok_mkoBitStart),
		ok_mkoBitCount(_ok_mkoBitCount), ok_cbkMsg(_ok_cbkMsg), ok_cbkWordNum(_ok_cbkWordNum), okStateList(_okStateList), okForm(_okForm), analiz_ok_id(_analiz_ok_id), analiz_ok(_analiz_ok), 
		device(_device), ok_cbkWordNumStart(_ok_cbkWordNumStart), ok_cbkWordNumCount(_ok_cbkWordNumCount)
	{}
	/// -- ИД;
	QString ok_id = "";
	/// -- Имя;
	QString ok_name = "";
	/// -- Описание;
	QString ok_descr = "";
	/// -- Адресс;
	QString ok_mkoAddr = "";
	/// -- Подадрес;
	QString ok_mkoSAddr = "";
	/// -- Слово;
	QString ok_mkoWord = "";
	/// -- Стартовый бит;
	QString ok_mkoBitStart = "";
	/// -- Количесвто бит;
	QString ok_mkoBitCount = "";
	/// -- Сообщение ЦБК;
	QString ok_cbkMsg = "";
	/// -- Количество слов ЦБК;
	QString ok_cbkWordNum = "";
	/// -- Конейнер значений;
	QList<okState> okStateList = QList<okState>();
	/// -- Формула;
	okFormula okForm;
	/// -- Анализ ОК;
	QString analiz_ok_id = "";
	/// -- Контейнер анализов ОК;
	QList<analiz_OK> analiz_ok = QList<analiz_OK>();
	/// -- Устройство.
	QString device = "";
	/// -- Стартовое слово ЦБК.
	QString ok_cbkWordNumStart = "";
	/// -- Количество слов ЦБК.
	QString ok_cbkWordNumCount = "";
};

/**
*	\class XMLReader
*	\brief -- класс XMLReader;
*/
class XMLReader
{
public:

	/**
	*	\brief -- Конструктор;
	*/
	XMLReader();

	/**
	*	\brief -- Чтение;
	*	\param device -- Устройство;
	*	\param _addr -- Адрес;
	*	\param _paddr -- Подадрес;
	*	\param _SD -- СД.
	*	\return Лист ОКов.
	*/
	QList<OK> read(QIODevice *device, QString _addr, QString _paddr, QString _SD, int _SD_count);

	/**
	*	\brief -- Ошибка;
	*	\return Ошибка.
	*/
	QString errorString() const;

	/**
	*	\brief -- Деструктор;
	*/
	~XMLReader();
private:

	/**
	*	\brief -- Чтение XML;
	*/
	void readXML();
	
	/**
	*	\brief -- Чтение устройства;
	*/
	void readDevice();

	/**
	*	\brief -- Чтение State;
	*	\return okState.
	*/
	okState readState();

	/**
	*	\brief -- Чтение формулы;
	*	\return okFormula.
	*/
	okFormula readFormula();

	/**
	*	\brief -- Анализ ОК;
	*	\return Анализ ок айди.
	*/
	QString readAnaliz_ok_id();

	/**
	*	\brief -- Чтение ИФ;
	*	\return analiz_OK.
	*/
	analiz_OK readAnaliz_ok_if();

	/**
	*	\brief -- Чтение ограничений полученных;
	*	\param poluch_min -- Полученное минимальное;
	*	\param poluch_max -- Полученное максимальное.
	*/
	void readOgran_poluch(QString &poluch_min, QString &poluch_max);

	/**
	*	\brief -- Чтение ограничений результата;
	*	\param res_min -- Результат минимальный;
	*	\param res_max -- Результат максимальный.
	*/
	void readOgran_res(QString &res_min, QString &res_max);
	
	/**
	*	\brief -- Чтение ОК;
	*	\param curDeviceName -- Выбранное устройство.
	*/
	void readOk(QString curDeviceName);
	
	/// -- Поток;
	QXmlStreamReader xml;

	/// -- Адреc;
	QString addr;
	/// -- Подадрес;
	QString saddr;
	/// -- СД;
	QString SD;
	/// -- Контейнер ОКов.
	QList<OK> okList;

	int SD_count;
};

#endif