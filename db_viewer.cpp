#include "db_viewer.h"
#include "WordItemDelegate.h"

DBView::DBView(QWidget *parent)
: QMainWindow(parent), table_model(0)
{
	if (!QFile::exists(QCoreApplication::applicationDirPath() + "/" + "db_names.ini"))
	{
		QList<TMDBs> db_names;
		db_names.push_back({ "ТМ", "АИК", "aik" });
		db_names.push_back({ "ТМ", "МЦА", "mca" });

		settings_dir_path = QCoreApplication::applicationDirPath() + "/" + "db_names.ini";
		QSettings db_names_settings(settings_dir_path, QSettings::IniFormat, this);
		db_names_settings.setIniCodec("UTF-8");
		db_names_settings.beginWriteArray("db_names");
		for (int i = 0; i < db_names.size(); ++i)
		{
			db_names_settings.setArrayIndex(i);
			db_names_settings.setValue("DBType", db_names.at(i).DBType);
			db_names_settings.setValue("rusName", db_names.at(i).rusName);
			db_names_settings.setValue("engName", db_names.at(i).engName);
		}
		db_names_settings.endArray();
		db_names_settings.sync();
	}
	
	settings_dir_path = QCoreApplication::applicationDirPath() + "/" + "db_names.ini";
	QSettings db_names_settings(settings_dir_path, QSettings::IniFormat, this);
	db_names_settings.setIniCodec("UTF-8");
	int size = db_names_settings.beginReadArray("db_names");
	for (int i = 0; i < size; ++i) 
	{
		TMDBs tmdbs;
		db_names_settings.setArrayIndex(i);
		tmdbs.DBType = db_names_settings.value("DBType").toString();
		tmdbs.rusName = db_names_settings.value("rusName").toString();
		tmdbs.engName = db_names_settings.value("engName").toString();
		db_names.push_back(tmdbs);
	}
	db_names_settings.endArray();

	grathWidg = new GrathWidg(this);
	narabotkaWidg = new NarabotkaWidg(this);
	mainStackWidget = new QStackedWidget(this);
	stackWidget = new QStackedWidget(this);
	widg = new QWidget(this);
	
	mainStackWidget->addWidget(widg);
	mainStackWidget->addWidget(grathWidg);
	mainStackWidget->addWidget(narabotkaWidg);
	mainStackWidget->setCurrentIndex(0);

	widgStandartFind = new QWidget(widg);
	widgMultyFind = new QWidget(widg);

	DB_IKwidg = new QWidget(widg);
	DB_IKwidg->setMaximumHeight(210);

	multiFormList = new QList<multyFindForm*>;
	multyLayout = new QGridLayout(widgMultyFind);
	widgMultyFind->setMinimumWidth(950);
	widgMultyFind->setMaximumWidth(950);
	stackWidget->addWidget(widgStandartFind);
	stackWidget->addWidget(widgMultyFind);
	stackWidget->addWidget(DB_IKwidg);
	
	TM_widgets = new QList<TM_name_widget*>();
	foreach(const TMDBs& TMDBs_obj, db_names)
	{
		TM_name_widget *TM_wdg = new TM_name_widget(TMDBs_obj.engName);
		TM_widgets->push_back(TM_wdg);
		stackWidget->addWidget(TM_wdg);
	}

	stackWidget->setCurrentWidget(widgStandartFind);

	multyForm = new multyFindForm(widgMultyFind);
	multyForm->hideDelButton();
	multyForm->index = 0;
	multyForm->show();
	findFormPosX = 0;
	findFormPosY = 0;
	multyLayout->addWidget(multyForm, 0, 0);
	++findFormPosX;
	multiFormList->append(multyForm);
	currentFormIndex = 0;

	formValueList.append(QMap<QString, QVariant>());
	formValueList[currentFormIndex].insert("MMKO1", true);
	formValueList[currentFormIndex].insert("MMKO2", true);
	formValueList[currentFormIndex].insert("LMKO1", true);
	formValueList[currentFormIndex].insert("LMKO2", true);
	formValueList[currentFormIndex].insert("format1", true);
	formValueList[currentFormIndex].insert("format2", true);
	formValueList[currentFormIndex].insert("format3", false);

	v_layout = new QVBoxLayout(widg);

	createActions();
	createMenus();

	MSKLabel = new QLabel("MSK", widg);
	MSK1Edit = new QLineEdit(widg);
	MSK1Edit->setMinimumHeight(25);
	MSK1Edit->setMaximumWidth(135);
	MSK1Edit->setInputMask("99.99.9999 99:99:99.999");

	MSK2Edit = new QLineEdit(widg);
	MSK2Edit->setMinimumHeight(25);
	MSK2Edit->setMaximumWidth(135);
	MSK2Edit->setInputMask("99.99.9999 99:99:99.999");

	ADRLabel = new QLabel("Адрес", widg);
	ADREdit = new QLineEdit(widg);
	ADREdit->setMinimumHeight(25);
	ADREdit->setMaximumWidth(100);
	ADREdit->setValidator(new QIntValidator(0, 31));

	PADRLabel = new QLabel("Подадрес", widg);
	PADREdit = new QLineEdit(widg);
	PADREdit->setMinimumHeight(25);
	PADREdit->setMaximumWidth(100);
	PADREdit->setValidator(new QIntValidator(0, 31));
	//PADREdit->setValidator(new QRegExpValidator(QRegExp("[A-Fa-f0-9]{0,10}")));

	CountSDLabel = new QLabel("Количество СД", widg);
	CountSDEdit = new QLineEdit(widg);
	CountSDEdit->setMinimumHeight(25);
	CountSDEdit->setMaximumWidth(135);
	CountSDEdit->setValidator(new QIntValidator(0, 32));

	OKLabel = new QLabel("№ ОК, Значение", widg);
	OKEdit = new QLineEdit(widg);
	OKEdit->setMinimumHeight(25);
	OKEdit->setMaximumWidth(100);

	BTLabel = new QLabel("БВ", widg);
	BT1Edit = new QLineEdit(widg);
	BT1Edit->setMinimumHeight(25);
	BT1Edit->setMaximumWidth(135);
	BT1Edit->setInputMask("99:99:99");

	BT2Edit = new QLineEdit(widg);
	BT2Edit->setMinimumHeight(25);
	BT2Edit->setMaximumWidth(135);
	BT2Edit->setInputMask("99:99:99");

	UTCLabel = new QLabel("UTC", widg);
	UTCEdit = new QLineEdit(widg);
	UTCEdit->setMinimumHeight(25);
	UTCEdit->setMaximumWidth(135);
	UTCEdit->setInputMask("99.99.9999 99:99:99");

	UTCEdit2 = new QLineEdit(widg);
	UTCEdit2->setMinimumHeight(25);
	UTCEdit2->setMaximumWidth(135);
	UTCEdit2->setInputMask("99.99.9999 99:99:99");

	diffBTLabel = new QLabel("Расхождение БВ", widg);
	diffBTEdit = new QLineEdit(widg);
	diffBTEdit->setMinimumHeight(25);
	diffBTEdit->setMaximumWidth(135);

	diffBTEdit2 = new QLineEdit(widg);
	diffBTEdit2->setMinimumHeight(25);
	diffBTEdit2->setMaximumWidth(135);

	MMKOGroupBox = new QGroupBox("Магистраль МКО", widg);
	MMKOCheckBox1 = new QCheckBox("0", widg);
	MMKOCheckBox1->setChecked(true);
	MMKOCheckBox2 = new QCheckBox("1", widg);
	MMKOCheckBox2->setChecked(true);
	QHBoxLayout *MMKOLayout = new QHBoxLayout();
	MMKOLayout->addWidget(MMKOCheckBox1);
	MMKOLayout->addWidget(MMKOCheckBox2);
	MMKOGroupBox->setLayout(MMKOLayout);

	LMKOGroupBox = new QGroupBox("Линия МКО", widg);
	LMKOCheckBox1 = new QCheckBox("О", widg);
	LMKOCheckBox1->setChecked(true);
	LMKOCheckBox2 = new QCheckBox("Р", widg);
	LMKOCheckBox2->setChecked(true);
	QHBoxLayout *LMKOLayout = new QHBoxLayout();
	LMKOLayout->addWidget(LMKOCheckBox1);
	LMKOLayout->addWidget(LMKOCheckBox2);
	LMKOGroupBox->setLayout(LMKOLayout);

	FormatGroupBox = new QGroupBox("Формат", widg);
	FormatCheckBox1 = new QCheckBox("1", widg);
	FormatCheckBox1->setChecked(true);
	FormatCheckBox2 = new QCheckBox("2", widg);
	FormatCheckBox2->setChecked(true);
	FormatCheckBox3 = new QCheckBox("4", widg);
	FormatCheckBox3->setChecked(false);
	QVBoxLayout *FormatLayout = new QVBoxLayout();
	FormatLayout->addWidget(FormatCheckBox1);
	FormatLayout->addWidget(FormatCheckBox2);
	FormatLayout->addWidget(FormatCheckBox3);
	FormatGroupBox->setLayout(FormatLayout);

	CWLabel = new QLabel("КС", widg);
	CWEdit = new QLineEdit(widg);
	CWEdit->setMinimumHeight(25);
	CWEdit->setMaximumWidth(135);

	AWLabel = new QLabel("ОС", widg);
	AWEdit = new QLineEdit(widg);
	AWEdit->setMinimumHeight(25);
	AWEdit->setMaximumWidth(135);

	KU_SPOBULabel = new QLabel("КУ СПО-БУ", widg);
	KU_SPOBUEdit = new QLineEdit(widg);
	KU_SPOBUEdit->setMinimumHeight(25);
	KU_SPOBUEdit->setMaximumWidth(130);

	KS_KPI2Label = new QLabel("КС КПИ2", widg);
	KS_KPI2Edit = new QLineEdit(widg);
	KS_KPI2Edit->setMinimumHeight(25);
	KS_KPI2Edit->setMaximumWidth(135);

	MD_CBKLabel = new QLabel("МД ЦБК", widg);
	MD_CBKEdit = new QLineEdit(widg);
	MD_CBKEdit->setMinimumHeight(25);
	MD_CBKEdit->setMaximumWidth(135);

	SDLabel = new QLabel("СД", widg);
	SDEdit = new QTextEdit(widg);
	SDEdit->setMinimumHeight(90);
	SDEdit->setMinimumWidth(460);

	metkaLabel = new QLabel("Метка", widg);
	metkaEdit = new QLineEdit(widg);
	metkaEdit->setMinimumHeight(25);
	metkaEdit->setMinimumWidth(206);

	QVBoxLayout *v2_layout = new QVBoxLayout();
	findIntervalButton = new QPushButton(/*"Поиск с интервалом",*/ widg);
	findIntervalButton->setShortcut(Qt::Key_F9);
	findIntervalButton->setFlat(true);
	QPixmap pix(":img/Interval.png");
	findIntervalButton->setIcon(pix);
	findIntervalButton->setIconSize(QSize(75, 65));

	findButton = new QPushButton(/*"Поиск",*/ widg);
	findButton->setShortcut(Qt::Key_F5);
	findButton->setFlat(true);
	QPixmap pix2(":img/Find.png");
	findButton->setIcon(pix2);
	findButton->setIconSize(QSize(124, 141));

	multyFindButton = new QPushButton(/*"Мультипоиск", */widg);
	multyFindButton->setFlat(true);
	QPixmap pix3(":img/MultiFind2.png");
	multyFindButton->setIcon(pix3);
	multyFindButton->setIconSize(pix3.size());

	findQueryButton = new QPushButton(/*"Поисковый запрос",*/ widg);
	findQueryButton->setFlat(true);
	QPixmap pix5(":img/find_query.png");
	findQueryButton->setIcon(pix5);
	findQueryButton->setIconSize(QSize(75, 65));

	translateToQ10Button = new QPushButton(DB_IKwidg);
	translateToQ10Button->setFlat(true);
	QPixmap pix4(":img/TranslateToQ10.png");
	translateToQ10Button->setIcon(pix4);
	translateToQ10Button->setIconSize(pix4.size());
	translateToQ10Button->hide();

	IKViewButton = new QPushButton(DB_IKwidg);
	IKViewButton->setFlat(true);
	IKViewButton->setText("Просмотр данных ИК");
	IKViewButton->hide();

	v2_layout->addWidget(multyFindButton);
	v2_layout->addWidget(findIntervalButton);
	v2_layout->addWidget(findQueryButton);
		
	QHBoxLayout *h_layout = new QHBoxLayout();
	h_layout->addWidget(stackWidget);
	h_layout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));
	h_layout->addWidget(IKViewButton);
	h_layout->addWidget(translateToQ10Button);
	h_layout->addWidget(findButton);
	h_layout->addLayout(v2_layout);

	mainTable = new QTableView(widg);
	table_model = new MyModel(widg);
	

	h_filterMKOLayout = new QHBoxLayout(widgStandartFind);
	DB_MKOmini1GridLayout = new QGridLayout;
	DB_MKOmini2GridLayout = new QGridLayout;
	DB_MKOmini3GridLayout = new QGridLayout;
	DB_MKOmini4GridLayout = new QGridLayout;
	DB_MKOmini5GridLayout = new QGridLayout;
	DB_MKOmini6GridLayout = new QGridLayout;
	DB_MKOmini7GridLayout = new QGridLayout;

	h_filterIKLayout = new QHBoxLayout(DB_IKwidg);
	DB_IKmini1GridLayout = new QGridLayout;
	DB_IKmini2GridLayout = new QGridLayout;
	DB_IKmini3GridLayout = new QGridLayout;

	mainTable->setAlternatingRowColors(true); //теперь строки зеброй красит
	mainTable->setStyleSheet(instr::get_table_style());
	mainTable->horizontalHeader()->setStyleSheet("QHeaderView::section {background-color: rgb(204, 204, 204);};  ");
	mainTable->setModel(table_model);
	mainTable->setFont(instr::mainFont());

	hideColumnList = new QList<int>;
	createTableHeaders();

	vbar = new QScrollBar(widg);

	QHBoxLayout *h_layout2 = new QHBoxLayout();
	h_layout2->addWidget(mainTable);
	h_layout2->addWidget(vbar);

	QHBoxLayout *h_layout3 = new QHBoxLayout();
	DB_MKO_button = new QPushButton("БД МКО", widg);
	DB_IK_button = new QPushButton("БД ИК", widg);

	TM_buttons = new QList<QPushButton*>;
	foreach(const TMDBs& TMDBs_obj, db_names)
	{
		QPushButton *btn = new QPushButton(QString("БД %1 %2").arg(TMDBs_obj.DBType).arg(TMDBs_obj.rusName), widg);
		btn->setProperty("btn_TM_ID", TM_buttons->count());
		TM_buttons->push_back(btn);
	}
	OK_open_Button = new QPushButton("Открыть", widg);
	h_layout3->setMargin(0);
	h_layout3->setSpacing(0);
	h_layout3->setContentsMargins(0, 0, 0, 0);

	h_layout3->addWidget(DB_MKO_button);
	h_layout3->addWidget(DB_IK_button);

	for (QPushButton *b : TM_buttons->toStdList())
		h_layout3->addWidget(b);
	h_layout3->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));

	v_layout->setMargin(11);
	v_layout->setSpacing(8);
	v_layout->addSpacing(20);
	v_layout->addLayout(h_layout3);
	v_layout->addLayout(h_layout, 1);
	v_layout->addLayout(h_layout2, 1000);

	progressBar = new QProgressBar(widg);
	progressBar->setMinimum(0);
	progressBar->setMaximum(230);
	progressBar->setValue(0);
	progressLabel = new QLabel("Начало работы", widg);
	progressLabel->setMinimumWidth(250);
	QFont font = progressLabel->font();
	font.setPointSize(12);
	progressLabel->setFont(font);
	QHBoxLayout *progressbarLayout = new QHBoxLayout();
	progressbarLayout->addWidget(progressLabel);
	progressbarLayout->addWidget(progressBar);

	v_layout->addLayout(progressbarLayout);

	setCentralWidget(mainStackWidget/*widg*/);

	filter1MKOGroupBox = new QGroupBox();
	filter2MKOGroupBox = new QGroupBox();
	filter3MKOGroupBox = new QGroupBox();
	filter4MKOGroupBox = new QGroupBox();
	filter5MKOGroupBox = new QGroupBox();
	filter6MKOGroupBox = new QGroupBox();
	filter7MKOGroupBox = new QGroupBox();
	filter1IkGroupBox = new QGroupBox();
	filter2IkGroupBox = new QGroupBox();
	filter3IkGroupBox = new QGroupBox();


	setDefaultFilters();
	query_str.clear();
	ignor = false;
	inWork = false;

	remote_frame_path = instr::GetIpFromSettings("frame_path");

	settings_dir_path = QCoreApplication::applicationDirPath() + "/" + "DB_settings.ini";
	allFilters = false;
	multyFindOn = false;
	interval = false;

	DB_IKWidgConstructor();

	thr_map.insert("MKO", new DBReaderThread("MKO"));

	foreach(const TMDBs& TMDBs_obj, db_names)
		thr_map.insert(TMDBs_obj.engName.toUpper(), new DBReaderThread(TMDBs_obj.DBType + " " + TMDBs_obj.rusName + " " + TMDBs_obj.engName));
	thr_map.insert("Frame", new DBReaderThread("Frame"));
	for (auto e : thr_map.toStdMap())
	{
		connect(e.second, &DBReaderThread::error, this, &DBView::error);
	}
	
	ip_map.insert("MKO", instr::GetIpFromSettings("MKO_db"));
	foreach(const TMDBs& TMDBs_obj, db_names)
		ip_map.insert(TMDBs_obj.engName.toUpper(), instr::GetIpFromSettings(TMDBs_obj.engName.toUpper() + "_db"));
	ip_map.insert("Frame", instr::GetIpFromSettings("Frame_db"));

	cur_thr = NULL;
	setDB_MKO();
	grathWidg->connectToDB("MKO");
	grathWidg->connectWithCollection(collName, ip_map["MKO"]);

	connect(findButton, &QPushButton::clicked, this, &DBView::startSearch);
	connect(findIntervalButton, &QPushButton::clicked, this, &DBView::startIntervalSearch);
	connect(multyFindButton, &QPushButton::clicked, this, &DBView::multyFind);
	
	//	connect(bv_th, &DBViewerBVReaderDBThread::finishedRead, this, &DBView::getBVtime);
	
	connect(vbar, &QScrollBar::valueChanged, this, &DBView::slideVBarIsMoved);
	connect(vbar, &QScrollBar::sliderPressed, this, &DBView::slideVBarPressed);
	connect(vbar, &QScrollBar::sliderReleased, this, &DBView::slideVBarReleased);
	connect(mainTable->verticalScrollBar(), &QScrollBar::valueChanged, this, &DBView::slideBarIsMoved);
	connect(multyForm, &multyFindForm::addNewForm, this, &DBView::addNewFindForm);
	connect(multyForm, &multyFindForm::showDialogSignal, this, &DBView::showMultyDialog);

	connect(SDEdit, &QTextEdit::textChanged, this, &DBView::SDTextChange);	
	connect(MSK1Edit, &QLineEdit::textChanged, this, &DBView::MSKTextCheck);
	connect(MSK2Edit, &QLineEdit::textChanged, this, &DBView::MSKTextCheck);
	connect(UTCEdit, &QLineEdit::textChanged, this, &DBView::MSKTextCheck);
	connect(UTCEdit2, &QLineEdit::textChanged, this, &DBView::MSKTextCheck);
	connect(BT1Edit, &QLineEdit::textChanged, this, &DBView::MSKTextCheck);
	connect(BT2Edit, &QLineEdit::textChanged, this, &DBView::MSKTextCheck);

	connect(mainTable, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(onTableClicked(const QModelIndex &)));

	for (QPushButton *b : TM_buttons->toStdList())
		connect(b, &QPushButton::clicked, this, &DBView::setDB_TM);

	connect(DB_MKO_button, &QPushButton::clicked, this, &DBView::setDB_MKO);
	connect(DB_IK_button, &QPushButton::clicked, this, &DBView::setDB_IK);
	connect(IKViewButton, &QPushButton::clicked, this, &DBView::IKView);
	connect(translateToQ10Button, &QPushButton::clicked, this, &DBView::Q10View);
	connect(findQueryButton, &QPushButton::clicked, this, &DBView::getQueryString);

	setCountRecord(countRecord);

	for (QMap<QString, DBReaderThread*>::iterator itr = thr_map.begin(); itr != thr_map.end(); itr++)
		(*itr)->start();

	QFile mFile("db_viewer_log.log");
	if (mFile.open(QFile::WriteOnly))
	{
		mFile.close();
	}

	/////////////////////////////////////////

	tm_aik_names.insert(133, "Измеренное значение тока потребления шины НК");
	tm_aik_names.insert(134, "Измеренное значение тока потребления шины К1");
	tm_aik_names.insert(135, "Измеренное значение тока потребления шины К2");

	tm_aik_meas.insert(133, "А");
	tm_aik_meas.insert(134, "А");
	tm_aik_meas.insert(135, "А");

	tm_mca_names.insert(1, "Несимметричность бортового питания");
	tm_mca_names.insert(2, "Сопротивление изоляции шины +");
	tm_mca_names.insert(3, "Сопротивление изоляции шины -");
	tm_mca_names.insert(4, "Напряжение ИБЭП");
	tm_mca_names.insert(5, "Ток потребления ИБЭП");

	tm_mca_meas.insert(1, "В");
	tm_mca_meas.insert(2, "Ом");
	tm_mca_meas.insert(3, "Ом");
	tm_mca_meas.insert(4, "В");
	tm_mca_meas.insert(5, "А");
}

void DBView::DB_IKWidgConstructor()
{
	nomerIKLabel = new QLabel("Номер ИК", DB_IKwidg);
	nStartFrameEdit = new QLineEdit(DB_IKwidg);
	nStartFrameEdit->setMinimumHeight(25);
	nStartFrameEdit->setMaximumWidth(135);
	
	nEndFrameEdit = new QLineEdit(DB_IKwidg);
	nEndFrameEdit->setMinimumHeight(25);
	nEndFrameEdit->setMaximumWidth(135);

	modeGroupBox = new QGroupBox("Формат ИК", DB_IKwidg);

	FormatIK15 = new QPushButton("ИК-15", DB_IKwidg);
	FormatIK15->setStyleSheet("background-color: rgb(149, 188, 242);");
	FormatIK8 = new QPushButton("ИК-8", DB_IKwidg);
	FormatIK8->setStyleSheet("background-color: rgb(149, 188, 242);");
	FormatIKWTF8 = new QPushButton("ВТФ-8", DB_IKwidg);
	FormatIKWTF8->setStyleSheet("background-color: rgb(149, 188, 242);");

	modeLayout = new QHBoxLayout();

	filePathLabel = new QLabel("Путь", DB_IKwidg);
	filePathEdit = new QLineEdit(DB_IKwidg);
	filePathEdit->setMinimumHeight(25);
	filePathEdit->setMinimumWidth(500);
	//informationEdit->setMaximumWidth(100);

	MD_CBK_Label = new QLabel("МД ЦБК", DB_IKwidg);
	MD_CBK_ComboBox = new QComboBox(DB_IKwidg);
	MD_CBK_ComboBox->setMinimumHeight(25);
	MD_CBK_ComboBox->setMaximumWidth(60);
	QStringList MD_CBK_list;
	MD_CBK_list << "Все";
	for (int i = 1; i <= 40; ++i)
	{
		MD_CBK_list << QString("М%1").arg(QString::number(i));
	}
	MD_CBK_ComboBox->addItems(MD_CBK_list);
	
	DTK_Label = new QLabel("ДТК", DB_IKwidg);
	DTK_ComboBox = new QComboBox(DB_IKwidg);
	DTK_ComboBox->setMinimumHeight(25);
	DTK_ComboBox->setMaximumWidth(90);
	QStringList DTK_list;
	DTK_list << "Все" << "НОРМА" << "НЕНОРМА";
	DTK_ComboBox->addItems(DTK_list);
	
	Kvitancii_Label = new QLabel("Квитанции", DB_IKwidg);
	Kvitancii_ComboBox = new QComboBox(DB_IKwidg);
	Kvitancii_ComboBox->setMinimumHeight(25);
	Kvitancii_ComboBox->setMaximumWidth(100);
	QStringList Kvitancii_list;
	Kvitancii_list << "Все" << "ДА КПИ" << "НЕТ КПИ" << "ДА СЧЗК" << "НЕТ СЧЗК";
	Kvitancii_ComboBox->addItems(Kvitancii_list);

	KFKlabel = new QLabel("КФК", DB_IKwidg);
	KFKEdit1 = new QLineEdit(DB_IKwidg);
	KFKEdit1->setMinimumHeight(25);
	KFKEdit1->setMaximumWidth(135);
	KFKEdit2 = new QLineEdit(DB_IKwidg);
	KFKEdit2->setMinimumHeight(25);
	KFKEdit2->setMaximumWidth(135);
	mdLayout = new QGridLayout;

	connect(FormatIK15, &QPushButton::clicked, this, &DBView::setFormatIKButton);
	connect(FormatIK8, &QPushButton::clicked, this, &DBView::setFormatIKButton);
	connect(FormatIKWTF8, &QPushButton::clicked, this, &DBView::setFormatIKButton);

}

void DBView::setDefaultFilters()
{
	widgStandartFind->setMaximumHeight(240);
	MSK1Edit->show();
	MSK2Edit->show();
	ADREdit->show();
	PADREdit->show();
	CountSDEdit->show();
	SDEdit->show();
	BT1Edit->show();
	BT2Edit->show();
	OKEdit->show();
	MSKLabel->show();
	ADRLabel->show();
	PADRLabel->show();
	BTLabel->show();
	OKLabel->show();
	SDLabel->show();

	CountSDLabel->hide();
	CountSDEdit->hide();
	CWLabel->hide();
	AWLabel->hide();
	KU_SPOBULabel->hide();
	KS_KPI2Label->hide();
	MD_CBKLabel->hide();
	UTCLabel->hide();
	diffBTLabel->hide();
	CWEdit->hide();
	AWEdit->hide();
	KU_SPOBUEdit->hide();
	KS_KPI2Edit->hide();
	MD_CBKEdit->hide();
	UTCEdit->hide();
	UTCEdit2->hide();
	diffBTEdit->hide();
	diffBTEdit2->hide();
	MMKOGroupBox->hide();
	LMKOGroupBox->hide();
	FormatGroupBox->hide();
	MMKOCheckBox1->hide();
	MMKOCheckBox2->hide();
	LMKOCheckBox1->hide();
	LMKOCheckBox2->hide();
	FormatCheckBox1->hide();
	FormatCheckBox2->hide();
	FormatCheckBox3->hide();
	allFilters = false;
	filter2MKOGroupBox->hide();
	filter5MKOGroupBox->hide();
}

void DBView::setAllFilters()
{
	widgStandartFind->setMaximumHeight(250);
	MSK1Edit->show();
	MSK2Edit->show();
	ADREdit->show();
	PADREdit->show();
	SDEdit->show();
	BT1Edit->show();
	BT2Edit->show();
	OKEdit->show();
	MSKLabel->show();
	ADRLabel->show();
	PADRLabel->show();
	CountSDLabel->show();
	SDLabel->show();
	BTLabel->show();
	OKLabel->show();

	CountSDEdit->show();
	CWLabel->show();
	AWLabel->show();
	KU_SPOBULabel->show();
	KS_KPI2Label->show();
	MD_CBKLabel->show();
	UTCLabel->show();
	diffBTLabel->show();
	CWEdit->show();
	AWEdit->show();
	KU_SPOBUEdit->show();
	KS_KPI2Edit->show();
	MD_CBKEdit->show();
	UTCEdit->show();
	UTCEdit2->show();
	diffBTEdit->show();
	diffBTEdit2->show();
	MMKOGroupBox->show();
	LMKOGroupBox->show();
	FormatGroupBox->show();
	MMKOCheckBox1->show();
	MMKOCheckBox2->show();
	LMKOCheckBox1->show();
	LMKOCheckBox2->show();
	FormatCheckBox1->show();
	FormatCheckBox2->show();
	FormatCheckBox3->show();
	allFilters = true;
	filter2MKOGroupBox->show();
	filter5MKOGroupBox->show();
}

void DBView::multyFind()
{
	if (!multyFindOn)
	{
		setDefaultFilters();
		widgMultyFind->setMinimumHeight(230);
		QPixmap pix(":img/MultiFind1.png");
		multyFindButton->setIcon(pix);
		multyFindButton->setIconSize(pix.size());
		stackWidget->setCurrentIndex(1);
		multyFindOn = true;
	}
	else
	{
		QPixmap pix(":img/MultiFind2.png");
		widgMultyFind->setMinimumHeight(100);
		multyFindButton->setIcon(pix);
		multyFindButton->setIconSize(pix.size());
		stackWidget->setCurrentIndex(0);
		multyFindOn = false;
	}
}

void DBView::createMenus()
{
	subdMenu = menuBar()->addMenu("СУБД");
	subdMenu->addAction(foundManagerAct);
	subdMenu->addAction(grathicManagerAct);
	subdMenu->addAction(narabotkiManagerAct);
	subdMenu->addAction(settingSUBDAct);

	TM_menus = new QList<QMenu*>;
	foreach(const TMDBs& TMDBs_obj, db_names)
	{
		QMenu *TM_menu = menuBar()->addMenu("Параметры");
		TM_menu->addAction(clearAct);
		TM_menu->addAction(openFiltersAct);
		TM_menu->addAction(openLastFiltersAct);
		TM_menu->addAction(saveFiltersAct);
		TM_menu->addAction(saveFiltersAsAct);
		TM_menu->addAction(printAct);
		TM_menu->addAction(copyAct);
		TM_menu->addAction(ipMonAct);
		TM_menus->push_back(TM_menu);
	}

	settingsMenuMKO = menuBar()->addMenu("Параметры");
	settingsMenuMKO->addAction(defaultFiltersAct);
	settingsMenuMKO->addAction(allFiltersAct);
	settingsMenuMKO->addAction(clearAct);
	settingsMenuMKO->addAction(openFiltersAct);
	settingsMenuMKO->addAction(openLastFiltersAct);
	settingsMenuMKO->addAction(saveFiltersAct);
	settingsMenuMKO->addAction(saveFiltersAsAct);
	settingsMenuMKO->addAction(ipMonAct);
	settingsMenuMKO->addAction(printAct);
	settingsMenuMKO->addAction(copyAct);

	settingsMenuIK = menuBar()->addMenu("Параметры");
	settingsMenuIK->addAction(clearAct);
	settingsMenuIK->addAction(openFiltersAct);
	settingsMenuIK->addAction(openLastFiltersAct);
	settingsMenuIK->addAction(saveFiltersAct);
	settingsMenuIK->addAction(saveFiltersAsAct);
	settingsMenuIK->addAction(printAct);
	settingsMenuIK->addAction(copyAct);
	settingsMenuIK->addAction(ipMonAct);

	viewMenu = menuBar()->addMenu("Вид");
	viewMenu->addAction(showHideColumnAct);
	viewMenu->addAction(zebraAct);
	zebraAct->setCheckable(true);
	zebraAct->setChecked(true);
	viewMenu->addAction(countRecAct);

	helpMenu = menuBar()->addMenu("Помощь");
	helpMenu->addAction(aboutAct);
}

void DBView::createActions()
{
	foundManagerAct = new QAction("Менеджер поиска", this);
	connect(foundManagerAct, SIGNAL(triggered()), this, SLOT(foundManager()));

	grathicManagerAct = new QAction("Менеджер графиков", this);
	connect(grathicManagerAct, SIGNAL(triggered()), this, SLOT(grathicManager()));

	narabotkiManagerAct = new QAction("Менеджер наработки", this);
	connect(narabotkiManagerAct, SIGNAL(triggered()), this, SLOT(narabotkaManager()));

	settingSUBDAct = new QAction("Настройка СУБД", this);
	//connect(settingSUBDAct, &QAction::triggered, this, &DBView::settingSUBD);
	connect(settingSUBDAct, SIGNAL(triggered()), this, SLOT(settingSUBD()));

	defaultFiltersAct = new QAction("Набор фильтров по умолчанию", this);
	defaultFiltersAct->setShortcut(QString("Ctrl+1"));
	connect(defaultFiltersAct, SIGNAL(triggered()), this, SLOT(setDefaultFilters()));
	
	allFiltersAct = new QAction("Отобразить все возможные фильтры", this);
	allFiltersAct->setShortcut(QString("Ctrl+2"));
	connect(allFiltersAct, SIGNAL(triggered()), this, SLOT(setAllFilters()));

	clearAct = new QAction("Очистить", this);
	clearAct->setShortcut(QString("Ctrl+N"));
	connect(clearAct, SIGNAL(triggered()), this, SLOT(clearFields()));

	openFiltersAct = new QAction("Открыть набор фильтров", this);
	openFiltersAct->setShortcut(QString("Ctrl+O"));
	connect(openFiltersAct, &QAction::triggered, this, &DBView::loadFiltersAs);

	openLastFiltersAct = new QAction("Открыть недавний набор фильтров", this);
	openLastFiltersAct->setShortcut(QString("Ctrl+Shift+O"));
	connect(openLastFiltersAct, &QAction::triggered, this, &DBView::loadFilters);

	saveFiltersAct = new QAction("Сохранить набор фильтров", this);
	saveFiltersAct->setShortcut(QString("Ctrl+S"));
	connect(saveFiltersAct, &QAction::triggered, this, &DBView::saveFilters);

	saveFiltersAsAct = new QAction("Сохранить набор фильтров как...", this);
	saveFiltersAsAct->setShortcut(QString("Ctrl+Shift+S"));
	connect(saveFiltersAsAct, &QAction::triggered, this, &DBView::saveFiltersAs);

	printAct = new QAction("Печать", this);
	printAct->setShortcut(QString("Ctrl+P"));
	connect(printAct, &QAction::triggered, this, &DBView::print_doc);

	ipMonAct = new QAction("IP Монитора", this);
	ipMonAct->setShortcut(QString("Ctrl+I"));
	connect(ipMonAct, &QAction::triggered, this, &DBView::ipForMon);

	aboutAct = new QAction("О программе", this);

	showHideColumnAct = new QAction("Скрыть колонки", this);
	connect(showHideColumnAct, &QAction::triggered, this, &DBView::showHideColumnDlg);

	zebraAct = new QAction("Зебра", this);
	connect(zebraAct, &QAction::triggered, this, &DBView::showZebra);

	countRecAct = new QAction("Количество строк в запросе", this);
	connect(countRecAct, &QAction::triggered, this, &DBView::dlgCountRecords);
	
	copyAct = new QAction("Копировать", this);
	copyAct->setShortcut(QString("Ctrl+C"));
	connect(copyAct, &QAction::triggered, this, &DBView::copy_cell);
}

DBView::~DBView()
{

}

void DBView::error(QString msg)
{
	QMessageBox msgBox;
	msgBox.setText(msg);
	msgBox.setIcon(QMessageBox::Warning);
	msgBox.addButton(QMessageBox::Ok);
	QPixmap pix(":img/ok.png");	
	msgBox.button(QMessageBox::Ok)->setIcon(pix);
	msgBox.button(QMessageBox::Ok)->setIconSize(QSize(30, 29));
	msgBox.button(QMessageBox::Ok)->setText("");

	msgBox.exec();
}

void DBView::startSearch()
{
	writeLog("Отправил запрос");
	changeProgressLabel(5, "Запрашиваю данные из БД");
	findButton->setEnabled(false);
	interval = false;
	numRecordsDown = 0;
	cursorDown = false;
	table_model->clear();
	createTableHeaders();

	standartFindList = getQueryStruct();
	ignor = false;
	blockDown = 0;
	testTime.start();
	testTime.restart();
	findButton->setEnabled(false);
	findIntervalButton->setEnabled(false);
	if (multyFindOn)
		cur_thr->needDataBlock(blockDown, blockUp, &formValueList, 1);
	else
		cur_thr->needDataBlock(blockDown, blockUp, &standartFindList, 1);

}

void DBView::startIntervalSearch()
{
	if (!interval)
	{
		if (multyFindOn)
			cur_thr->setQueryList(formValueList);
		else
			cur_thr->setQueryList(standartFindList);

		findButton->setEnabled(false);
		firstInterval = true;
		numRecordsDown = 0;
		numRecordsUp = 200;
		countStrings = 200;
		cursorDown = true;

		if (multyFindOn)
			cur_thr->needCount(&formValueList);
		else
			cur_thr->needCount(&standartFindList);

		int a = getCount();	// start pos
		int b = 0;	// block_size
		if (a > 200)
		{
			a -= 200;
			b = 200;
		}
		else
		{
			b = a;
			a = 0;
		}

		if (multyFindOn)
			cur_thr->needDataBlock(a, b, &formValueList, 0);
		else
			cur_thr->needDataBlock(a, b, &standartFindList, 0);	// Последний параметр needCount = 0 - НЕ ТОЧНО

		vbar->setMaximum(getCount());
		ignor = false;
		blockDown = 0;
		blockUp = 200;
		interval = true;
		cur_thr->runInterval();
	}
	else
	{
		firstInterval = false;
		//findIntervalButton->setText("Поиск с интервалом");
		findButton->setEnabled(true);
		interval = false;
		cur_thr->offInterval();
	}
}

void DBView::createTableHeaders()
{
	if (table_model)
	{
		if (stackWidget->currentWidget() == widgStandartFind || stackWidget->currentWidget() == widgMultyFind)
		{
			table_model->setHorizontalHeaderLabels(QStringList() << "№" << "MSK" << "БВ"
				<< "UTC" << "Формат" << "МКО" << "Адрес" << "Подадрес" << "Число\nСД"
				<< "СД" << "КС" << "ОС" << "Расхождение\n БВ");
			WordItemDelegate* word_delegate = new WordItemDelegate;
			mainTable->setItemDelegateForColumn(9, word_delegate);
		}

		for (TM_name_widget *wdg : TM_widgets->toStdList())
		{
			if (stackWidget->currentWidget() == wdg)
			{
				table_model->setHorizontalHeaderLabels(QStringList() << "№" << "MSK" << "БВ"
					<< "Номер\nТМ" << "Наименование\nТМ" << "Измеренное\nзначение" << "Единица\nизмерения ИЗ");
				mainTable->setItemDelegateForColumn(9, NULL);
			}
		}

		if (stackWidget->currentWidget() == DB_IKwidg)
		{
			table_model->setHorizontalHeaderLabels(QStringList() << "№" << "MSK" << "БВ"
				<< "Метка" << "Папка" << "Номер\nИК" << "Режим" << "МД\nЦБК" << "Квитанции" << "ККС" << "ДТК" << "КС" << "КФК\nЧБН" << "q10" << "Путь к кадру");
			mainTable->setItemDelegateForColumn(9, NULL);
		}
	}
	mainTable->verticalHeader()->hide();
	mainTable->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	mainTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	mainTable->resizeColumnsToContents();
	mainTable->resizeRowsToContents();
	QModelIndex ind = table_model->index(1, 7);
	table_model->setData(ind, 1, Qt::AlignCenter);
	
	
	hideColumn(*hideColumnList);
	if (stackWidget->currentWidget() == DB_IKwidg)
		mainTable->hideColumn(14);	// Скрыть колонку "Путь к кадру"
}

void DBView::showData()
{
	if (show)
		return;
	show = true;
	ignor = true;
	inWork = true;
	writeLog(QString("showData ignor:%1 inWork:%2").arg(ignor ? "true" : "false").arg(inWork ? "true" : "false"));
	std::vector<QVariant> vect = cur_thr->getDataBlock();
	int i;
	if (interval)
	{
		standartFindList = getQueryStruct();
		int a = getCount();
		i = a - vect.size();
		if (table_model->rowCount() > 200)
		{
			table_model->clear();
			createTableHeaders();
		}
		vbar->setMaximum(getCount());
	}
	else
	{
		i = blockDown;
		vbar->setMaximum(getCount());
	}

	qDebug("Time startSearth -> showData : %d ms", testTime.elapsed());
	progressLabel->setText("Анализ результатов");
	QApplication::processEvents();
	int progress = progressBar->value();
	foreach(const QVariant& q_obj, vect)
	{
		QList<QStandardItem*> st_list;
		st_list.push_back(new QStandardItem(QString::number(++i)));	// №
		if (stackWidget->currentWidget() == widgStandartFind || stackWidget->currentWidget() == widgMultyFind)
		{
			MsgFromMonitorDB obj = qvariant_cast<MsgFromMonitorDB>(q_obj);
			qint64 tmp_long = obj.dt;
			QString tmp_time_str = QDateTime::fromMSecsSinceEpoch(tmp_long / 1000).toString(" dd.MM.yyyy hh:mm:ss.zzz");
			tmp_time_str += QString(".%1 ").arg(tmp_long % 1000, 3, 10, QChar('0'));
			st_list.push_back(new QStandardItem(tmp_time_str));	//MSK
			st_list.push_back(new QStandardItem(QDateTime::fromTime_t(obj.bv * 4).toUTC().toString(" hh:mm:ss ")));	//БВ
			qint64 utc_long = obj.utc;
			QString tmp_timeUTC_str = QDateTime::fromMSecsSinceEpoch(utc_long).toString(" dd.MM.yyyy hh:mm:ss ");
			st_list.push_back(new QStandardItem(tmp_timeUTC_str));						//UTC

			QString f_string = (obj.receive == 0) ? "1" : (((obj.saddr == 0) || (obj.saddr == 31)) ? "4" : "2");

			st_list.push_back(new QStandardItem(f_string));		// Формат
			QString lineMKO = (obj.reserveBus == false) ? "О" : "Р";
			st_list.push_back(new QStandardItem(QString("%1.%2").arg(obj.chan).arg(lineMKO)));	// Магистраль и линия МКО

			//st_list.push_back(new QStandardItem(QString("%1").arg(obj.reserveBus)));	// Линия МКО
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.addr)));			// Адрес
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.saddr)));			// Подадрес
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.nWords)));		// Количество слов данных


			QStandardItem* new_sd_item = new QStandardItem(QString("<font color = \"%1\" style = \"Calibri\" size = 2><pre><b>%2</b></pre></font>").arg(instr::get_aik_colour(obj.addr)).arg(obj.words));
			new_sd_item->setData(obj.words, 44);
			st_list.push_back(new_sd_item);
			//st_list.push_back(new QStandardItem(obj.words));
			st_list.push_back(new QStandardItem(QString(" %1 ").arg(obj.cwd, 4, 16, QChar('0')).toUpper()));
			st_list.push_back(new QStandardItem(QString(" %1 ").arg(obj.swd, 4, 16, QChar('0')).toUpper()));

			st_list.push_back(new QStandardItem(QString("%1").arg(obj.bv_dev)));
		}

		for (TM_name_widget *wdg : TM_widgets->toStdList())
		{
			if (stackWidget->currentWidget() == wdg)
			{
				MsgFromTMDB obj = qvariant_cast<MsgFromTMDB>(q_obj);
				qint64 tmp_long = obj.dt;
				QString tmp_time_str = QDateTime::fromMSecsSinceEpoch(tmp_long).toString(" dd.MM.yyyy hh:mm:ss.zzz ");
				st_list.push_back(new QStandardItem(tmp_time_str));	//MSK
				st_list.push_back(new QStandardItem(QDateTime::fromTime_t(obj.bv * 4).toUTC().toString(" hh:mm:ss ")));	//БВ
				st_list.push_back(new QStandardItem(QString("%1").arg(obj.tm_code)));	// Сигнал ТМ
				st_list.push_back(new QStandardItem(tm_mca_names[obj.tm_code]));	// Наименование ТМ
				st_list.push_back(new QStandardItem(QString("%1").arg(instr::variantToString(obj.value))));	// Значение ТМ
				st_list.push_back(new QStandardItem(tm_mca_meas[obj.tm_code]));	// Единица измерения
			}
		}

		if (stackWidget->currentWidget() == DB_IKwidg)
		{
			MsgFromFrameDB obj = qvariant_cast<MsgFromFrameDB>(q_obj);
			qint64 tmp_long = obj.dt;
			QString tmp_time_str = QDateTime::fromMSecsSinceEpoch(tmp_long).toString(" dd.MM.yyyy hh:mm:ss.zzz ");
			st_list.push_back(new QStandardItem(tmp_time_str));	//MSK
			st_list.push_back(new QStandardItem(QDateTime::fromTime_t(obj.bv * 4).toUTC().toString(" hh:mm:ss ")));	//БВ
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.label)));	// Метка
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.folder)));	// Папка
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.frame_num)));	// Номер ИК
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.mode)));	// Режим
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.arrays)));	// МД ИК
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.kvit_string())));	// Квитанции
			QString kks = (obj.kks) ? "НОРМА" : "НЕ НОРМА";
			st_list.push_back(new QStandardItem(QString("%1").arg(kks)));	// ККС
			QString dtk = (obj.dtk) ? "НОРМА" : "НЕ НОРМА";
			st_list.push_back(new QStandardItem(QString("%1").arg(dtk)));	// ДТК 
			QString ks = (obj.kks) ? "НОРМА" : "НЕ НОРМА";
			if (obj.mode == "ВТФ")
				st_list.push_back(new QStandardItem("-"));	// КС
			else
				st_list.push_back(new QStandardItem(QString("%1").arg(ks)));	// КС Контрольная сумма
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.kfk)));	// КФК ЧБН - число бит несравнения
			QString q10 = (obj.q10_exist) ? "ЕСТЬ" : "НЕТ";
			st_list.push_back(new QStandardItem(QString("%1").arg(q10)));	// q10
			st_list.push_back(new QStandardItem(QString("%1").arg(obj.path)));	// Путь к кадру
		}
		table_model->appendRow(st_list);
		if (!interval)
		{
			if (progress % 50 == 0)
			{
				progressBar->setValue(progress += 50);
				QApplication::processEvents();
			}
		}
	}
	progressLabel->setText("Вывожу обмены...");
	if (interval)
		//mainTable->verticalScrollBar()->
	{


		if (mainTable->verticalScrollBar()->value() > (mainTable->verticalScrollBar()->maximum() - 3))
			mainTable->scrollToBottom();
		if (firstInterval)
		{
			table_model->clear();
			createTableHeaders();
			firstInterval = false;
		}
	}
	if (cursorDown)
	{
		//mainTable->verticalScrollBar()->setValue(countStrings - 20);
		QTimer::singleShot(1 * 1500, this, SLOT(cursurUpdate()));	//TODO fix: ну херня же	//таблица медленно рисуется и не получается сразу курсор вниз выставить
		cursorDown = false;
	}
	else
		QTimer::singleShot(1 * 1000, this, SLOT(ignorTimer()));	//немного фиксит ложные срабатывания скролла
	//ignor = false;
	if (!interval)
		findButton->setEnabled(true);
	findIntervalButton->setEnabled(true);
	mainTable->resizeColumnsToContents();
	if (vect.empty())
		progressLabel->setText("Запрос не выдал результатов");
	else
		progressLabel->setText("Завершено");
	progressBar->setValue(230);
	//progressBar->setValue(0);
	progressBar->reset();
	writeLog("Finish showData");
	show = false;
}

void DBView::slideBarIsMoved(int n)
{
	if (ignor || inWork || interval)
		return;
	delta = 40;
	//ignor = true;

	if ((mainTable->verticalScrollBar()->value() > (numRecordsUp - delta)))
	{
		int nowCursor = mainTable->verticalScrollBar()->value(); //debug
		ignor = true;
		table_model->clear();
		createTableHeaders();
		blockDown += countStrings - delta;
		blockUp += countStrings - delta;
		vbar->setValue(blockDown);
		testTime.restart();
		if (multyFindOn)
			cur_thr->needDataBlock(blockDown, countStrings, &formValueList, 0, 1);
		else
			cur_thr->needDataBlock(blockDown, countStrings, &standartFindList, 0, 1);
		//ignor = false;
		return;
	}
	//ignor = true;

	if ((mainTable->verticalScrollBar()->value() <= numRecordsDown /*+ 1*/))
	{
		int nowCursor = mainTable->verticalScrollBar()->value(); //debug
		ignor = true;
		if (blockDown == 0)
		{
			ignor = false;
			return;
		}
		table_model->clear();
		createTableHeaders();
		blockDown -= countStrings - delta;
		blockUp -= countStrings - delta;
		cursorDown = true;
		if (blockDown < 0)
		{
			blockDown = 0;
			blockUp = countStrings;
			cursorDown = false;
		}
		vbar->setValue(blockDown);
		testTime.restart();
		if (multyFindOn)
			cur_thr->needDataBlock(blockDown, countStrings, &formValueList, 0, 0);
		else
			cur_thr->needDataBlock(blockDown, countStrings, &standartFindList, 0, 0);
	}
	//	ignor = false;
}

void DBView::slideVBarIsMoved(int n)
{
	QToolTip::showText(QCursor::pos(), QString::number(vbar->value()), vbar);
	if (ignor || inWork || interval)
		return;

	blockDown = vbar->value() - 100;
	blockUp = vbar->value() + 100;
	ignor = true;
	table_model->clear();
	createTableHeaders();
	if (blockDown < 0)
		blockDown = 0;
	writeLog("Запрос скроллбар");
	if (multyFindOn)
		cur_thr->needDataBlock(blockDown, countStrings, &formValueList, 0);
	else
		cur_thr->needDataBlock(blockDown, countStrings, &standartFindList, 0);
}

int DBView::getCount()
{
	return cur_thr->getCount();
}

void DBView::settingSUBD(bool show)
{
	dlg = new QDialog(this);
	dlg->setWindowTitle("Выбор коллекции");
	QDialogButtonBox* buttons = new QDialogButtonBox(dlg);

	buttons->addButton(QDialogButtonBox::Ok);
	buttons->addButton(QDialogButtonBox::Cancel);
	buttons->button(QDialogButtonBox::Ok)->setText("Использовать выбранную коллекцию");
	buttons->button(QDialogButtonBox::Cancel)->setText("Использовать последнюю коллекцию");
	collectionList = new QListView(this);
	QHBoxLayout *h_lay = new QHBoxLayout;
	h_lay->addWidget(buttons);

	QVBoxLayout *v_lay = new QVBoxLayout(dlg);
	v_lay->addWidget(collectionList);
	v_lay->addLayout(h_lay);

	cur_thr->needCollectionNames();

	std::list<std::string> coll_list = cur_thr->getCollList();
	QStringList qlistColls;
	for (int i = 0; i < coll_list.size(); i++)
	{
		qlistColls.push_back(QString::fromStdString(coll_list.front()));
		coll_list.pop_front();
		coll_list.push_back("");
	}

	model = new QStringListModel(this);

	model->setStringList(qlistColls);

	collectionList->setModel(model);
	collectionList->setEditTriggers(QAbstractItemView::NoEditTriggers);

	connect(collectionList, &QListView::doubleClicked, this, &DBView::collectionViewDoubleClicked);
	connect(buttons->button(QDialogButtonBox::Ok), SIGNAL(clicked()), this, SLOT(useCollection()));
	connect(buttons->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), this, SLOT(useLastCollection()));
	connect(this, SIGNAL(acc()), dlg, SLOT(accept()));
	if(show)
		dlg->exec();
	dlg->deleteLater();
}

void DBView::saveFilters()
{
	if (settings_dir_path.isEmpty())
		settings_dir_path = QCoreApplication::applicationDirPath() + "/" + "DB_settings.ini";
	QSettings *settings = new QSettings(settings_dir_path, QSettings::IniFormat, this);
	settings->remove("");
	settings->beginGroup("StandartSettigs");
	settings->setValue("MSK1Edit", MSK1Edit->text());
	settings->setValue("MSK2Edit", MSK2Edit->text());
	settings->setValue("BT1Edit", BT1Edit->text());
	settings->setValue("BT2Edit", BT2Edit->text());
	settings->setValue("ADREdit", ADREdit->text());
	settings->setValue("PADREdit", PADREdit->text());
	settings->setValue("CountSDEdit", CountSDEdit->text());
	settings->setValue("OKEdit", OKEdit->text());
	settings->setValue("SDEdit", SDEdit->toPlainText());
	settings->setValue("CWEdit", CWEdit->text());
	settings->setValue("AWEdit", AWEdit->text());
	settings->endGroup();
	settings->beginGroup("AllSettigs");
	settings->setValue("UTCEdit", UTCEdit->text());
	settings->setValue("UTCEdit2", UTCEdit2->text());
	settings->setValue("diffBTEdit", diffBTEdit->text());
	settings->setValue("diffBTEdit2", diffBTEdit2->text());
	settings->setValue("KU_SPOBUEdit", KU_SPOBUEdit->text());
	settings->setValue("KS_KPI2Edit", KS_KPI2Edit->text());

	settings->setValue("MMKOCheckBox1", MMKOCheckBox1->isChecked());
	settings->setValue("MMKOCheckBox2", MMKOCheckBox2->isChecked());
	settings->setValue("LMKOCheckBox1", LMKOCheckBox1->isChecked());
	settings->setValue("LMKOCheckBox2", LMKOCheckBox2->isChecked());
	settings->setValue("FormatCheckBox1", FormatCheckBox1->isChecked());
	settings->setValue("FormatCheckBox2", FormatCheckBox2->isChecked());
	settings->setValue("FormatCheckBox3", FormatCheckBox3->isChecked());
	settings->endGroup();

	settings->setValue("Forms/countForms", formValueList.size() - 1);

	for (int i = 0; i < formValueList.size(); i++)
	{
		settings->beginGroup(QString("multyFilter%1").arg(i));
		settings->setValue("MSK1", formValueList.at(i)["MSK1"]);
		settings->setValue("MSK2", formValueList.at(i)["MSK2"]);
		settings->setValue("BT1", formValueList.at(i)["BT1"]);
		settings->setValue("BT2", formValueList.at(i)["BT2"]);
		settings->setValue("addr", formValueList.at(i)["addr"]);
		settings->setValue("paddr", formValueList.at(i)["paddr"]);
		settings->setValue("countSD", formValueList.at(i)["countSD"]);
		settings->setValue("OK", formValueList.at(i)["OK"]);
		settings->setValue("SD", formValueList.at(i)["SD"]);
		settings->setValue("CW", formValueList.at(i)["CW"]);
		settings->setValue("AW", formValueList.at(i)["AW"]);

		settings->setValue("UTC1", formValueList.at(i)["UTC1"]);
		settings->setValue("UTC2", formValueList.at(i)["UTC2"]);
		settings->setValue("diffBT1", formValueList.at(i)["diffBT1"]);
		settings->setValue("diffBT2", formValueList.at(i)["diffBT2"]);
		settings->setValue("KU_SPOBU", formValueList.at(i)["KU_SPOBU"]);
		settings->setValue("KS_KPI2", formValueList.at(i)["KS_KPI2"]);
		settings->setValue("MMKO1", formValueList.at(i)["MMKO1"]);
		settings->setValue("MMKO2", formValueList.at(i)["MMKO2"]);
		settings->setValue("LMKO1", formValueList.at(i)["LMKO1"]);
		settings->setValue("LMKO2", formValueList.at(i)["LMKO2"]);
		settings->setValue("format1", formValueList.at(i)["format1"]);
		settings->setValue("format2", formValueList.at(i)["format2"]);
		settings->setValue("format3", formValueList.at(i)["format3"]);
		settings->endGroup();
	}
	settings->sync();
}

void DBView::loadFilters()
{
	if (settings_dir_path.isEmpty())
		settings_dir_path = QCoreApplication::applicationDirPath() + "/" + "DB_settings.ini";
	QSettings *settings = new QSettings(settings_dir_path, QSettings::IniFormat, this);
	settings->beginGroup("StandartSettigs");
	MSK1Edit->setText(settings->value("MSK1Edit", "").toString());
	MSK2Edit->setText(settings->value("MSK2Edit", "").toString());
	BT1Edit->setText(settings->value("BT1Edit", "").toString());
	BT2Edit->setText(settings->value("BT2Edit", "").toString());
	ADREdit->setText(settings->value("ADREdit", "").toString());
	PADREdit->setText(settings->value("PADREdit", "").toString());
	CountSDEdit->setText(settings->value("CountSDEdit", "").toString());
	OKEdit->setText(settings->value("OKEdit", "").toString());
	SDEdit->setText(settings->value("SDEdit", "").toString());
	settings->endGroup();
	settings->beginGroup("AllSettigs");
	UTCEdit->setText(settings->value("UTCEdit", "").toString());
	UTCEdit2->setText(settings->value("UTCEdit2", "").toString());
	diffBTEdit->setText(settings->value("diffBTEdit", "").toString());
	diffBTEdit2->setText(settings->value("diffBTEdit2", "").toString());
	CWEdit->setText(settings->value("CWEdit", "").toString());
	AWEdit->setText(settings->value("AWEdit", "").toString());
	KU_SPOBUEdit->setText(settings->value("KU_SPOBUEdit", "").toString());
	KS_KPI2Edit->setText(settings->value("KS_KPI2Edit", "").toString());

	MMKOCheckBox1->setChecked(settings->value("FormatCheckBox1", true).toBool());
	MMKOCheckBox2->setChecked(settings->value("MMKOCheckBox2", true).toBool());
	LMKOCheckBox1->setChecked(settings->value("LMKOCheckBox1", true).toBool());
	LMKOCheckBox2->setChecked(settings->value("LMKOCheckBox2", true).toBool());
	FormatCheckBox1->setChecked(settings->value("FormatCheckBox1", true).toBool());
	FormatCheckBox2->setChecked(settings->value("FormatCheckBox2", true).toBool());
	FormatCheckBox3->setChecked(settings->value("FormatCheckBox3", true).toBool());
	settings->endGroup();
	// удалить формочки, что присутствуют сейчас
	for (int i = currentFormIndex; i > 0; i--)
	{
		delFindForm(i);
	}
	//количество загружаемых формочек
	int countForms = settings->value("Forms/countForms", 1).toInt();
	//добавить новые формочки
	for (int i = 0; i < countForms; i++)
		addNewFindForm();
	//заполнить формочки
	for (int i = 0; i <= countForms; i++)
	{
		settings->beginGroup(QString("multyFilter%1").arg(i));

		formValueList[i].insert("MSK1", settings->value("MSK1").toString());
		formValueList[i].insert("MSK2", settings->value("MSK2").toString());
		formValueList[i].insert("BT1", settings->value("BT1").toString());
		formValueList[i].insert("BT2", settings->value("BT2").toString());
		formValueList[i].insert("addr", settings->value("addr").toString());
		formValueList[i].insert("paddr", settings->value("paddr").toString());
		formValueList[i].insert("countSD", settings->value("countSD").toString());
		formValueList[i].insert("OK", settings->value("OK").toString());
		formValueList[i].insert("SD", settings->value("SD").toString());
		formValueList[i].insert("CW", settings->value("CW").toString());
		formValueList[i].insert("AW", settings->value("AW").toString());
		formValueList[i].insert("UTC1", settings->value("UTC1").toString());
		formValueList[i].insert("UTC2", settings->value("UTC2").toString());
		formValueList[i].insert("diffBT1", settings->value("diffBT1").toString());
		formValueList[i].insert("diffBT2", settings->value("diffBT2").toString());
		formValueList[i].insert("KU_SPOBU", settings->value("KU_SPOBU").toString());
		formValueList[i].insert("KS_KPI2", settings->value("KS_KPI2").toString());

		formValueList[i].insert("MMKO1", settings->value("MMKO1").toBool());
		formValueList[i].insert("MMKO2", settings->value("MMKO2").toBool());
		formValueList[i].insert("LMKO1", settings->value("LMKO1").toBool());
		formValueList[i].insert("LMKO2", settings->value("LMKO2").toBool());
		formValueList[i].insert("format1", settings->value("format1").toBool());
		formValueList[i].insert("format2", settings->value("format2").toBool());
		formValueList[i].insert("format3", settings->value("format3").toBool());

		textFromlistToForm(i);
		settings->endGroup();
	}
}

void DBView::saveFiltersAs()
{
	settings_dir_path = QFileDialog::getSaveFileName(this, "Сохранить набор фильтров",
		settings_dir_path, "DB_settings(*.ini)");
	saveFilters();
}

void DBView::loadFiltersAs()
{
	settings_dir_path = QFileDialog::getOpenFileName(this, "Открыть набор фильтров",
		settings_dir_path, "DB_settings(*.ini)");
	loadFilters();
}

void DBView::addNewFindForm()
{
	bool addNew = true;
	if (findFormPosX == 4)
	{
		findFormPosX = 0;
		++findFormPosY;
	}
	if (findFormPosX == 3 && findFormPosY == 2)
		addNew = false;

	multyForm = new multyFindForm(widgMultyFind);
	if (!addNew)
		multyForm->hideAddButton();
	multyForm->index = multiFormList->size();
	if (multyForm->index == 0)
		multyForm->hideDelButton();
	multyForm->show();
	multyLayout->addWidget(multyForm, findFormPosX, findFormPosY/*, Qt::AlignLeft | Qt::AlignTop*/);
	++findFormPosX;
	multiFormList->append(multyForm);
	++currentFormIndex;
	multiFormList->at(currentFormIndex - 1)->hideAddButton();
	connect(multiFormList->at(currentFormIndex), &multyFindForm::addNewForm, this, &DBView::addNewFindForm);
	connect(multiFormList->at(currentFormIndex), &multyFindForm::delFormSignal, this, &DBView::delFindForm);
	connect(multiFormList->at(currentFormIndex), &multyFindForm::showDialogSignal, this, &DBView::showMultyDialog);

	formValueList.append(QMap<QString, QVariant>());
	
	formValueList[currentFormIndex].insert("MMKO1", true);
	formValueList[currentFormIndex].insert("MMKO2", true);
	formValueList[currentFormIndex].insert("LMKO1", true);
	formValueList[currentFormIndex].insert("LMKO2", true);
	formValueList[currentFormIndex].insert("format1", true);
	formValueList[currentFormIndex].insert("format2", true);
	formValueList[currentFormIndex].insert("format3", false);

}

void DBView::delFindForm(int curIndex)
{
	//int i = multyForm->index ;
	//delete multiFormList->at(multyForm->index);
	delete multiFormList->at(curIndex);
	multiFormList->removeAt(curIndex);
	multiFormList->at(multiFormList->size() - 1)->showAddButton();
	changeIndex(curIndex);
	//удалить соответствующую структуру формы
	formValueList.removeAt(curIndex);
}

void DBView::changeIndex(int curentIndex)
{
	for (curentIndex; curentIndex < multiFormList->size(); curentIndex++)
	{
		multiFormList->at(curentIndex)->index -= 1;
	}
	currentFormIndex = multiFormList->size() - 1;
	changePosFormList();
}

void DBView::changePosFormList()
{
	delete multyLayout;
	multyLayout = new QGridLayout(widgMultyFind);
	findFormPosX = 0;
	findFormPosY = 0;
	for (int i = 0; i < multiFormList->size(); i++)
	{
		if (findFormPosX == 4)
		{
			findFormPosX = 0;
			++findFormPosY;
		}
		multyLayout->addWidget(multiFormList->at(i), findFormPosX, findFormPosY);
		findFormPosX++;
	}
}

void DBView::showMultyDialog(int curIndex)
{
	indexForm = curIndex;
	multyDialog = new multyFindWidget();
	multyDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
	
	multyDialog->MSK1Edit->setText(formValueList.at(curIndex)["MSK1"].toString());
	multyDialog->MSK2Edit->setText(formValueList.at(curIndex)["MSK2"].toString());
	multyDialog->BT1Edit->setText(formValueList.at(curIndex)["BT1"].toString());
	multyDialog->BT2Edit->setText(formValueList.at(curIndex)["BT2"].toString());
	multyDialog->UTCEdit->setText(formValueList.at(curIndex)["UTC1"].toString());
	multyDialog->UTCEdit2->setText(formValueList.at(curIndex)["UTC2"].toString());
	multyDialog->diffBTEdit->setText(formValueList.at(curIndex)["diffBT1"].toString());
	multyDialog->diffBTEdit2->setText(formValueList.at(curIndex)["diffBT2"].toString());
	multyDialog->ADREdit->setText(formValueList.at(curIndex)["addr"].toString());
	multyDialog->PADREdit->setText(formValueList.at(curIndex)["paddr"].toString());
	multyDialog->CountSDEdit->setText(formValueList.at(curIndex)["countSD"].toString());
	multyDialog->OKEdit->setText(formValueList.at(curIndex)["OK"].toString());
	multyDialog->SDEdit->setText(formValueList.at(curIndex)["SD"].toString());
	multyDialog->CWEdit->setText(formValueList.at(curIndex)["CW"].toString());
	multyDialog->AWEdit->setText(formValueList.at(curIndex)["AW"].toString());
	multyDialog->KU_SPOBUEdit->setText(formValueList.at(curIndex)["KU_SPOBU"].toString());
	multyDialog->KS_KPI2Edit->setText(formValueList.at(curIndex)["KS_KPI2"].toString());

	multyDialog->MMKOCheckBox1->setChecked(formValueList.at(curIndex)["MMKO1"].toBool());
	multyDialog->MMKOCheckBox2->setChecked(formValueList.at(curIndex)["MMKO2"].toBool());
	multyDialog->LMKOCheckBox1->setChecked(formValueList.at(curIndex)["LMKO1"].toBool());
	multyDialog->LMKOCheckBox2->setChecked(formValueList.at(curIndex)["LMKO2"].toBool());
	multyDialog->FormatCheckBox1->setChecked(formValueList.at(curIndex)["format1"].toBool());
	multyDialog->FormatCheckBox2->setChecked(formValueList.at(curIndex)["format2"].toBool());
	multyDialog->FormatCheckBox3->setChecked(formValueList.at(curIndex)["format3"].toBool());

	connect(multyDialog, &multyFindWidget::okButtonSignal, this, &DBView::setMultyQueryString);
	multyDialog->show();
	//multyDialog->exec();
	//multyDialog->close();

	//delete multyDialog;
}

void DBView::setMultyQueryString()
{
	formValueList[indexForm].clear();
	if (multyDialog->MSK1Edit->text() != ".. ::.")
		formValueList[indexForm].insert("MSK1", multyDialog->MSK1Edit->text());
	if (multyDialog->MSK2Edit->text() != ".. ::.")
		formValueList[indexForm].insert("MSK2", multyDialog->MSK2Edit->text());
	if (multyDialog->BT1Edit->text() != "::")
		formValueList[indexForm].insert("BT1", multyDialog->BT1Edit->text());
	if (multyDialog->BT2Edit->text() != "::")
		formValueList[indexForm].insert("BT2", multyDialog->BT2Edit->text());

	if (multyDialog->UTCEdit->text() != ".. ::")
		formValueList[indexForm].insert("UTC1", multyDialog->UTCEdit->text());

	if (multyDialog->UTCEdit2->text() != ".. ::")
		formValueList[indexForm].insert("UTC2", multyDialog->UTCEdit2->text());

	if (!multyDialog->diffBTEdit->text().isEmpty())
		formValueList[indexForm].insert("diffBT1", multyDialog->diffBTEdit->text());
	if (!multyDialog->diffBTEdit2->text().isEmpty())
		formValueList[indexForm].insert("diffBT2", multyDialog->diffBTEdit2->text());
	if (!multyDialog->ADREdit->text().isEmpty())
		formValueList[indexForm].insert("addr", multyDialog->ADREdit->text());
	if (!multyDialog->PADREdit->text().isEmpty())
		formValueList[indexForm].insert("paddr", multyDialog->PADREdit->text());
	if (!multyDialog->CountSDEdit->text().isEmpty())
		formValueList[indexForm].insert("countSD", multyDialog->CountSDEdit->text());
	if (!multyDialog->OKEdit->text().isEmpty())
		formValueList[indexForm].insert("OK", multyDialog->OKEdit->text());
	QString tmp_sd_string = multyDialog->SDEdit->toPlainText();
	if (!multyDialog->SDEdit->toPlainText().isEmpty())
		formValueList[indexForm].insert("SD", multyDialog->SDEdit->toPlainText());
	//formValueList[indexForm].insert("SD.replace(" ", ""));
	if (!multyDialog->CWEdit->text().isEmpty())
		formValueList[indexForm].insert("CW", multyDialog->CWEdit->text());
	if (!multyDialog->AWEdit->text().isEmpty())
		formValueList[indexForm].insert("AW", multyDialog->AWEdit->text());
	if (!multyDialog->KU_SPOBUEdit->text().isEmpty())
		formValueList[indexForm].insert("KU_SPOBU", multyDialog->KU_SPOBUEdit->text());
	if (!multyDialog->KS_KPI2Edit->text().isEmpty())
		formValueList[indexForm].insert("KS_KPI2", multyDialog->KS_KPI2Edit->text());
	if (!multyDialog->MMKOCheckBox1->text().isEmpty())
		formValueList[indexForm].insert("MMKO1", multyDialog->MMKOCheckBox1->isChecked());
	if (!multyDialog->MMKOCheckBox2->text().isEmpty())
		formValueList[indexForm].insert("MMKO2", multyDialog->MMKOCheckBox2->isChecked());
	if (!multyDialog->LMKOCheckBox1->text().isEmpty())
		formValueList[indexForm].insert("LMKO1", multyDialog->LMKOCheckBox1->isChecked());
	if (!multyDialog->LMKOCheckBox2->text().isEmpty())
		formValueList[indexForm].insert("LMKO2", multyDialog->LMKOCheckBox2->isChecked());
	if (!multyDialog->FormatCheckBox1->text().isEmpty())
		formValueList[indexForm].insert("format1", multyDialog->FormatCheckBox1->isChecked());
	if (!multyDialog->FormatCheckBox2->text().isEmpty())
		formValueList[indexForm].insert("format2", multyDialog->FormatCheckBox2->isChecked());
	if (!multyDialog->FormatCheckBox3->text().isEmpty())
		formValueList[indexForm].insert("format3", multyDialog->FormatCheckBox3->isChecked());

	textFromlistToForm(indexForm);
	multyDialog->close();
	delete multyDialog;
}

void DBView::textFromlistToForm(int index)
{
	QString formStr;

	if (formValueList.at(index).contains("MSK1"))
		formStr += "MSK1:" + formValueList.at(index)["MSK1"].toString() + "; ";
	if (formValueList.at(index).contains("MSK2"))
		formStr += "MSK2:" + formValueList.at(index)["MSK2"].toString() + "; ";
	if (formValueList.at(index).contains("BT1"))
		formStr += "БВ1:" + formValueList.at(index)["BT1"].toString() + "; ";
	if (formValueList.at(index).contains("BT2"))
		formStr += "БВ2:" + formValueList.at(index)["BT2"].toString() + "; ";
	if (formValueList.at(index).contains("UTC1"))
		formStr += "UTC1:" + formValueList.at(index)["UTC1"].toString() + "; ";
	if (formValueList.at(index).contains("UTC2"))
		formStr += "UTC2:" + formValueList.at(index)["UTC2"].toString() + "; ";
	if (formValueList.at(index).contains("diffBT1"))
		formStr += "Расхождение1:" + formValueList.at(index)["diffBT1"].toString() + "; ";
	if (formValueList.at(index).contains("diffBT2"))
		formStr += "Расхождение2:" + formValueList.at(index)["diffBT2"].toString() + "; ";
	if (formValueList.at(index).contains("addr"))
		formStr += "Адрес:" + formValueList.at(index)["addr"].toString() + "; ";
	if (formValueList.at(index).contains("paddr"))
		formStr += "Подадрес:" + formValueList.at(index)["paddr"].toString() + "; ";
	if (formValueList.at(index).contains("countSD"))
		formStr += "Количество СД:" + formValueList.at(index)["countSD"].toString() + "; ";
	if (formValueList.at(index).contains("OK"))
		formStr += "№ОК:" + formValueList.at(index)["OK"].toString() + "; ";
	if (formValueList.at(index).contains("SD"))
		formStr += "СД:" + formValueList.at(index)["SD"].toString() + "; ";
	if (formValueList.at(index).contains("CW"))
		formStr += "КС:" + formValueList.at(index)["CW"].toString() + "; ";
	if (formValueList.at(index).contains("AW"))
		formStr += "ОС:" + formValueList.at(index)["AW"].toString() + "; ";
	if (formValueList.at(index).contains("KU_SPOBU"))
		formStr += "КУ СПО-БУ:" + formValueList.at(index)["KU_SPOBU"].toString() + "; ";
	if (formValueList.at(index).contains("KS_KPI2"))
		formStr += "КС КПИ2:" + formValueList.at(index)["KS_KPI2"].toString() + "; ";

	multiFormList->at(index)->setTextToEdit(formStr);
}

QList<QMap<QString, QVariant> > DBView::getQueryStruct()
{

	for (TM_name_widget *wdg : TM_widgets->toStdList())
		if (stackWidget->currentWidget() == wdg)
			return wdg->getQueryStruct();


	QList<QMap<QString, QVariant> > list;
	QMap<QString, QVariant> tmp_query;
	list.append(tmp_query);
	if (MSK1Edit->text() != ".. ::.")
		list[0].insert("MSK1", MSK1Edit->text());
	if (MSK2Edit->text() != ".. ::.")
		list[0].insert("MSK2", MSK2Edit->text());
	if (BT1Edit->text() != "::")
		list[0].insert("BT1", BT1Edit->text());
	if (BT2Edit->text() != "::")
		list[0].insert("BT2", BT2Edit->text());

	if (stackWidget->currentWidget() == widgStandartFind || stackWidget->currentWidget() == widgMultyFind)
	{
		if (UTCEdit->text() != ".. ::")
			list[0].insert("UTC1", UTCEdit->text());
		if (UTCEdit2->text() != ".. ::")
			list[0].insert("UTC1", UTCEdit2->text());

		if (!(ADREdit->text().isEmpty()))
			list[0].insert("addr", ADREdit->text());
		if (!(PADREdit->text().isEmpty()))
			list[0].insert("paddr", PADREdit->text());
		if (!(CountSDEdit->text().isEmpty()))
			list[0].insert("countSD", CountSDEdit->text());
		if (!(SDEdit->toPlainText().isEmpty()))
			list[0].insert("SD", SDEdit->toPlainText());//.replace(" ", "");
		if (!(OKEdit->text().isEmpty()))
			list[0].insert("OK", OKEdit->text());

		if (allFilters)
		{
			if (!CWEdit->text().isEmpty())
				list[0].insert("CW", CWEdit->text());

			if (!AWEdit->text().isEmpty())
				list[0].insert("AW", AWEdit->text());

			if (!KU_SPOBUEdit->text().isEmpty())
				list[0].insert("KU_SPOBU", KU_SPOBUEdit->text());

			if (!KS_KPI2Edit->text().isEmpty())
				list[0].insert("KS_KPI2", KS_KPI2Edit->text());


			list[0].insert("MMKO1", MMKOCheckBox1->isChecked());
			list[0].insert("MMKO2", MMKOCheckBox2->isChecked());
			list[0].insert("LMKO1", LMKOCheckBox1->isChecked());
			list[0].insert("LMKO2", LMKOCheckBox2->isChecked());
			list[0].insert("format1", FormatCheckBox1->isChecked());
			list[0].insert("format2", FormatCheckBox2->isChecked());
			list[0].insert("format3", FormatCheckBox3->isChecked());
		}
	}

	if (stackWidget->currentWidget() == DB_IKwidg)
	{
		if (!(nStartFrameEdit->text().isEmpty()))
			list[0].insert("startFrame", nStartFrameEdit->text());
		if (!(nEndFrameEdit->text().isEmpty()))
			list[0].insert("endFrame", nEndFrameEdit->text());


		list[0].insert("pi15", (FormatIK15->styleSheet() != ""));
		list[0].insert("pi8", (FormatIK8->styleSheet() != ""));
		list[0].insert("vtf", (FormatIKWTF8->styleSheet() != ""));

		if (MD_CBK_ComboBox->currentText()!="Все")
			list[0].insert("mdcbk", MD_CBK_ComboBox->currentText());
	}

	return list;
}

void DBView::ipForMon()
{
	dlg = new QDialog(this);
	QDialogButtonBox* buttons = new QDialogButtonBox(dlg);
	dlg->setWindowTitle("IP Монитора");
	buttons->addButton(QDialogButtonBox::Ok);
	buttons->button(QDialogButtonBox::Ok)->setFlat(true);
	QPixmap pix(":img/ok.png");
	buttons->button(QDialogButtonBox::Ok)->setIcon(pix);
	buttons->button(QDialogButtonBox::Ok)->setIconSize(QSize(30, 29));
	buttons->button(QDialogButtonBox::Ok)->setText("");

	buttons->addButton(QDialogButtonBox::Cancel);
	buttons->button(QDialogButtonBox::Cancel)->setFlat(true);
	QPixmap pix2(":img/cancel.png");
	buttons->button(QDialogButtonBox::Cancel)->setIcon(pix2);
	buttons->button(QDialogButtonBox::Cancel)->setIconSize(QSize(30, 29));
	buttons->button(QDialogButtonBox::Cancel)->setText("");
	

	QLabel ipMKOLabel;
	ipMKOLabel.setText("MKO");
	QLabel ipAIKLabel;
	ipAIKLabel.setText("AIK");
	QLabel ipMCALabel;
	ipMCALabel.setText("MCA");
	QLabel ipFrameLabel;
	ipFrameLabel.setText("Frame");
	QLabel ipGrathLabel;
	ipGrathLabel.setText("График");

	QLineEdit ipMKOEdit(ip_map.value("MKO"));
	ipMKOEdit.setMaximumHeight(25);
	ipMKOEdit.setMaximumWidth(180);
	QLineEdit ipAIKEdit(ip_map.value("AIK"));
	ipAIKEdit.setMaximumHeight(25);
	ipAIKEdit.setMaximumWidth(180);
	QLineEdit ipMCAEdit(ip_map.value("MCA"));
	ipMCAEdit.setMaximumHeight(25);
	ipMCAEdit.setMaximumWidth(180);
	QLineEdit ipFrameEdit(ip_map.value("Frame"));
	ipFrameEdit.setMaximumHeight(25);
	ipFrameEdit.setMaximumWidth(180);


	QHBoxLayout *h_ip_lay1 = new QHBoxLayout;
	h_ip_lay1->addWidget(&ipMKOLabel);
	h_ip_lay1->addWidget(&ipMKOEdit);
	QHBoxLayout *h_ip_lay2 = new QHBoxLayout;
	h_ip_lay2->addWidget(&ipAIKLabel);
	h_ip_lay2->addWidget(&ipAIKEdit);
	QHBoxLayout *h_ip_lay3 = new QHBoxLayout;
	h_ip_lay3->addWidget(&ipMCALabel);
	h_ip_lay3->addWidget(&ipMCAEdit);
	QHBoxLayout *h_ip_lay4 = new QHBoxLayout;
	h_ip_lay4->addWidget(&ipFrameLabel);
	h_ip_lay4->addWidget(&ipFrameEdit);

	QHBoxLayout *h_lay = new QHBoxLayout;
	h_lay->addWidget(buttons);
	
	QVBoxLayout *v_lay = new QVBoxLayout(dlg);

	v_lay->addLayout(h_ip_lay1);
	v_lay->addLayout(h_ip_lay2);
	v_lay->addLayout(h_ip_lay3);
	v_lay->addLayout(h_ip_lay4);
	
	v_lay->addLayout(h_lay);
	connect(buttons->button(QDialogButtonBox::Ok), SIGNAL(clicked()), dlg, SLOT(accept()));
	connect(buttons->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), dlg, SLOT(reject()));
	ipMKOEdit.setFocus();
	if (dlg->exec() == QDialog::Accepted)
	{
		//mon_ip = ip.text();
		ip_map.insert("MKO", ipMKOEdit.text());
		ip_map.insert("AIK", ipAIKEdit.text());
		ip_map.insert("MCA", ipMCAEdit.text());
		ip_map.insert("Frame", ipFrameEdit.text());
		
		settingSUBD(false);		// Вызов без отображения,
		useLastCollection();	// что бы всё проиницилизироваллось и грузанулись таблицы с удалённой машины
		settingSUBD();			// Вызов с отображением, уже будут таблицы с удалённой машины

	}
	dlg->deleteLater();
}

void DBView::changeProgressLabel(int value, QString msg)
{
	progressBar->setValue(value);
	progressLabel->setText(msg);
	QApplication::processEvents();
}

void DBView::SDTextChange()		
{
	QString text = SDEdit->toPlainText();
	QStringList list = text.split(" ");
	for(QString str : list)
	{
		if (!QRegExp("^[0-9A-Fa-f]{4}$").exactMatch(str) && !QRegExp("^[!][0-9A-Fa-f]{4}$").exactMatch(str)
			&& !QRegExp("^[x]{4}$").exactMatch(str) && !QRegExp("^[0-9A-Fa-f]{4}[m][0-9A-Fa-f]{4}$").exactMatch(str))
		{
			SDEdit->setStyleSheet("QTextEdit { background: rgb(255, 128, 128);}");
			return;
		}
	}
	SDEdit->setStyleSheet("");
}

void DBView::MSKTextCheck(QString str)
{
	QObject *obj = QObject::sender();
	QLineEdit *ld = qobject_cast<QLineEdit *>(obj);
	if (str == ".. ::." || str == "::")
	{
		ld->setStyleSheet("");
		return;
	}
	if (obj == MSK1Edit || obj == MSK2Edit || obj == UTCEdit || obj == UTCEdit2)
	{
		if (QDateTime::fromString(ld->text(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch() == 0)
		{
			int d = QDateTime::fromString(MSK1Edit->text(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			ld->setStyleSheet("QLineEdit { background: rgb(255, 128, 128);}");
		}
		else
			ld->setStyleSheet("");
	}
	if (obj == BT1Edit || obj == BT2Edit)
	{
		if (QDateTime::fromString(ld->text(), "hh:mm:ss").toMSecsSinceEpoch() == 0)
		{
			ld->setStyleSheet("QLineEdit { background: rgb(255, 128, 128);}");
		}
		else
			ld->setStyleSheet("");
	}
}

void DBView::foundManager()
{
	mainStackWidget->setCurrentWidget(widg);
	this->setWindowTitle("Панель управления СУБД - Менеджер поиска");
}

void DBView::grathicManager()
{
	mainStackWidget->setCurrentWidget(grathWidg);
	this->setWindowTitle("Панель управления СУБД - Менеджер графиков");
}

void DBView::narabotkaManager()
{
	mainStackWidget->setCurrentWidget(narabotkaWidg);
	this->setWindowTitle("Панель управления СУБД - Менеджер наработки");
}

void DBView::setDB_TM()
{
	int btn_TM_ID = sender()->property("btn_TM_ID").toInt();

	DB_MKO_button->setEnabled(true);
	DB_IK_button->setEnabled(true);
	for (QPushButton *b : TM_buttons->toStdList())
		b->setEnabled(true);
	TM_buttons->at(btn_TM_ID)->setEnabled(false);
	
	table_model->set_db(db_names.at(btn_TM_ID).engName.toUpper());

	if (cur_thr)
	{
		disconnect(cur_thr, &DBReaderThread::finishedRead, this, &DBView::showData);
		disconnect(cur_thr, &DBReaderThread::error, this, &DBView::error);
		disconnect(cur_thr, &DBReaderThread::changeProgressMsg, this, &DBView::changeProgressLabel);
	}

	cur_thr = thr_map[db_names.at(btn_TM_ID).engName.toUpper()];

	connect(cur_thr, &DBReaderThread::finishedRead, this, &DBView::showData);
	connect(cur_thr, &DBReaderThread::error, this, &DBView::error);
	connect(cur_thr, &DBReaderThread::changeProgressMsg, this, &DBView::changeProgressLabel);


	//grathWidg->connectToDB("MCA");
	uncheckedMenuDB();
	TM_menus->at(btn_TM_ID)->menuAction()->setVisible(true);
	TM_buttons->at(btn_TM_ID)->setStyleSheet("background-color: rgb(149, 188, 242)");

	//stackWidget->setCurrentWidget(DB_TM_MCAwidg);
	stackWidget->setCurrentWidget(TM_widgets->at(btn_TM_ID));
	table_model->clear();
	createTableHeaders();
}

void DBView::setDB_MKO()
{
	table_model->set_db("MKO");
	if (cur_thr)
	{
		disconnect(cur_thr, &DBReaderThread::finishedRead, this, &DBView::showData);
		disconnect(cur_thr, &DBReaderThread::error, this, &DBView::error);
		disconnect(cur_thr, &DBReaderThread::changeProgressMsg, this, &DBView::changeProgressLabel);
	}

	cur_thr = thr_map["MKO"];

	connect(cur_thr, &DBReaderThread::finishedRead, this, &DBView::showData);
	connect(cur_thr, &DBReaderThread::error, this, &DBView::error);
	connect(cur_thr, &DBReaderThread::changeProgressMsg, this, &DBView::changeProgressLabel);


	DB_IK_button->setEnabled(true);
	DB_MKO_button->setEnabled(false);	
	for (QPushButton *b : TM_buttons->toStdList())
		b->setEnabled(true);
	//grathWidg->connectToDB("MKO");
	uncheckedMenuDB();
	settingsMenuMKO->menuAction()->setVisible(true);
	widgStandartFind->setMaximumHeight(1000);
	DB_MKO_button->setStyleSheet("background-color: rgb(149, 188, 242)");

	stackWidget->setCurrentWidget(widgStandartFind);
	table_model->clear();
	createTableHeaders();

	multyFindButton->show();
	findQueryButton->show();

	if (h_filterMKOLayout->count() != 0)
		h_filterMKOLayout->removeItem(h_filterMKOLayout->takeAt(4));	// Удаляю SpaceItem, что бы лайаут не уезжал

	DB_MKOmini1GridLayout->addWidget(MSKLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(MSK1Edit, 1, 0);
	DB_MKOmini1GridLayout->addWidget(MSK2Edit, 1, 1);
	DB_MKOmini1GridLayout->addWidget(BTLabel, 2, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(BT1Edit, 3, 0);
	DB_MKOmini1GridLayout->addWidget(BT2Edit, 3, 1);
	DB_MKOmini1GridLayout->addWidget(UTCLabel, 4, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(UTCEdit, 5, 0);
	DB_MKOmini1GridLayout->addWidget(UTCEdit2, 5, 1);
	DB_MKOmini1GridLayout->addWidget(diffBTLabel, 6, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(diffBTEdit, 7, 0);
	DB_MKOmini1GridLayout->addWidget(diffBTEdit2, 7, 1);
	DB_MKOmini1GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 8, 6);

	DB_MKOmini2GridLayout->addWidget(MMKOGroupBox, 0, 0, 2, 1, Qt::AlignCenter);
	DB_MKOmini2GridLayout->addWidget(LMKOGroupBox, 0, 1, 2, 1, Qt::AlignCenter);
	DB_MKOmini2GridLayout->addWidget(FormatGroupBox, 0, 2, 4, 1, Qt::AlignLeft);
	DB_MKOmini2GridLayout->addWidget(CWLabel, 2, 0, Qt::AlignHCenter);
	DB_MKOmini2GridLayout->addWidget(CWEdit, 3, 0);
	DB_MKOmini2GridLayout->addWidget(AWLabel, 2, 1, Qt::AlignHCenter);
	DB_MKOmini2GridLayout->addWidget(AWEdit, 3, 1);
	DB_MKOmini2GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	DB_MKOmini3GridLayout->addWidget(ADRLabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(ADREdit, 1, 0);
	DB_MKOmini3GridLayout->addWidget(PADRLabel, 0, 1, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(PADREdit, 1, 1);
	DB_MKOmini3GridLayout->addWidget(CountSDLabel, 0, 2, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(CountSDEdit, 1, 2);

	DB_MKOmini3GridLayout->addWidget(SDLabel, 2, 0, 1, 3, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(SDEdit, 3, 0, 3, 3);
	DB_MKOmini3GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	DB_MKOmini4GridLayout->addWidget(OKLabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini4GridLayout->addWidget(OKEdit, 1, 0);

	DB_MKOmini5GridLayout->addWidget(KU_SPOBULabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini5GridLayout->addWidget(KU_SPOBUEdit, 1, 0);
	DB_MKOmini5GridLayout->addWidget(KS_KPI2Label, 0, 1, Qt::AlignHCenter);
	DB_MKOmini5GridLayout->addWidget(KS_KPI2Edit, 1, 1);
	DB_MKOmini5GridLayout->addWidget(MD_CBKLabel, 0, 2, Qt::AlignHCenter);
	DB_MKOmini5GridLayout->addWidget(MD_CBKEdit, 1, 2);

	DB_MKOmini6GridLayout->addWidget(metkaLabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini6GridLayout->addWidget(metkaEdit, 1, 0);

	filter1MKOGroupBox->setLayout(DB_MKOmini1GridLayout);
	filter2MKOGroupBox->setLayout(DB_MKOmini2GridLayout);
	filter3MKOGroupBox->setLayout(DB_MKOmini3GridLayout);
	filter4MKOGroupBox->setLayout(DB_MKOmini4GridLayout);
	filter5MKOGroupBox->setLayout(DB_MKOmini5GridLayout);
	filter6MKOGroupBox->setLayout(DB_MKOmini6GridLayout);

	DB_MKOmini7GridLayout->addWidget(filter4MKOGroupBox, 0, 0);
	DB_MKOmini7GridLayout->addWidget(filter5MKOGroupBox, 1, 0);
	DB_MKOmini7GridLayout->addWidget(filter6MKOGroupBox, 2, 0);

	filter7MKOGroupBox->setLayout(DB_MKOmini7GridLayout);

	h_filterMKOLayout->addWidget(filter1MKOGroupBox);
	h_filterMKOLayout->addWidget(filter2MKOGroupBox);
	h_filterMKOLayout->addWidget(filter3MKOGroupBox);
	h_filterMKOLayout->addWidget(filter7MKOGroupBox);
	h_filterMKOLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));

}

void DBView::setDB_IK()
{
	DB_IK_button->setEnabled(false);
	DB_MKO_button->setEnabled(true);
	for (QPushButton *b : TM_buttons->toStdList())
		b->setEnabled(true);
	//table_model->set_db("IK");
	table_model->set_db("Frame");
	if (cur_thr)
	{
		disconnect(cur_thr, &DBReaderThread::finishedRead, this, &DBView::showData);
		disconnect(cur_thr, &DBReaderThread::error, this, &DBView::error);
		disconnect(cur_thr, &DBReaderThread::changeProgressMsg, this, &DBView::changeProgressLabel);
	}

	cur_thr = thr_map["Frame"];

	connect(cur_thr, &DBReaderThread::finishedRead, this, &DBView::showData);
	connect(cur_thr, &DBReaderThread::error, this, &DBView::error);
	connect(cur_thr, &DBReaderThread::changeProgressMsg, this, &DBView::changeProgressLabel);

	uncheckedMenuDB();
	stackWidget->setMaximumWidth(1200);
	settingsMenuIK->menuAction()->setVisible(true);
	DB_IKwidg->setMaximumHeight(1000);
	DB_IK_button->setStyleSheet("background-color: rgb(149, 188, 242)");

	stackWidget->setCurrentWidget(DB_IKwidg);
	table_model->clear();
	createTableHeaders();

	translateToQ10Button->show();
	IKViewButton->show();
	OK_open_Button->show();

	if (h_filterIKLayout->count() != 0)
		h_filterIKLayout->removeItem(h_filterIKLayout->takeAt(3));	// Удаляю SpaceItem, что бы лайаут не уезжал

	DB_IKmini1GridLayout->addWidget(MSKLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	DB_IKmini1GridLayout->addWidget(MSK1Edit, 1, 0);
	DB_IKmini1GridLayout->addWidget(MSK2Edit, 1, 1);
	DB_IKmini1GridLayout->addWidget(BTLabel, 2, 0, 1, 2, Qt::AlignHCenter);
	DB_IKmini1GridLayout->addWidget(BT1Edit, 3, 0);
	DB_IKmini1GridLayout->addWidget(BT2Edit, 3, 1);
	DB_IKmini1GridLayout->addWidget(nomerIKLabel, 4, 0, 1, 2, Qt::AlignHCenter);
	DB_IKmini1GridLayout->addWidget(nStartFrameEdit, 5, 0);
	DB_IKmini1GridLayout->addWidget(nEndFrameEdit, 5, 1);

	DB_IKmini1GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);
	modeLayout->addWidget(FormatIK15);
	modeLayout->addWidget(FormatIK8);
	modeLayout->addWidget(FormatIKWTF8);
	modeGroupBox->setLayout(modeLayout);
	DB_IKmini2GridLayout->addWidget(modeGroupBox, 0, 0, 1, 2, Qt::AlignCenter);
	DB_IKmini2GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);
	mdLayout->addWidget(MD_CBK_Label, 0, 0, Qt::AlignCenter);
	mdLayout->addWidget(DTK_Label, 0, 1, Qt::AlignCenter);
	mdLayout->addWidget(Kvitancii_Label, 0, 2, Qt::AlignCenter);
	mdLayout->addWidget(MD_CBK_ComboBox, 1, 0);
	mdLayout->addWidget(DTK_ComboBox, 1, 1);
	mdLayout->addWidget(Kvitancii_ComboBox, 1, 2);
	DB_IKmini2GridLayout->addLayout(mdLayout, 1, 0, 1, 2, Qt::AlignCenter);

	DB_IKmini2GridLayout->addWidget(KFKlabel, 2, 0, 1, 2, Qt::AlignCenter);
	DB_IKmini2GridLayout->addWidget(KFKEdit1, 3, 0);
	DB_IKmini2GridLayout->addWidget(KFKEdit2, 3, 1);

	DB_IKmini3GridLayout->addWidget(filePathLabel, 0, 0, Qt::AlignHCenter);
	DB_IKmini3GridLayout->addWidget(filePathEdit, 1, 0);
	DB_IKmini3GridLayout->addWidget(OK_open_Button, 2, 0, 1, 2, Qt::AlignCenter);

	DB_IKmini3GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	filter1IkGroupBox->setLayout(DB_IKmini1GridLayout);
	filter2IkGroupBox->setLayout(DB_IKmini2GridLayout);
	filter3IkGroupBox->setLayout(DB_IKmini3GridLayout);

	h_filterIKLayout->addWidget(filter1IkGroupBox);
	h_filterIKLayout->addWidget(filter2IkGroupBox);
	h_filterIKLayout->addWidget(filter3IkGroupBox);
	h_filterIKLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));
	mainTable->resizeColumnsToContents();
	mainTable->hideColumn(14);	// Скрыть колонку "Путь к кадру"
}

void DBView::uncheckedMenuDB()
{
	DB_MKO_button->setStyleSheet("");
	DB_IK_button->setStyleSheet("");
	for (QPushButton *b : TM_buttons->toStdList())
		b->setStyleSheet("");
	
	setDefaultFilters();
	translateToQ10Button->hide();
	IKViewButton->hide();
	OK_open_Button->hide();
	multyFindButton->hide();
	findQueryButton->hide();
	multyFindOn = true;
	multyFind();
	hideColumnList->clear();
	widgStandartFind->setMaximumHeight(100);
	DB_IKwidg->setMaximumHeight(100);
	settingsMenuMKO->menuAction()->setVisible(false);
	settingsMenuIK->menuAction()->setVisible(false);

	for(auto menu : TM_menus->toStdList())
		menu->menuAction()->setVisible(false);

	mainTable->showColumn(14);	//В БД ИК эта колонка скрывалась
	stackWidget->setMaximumWidth(1980);
}

void DBView::onTableClicked(const QModelIndex &index)
{
	if (stackWidget->currentWidget() == widgStandartFind || stackWidget->currentWidget() == widgMultyFind)	// MKO
	{

		if (index.column() == 9)
		{
			QModelIndex tmp_index = table_model->index(index.row(), index.column() - 1);
			QString countSD = tmp_index.data().toString();
			if (countSD == "0")
				countSD = "32";
			tmp_index = table_model->index(index.row(), index.column() - 2);
			QString padr = tmp_index.data().toString();
			tmp_index = table_model->index(index.row(), index.column() - 3);
			QString adr = tmp_index.data().toString();
			tmp_index = table_model->index(index.row(), index.column());
			QString SD = tmp_index.data(44).toString();
			OK_dlg(adr, padr, SD, countSD, "OK");
			
		}
	}

	if (stackWidget->currentWidget() == DB_IKwidg)	// IK
	{
		if (index.column() == 7)
		{
			QModelIndex tmp_index = table_model->index(index.row(), index.column() - 1);
			QString mode = tmp_index.data().toString();

			tmp_index = table_model->index(index.row(), index.column() + 7);
			QString file_name = tmp_index.data().toString();
			qDebug() << file_name;
			file_name.remove(0, 2);
			QString frame_path = QString("%1//%2").arg(remote_frame_path).arg(file_name);
			QFile frame_file(frame_path);
			frame_file.open(QIODevice::ReadOnly);
			QByteArray ba = frame_file.readAll();
			frame_file.close();

			dlg = new QDialog(this, Qt::WindowSystemMenuHint);
			QPushButton *okBut = new QPushButton(dlg);
			okBut->setFlat(true);
			QPixmap pix(":img/ok.png");
			okBut->setIcon(pix);
			okBut->setIconSize(QSize(30, 29));
			connect(okBut, &QPushButton::clicked, dlg, &QDialog::deleteLater);
			QVBoxLayout *v_lay = new QVBoxLayout(dlg);
			QTableView *MDTable = new QTableView(dlg);
			MDTable->setAlternatingRowColors(true); //теперь строки зеброй красит
			MDTable->setStyleSheet(instr::get_table_style());
			MDTable->horizontalHeader()->setStyleSheet("QHeaderView::section {background-color: rgb(204, 204, 204);};  ");
			MyModel *MDTable_model = new MyModel(dlg);
			MDTable->setModel(MDTable_model);
			MDTable_model->setHorizontalHeaderLabels(QStringList() << "БВ" << "МД ЦБК");

			QList<QStandardItem*> st_list;

			MDTable->verticalHeader()->hide();
			MDTable->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
			MDTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
			MDTable->resizeColumnsToContents();
			MDTable->resizeRowsToContents();
			int iWidth = MDTable->verticalHeader()->sizeHint().width();
			for (int i = 0; i < MDTable_model->columnCount(); i++)
				iWidth += MDTable->horizontalHeader()->sectionSize(i);
			int iHeight = MDTable->horizontalHeader()->sizeHint().height();;
			for (int i = 0; i < MDTable_model->rowCount(); i++)
				iHeight += MDTable->verticalHeader()->sectionSize(i);
			MDTable->adjustSize();
			MDTable->horizontalHeader()->adjustSize();

			if (MDTable_model->rowCount() > 25)
			{
				MDTable->setMinimumHeight(780);
				iWidth += MDTable->verticalScrollBar()->sizeHint().width();
			}
			else
				MDTable->setMinimumHeight(iHeight + 2);	// +2 
			MDTable->setMinimumWidth(iWidth);

			v_lay->addWidget(MDTable);

			v_lay->addWidget(okBut, 0, Qt::AlignRight);
			
			dlg->setWindowTitle("Анализ МД ЦБК");
			connect(MDTable, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(MDCBKClicked(const QModelIndex &)));

			
			dlg->show();
		}
	}
}

void DBView::MDCBKClicked(const QModelIndex &index)
{

	if (index.column() == 0)
		return;
	QModelIndex tmp_index = index.model()->index(index.row(), 0);
	QString ttt = tmp_index.data().toString();
	ttt.remove(0, 1);
	ttt.remove(ttt.size()-1, 1);
	QTime tmp_time = QTime::fromString(ttt, "hh:mm:ss");
	int bv = QTime(0,0,0).secsTo(tmp_time) / 4;
	
	tmp_index = index.model()->index(index.row(), index.column());
	QString MD_name = tmp_index.data().toString();
	if (MD_name == "")
		return;
	int MD = MD_name.remove(0, 1).toInt(0, 10);
}

void DBView::OK_dlg(QString adr, QString padr, QString SD, QString countSD, QString whoIs)
{
	dlg = new QDialog(this, Qt::WindowSystemMenuHint);

	QPushButton *okBut = new QPushButton(dlg);
	okBut->setFlat(true);
	QPixmap pix(":img/ok.png");
	okBut->setIcon(pix);
	okBut->setIconSize(QSize(30, 29));

	QTableView *OkTable = new QTableView(dlg);
	QStandardItemModel *ok_table_model = new QStandardItemModel(dlg);
	OkTable->setModel(ok_table_model);
	QStringList tmp;
	if (whoIs == "MDCBK")
	{
		ok_table_model->setHorizontalHeaderLabels(QStringList() << "№ сообщ/СД в МД ЦБК" << "СД(HEX)" << "Информация в слове данных" << "Информация в разряде"
			<< "Номер разряда" << "Состояние");
	}
	else
	{
		(whoIs == "4MDCBK") ? tmp << "№ сообщ/СД в МД ЦБК" << "СД(HEX)" : tmp << "А/ПА/№СД";
		ok_table_model->setHorizontalHeaderLabels(QStringList() << "№ сигнала" << tmp << "Номер разряда"
			<< "Функциональное назначение сигнала ОК" << "Сигнал состояния ОК");
	}

	OkTable->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	OkTable->setAlternatingRowColors(true); //теперь строки зеброй красит
	OkTable->horizontalHeader()->setStyleSheet("QHeaderView::section {background-color: rgb(204, 204, 204);};  ");
	OkTable->verticalHeader()->hide();
	OkTable->setStyleSheet(instr::get_table_style());

	QVBoxLayout *v_lay = new QVBoxLayout(dlg);

	v_lay->addWidget(OkTable);
	v_lay->addWidget(okBut, 0, Qt::AlignRight);

	connect(okBut, &QPushButton::clicked, dlg, &QDialog::deleteLater);

	OkTable->resizeColumnsToContents();
	int iWidth = OkTable->verticalHeader()->sizeHint().width();
	for (int i = 0; i < ok_table_model->columnCount(); i++)
	{
		iWidth += OkTable->horizontalHeader()->sectionSize(i);
	}

	int iHeight = OkTable->horizontalHeader()->sizeHint().height();;
	for (int i = 0; i < ok_table_model->rowCount(); i++)
	{
		iHeight += OkTable->verticalHeader()->sectionSize(i);
	}

	OkTable->adjustSize();
	OkTable->horizontalHeader()->adjustSize();

	if (ok_table_model->rowCount() > 25)
		OkTable->setMinimumHeight(780);
	else
		OkTable->setMinimumHeight(iHeight + 2);	// +2 


	OkTable->setMinimumWidth(iWidth);	
	
	dlg->show();
}

void DBView::Any_dlg(QString SD)
{
	dlg = new QDialog(this, Qt::WindowSystemMenuHint);

	QPushButton *okBut = new QPushButton(dlg);
	okBut->setFlat(true);
	QPixmap pix(":img/ok.png");
	okBut->setIcon(pix);
	okBut->setIconSize(QSize(30, 29));

	QTableView *OkTable = new QTableView(dlg);
	QStandardItemModel *ok_table_model = new QStandardItemModel(dlg);
	OkTable->setModel(ok_table_model);
	ok_table_model->setHorizontalHeaderLabels(QStringList() << "СД в МД ЦБК" << "СД(HEX)");
	
	OkTable->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	//OkTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	OkTable->setAlternatingRowColors(true); //теперь строки зеброй красит
	OkTable->horizontalHeader()->setStyleSheet("QHeaderView::section {background-color: rgb(204, 204, 204);};  ");
	//OkTable->verticalHeader()->setStyleSheet("QHeaderView::section {background-color: rgb(204, 204, 204);};  ");
	OkTable->verticalHeader()->hide();
	OkTable->setStyleSheet(instr::get_table_style());

	QVBoxLayout *v_lay = new QVBoxLayout(dlg);

	v_lay->addWidget(OkTable);
	v_lay->addWidget(okBut, 0, Qt::AlignRight);

	connect(okBut, &QPushButton::clicked, dlg, &QDialog::deleteLater);


	SD.replace("\n", " ");
	QStringList myStringList = SD.split(' ');		// TODO баг сплита на во время переноса строки \n

	QList<QStandardItem*> st_list;
	int i = 0;
	for(QString curSD: myStringList)
	{
		st_list.clear();
		++i;
		curSD = curSD.toUpper();
		while (curSD.size() < 4)
			curSD.prepend("0");
		curSD.prepend("0x");
		st_list.push_back(new QStandardItem(QString("%1").arg(QString::number(i))));
		st_list.push_back(new QStandardItem(curSD));
		ok_table_model->appendRow(st_list);
	}

	OkTable->resizeColumnsToContents();
	int iWidth = OkTable->verticalHeader()->sizeHint().width();
	for (int i = 0; i < ok_table_model->columnCount(); i++)
	{
		iWidth += OkTable->horizontalHeader()->sectionSize(i);
	}

	int iHeight = OkTable->horizontalHeader()->sizeHint().height();;
	for (int i = 0; i < ok_table_model->rowCount(); i++)
	{
		iHeight += OkTable->verticalHeader()->sectionSize(i);
	}

	OkTable->adjustSize();
	OkTable->horizontalHeader()->adjustSize();

	if (ok_table_model->rowCount() > 25)
	{
		OkTable->setMinimumHeight(780);
		//iWidth += OkTable->verticalScrollBar()->sizeHint().width();
	}
	else
		OkTable->setMinimumHeight(iHeight + 2);	// +2 


	dlg->setWindowTitle(QString("Результаты вычисления "));
	OkTable->setMinimumWidth(iWidth);

	dlg->show();
}

QString DBView::checkOkState(OK ok, QString SD, QList<OK> okList, int &color, bool MDCBK, bool ifState)
{
	QString res = "";
	SD.replace("  ", " ");
	SD.replace("\n", " ");
	QStringList myStringList = SD.split(' ');		// TODO баг сплита на во время переноса строки \n
	QString curSD;
	if (MDCBK)
	{
		int pos = (ok.ok_cbkMsg.toInt() - 1) * 32 + ok.ok_cbkWordNumStart.toInt(); // Позиция нужного слова
		curSD = myStringList.at(pos - 1);
	}
	else
		curSD = myStringList.at(ok.ok_mkoWord.toInt() - 1);
	if (ok.ok_id == "2519")	// TODO проверить ифы
	{
		//int a;
		//a = a + 1; 
	}
	qDebug() << ok.ok_id;
	curSD.prepend("0x");
	if (ok.analiz_ok_id != "")
	{
		QString val_str = "";
		int val = 0;
		if (!ifState)
		{
			foreach(OK tmp_ok, okList)
			{
				if (tmp_ok.ok_id == ok.analiz_ok_id)		// TODO ПРОВЕРИТЬ АНАЛИЗ ОК ok id="2519"> и <analiz_ok id="1215">
				{
					val_str = checkOkState(tmp_ok, SD, okList, color, MDCBK, 1);
					val = val_str.toInt(0, 10);
					break;
				}
			}
		}
	//	if (val_str == "")
		//	return "Ошибка";

		for (int i = 0; i < ok.analiz_ok.size(); ++i)	
		{
			if (ok.analiz_ok.at(i).value == val)
			{
				int iVal = curSD.toInt(0, 16);
				int mask = 65535;
				mask = mask >> (16 - ok.ok_mkoBitCount.toInt());
				mask = mask << ok.ok_mkoBitStart.toInt();

				int res_ok = iVal & mask;
				res_ok = res_ok >> ok.ok_mkoBitStart.toInt();

				for (int j = 0; j < ok.analiz_ok.at(i).state.size(); ++j)
				{
					if (ok.analiz_ok.at(i).state.at(j).value == QString::number(res_ok))
					{
						res = ok.analiz_ok.at(i).state.at(j).descr;
						if (ifState)
							res = ok.analiz_ok.at(i).state.at(j).value;
						break;
					}
				}
				if (res == "")
				{
					res = "Ошибка";
				}
			}
		}

		
	}
	if (!ok.okForm.available)	// Если не формула, то обычные стейты
	{

		int iVal = curSD.toInt(0, 16);
		int mask = 65535;
		mask = mask >> (16 - ok.ok_mkoBitCount.toInt());
		mask = mask << ok.ok_mkoBitStart.toInt();

		int res_ok = iVal & mask;
		res_ok = res_ok >> ok.ok_mkoBitStart.toInt();
			
		for (int i = 0; i < ok.okStateList.size(); ++i)
		{
			if (ok.okStateList.at(i).value.toInt(0, 10) == res_ok)
			{
				res = ok.okStateList.at(i).descr;
				if (ifState)
					res = ok.okStateList.at(i).value;
				break;
			}
			if (ok.okStateList.at(i).value == "")	// <state descr="Резерв"/>
			{
				res = ok.okStateList.at(i).descr;
				break;
			}
		}
		if (res == "")
		{
			res = "Ошибка";
		}
	}
	else
	{
		if (ok.okForm.form != "")
		{
			int iVal = curSD.toInt(0, 16);
			int mask = 65535;
			mask = mask >> (16 - ok.ok_mkoBitCount.toInt());
			mask = mask << ok.ok_mkoBitStart.toInt();

			int res_ok = iVal & mask;
			res_ok = res_ok >> ok.ok_mkoBitStart.toInt();

			if (ok.okForm.ogran_poluch_min != "" && ok.okForm.ogran_poluch_max != "")
				if (res_ok < ok.okForm.ogran_poluch_min.toInt(0, 10) || res_ok > ok.okForm.ogran_poluch_max.toInt(0, 10))
					return "Ошибка";
				
			for (int i = 0; i < ok.okForm.const_poluch_list.size(); ++i)
				if (ok.okForm.const_poluch_list.at(i).value.toInt(0, 10) == res_ok)
					return ok.okForm.const_poluch_list.at(i).descr;
			
			QString formula = ok.okForm.form;
			formula.replace(QString("полученное_значение"), QString(QString::number(res_ok)));
			formula.replace(QString(","), QString("."));
			
			QScriptEngine engine;
			res = QString::number(engine.evaluate(formula).toNumber());

			if (ok.okForm.ogran_res_min != "" && ok.okForm.ogran_res_max != "")
			if (res.toInt(0, 10) < ok.okForm.ogran_res_min.toInt(0, 10) || res.toInt(0, 10) > ok.okForm.ogran_res_max.toInt(0, 10))
			{
				color = 1;
				return res;
			}
			
			for (int i = 0; i < ok.okForm.const_res_list.size(); ++i)
				if (ok.okForm.const_res_list.at(i).value.toInt(0, 10) == res.toInt(0, 10))
					return ok.okForm.const_res_list.at(i).descr;
			
			if (ok.okForm.cc == "16")
			{
				res = QString("%1").arg(res.toInt(0, 10), 0, 16);
				res.prepend(QString("0x"));
			}
			res.append(QString(" %1").arg(ok.okForm.ci));
		}
	}
	return res;
}

void DBView::showHideColumnDlg()
{
	dlg = new QDialog(this, Qt::WindowSystemMenuHint);
	dlg->setWindowTitle(QString("Скрыть колонки"));
	QPushButton okBut(dlg);
	okBut.setFlat(true);
	QPixmap pix(":img/ok.png");
	okBut.setIcon(pix);
	okBut.setIconSize(QSize(30, 29));

	QLabel *label;
	QCheckBox *check;
	QList<QLabel*> label_list;
	QList<QCheckBox*> check_list;

	QGridLayout heder_lay(dlg);

	hideColumnList->clear();
	int j = 0;
	for (int i = 0; i < mainTable->model()->columnCount(); i++)
	{
		label = new QLabel(mainTable->model()->headerData(i, Qt::Horizontal).toString(), dlg);
		check = new QCheckBox(dlg);
		label_list.push_back(label);
		check_list.push_back(check);
		
		heder_lay.addWidget(label, i, 0);
		heder_lay.addWidget(check, i, 1);
		j = i;
	}
	heder_lay.addWidget(&okBut, j + 1, 1);

	connect(&okBut, SIGNAL(clicked()), dlg, SLOT(accept()));
	if (dlg->exec() == QDialog::Accepted)
	{
		for (int i = 0; i < check_list.count(); ++i)
		{
			if (check_list.at(i)->isChecked())
				hideColumnList->push_back(1);
			else
				hideColumnList->push_back(0);
		}
		hideColumn(*hideColumnList);
	}
	dlg->deleteLater();
}

void DBView::hideColumn(QList<int> check_list)
{
	for (int i = 0; i < mainTable->model()->columnCount(); i++)
		mainTable->showColumn(i);

	if (check_list.isEmpty())
		return;
	for (int i = 0; i < mainTable->model()->columnCount(); i++)
	{
		if (check_list.at(i) == 1)
		{
			mainTable->hideColumn(i);
		}
	}
}

void DBView::showZebra()
{
	if (zebraAct->isChecked())
	{
		mainTable->setAlternatingRowColors(true); //теперь строки зеброй красит
		mainTable->horizontalHeader()->setStyleSheet("QHeaderView::section {background-color: rgb(204, 204, 204);};  ");
		mainTable->setStyleSheet(instr::get_table_style());
	}
	else
	{
		mainTable->setStyleSheet("");
		mainTable->setAlternatingRowColors(false);		
		mainTable->horizontalHeader()->setStyleSheet("");
	}
}

void DBView::copy_cell()
{
	QString clipboardString;
	QModelIndexList selectedIndexes = mainTable->selectionModel()->selectedIndexes();
	QString dispayHeadres;

	if (selectedIndexes.count() == 1)
	{

		QApplication::clipboard()->setText(selectedIndexes[0].data(Qt::DisplayRole).toString());
		return;
	}
	// хедеры
	for (int i = 0; i < selectedIndexes.count(); ++i)
	{
		QModelIndex current = selectedIndexes[i];
		if (mainTable->isColumnHidden(current.column()))
			continue;

		dispayHeadres = mainTable->model()->headerData(current.column(), Qt::Horizontal).toString();
		dispayHeadres.replace("\n", " ");
		// Последний не проверяем
		if (i + 1 < selectedIndexes.count())
		{
			QModelIndex next = selectedIndexes[i + 1];
			if (next.row() != current.row())
			{
				dispayHeadres.append("\n");
				clipboardString.append(dispayHeadres);
				break;
			}
			dispayHeadres.append(" \t ");
		}
	clipboardString.append(dispayHeadres);
	}	

	// Дата
	for (int i = 0; i < selectedIndexes.count(); ++i)
	{
		QModelIndex current = selectedIndexes[i];
		if (mainTable->isColumnHidden(current.column()))
			continue;
				
		QString displayText = current.data(Qt::DisplayRole).toString().remove("\n");
		// Последний не проверяем
		if (i + 1 < selectedIndexes.count())
		{
			QModelIndex next = selectedIndexes[i + 1];

			if (next.row() != current.row())
			{
				next.data(Qt::DisplayRole).toString().remove("\n");
				displayText.append("\n");
			}
			else
			{
				next.data(Qt::DisplayRole).toString().remove("\n");
				displayText.append(" \t ");
			}
		}
		clipboardString.append(displayText);
	}

	QApplication::clipboard()->setText(clipboardString);
}

void DBView::print_doc()
{
	QFile file("print.html");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;

	QTextStream out(&file);

	QModelIndexList selectedIndexes = mainTable->selectionModel()->selectedIndexes();

	int col = 0;
	int row = 0;
	int colCount;
	int rowCount;

	if (selectedIndexes.count() != 0)	// если печатаем выделенное
	{
		QModelIndex current = selectedIndexes[0];
		col = current.column();
		row = current.row();
		colCount = col;
		rowCount = row;
		// количество столбцов
		for (int i = 0; i < selectedIndexes.count(); ++i)
		{
			if (i + 1 < selectedIndexes.count())
			{
				QModelIndex next = selectedIndexes[i + 1];
				if (next.row() != current.row())
				{
					break;
				}
				colCount++;
			}
		}
		//количество строк
		for (int i = 0; i < selectedIndexes.count(); ++i)
		{
			if (i + 1 < selectedIndexes.count())
			{
				QModelIndex next = selectedIndexes[i + 1];
				if (next.row() != current.row())
				{
					rowCount++;
					current = next;
				}
			}
		}
	}
	else
	{
		colCount = mainTable->model()->columnCount();
		rowCount = mainTable->model()->rowCount();
	}

	out << "<html>\n"
		"<head>\n"
		"<meta Content=\"Text/html; charset=UTF-8\">\n"
		<< QString("<title>%1</title>\n").arg("Print")
		<< "<style>\n"
		<< "   body {\n"
		"margin: 0; \n"
		"}\n"
		<< "</style>\n"
		<< "</head>\n"
		"<body bgcolor=#ffffff link=#5000A0 margin=0>\n"
		"<table border=1 cellspacing=0 cellpadding=2 margin=0>\n";

	// headers
	out << "<thead><tr bgcolor=#f0f0f0>";
	for (int icolumn = col; icolumn <= colCount; icolumn++)
	if (!mainTable->isColumnHidden(icolumn))
		out << QString("<th><font size=2>%1</font></th>").arg(mainTable->model()->headerData(icolumn, Qt::Horizontal).toString());
	out << "</tr></thead>\n";

	// data table
	for (int irow = row; irow <= rowCount; irow++) {
		out << "<tr>";
		for (int icolumn = col; icolumn <= colCount; icolumn++) {
			if (!mainTable->isColumnHidden(icolumn)) {
				QString data = mainTable->model()->data(mainTable->model()->index(irow, icolumn)).toString().simplified();
				out << QString("<td bkcolor=0><font size=1>%1</font></td>").arg((!data.isEmpty()) ? data : QString("&nbsp;"));
			}
		}
		out << "</tr>\n";
	}
	out << "</table>\n"
		"</body>\n"
		"</html>\n";

	file.close();
	QDesktopServices::openUrl(QUrl("print.html"));

	//delete document;
}

void DBView::clearFields()
{
	MSK1Edit->clear();
	MSK2Edit->clear();
	BT1Edit->clear();
	BT2Edit->clear();
	if (stackWidget->currentWidget() == widgStandartFind || stackWidget->currentWidget() == widgMultyFind)
	{

		if (!multyFindOn)
		{
			ADREdit->clear();
			PADREdit->clear();
			CountSDEdit->clear();
			SDEdit->clear();
			OKEdit->clear();
			CWEdit->clear();
			AWEdit->clear();
			KU_SPOBUEdit->clear();
			KS_KPI2Edit->clear();
			MD_CBKEdit->clear();
			UTCEdit->clear();
			UTCEdit2->clear();
			diffBTEdit->clear();
			diffBTEdit2->clear();
			MMKOCheckBox1->setChecked(true);
			MMKOCheckBox2->setChecked(true);
			LMKOCheckBox1->setChecked(true);
			LMKOCheckBox2->setChecked(true);
			FormatCheckBox1->setChecked(true);
			FormatCheckBox2->setChecked(true);
			FormatCheckBox3->setChecked(false);
			diffBTEdit->clear();
			diffBTEdit2->clear();
			metkaEdit->clear();
		}
		else
		{
			for (int i = 0; i < formValueList.size(); ++i)
			{
				formValueList[i] = QMap<QString, QVariant>();
				multiFormList->at(i)->setTextToEdit("");
			}
		}
	}

	for (TM_name_widget *wdg : TM_widgets->toStdList())
		if (stackWidget->currentWidget() == wdg)
			wdg->clearFields();

	if (stackWidget->currentWidget() == DB_IKwidg)
	{
		nStartFrameEdit->clear();
		nEndFrameEdit->clear();
		filePathEdit->clear();
		KFKEdit1->clear();
		KFKEdit2->clear();
	}

}

void DBView::dlgCountRecords()
{
	dlg = new QDialog(this);
	QDialogButtonBox* buttons = new QDialogButtonBox(dlg);
	dlg->setWindowTitle("Настройка количества запрашиваемых записей");
	buttons->addButton(QDialogButtonBox::Ok);
	buttons->button(QDialogButtonBox::Ok)->setFlat(true);
	QPixmap pix(":img/ok.png");
	buttons->button(QDialogButtonBox::Ok)->setIcon(pix);
	buttons->button(QDialogButtonBox::Ok)->setIconSize(QSize(30, 29));
	buttons->button(QDialogButtonBox::Ok)->setText("");

	buttons->addButton(QDialogButtonBox::Cancel);
	buttons->button(QDialogButtonBox::Cancel)->setFlat(true);
	QPixmap pix2(":img/cancel.png");
	buttons->button(QDialogButtonBox::Cancel)->setIcon(pix2);
	buttons->button(QDialogButtonBox::Cancel)->setIconSize(QSize(30, 29));
	buttons->button(QDialogButtonBox::Cancel)->setText("");

	QLineEdit recCount(QString::number(countRecord));

	recCount.setMaximumHeight(25);
	recCount.setMaximumWidth(180);
	QHBoxLayout *h_lay = new QHBoxLayout;
	h_lay->addWidget(buttons);
	
	QVBoxLayout *v_lay = new QVBoxLayout(dlg);
	v_lay->addWidget(&recCount);
	v_lay->addLayout(h_lay);
	connect(buttons->button(QDialogButtonBox::Ok), SIGNAL(clicked()), dlg, SLOT(accept()));
	connect(buttons->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), dlg, SLOT(reject()));
	recCount.setFocus();
	if (dlg->exec() == QDialog::Accepted)
	{
		QString str = recCount.text();
		countRecord = str.toInt();
		if (countRecord < 200)
			countRecord = 200;
		setCountRecord(countRecord);
	}
	dlg->deleteLater();
}

void DBView::setCountRecord(int count)
{
	numRecordsUp = count;
	countStrings = count;
	blockUp = count;
}

void DBView::Q10View()
{
	QModelIndexList selectedIndexes = mainTable->selectionModel()->selectedIndexes();
	//QString file_name = QString("%1//%2").arg(remote_frame_path).arg(table_model->index(selectedIndexes[0].row(), 14).data().toString());
	QString file_name = QString("\"%1\"").arg(table_model->index(selectedIndexes[0].row(), 14).data().toString());
	QProcess::startDetached("film_viewer " + file_name);
}

Image::Image() : Qim(100, 100, QImage::Format_RGB32) {}

void Image::form_image(QList<QList<unsigned char> >& data)
{

	int str_count = data.count();
	int byte_in_str = data[0].count();

	if (str_count > 1000)
	{
		int pix_map = str_count / 1000;
		int new_str_count = str_count / pix_map;
		if (str_count % pix_map)
			new_str_count++;

		int new_byte_in_str = byte_in_str / pix_map;
		if (byte_in_str % pix_map)
			new_byte_in_str++;


		Qim = QImage(new_byte_in_str, new_str_count, QImage::Format_RGB32);


		rows = new_str_count;
		cols = new_byte_in_str;

		int tmp_sum = 0;
		int tmp_count = 0;
		for (int i = 0; i < new_str_count; i++)
		{
			for (int j = 0; j < new_byte_in_str; j++)
			{
				tmp_sum = 0;
				tmp_count = 0;
				for (int k = 0; k < pix_map; k++)
				{
					for (int l = 0; l < pix_map; l++)
					{
						int tmp_i = i*pix_map + k;
						int tmp_j = j*pix_map + l;
						if ((tmp_i < str_count) && (tmp_j < byte_in_str))
						{
							tmp_sum += data[tmp_i][tmp_j];
							tmp_count++;
						}
					}
				}

				Qim.setPixel(j, i, qRgb(tmp_sum / tmp_count, tmp_sum / tmp_count, tmp_sum / tmp_count));
			}
		}

	}
	else
	{
		Qim = QImage(byte_in_str, str_count, QImage::Format_RGB32);

		rows = str_count;
		cols = byte_in_str;

		for (int i = 0; i < str_count; i++)
		{
			for (int j = 0; j < byte_in_str; j++)
			{
				unsigned char tmp_char = data[i][j];
				Qim.setPixel(j, i, qRgb(tmp_char, tmp_char, tmp_char));
			}
		}
	}

	Qim = Qim.scaled(1900, 1000, Qt::IgnoreAspectRatio, Qt::FastTransformation);
	setFixedSize(1900, 1000);
}

void Image::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.save();
	painter.drawImage(QRect(0, 0, 1900, 1000), Qim);
	painter.restore();
}

void DBView::IKView()
{
	QModelIndexList selectedIndexes = mainTable->selectionModel()->selectedIndexes();
	//QString file_name = QString("%1//%2").arg(remote_frame_path).arg(table_model->index(selectedIndexes[0].row(), 14).data().toString());
	QString file_name = table_model->index(selectedIndexes[0].row(), 14).data().toString();
	QString mode = table_model->index(selectedIndexes[0].row(), 6).data().toString();

	QFile file(file_name);
	if (!file.open(QIODevice::ReadOnly))
	{
		return;
	}

	if (((file.size() == 0x2500) && (mode != "ВТФ"))
		|| ((file.size() == 0x72A700) && (mode != "ПИ15"))
		|| ((file.size() == 0x0EE8) && (mode != "ПИ8")))
	{
		return;
	}

	QDataStream ik_stream(&file);

	QList<QList< unsigned char> > tmp_data;
	unsigned char tmp;

	dlg = new QDialog(this, Qt::WindowSystemMenuHint);
	QPushButton *okBut = new QPushButton(dlg);
	okBut->setFlat(true);
	QPixmap pix(":img/ok.png");
	okBut->setIcon(pix);
	okBut->setIconSize(QSize(30, 29));
	connect(okBut, &QPushButton::clicked, dlg, &QDialog::deleteLater);
	QVBoxLayout *v_lay = new QVBoxLayout(dlg);
	Image* ik_widg = new Image;
	ik_widg->form_image(tmp_data);
	v_lay->addWidget(ik_widg);

	v_lay->addWidget(okBut, 0, Qt::AlignRight);
	dlg->setMaximumHeight(1024);
	dlg->setWindowTitle("Просмотр данных ИК");
	
	dlg->show();
}

void DBView::getQueryString()
{
	QString query = "";
	if (multyFindOn)
		query = cur_thr->getQueryString(&formValueList);
	else
	{
		QList<QMap<QString, QVariant> > FindList;
		FindList = getQueryStruct();
		query = cur_thr->getQueryString(&FindList);
	}

	QApplication::clipboard()->setText(query);
	QMessageBox msgBox;
	msgBox.setText(QString("В буфер скопирован запрос: ") + query);
	msgBox.setIcon(QMessageBox::Information);
	msgBox.addButton(QMessageBox::Ok);
	QPixmap pix(":img/ok.png");
	msgBox.button(QMessageBox::Ok)->setIcon(pix);
	msgBox.button(QMessageBox::Ok)->setIconSize(QSize(30, 29));
	msgBox.button(QMessageBox::Ok)->setText("");

	msgBox.exec();
}

void DBView::setFormatIKButton()
{
	QObject *obj = QObject::sender();


	if (obj == FormatIK15)
	{
		if(FormatIK15->styleSheet() == "")
			FormatIK15->setStyleSheet("background-color: rgb(149, 188, 242);");
		else
			FormatIK15->setStyleSheet("");
	}
	if (obj == FormatIK8)
	{
		if (FormatIK8->styleSheet() == "")
			FormatIK8->setStyleSheet("background-color: rgb(149, 188, 242);");
		else
			FormatIK8->setStyleSheet("");
	}
	if (obj == FormatIKWTF8)
	{
		if (FormatIKWTF8->styleSheet() == "")
			FormatIKWTF8->setStyleSheet("background-color: rgb(149, 188, 242);");
		else
			FormatIKWTF8->setStyleSheet("");
	}
}