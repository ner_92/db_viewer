#include "multyFindWidget.h"

//#include <QDateTime>

//#include <QCursor>
multyFindWidget::multyFindWidget(QWidget *parent)
: QDialog(parent)
{
	widg = new QWidget(this);
	QVBoxLayout *v_layout = new QVBoxLayout(this);
	//setFixedSize(100, 100);
	setMinimumWidth(1600);
	MSKLabel = new QLabel("MSK", widg);
	MSK1Edit = new QLineEdit(widg);
	MSK1Edit->setMinimumHeight(25);
	MSK1Edit->setMaximumWidth(135);
	MSK1Edit->setInputMask("99.99.9999 99:99:99.999");

	MSK2Edit = new QLineEdit(widg);
	MSK2Edit->setMaximumHeight(25);
	MSK2Edit->setMaximumWidth(135);
	MSK2Edit->setInputMask("99.99.9999 99:99:99.999");

	ADRLabel = new QLabel("Адрес", widg);
	ADREdit = new QLineEdit(widg);
	ADREdit->setMaximumHeight(25);
	ADREdit->setMaximumWidth(100);
	ADREdit->setValidator(new QIntValidator(0, 31));

	PADRLabel = new QLabel("Подадрес", widg);
	PADREdit = new QLineEdit(widg);
	PADREdit->setMaximumHeight(25);
	PADREdit->setMaximumWidth(100);
	PADREdit->setValidator(new QIntValidator(0, 31));

	CountSDLabel = new QLabel("Количество СД", widg);
	CountSDEdit = new QLineEdit(widg);
	CountSDEdit->setMaximumHeight(25);
	CountSDEdit->setMaximumWidth(135);
	CountSDEdit->setValidator(new QIntValidator(0, 32));

	OKLabel = new QLabel("№ ОК, Значение", widg);
	OKEdit = new QLineEdit(widg);
	OKEdit->setMaximumHeight(25);
	OKEdit->setMaximumWidth(100);

	BTLabel = new QLabel("БВ", widg);
	BT1Edit = new QLineEdit(widg);
	BT1Edit->setMaximumHeight(25);
	BT1Edit->setMaximumWidth(135);
	BT1Edit->setInputMask("99:99:99");

	BT2Edit = new QLineEdit(widg);
	BT2Edit->setMaximumHeight(25);
	BT2Edit->setMaximumWidth(135);
	BT2Edit->setInputMask("99:99:99");

	UTCLabel = new QLabel("UTC", widg);
	UTCEdit = new QLineEdit(widg);
	UTCEdit->setMaximumHeight(25);
	UTCEdit->setMaximumWidth(135);
	UTCEdit->setInputMask("99.99.9999 99:99:99");

	UTCEdit2 = new QLineEdit(widg);
	UTCEdit2->setMaximumHeight(25);
	UTCEdit2->setMaximumWidth(135);
	UTCEdit2->setInputMask("99.99.9999 99:99:99");

	diffBTLabel = new QLabel("Расхождение БВ", widg);
	diffBTEdit = new QLineEdit(widg);
	diffBTEdit->setMaximumHeight(25);
	diffBTEdit->setMaximumWidth(135);

	diffBTEdit2 = new QLineEdit(widg);
	diffBTEdit2->setMaximumHeight(25);
	diffBTEdit2->setMaximumWidth(135);

	MMKOGroupBox = new QGroupBox("Магистраль МКО", widg);
	MMKOCheckBox1 = new QCheckBox("0", widg);
	MMKOCheckBox1->setChecked(true);
	MMKOCheckBox2 = new QCheckBox("1", widg);
	MMKOCheckBox2->setChecked(true);
	QHBoxLayout *MMKOLayout = new QHBoxLayout();
	MMKOLayout->addWidget(MMKOCheckBox1);
	MMKOLayout->addWidget(MMKOCheckBox2);
	MMKOGroupBox->setLayout(MMKOLayout);

	LMKOGroupBox = new QGroupBox("Линия МКО", widg);
	LMKOCheckBox1 = new QCheckBox("О", widg);
	LMKOCheckBox1->setChecked(true);
	LMKOCheckBox2 = new QCheckBox("Р", widg);
	LMKOCheckBox2->setChecked(true);
	QHBoxLayout *LMKOLayout = new QHBoxLayout(widg);
	LMKOLayout->addWidget(LMKOCheckBox1);
	LMKOLayout->addWidget(LMKOCheckBox2);
	LMKOGroupBox->setLayout(LMKOLayout);

	FormatGroupBox = new QGroupBox("Формат", widg);
	FormatCheckBox1 = new QCheckBox("1", widg);
	FormatCheckBox1->setChecked(true);
	FormatCheckBox2 = new QCheckBox("2", widg);
	FormatCheckBox2->setChecked(true);
	FormatCheckBox3 = new QCheckBox("4", widg);
	FormatCheckBox3->setChecked(false);
	QVBoxLayout *FormatLayout = new QVBoxLayout(widg);
	FormatLayout->addWidget(FormatCheckBox1);
	FormatLayout->addWidget(FormatCheckBox2);
	FormatLayout->addWidget(FormatCheckBox3);
	FormatGroupBox->setLayout(FormatLayout);

	CWLabel = new QLabel("КС", widg);
	CWEdit = new QLineEdit(widg);
	CWEdit->setMaximumHeight(25);
	CWEdit->setMaximumWidth(135);

	AWLabel = new QLabel("ОС", widg);
	AWEdit = new QLineEdit(widg);
	AWEdit->setMaximumHeight(25);
	AWEdit->setMaximumWidth(135);

	KU_SPOBULabel = new QLabel("КУ СПО-БУ", widg);
	KU_SPOBUEdit = new QLineEdit(widg);
	KU_SPOBUEdit->setMaximumHeight(25);
	KU_SPOBUEdit->setMaximumWidth(135);

	KS_KPI2Label = new QLabel("КС КПИ2", widg);
	KS_KPI2Edit = new QLineEdit(widg);
	KS_KPI2Edit->setMaximumHeight(25);
	KS_KPI2Edit->setMaximumWidth(135);

	MD_CBKLabel = new QLabel("МД ЦБК", widg);
	MD_CBKEdit = new QLineEdit(widg);
	MD_CBKEdit->setMinimumHeight(25);
	MD_CBKEdit->setMaximumWidth(135);

	SDLabel = new QLabel("СД", widg);
	SDEdit = new QTextEdit(widg);
	//SDEdit->setMinimumHeight(90);
	SDEdit->setMaximumHeight(142);
	SDEdit->setMinimumWidth(460);

	metkaLabel = new QLabel("Метка", widg);
	metkaEdit = new QLineEdit(widg);
	metkaEdit->setMinimumHeight(25);
	metkaEdit->setMinimumWidth(206);

	okButton = new QPushButton(widg);
	okButton->setFlat(true);
	QPixmap pix(":img/ok.png");
	okButton->setIcon(pix);
	okButton->setIconSize(QSize(30, 29));

	cancelButton = new QPushButton(widg);
	cancelButton->setFlat(true);
	QPixmap pix2(":img/cancel.png");
	cancelButton->setIcon(pix2);
	cancelButton->setIconSize(QSize(30, 29));

	h_filterMKOLayout = new QHBoxLayout();

	DB_MKOmini1GridLayout = new QGridLayout;
	DB_MKOmini2GridLayout = new QGridLayout;
	DB_MKOmini3GridLayout = new QGridLayout;
	DB_MKOmini4GridLayout = new QGridLayout;
	DB_MKOmini5GridLayout = new QGridLayout;
	DB_MKOmini6GridLayout = new QGridLayout;
	DB_MKOmini7GridLayout = new QGridLayout;

	filter1MKOGroupBox = new QGroupBox();
	filter2MKOGroupBox = new QGroupBox();
	filter3MKOGroupBox = new QGroupBox();
	filter4MKOGroupBox = new QGroupBox();
	filter5MKOGroupBox = new QGroupBox();
	filter6MKOGroupBox = new QGroupBox();
	filter7MKOGroupBox = new QGroupBox();

	DB_MKOmini1GridLayout->addWidget(MSKLabel, 0, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(MSK1Edit, 1, 0);
	DB_MKOmini1GridLayout->addWidget(MSK2Edit, 1, 1);
	DB_MKOmini1GridLayout->addWidget(BTLabel, 2, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(BT1Edit, 3, 0);
	DB_MKOmini1GridLayout->addWidget(BT2Edit, 3, 1);
	DB_MKOmini1GridLayout->addWidget(UTCLabel, 4, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(UTCEdit, 5, 0);
	DB_MKOmini1GridLayout->addWidget(UTCEdit2, 5, 1);
	DB_MKOmini1GridLayout->addWidget(diffBTLabel, 6, 0, 1, 2, Qt::AlignHCenter);
	DB_MKOmini1GridLayout->addWidget(diffBTEdit, 7, 0);
	DB_MKOmini1GridLayout->addWidget(diffBTEdit2, 7, 1);
	DB_MKOmini1GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 8, 0);

	DB_MKOmini2GridLayout->addWidget(MMKOGroupBox, 0, 0, 2, 1, Qt::AlignCenter);
	DB_MKOmini2GridLayout->addWidget(LMKOGroupBox, 0, 1, 2, 1, Qt::AlignCenter);
	DB_MKOmini2GridLayout->addWidget(FormatGroupBox, 0, 2, 4, 1, Qt::AlignLeft);
	DB_MKOmini2GridLayout->addWidget(CWLabel, 2, 0, Qt::AlignHCenter);
	DB_MKOmini2GridLayout->addWidget(CWEdit, 3, 0);
	DB_MKOmini2GridLayout->addWidget(AWLabel, 2, 1, Qt::AlignHCenter);
	DB_MKOmini2GridLayout->addWidget(AWEdit, 3, 1);
	DB_MKOmini2GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 6);

	DB_MKOmini3GridLayout->addWidget(ADRLabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(ADREdit, 1, 0);
	DB_MKOmini3GridLayout->addWidget(PADRLabel, 0, 1, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(PADREdit, 1, 1);
	DB_MKOmini3GridLayout->addWidget(CountSDLabel, 0, 2, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(CountSDEdit, 1, 2);
	DB_MKOmini3GridLayout->addWidget(SDLabel, 2, 0, 1, 3, Qt::AlignHCenter);
	DB_MKOmini3GridLayout->addWidget(SDEdit, 3, 0, 3, 3);
	DB_MKOmini3GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 4, 4);

	DB_MKOmini4GridLayout->addWidget(OKLabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini4GridLayout->addWidget(OKEdit, 1, 0);

	DB_MKOmini5GridLayout->addWidget(KU_SPOBULabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini5GridLayout->addWidget(KU_SPOBUEdit, 1, 0);
	DB_MKOmini5GridLayout->addWidget(KS_KPI2Label, 0, 1, Qt::AlignHCenter);
	DB_MKOmini5GridLayout->addWidget(KS_KPI2Edit, 1, 1);
	DB_MKOmini5GridLayout->addWidget(MD_CBKLabel, 0, 2, Qt::AlignHCenter);
	DB_MKOmini5GridLayout->addWidget(MD_CBKEdit, 1, 2);

	DB_MKOmini6GridLayout->addWidget(metkaLabel, 0, 0, Qt::AlignHCenter);
	DB_MKOmini6GridLayout->addWidget(metkaEdit, 1, 0);

	filter1MKOGroupBox->setLayout(DB_MKOmini1GridLayout);
	filter2MKOGroupBox->setLayout(DB_MKOmini2GridLayout);
	filter3MKOGroupBox->setLayout(DB_MKOmini3GridLayout);
	filter4MKOGroupBox->setLayout(DB_MKOmini4GridLayout);
	filter5MKOGroupBox->setLayout(DB_MKOmini5GridLayout);
	filter6MKOGroupBox->setLayout(DB_MKOmini6GridLayout);

	DB_MKOmini7GridLayout->addWidget(filter4MKOGroupBox, 0, 0);
	DB_MKOmini7GridLayout->addWidget(filter5MKOGroupBox, 1, 0);
	DB_MKOmini7GridLayout->addWidget(filter6MKOGroupBox, 2, 0);
	DB_MKOmini7GridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding), 3, 1);

	filter7MKOGroupBox->setLayout(DB_MKOmini7GridLayout);

	h_filterMKOLayout->addWidget(filter1MKOGroupBox);
	h_filterMKOLayout->addWidget(filter2MKOGroupBox);
	h_filterMKOLayout->addWidget(filter3MKOGroupBox);
	h_filterMKOLayout->addWidget(filter7MKOGroupBox);
	h_filterMKOLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));
	QHBoxLayout *lay_buttons = new QHBoxLayout();
	lay_buttons->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));
	lay_buttons->addWidget(okButton);
	lay_buttons->addWidget(cancelButton);

	v_layout->setMargin(11);
	v_layout->setSpacing(8);
	v_layout->addLayout(h_filterMKOLayout);
	v_layout->addLayout(lay_buttons);

	connect(cancelButton, &QPushButton::clicked, this, &QPushButton::close);
	connect(cancelButton, &QPushButton::clicked, this, &multyFindWidget::del);
	connect(okButton, &QPushButton::clicked, this, &multyFindWidget::pushOkButton);
	connect(SDEdit, &QTextEdit::textChanged, this, &multyFindWidget::SDTextChange);

	connect(MSK1Edit, &QLineEdit::textChanged, this, &multyFindWidget::MSKTextCheck);
	connect(MSK2Edit, &QLineEdit::textChanged, this, &multyFindWidget::MSKTextCheck);
	connect(UTCEdit, &QLineEdit::textChanged, this, &multyFindWidget::MSKTextCheck);
	connect(UTCEdit2, &QLineEdit::textChanged, this, &multyFindWidget::MSKTextCheck);
	connect(BT1Edit, &QLineEdit::textChanged, this, &multyFindWidget::MSKTextCheck);
	connect(BT2Edit, &QLineEdit::textChanged, this, &multyFindWidget::MSKTextCheck);
}

multyFindWidget::~multyFindWidget()
{
	
}

void multyFindWidget::pushOkButton()
{
	emit okButtonSignal();
}

void multyFindWidget::del()
{
	delete this;
}

void multyFindWidget::SDTextChange()
{
	QString text = SDEdit->toPlainText();
	QStringList list = text.split(" ");
	for (QString str : list)
	{
		if (!QRegExp("^[0-9A-Fa-f]{4}$").exactMatch(str) && !QRegExp("^[!][0-9A-Fa-f]{4}$").exactMatch(str)
			&& !QRegExp("^[x]{4}$").exactMatch(str) && !QRegExp("^[0-9A-Fa-f]{4}[m][0-9A-Fa-f]{4}$").exactMatch(str))
		{
			SDEdit->setStyleSheet("QTextEdit { background: rgb(255, 128, 128);}");
			return;
		}
	}
	SDEdit->setStyleSheet("");
}

void multyFindWidget::MSKTextCheck(QString str)
{
	QObject *obj = QObject::sender();
	QLineEdit *ld = qobject_cast<QLineEdit *>(obj);
	if (str == ".. ::." || str == "::")
	{
		ld->setStyleSheet("");
		return;
	}

	if (obj == MSK1Edit || obj == MSK2Edit || obj == UTCEdit || obj == UTCEdit2)
	{
		if (QDateTime::fromString(ld->text(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch() == 0)
		{
			ld->setStyleSheet("QLineEdit { background: rgb(255, 128, 128);}");
		}
		else
			ld->setStyleSheet("");
	}
	if (obj == BT1Edit || obj == BT2Edit)
	{
		if (QDateTime::fromString(ld->text(), "hh:mm:ss").toMSecsSinceEpoch() == 0)
		{
			ld->setStyleSheet("QLineEdit { background: rgb(255, 128, 128);}");
		}
		else
			ld->setStyleSheet("");
	}
}