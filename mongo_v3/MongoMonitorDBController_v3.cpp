#include "MongoMonitorDBController_v3.h"
#include <qiodevice.h>
#include <qdatastream.h>
#include <qcoreapplication.h>
#include <qfile.h>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::sub_array;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

const QString MongoMonitorDBController_v3::mon_db_name = "mon_db";

MongoMonitorDBInterface_v3::MongoMonitorDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent)
: MongoViewerDBController_v3(_settings_path, _db_name, _db_rusname, parent), max_gr_insert(100)
{

}

MongoMonitorDBController_v3::MongoMonitorDBController_v3(QObject* parent)
: MonitorDBController(MongoController_v3::read_delay)
{
	MonitorDBInterface = std::shared_ptr<MongoMonitorDBInterface_v3>(new MongoMonitorDBInterface_v3(QCoreApplication::applicationDirPath() + "/mondb_set.conf", MongoMonitorDBController_v3::mon_db_name, "МКО", parent));
	DBInterface = std::dynamic_pointer_cast<CommonDBInterface>(MonitorDBInterface);

	connect(MonitorDBInterface.get(), &MongoCommonDBController_v3::finishedReadDataBlock, this, &CommonDBController::finishedReadDataBlock);
	connect(MonitorDBInterface.get(), &MongoCommonDBController_v3::changeProgressMsg, this, &CommonDBController::changeProgressMsg);
	connect(MonitorDBInterface.get(), &MongoCommonDBController_v3::synchronizeFinished, this, &CommonDBController::synchronizeFinished);
	connect(MonitorDBInterface.get(), &MongoController_v3::error, this, &CommonDBController::error);
}

bool MongoMonitorDBController_v3::insertMKOData(const std::vector<MsgFromMonitorDB>& msg_list)
{
	return MonitorDBInterface->insertMKOData(msg_list);
}

bool MongoMonitorDBInterface_v3::insertMKOData(const std::vector<MsgFromMonitorDB>& msg_list)
{
	bool res = false;

	try
	{
		if (!isConnected())
			return res;

		const int n = msg_list.size();
		auto start_itr = msg_list.begin(), end_itr = msg_list.begin();
		std::vector<bsoncxx::document::view_or_value> vect;
		int size = 0, cur_ind = 0;
		while (end_itr != msg_list.end())
		{
			size = ((cur_ind += max_gr_insert)  < n) ? max_gr_insert
				: (n - cur_ind + max_gr_insert);
			end_itr = std::next(start_itr, size);
			if (vect.size() != size)
				vect.resize(size);

			std::transform(start_itr, end_itr, vect.begin(),
				std::bind(&MongoMonitorDBInterface_v3::convertToObj, this, std::placeholders::_1));

			if (!vect.empty())
				res = insertData(vect);

			start_itr = end_itr;
		}
		return res;
	}
	catch (std::exception& exp)
	{
		emit error(QString("Ошибка добавления данных сообщений MSGLIST") + exp.what());
	}
	return false;
}

bsoncxx::document::view_or_value MongoMonitorDBInterface_v3::convertToObj(const MsgFromMonitorDB& msg) const
{

	bsoncxx::builder::basic::document doc{};
	doc.append(kvp("dt", (int64_t)msg.dt));
	doc.append(kvp("addr", msg.addr));
	doc.append(kvp("saddr", msg.saddr));
	doc.append(kvp("chan", msg.chan));
	doc.append(kvp("nWords", msg.nWords));
	doc.append(kvp("cwd", msg.cwd));
	doc.append(kvp("receive", msg.receive));
	doc.append(kvp("swd", msg.swd));
	doc.append(kvp("reserveBus", msg.reserveBus));
	const std::vector<unsigned short>& tmp_words = msg.data;
	doc.append(kvp("words", [&tmp_words](sub_array child) {
			foreach(const unsigned short w, tmp_words)
			{
				child.append(w);
			}
		}));

	return doc.extract();
}


MongoMonitorDBController_v3::~MongoMonitorDBController_v3()
{

}


QVariant MongoMonitorDBInterface_v3::convertMapToMsg(const QVariantMap& obj)
{
	QTime testTime2;
	testTime2.start();

	MsgFromMonitorDB msg;
	MsgFromBVDB msg_BV;

	msg.dt = obj["dt"].toLongLong();
	msg.addr = obj["addr"].toInt();
	msg.saddr = obj["saddr"].toInt();
	msg.chan = obj["chan"].toInt();
	msg.nWords = obj["nWords"].toInt();
	msg.cwd = obj["cwd"].toInt();
	msg.receive = obj["receive"].toBool();
	msg.swd = obj["swd"].toInt();
	msg.reserveBus = obj["reserveBus"].toBool();
	const QVariantList& _words = obj["words"].toList();

	int i = 0;
	msg.data.clear();
	for (auto& _word : _words)
	{
		//msg.data.push_back(fields.next().numberInt());

		if (i > 0)
		{
			/*if (!(i % 2))
			msg.words += " ";*/

			if (!(i % 8))
				msg.words += "\n";
			else
			{
				if (!(i % 4))
					msg.words += " ";
				msg.words += " ";
			}
		}
		unsigned short tmp_word = _word.toInt();
		msg.data.push_back(tmp_word);
		msg.words += QString("%1").arg(tmp_word, 4, 16, QChar('0')).toUpper();
		i++;
	}
	if (useBV)
	{

		std::vector<MsgFromBVDB>::iterator bv_itr = std::find_if(bv_vector.begin(), bv_vector.end(), [msg](MsgFromBVDB bv_msg)
		{return bv_msg.time_stamp <= (msg.dt/1000); });

		if (bv_itr == bv_vector.end())
		{
			msg.bv = 0;
			msg.utc = 0;
			msg.bv_dev = 0;
		}
		else
		{
			msg.bv = bv_itr->bv;
			msg.utc = bv_itr->utc;
			if (bv_itr == (bv_vector.end()-1))
				msg.bv_dev = 0;
			else
			{
				std::vector<MsgFromBVDB>::iterator prev_bv = bv_itr + 1;
				msg.bv_dev = bv_itr->time_stamp - prev_bv->time_stamp;
			}
		}
	}
	return QVariant::fromValue(msg);
}

QString MongoMonitorDBInterface_v3::msgToQuery(QList<QMap<QString, QVariant> > *queryList)
{
	QString stackQuery;
	QString andQuery;
	QString query;
	for (int i = 0; i < queryList->size(); i++)
	{
		stackQuery.clear();

		if (queryList->at(i).contains("BT1") || queryList->at(i).contains("BT2") || queryList->at(i).contains("UTC1") || queryList->at(i).contains("UTC2"))
		{
			QString bv_query = "";

			if (queryList->at(i).contains("BT1") || queryList->at(i).contains("BT2"))
			{
				QTime c = QTime::fromString(queryList->at(i)["BT1"].toString(), "hh:mm:ss");
				int BVDiscret1 = c.second() + c.minute() * 60 + c.hour() * 3600;
				queryList->at(i).contains("BT1") ? BVDiscret1 /= 4 : BVDiscret1 = 0;
				c = QTime::fromString(queryList->at(i)["BT2"].toString(), "hh:mm:ss");
				int BVDiscret2 = c.second() + c.minute() * 60 + c.hour() * 3600;
				queryList->at(i).contains("BT2") ? BVDiscret2 /= 4 : BVDiscret2 = 21600;
				BVDiscret2 += 1;	// на дискрет больше, чтобы отображать все обмены введёного дискрета, а не один
				QStringList dtList = BVController->getDTbyBV(BVDiscret1, BVDiscret2);
				QString tmp_string = "";
				QString and_string = "";
				bool and_flag = false;	// Если в листе нечётное количество, последний dt останется без {$and}
				if (!dtList.isEmpty())
				{
					for (int i = 0; i <= dtList.size() - 1; i++)
					{
						and_flag = false;
						if (i % 2 == 0)
							tmp_string += QString("{\"dt\":{\"$gte\":%1}},").arg(dtList.at(i));
						else
						{
							and_flag = true;
							tmp_string += QString("{\"dt\":{\"$lte\":%1}},").arg(dtList.at(i));
							tmp_string.remove(tmp_string.size() - 1, 1);
							and_string += QString("{\"$and\":[%1]},").arg(tmp_string);
							tmp_string.clear();
						}
						
					}
					if (!and_flag)
					{
						tmp_string.remove(tmp_string.size() - 1, 1);
						and_string += QString("{\"$and\":[%1]},").arg(tmp_string);
					}
					and_string.remove(and_string.size() - 1, 1);
					stackQuery += QString("{\"$or\":[%1]},").arg(and_string);

					//QString dt = dtList.at(dtList.size() - 1);
					//stackQuery += QString("{dt:{$lt: %1}},").arg(dt);
				}
				else
				{
					stackQuery += QString("{\"dt\":{\"$eq\": 0}},"); // Ломаю поиск, если БВ не нашёлся
				}

			}

			if (queryList->at(i).contains("UTC1"))
			{
				qint64 tmp_time = QDateTime::fromString(queryList->at(i)["UTC1"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
				tmp_time *= 1000;
				bv_query = QString("{\"utc\":{\"$gte\":%1}}").arg(QString::number(tmp_time));
				QStringList dtList = BVController->getDTbyBV(bv_query);
				if (!dtList.isEmpty())
				{

					QString dt = dtList.at(0);
					stackQuery += QString("{\"dt\":{\"$gte\": %1}},").arg(dt);
				}
				else
				{
					stackQuery += QString("{\"dt\":{\"$eq\": 0}},"); // Ломаю поиск, если БВ не нашёлся
				}
			}
			if (queryList->at(i).contains("UTC2"))
			{
				qint64 tmp_time = QDateTime::fromString(queryList->at(i)["UTC2"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
				tmp_time *= 1000;
				bv_query = QString("{\"utc\":{\"$lte\":%1}}").arg(QString::number(tmp_time));
				QStringList dtList = BVController->getDTbyBV(bv_query);
				if (!dtList.isEmpty())
				{

					QString dt = dtList.at(0);
					stackQuery += QString("{\"dt\":{\"$lte\": %1}},").arg(dt);
				}
				else
				{
					stackQuery += QString("{\"dt\":{\"$eq\": 0}},"); // Ломаю поиск, если БВ не нашёлся
				}
			}
		}
		if (queryList->at(i).contains("MSK1"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK1"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$gte\": %1}},").arg(QString::number(tmp_time));
		}
		if (queryList->at(i).contains("MSK2"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK2"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$lte\": %1}},").arg(QString::number(tmp_time));
		}
		if (queryList->at(i).contains("addr"))
		{
			int tmp_addr = queryList->at(i)["addr"].toInt();
			if (tmp_addr < 0)
				stackQuery += QString("{\"addr\":{\"$ne\":%1}},").arg(-tmp_addr);
			else
				stackQuery += QString("{\"addr\":{\"$eq\":%1}},").arg(tmp_addr);
		}
		if (queryList->at(i).contains("paddr"))
		{
			stackQuery += QString("{\"saddr\":{\"$eq\":%1}},").arg(queryList->at(i)["paddr"].toInt());
		}

		if (queryList->at(i).contains("countSD"))
		{
			stackQuery += QString("{\"nWords\":{\"$eq\":%1}},").arg(queryList->at(i)["countSD"].toInt());
		}
		if (queryList->at(i).contains("SD"))
		{
			//"{$or:[{$and:[{addr:{$eq:4}},{saddr:{$eq:4}},{$where: \"sdmask(this.words[0], 0xFF00) == 2560\"}]},{$and:[{addr:{$eq:4}},{saddr:{$eq:3}},{$where: \"sdmask(this.words[0], 0xFF00) == 2560\"}]}]}";
			QStringList sd_list = queryList->at(i)["SD"].toString().split(" ");
			for (int i = 0; i < sd_list.size(); ++i)
			{
				if (sd_list.at(i).size() == 9)	//8000mFF00
				{
					QStringList tmp_sd_list = sd_list.at(i).split("m");
					uint etalon = strToHexOneWord(tmp_sd_list.at(0));
					uint maska = strToHexOneWord(tmp_sd_list.at(1));
					uint one_mask = etalon & maska;
					uint null_mask = (~etalon) & maska;
					uint res = etalon & maska;
					if (one_mask != 0)
						stackQuery += QString("{\"words.%1\" : {\"$bitsAllSet\" : %2}},").arg(i).arg(one_mask);
					if (null_mask != 0)
						stackQuery += QString("{\"words.%1\" : {\"$bitsAllClear\" : %2}},").arg(i).arg(null_mask);
					//stackQuery += QString("{\"$expr\": {\"$eq\":[ {\"$multiply\":[\"words.%1\", \"NumberDecimal\"(\"%2\")] },\"NumberDecimal\"(\"%3\")]}},").arg(QString::number(i)).arg(QString::number(maska)).arg(QString::number(res));

					//{"$expr": {"$eq":[{"$multiply":[{"$arrayElemAt":["$words", 0]}, NumberDecimal("1")] }, NumberDecimal("1")]}}
				}

				if (sd_list.at(i).size() == 5)	//!8000
				{
					QString tmp_str = sd_list.at(i);
					tmp_str.remove(0, 1);
					stackQuery += QString("{\"words.%1\" : {\"$ne\" : %2}},").arg(i).arg(strToHexOneWord(tmp_str));
				}

				if (sd_list.at(i).size() == 4)	//
				{
					if (sd_list.at(i) != "xxxx")	//8000
					{
						stackQuery += QString("{\"words.%1\" : {\"$eq\" : %2}},").arg(i).arg(strToHexOneWord(sd_list.at(i)));
					}
				}

			}
			//QString tmp = queryList->at(i)->SD;
			//int q = tmp.size();
			//while (tmp.size() % 4)
			//	tmp += "0";
			//stackQuery += QString("{words:[");
			//for (int pos = 0; pos <= tmp.size() - 1; pos += 4)
			//{
			//	QStringRef str(&tmp, pos, 4);
			//	bool ok = false;
			//	uint hex = str.toUInt(&ok, 16);
			//	stackQuery += QString("%1,").arg(QString::number(hex));
			//}
			//stackQuery.remove(stackQuery.size() - 1, 1); //удалить последнюю запятую
			//stackQuery += QString("]},");
		}
		//if (!queryList->at(i)->OK.isEmpty())
		//{
		//	stackQuery += QString("{saddr:{$eq:%1}},").arg(queryList->at(i)->OK);
		//}
		if (queryList->at(i).contains("CW"))
		{
			stackQuery += strToHex("\"cwd\"", queryList->at(i)["CW"].toString());
			//stackQuery += QString("{cwd:{$eq:%1}},").arg(queryList->at(i)->CW);
		}
		if (queryList->at(i).contains("AW"))
		{
			stackQuery += strToHex("\"swd\"", queryList->at(i)["AW"].toString());
			//stackQuery += QString("{swd:{$eq:%1}},").arg(queryList->at(i)->AW);
		}
		if (queryList->at(i).contains("MMKO1") && !queryList->at(i)["MMKO1"].toBool())
			stackQuery += QString("{\"chan\":{\"$ne\": 0}},");
		if (queryList->at(i).contains("MMKO2") && !queryList->at(i)["MMKO2"].toBool())
			stackQuery += QString("{\"chan\":{\"$ne\": 1}},");

		if (queryList->at(i).contains("LMKO1") && !queryList->at(i)["LMKO1"].toBool())
			stackQuery += QString("{\"reserveBus\":{\"$ne\": false}},");
		if (queryList->at(i).contains("LMKO2") && !queryList->at(i)["LMKO2"].toBool())
			stackQuery += QString("{\"reserveBus\":{\"$ne\": true}},");

		if (queryList->at(i).contains("fullquery"))
			stackQuery += queryList->at(i)["fullquery"].toString()+",";

		bool tmp_format1 = true;
		bool tmp_format2 = true;
		bool tmp_format4 = true;
		if (queryList->at(i).contains("format1") && !queryList->at(i)["format1"].toBool())
			tmp_format1 = false;
		if (queryList->at(i).contains("format2") && !queryList->at(i)["format2"].toBool())
			tmp_format2 = false;
		if (queryList->at(i).contains("format3") && !queryList->at(i)["format3"].toBool())
			tmp_format4 = false;

		if (!tmp_format1)
			stackQuery += QString("{\"receive\":{\"$eq\": true}},");

		if (!tmp_format2 && tmp_format4)
			stackQuery += QString("{\"$or\":[{\"receive\":{\"$eq\": false}}, {\"$or\":[{\"saddr\":{\"$eq\": 0}}, {\"saddr\":{\"$eq\": 31}}]}]},");

		if (!tmp_format2 && !tmp_format4)
			stackQuery += QString("{\"receive\":{\"$eq\": false}},");

		if (tmp_format2 && !tmp_format4)
			stackQuery += QString("{\"$and\":[{\"saddr\":{\"$ne\": 0}}, {\"saddr\":{\"$ne\": 31}}]},");

		QString last_id = getLastId();

		switch (rezhim)
		{
		case 1:
			if (last_id.isEmpty())
				last_id = "ObjectId(\"000000000000000000000000\")";
			stackQuery += QString("{\"_id\":{\"$gt\": {\"$oid\" : \"%1\"}}}").arg(last_id);
			stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
			andQuery += stackQuery;
			break;
		case 2:
			// Замедляет работу
			//if (last_id.isEmpty())
			//	last_id = "ObjectId(\"FFFFFFFFFFFFFFFFFFFFFFFF\")";
			//stackQuery += QString("{_id:{$lt:%1}}").arg(last_id);
			//stackQuery = QString("{$and:[%1]},").arg(stackQuery);
			//andQuery += stackQuery;
			break;
		default:
			if (!stackQuery.isEmpty())
			{
				stackQuery.remove(stackQuery.size() - 1, 1); //удалить последнюю запятую
				stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
				andQuery += stackQuery;
			}
		}

	}  // queryList->size()
	if (!andQuery.isEmpty())
	{
		andQuery.remove(andQuery.size() - 1, 1); //удалить последнюю запятую
		query = QString("\"$or\":[%1]").arg(andQuery);
	}
	//query = "{$or:[{$and:[{addr:{$eq:4}},{saddr:{$eq:4}},{$where: \"sdmask(this.words[0], 0xFF00) == 2560\"}]},{$and:[{addr:{$eq:4}},{saddr:{$eq:3}},{$where: \"sdmask(this.words[0], 0xFF00) == 2560\"}]}]}";
	query = QString("{%1}").arg(query);
	//mon_th->setQueryString(query);
	qDebug(query.toStdString().c_str());
	return query;
}

QString MongoMonitorDBInterface_v3::strToHex(QString pole_name, QString pole_data)
{
	QString res_str;
	QString tmp = pole_data;
	int q = tmp.size();
	while (tmp.size() % 4)
		tmp += "0";
	res_str += QString("{%1:").arg(pole_name);
	for (int pos = 0; pos < tmp.size(); pos += 4)
	{
		QStringRef str(&tmp, pos, 4);
		bool ok = false;
		uint hex = str.toUInt(&ok, 16);
		res_str += QString("%1,").arg(QString::number(hex));
	}
	res_str.remove(res_str.size() - 1, 1); //удалить последнюю запятую
	res_str += QString("},");
	return res_str;
}

uint MongoMonitorDBInterface_v3::strToHexOneWord(QString str)
{
	//QStringRef tmp_str(&str, 0, 4);
	bool ok = false;
	uint hex = str.toUInt(&ok, 16);
	return hex;
}
