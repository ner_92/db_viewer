#include "MongoProtocolDBController_v3.h"
#include <qiodevice.h>
#include <qdatastream.h>
#include <qcoreapplication.h>
#include <qfile.h>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::sub_array;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

const QString MongoProtocolDBController_v3::mon_db_name = "protocol_db";

MongoProtocolDBInterface_v3::MongoProtocolDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent)
: MongoViewerDBController_v3(_settings_path, _db_name, _db_rusname, parent), max_gr_insert(100)
{

}

MongoProtocolDBController_v3::MongoProtocolDBController_v3(QObject* parent)
: ProtocolDBController(MongoController_v3::read_delay)
{
	ProtocolDBInterface = std::shared_ptr<MongoProtocolDBInterface_v3>(new MongoProtocolDBInterface_v3(QCoreApplication::applicationDirPath() + "/protocoldb_set.conf", MongoProtocolDBController_v3::mon_db_name, "Протокол", parent));
	DBInterface = std::dynamic_pointer_cast<CommonDBInterface>(ProtocolDBInterface);

	connect(ProtocolDBInterface.get(), &MongoCommonDBController_v3::finishedReadDataBlock, this, &CommonDBController::finishedReadDataBlock);
	connect(ProtocolDBInterface.get(), &MongoCommonDBController_v3::changeProgressMsg, this, &CommonDBController::changeProgressMsg);
	connect(ProtocolDBInterface.get(), &MongoCommonDBController_v3::synchronizeFinished, this, &CommonDBController::synchronizeFinished);
	connect(ProtocolDBInterface.get(), &MongoController_v3::error, this, &CommonDBController::error);
}

MongoProtocolDBController_v3::~MongoProtocolDBController_v3()
{

}

QVariant MongoProtocolDBInterface_v3::convertMapToMsg(const QVariantMap& obj)
{
	QTime testTime2;
	testTime2.start();

	MsgFromProtocolDB msg;
	MsgFromBVDB msg_BV;

	msg.dt = obj["dt"].toLongLong();
	msg.msg = obj["msg"].toString();

	if (useBV)
	{

		std::vector<MsgFromBVDB>::iterator bv_itr = std::find_if(bv_vector.begin(), bv_vector.end(), [msg](MsgFromBVDB bv_msg)
		{return bv_msg.time_stamp <= msg.dt; });

		if (bv_itr == bv_vector.end())
		{
			msg.bv = 0;
		}
		else
		{
			msg.bv = bv_itr->bv;
		}
	}

	return QVariant::fromValue(msg);
}

QString MongoProtocolDBInterface_v3::msgToQuery(QList<QMap<QString, QVariant> > *queryList)
{
	QString stackQuery;
	QString andQuery;
	QString query;
	for (int i = 0; i < queryList->size(); i++)
	{
		stackQuery.clear();

		if (queryList->at(i).contains("MSK1"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK1"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			//tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$gte\": %1}},").arg(QString::number(tmp_time));
		}
		if (queryList->at(i).contains("MSK2"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK2"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			//tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$lte\": %1}},").arg(QString::number(tmp_time));
		}

		if (queryList->at(i).contains("fullquery"))
			stackQuery += queryList->at(i)["fullquery"].toString() + ",";

		if (queryList->at(i).contains("MSG"))
			stackQuery += QString("{\"$text\": {\"$search\" : \"\\\"%1\\\"\"}},").arg(queryList->at(i)["MSG"].toString());


		QString last_id = getLastId();

		switch (rezhim)
		{
		case 1:
			if (last_id.isEmpty())
				last_id = "ObjectId(\"000000000000000000000000\")";
			stackQuery += QString("{\"_id\":{\"$gt\": {\"$oid\" : \"%1\"}}}").arg(last_id);
			stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
			andQuery += stackQuery;
			break;
		case 2:
			// Замедляет работу
			//if (last_id.isEmpty())
			//	last_id = "ObjectId(\"FFFFFFFFFFFFFFFFFFFFFFFF\")";
			//stackQuery += QString("{_id:{$lt:%1}}").arg(last_id);
			//stackQuery = QString("{$and:[%1]},").arg(stackQuery);
			//andQuery += stackQuery;
			break;
		default:
			if (!stackQuery.isEmpty())
			{
				stackQuery.remove(stackQuery.size() - 1, 1); //удалить последнюю запятую
				stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
				andQuery += stackQuery;
			}
		}

	}
	if (!andQuery.isEmpty())
	{
		andQuery.remove(andQuery.size() - 1, 1); //удалить последнюю запятую
		query = QString("\"$or\":[%1]").arg(andQuery);
	}
	query = QString("{%1}").arg(query);
	qDebug(query.toStdString().c_str());
	return query;
}


bool MongoProtocolDBInterface_v3::insertProtocolData(QString _msg, qint64 dt)
{
	bsoncxx::document::value obj = make_document(
		kvp("dt", ((dt == 0) ? ((int64_t)(QDateTime::currentMSecsSinceEpoch())) : (int64_t)dt)),
		kvp("msg", _msg.toStdString())
	);

	return insertData(std::move(obj));
}


bool MongoProtocolDBInterface_v3::insertProtocolDataArr(const std::vector<MsgFromProtocolDB> msg_list)
{

	bool res = false;

	try
	{
		if (!isConnected())
			return res;

		const int n = msg_list.size();
		auto start_itr = msg_list.begin(), end_itr = msg_list.begin();
		std::vector<bsoncxx::document::view_or_value> vect;
		int size = 0, cur_ind = 0;
		while (end_itr != msg_list.end())
		{
			size = ((cur_ind += max_gr_insert)  < n) ? max_gr_insert
				: (n - cur_ind + max_gr_insert);
			end_itr = std::next(start_itr, size);
			if (vect.size() != size)
				vect.resize(size);

			std::transform(start_itr, end_itr, vect.begin(),
				std::bind(&MongoProtocolDBInterface_v3::convertToObj, this, std::placeholders::_1));

			if (!vect.empty())
				res = insertData(vect);

			start_itr = end_itr;
		}
		return res;
	}
	catch (std::exception& exp)
	{
		emit error(QString("Ошибка добавления данных сообщений MSGLIST") + exp.what());
	}
	return false;

}

bsoncxx::document::view_or_value MongoProtocolDBInterface_v3::convertToObj(const MsgFromProtocolDB& msg) const
{
	bsoncxx::document::value obj = make_document(
		kvp("dt", (int64_t)msg.dt),
		kvp("msg", msg.msg.toStdString())
	);

	return std::move(obj);
}


bool MongoProtocolDBController_v3::insertProtocolData(QString _msg, qint64 dt)
{
	return ProtocolDBInterface->insertProtocolData(_msg, dt);
}

bool MongoProtocolDBController_v3::insertProtocolDataArr(const std::vector<MsgFromProtocolDB> msg_list)
{
	return ProtocolDBInterface->insertProtocolDataArr(msg_list);
}

bool MongoProtocolDBInterface_v3::tryConnect(QString col_name, QString db_ip, bool create)
{
	bool res = MongoViewerDBController_v3::tryConnect(col_name, db_ip, create);
	if (res)
	{
		QStringList ind_list = getIndexesList();
		bool contains_msg_ind = false;
		for (auto& _ind : ind_list)
		{
			if (_ind.contains("msg"))
				contains_msg_ind = true;
			qDebug() << _ind;
		}

		if (!contains_msg_ind)
		{
			createIndex(bsoncxx::from_json("{\"msg\":\"text\"}"));
			qDebug() << "created msg index";
		}
	}
	return res;
}