#ifndef MONGOBVDBCONTROLLERV3_H
#define MONGOBVDBCONTROLLERV3_H

#include "mongo_controller_v3.h"
#include "../BVDBController.hpp"
#include "MongoCommonDBController_v3.h"
#include <qtimer.h>
#include <qsettings.h>
#include <QDateTime>

/// -- Интерфейс для связи с БД БВ.
class MongoBVDBInterface_v3 : public MongoCommonDBController_v3
{
	Q_OBJECT
public:
	/**
	*	\brief -- Конструктор класса MongoBVDBInterface;
	*	\param parent -- родительский объект.
	*/
	MongoBVDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent = 0);

	/**
	*	\brief -- Конвертация из объекта базы в структуру;
	*	\param obj -- объект из базы;
	*	\return -- сообщение в виде структуры.
	*/
	QVariant convertMapToMsg(const QVariantMap& obj);
	/**
	*	\brief -- Конвертация из объекта базы в структуру БД БВ;
	*	\param obj -- объект из базы;
	*	\return -- сообщение в виде структуры БД БВ.
	*/
	MsgFromBVDB convertObjToMsgBV(const QVariantMap& obj);
	/**
	*	\brief -- Преобразование запроса из структуры в строку;
	*	\param queryList -- запрос в виде структуры;
	*	\return -- запрос в виде строки.
	*/
	QString msgToQuery(QList<QMap<QString, QVariant> > *queryList);
	//	std::vector<MsgFromBVDB> BVvector;
	/**
	*	\brief -- Запись значения БВ в базу
	*	\param time_stamp -- метка времени прихода БВ
	*	\param bv -- значение БВ в числе дискретов
	*	\param utc -- значение UTC в формате time_t
	*/
	virtual bool insertBVData(long long time_stamp, unsigned short bv, long long utc);
	/**
	*	\brief -- Запрос сообщения из базы БВ по МСК времени;
	*	\param dt -- МСК время, для которого нужно получить запись;
	*	\return -- запись из БД БВ, соответствующую dt.
	*/
	virtual MsgFromBVDB getBVbyDT(long long dt);
	/**
	*	\brief -- Запрос списка МСК времен, соответствующих запросу;
	*	\param query -- запрос к БД БВ;
	*	\return -- список сообщений из БД БВ.
	*/
	virtual QStringList getDTbyBV(QString query);
	/**
	*	\brief -- Запрос списка МСК времен, соответствующих диапазону БВ.
	*	\param BV1 -- начальное БВ;
	*	\param BV2 -- конечное БВ;
	*	\return -- список МСК времен.
	*/
	virtual QStringList getDTbyBV(int BV1, int BV2);

	virtual std::vector<MsgFromBVDB> getBVbyDT_12(long long dt1, long long dt2);
};

/**
*!\class MongoBVDBController
*	\brief Контроллер БД БВ
*
*/
class MongoBVDBController_v3 : public BVDBController
{
	Q_OBJECT
public:
	static const QString mon_db_name;

	/**
	*	\brief Конструктор класса MonitorDBReader
	*	\param parent - родительский объект
	*/
	MongoBVDBController_v3(QObject* parent = 0);

	/**
	*	\brief -- Запись значения БВ в базу
	*	\param time_stamp -- метка времени прихода БВ
	*	\param bv -- значение БВ в числе дискретов
	*	\param utc -- значение UTC в формате time_t
	*/
	virtual bool insertBVData(long long time_stamp, unsigned short bv, long long utc);
	/**
	*	\brief -- Запрос сообщения из базы БВ по МСК времени;
	*	\param dt -- МСК время, для которого нужно получить запись;
	*	\return -- запись из БД БВ, соответствующую dt.
	*/
	virtual MsgFromBVDB getBVbyDT(long long dt);
	/**
	*	\brief -- Запрос списка МСК времен, соответствующих запросу;
	*	\param query -- запрос к БД БВ;
	*	\return -- список сообщений из БД БВ.
	*/
	virtual QStringList getDTbyBV(QString query);
	/**
	*	\brief -- Запрос списка МСК времен, соответствующих диапазону БВ.
	*	\param BV1 -- начальное БВ;
	*	\param BV2 -- конечное БВ;
	*	\return -- список МСК времен.
	*/
	virtual QStringList getDTbyBV(int BV1, int BV2);

	virtual std::vector<MsgFromBVDB> getBVbyDT_12(long long dt1, long long dt2);
private:
	std::shared_ptr<MongoBVDBInterface_v3> BVDBInterface;
};

#endif // MONGOBVDBCONTROLLER_H