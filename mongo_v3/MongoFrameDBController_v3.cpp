#include "MongoFrameDBController_v3.h"
#include <qiodevice.h>
#include <qdatastream.h>
#include <qcoreapplication.h>
#include <qfile.h>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::sub_array;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

const QString MongoFrameDBController_v3::mon_db_name = "frame_db";

MongoFrameDBInterface_v3::MongoFrameDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent)
: MongoViewerDBController_v3(_settings_path, _db_name, _db_rusname, parent)
{

}

MongoFrameDBController_v3::MongoFrameDBController_v3(QObject* parent)
: FrameDBController(MongoController_v3::read_delay)
{
	FrameDBInterface = std::shared_ptr<MongoFrameDBInterface_v3>(new MongoFrameDBInterface_v3(QCoreApplication::applicationDirPath() + "/framedb_set.conf", MongoFrameDBController_v3::mon_db_name, "Кадровая информация", parent));
	DBInterface = std::dynamic_pointer_cast<CommonDBInterface>(FrameDBInterface);

	connect(FrameDBInterface.get(), &MongoCommonDBController_v3::finishedReadDataBlock, this, &CommonDBController::finishedReadDataBlock);
	connect(FrameDBInterface.get(), &MongoCommonDBController_v3::changeProgressMsg, this, &CommonDBController::changeProgressMsg);
	connect(FrameDBInterface.get(), &MongoCommonDBController_v3::synchronizeFinished, this, &CommonDBController::synchronizeFinished);
	connect(FrameDBInterface.get(), &MongoController_v3::error, this, &CommonDBController::error);
}

MongoFrameDBController_v3::~MongoFrameDBController_v3()
{

}

QVariant MongoFrameDBInterface_v3::convertMapToMsg(const QVariantMap& obj)
{
	QTime testTime2;
	testTime2.start();

	MsgFromFrameDB msg;
	MsgFromBVDB msg_BV;
	msg.dt = obj["dt"].toLongLong();
	msg.folder = obj["folder"].toString();
	msg.frame_num = obj["frame_num"].toInt();
	msg.mode = obj["mode"].toString();
	msg.arrays = obj["arrays"].toString();
	msg.kvit = obj["kvit"].toInt();
	qint8 frame_flags = obj["frame_flags"].toInt();
	msg.kks = ((frame_flags & KKS_FLAG) != 0);
	msg.dtk = ((frame_flags & DTK_FLAG) != 0);
	msg.ks = ((frame_flags & KS_FLAG) != 0);
	msg.kfk = obj["kfk"].toInt();
	msg.path = obj["path"].toString();

	///TODO FIX
	msg.label = "ШВ";

	if (useBV)
	{

		std::vector<MsgFromBVDB>::iterator bv_itr = std::find_if(bv_vector.begin(), bv_vector.end(), [msg](MsgFromBVDB bv_msg)
		{return bv_msg.time_stamp <= msg.dt; });

		if (bv_itr == bv_vector.end())
		{
			msg.bv = 0;
		}
		else
		{
			msg.bv = bv_itr->bv;
		}
	}

	return QVariant::fromValue(msg);
}

QString MongoFrameDBInterface_v3::msgToQuery(QList<QMap<QString, QVariant> > *queryList)
{
	QString stackQuery;
	QString andQuery;
	QString query;
	for (int i = 0; i < queryList->size(); i++)
	{
		stackQuery.clear();

		if (queryList->at(i).contains("MSK1"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK1"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			//tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$gte\": %1}},").arg(QString::number(tmp_time));
		}
		if (queryList->at(i).contains("MSK2"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK2"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			//tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$lte\": %1}},").arg(QString::number(tmp_time));
		}
		if (queryList->at(i).contains("fullquery"))
			stackQuery += queryList->at(i)["fullquery"].toString() + ",";
		if (queryList->at(i).contains("startFrame"))
		{
			stackQuery += QString("{\"frame_num\":{\"$gte\": %1}},").arg(queryList->at(i)["startFrame"].toInt());
		}
		if (queryList->at(i).contains("endFrame"))
		{
			stackQuery += QString("{\"frame_num\":{\"$lte\": %1}},").arg(queryList->at(i)["endFrame"].toInt());
		}

		bool need_pi15 = (queryList->at(i).contains("pi15") && (queryList->at(i)["pi15"].toBool()));
		bool need_pi8 = (queryList->at(i).contains("pi8") && (queryList->at(i)["pi8"].toBool()));
		bool need_vtf = (queryList->at(i).contains("vtf") && (queryList->at(i)["vtf"].toBool()));


		if (need_pi15 || need_pi8 || need_vtf)
		{
			stackQuery += QString("{\"$or\":[");

			if (need_pi15)
				stackQuery += QString("{\"mode\":{\"$eq\": \"ПИ15\"}},");

			if (need_pi8)
				stackQuery += QString("{\"mode\":{\"$eq\": \"ПИ8\"}},");

			if (need_vtf)
				stackQuery += QString("{\"mode\":{\"$eq\": \"ВТФ\"}},");


			stackQuery.chop(1);

			stackQuery += QString("]},");
		}


		if (queryList->at(i).contains("mdcbk"))
		{
			stackQuery += QString("{\"arrays\": {\"$regex\": \"%1\", \"$options\": \"i\"}},").arg(queryList->at(i)["mdcbk"].toString());
		}

		QString last_id = getLastId();

		switch (rezhim)
		{
		case 1:
			if (last_id.isEmpty())
				last_id = "ObjectId(\"000000000000000000000000\")";
			stackQuery += QString("{\"_id\":{\"$gt\": {\"$oid\" : \"%1\"}}}").arg(last_id);
			stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
			andQuery += stackQuery;
			break;
		case 2:
			// Замедляет работу
			//if (last_id.isEmpty())
			//	last_id = "ObjectId(\"FFFFFFFFFFFFFFFFFFFFFFFF\")";
			//stackQuery += QString("{_id:{$lt:%1}}").arg(last_id);
			//stackQuery = QString("{$and:[%1]},").arg(stackQuery);
			//andQuery += stackQuery;
			break;
		default:
			if (!stackQuery.isEmpty())
			{
				stackQuery.remove(stackQuery.size() - 1, 1); //удалить последнюю запятую
				stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
				andQuery += stackQuery;
			}
		}

	}
	if (!andQuery.isEmpty())
	{
		andQuery.remove(andQuery.size() - 1, 1); //удалить последнюю запятую
		query = QString("\"$or\":[%1]").arg(andQuery);
	}
	query = QString("{%1}").arg(query);
	qDebug(query.toStdString().c_str());
	return query;
}


bool MongoFrameDBInterface_v3::insertFrameData(MsgFromFrameDB _msg, qint64 dt)
{
	qint8 frame_flags = 0;
	if (_msg.kks)
		frame_flags += KKS_FLAG;
	if (_msg.dtk)
		frame_flags += DTK_FLAG;
	if (_msg.ks)
		frame_flags += KS_FLAG;

	bsoncxx::document::value obj = make_document(
		kvp("dt", ((dt == 0) ? ((int64_t)(QDateTime::currentMSecsSinceEpoch())) : (int64_t)dt)),
		kvp("folder", _msg.folder.toStdString()),
		kvp("frame_num", _msg.frame_num),
		kvp("mode", _msg.mode.toStdString()),
		kvp("arrays", _msg.arrays.toStdString()),
		kvp("kvit", _msg.kvit),
		kvp("frame_flags", frame_flags),
		kvp("kfk", _msg.kfk),
		kvp("path", _msg.path.toStdString())
	);

	return insertData(obj.view());
}





bool MongoFrameDBController_v3::insertFrameData(MsgFromFrameDB _msg, qint64 dt)
{
	return FrameDBInterface->insertFrameData(_msg, dt);
}

