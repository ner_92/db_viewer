#ifndef MONGOMONITORDBCONTROLLERV3_H
#define MONGOMONITORDBCONTROLLERV3_H


#include "mongo_controller_v3.h"
#include "MongoCommonDBController_v3.h"
#include "../MonitorDBController.hpp"
#include "../BVDBController.hpp"
#include <qtimer.h>
#include <qsettings.h>
#include <QTime>
#include <QTcpSocket>

/**
*!\class MonitorDBController
*	\brief -- Управление БД монитора под MongoDB.
*
*/
class MongoMonitorDBInterface_v3 : public MongoViewerDBController_v3
{
	Q_OBJECT
public:
	/**
	*	\brief -- Конструктор класса MonitorDBReader;
	*	\param _settings_path -- путь к файлу с настройками;
	*	\param _db_name -- имя БД;
	*	\param _db_rusname -- русское имя БД;
	*	\param parent -- родительский объект.
	*/
	MongoMonitorDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent = 0);
	/**
	*	\brief -- Конвертация из объекта базы в структуру;
	*	\param obj -- объект из базы;
	*	\return -- сообщение в виде структуры.
	*/
	QVariant convertMapToMsg(const QVariantMap& obj);
	/**
	*	\brief -- Добавление сообщений в БД;
	*	\param msg_list -- сообщения;
	*	\return -- флаг успешности добавления.
	*/
	bool insertMKOData(const std::vector<MsgFromMonitorDB>& msg_list);
	/**
	*	\brief -- Конвертация сообщения в объект BSON;
	*	\param msg -- сообщение для конвертации;
	*	\return -- объект BSON, полученный после конвертации.
	*/
	bsoncxx::document::view_or_value convertToObj(const MsgFromMonitorDB& msg) const;
	/**
	*	\brief -- Преобразование запроса из структуры в строку.
	*	\param queryList -- запрос в виде структуры;
	*	\return -- запрос в виде строки.
	*/
	QString msgToQuery(QList<QMap<QString, QVariant> > *queryList);
private:
	/**
	*	\brief -- Преобразование строки в последовательность чисел;
	*	\param pole_name -- имя поля запроса;
	*	\parm pole_data -- данные для конвертации;
	*	\return -- последовательность чисел в виде строки.
	*/
	QString strToHex(QString pole_name, QString pole_data);
	/**
	*	\brief -- Преобразование строки в число;
	*	\param str -- строка для преобразования;
	*	\return -- полученное число.
	*/
	uint strToHexOneWord(QString str);
	/// -- Максимальный размер посылки в БД(кол-во сообщений).
	const int max_gr_insert;
};

/**
*!\class MonitorDBController
*	\brief Управление БД монитора под MongoDB
*
*/
class MongoMonitorDBController_v3 : public MonitorDBController
{
	Q_OBJECT
public:
	/// -- Имя БД;
	static const QString mon_db_name;
	/**
	*	\brief -- Конструктор класса MongoMonitorDBController;
	*	\param parent -- родительский объект.
	*/
	MongoMonitorDBController_v3(QObject* parent = 0);
	/**
	*	\brief -- Деструктор класса MongoMonitorDBController;
	*/
	~MongoMonitorDBController_v3();

	/**
	*	\brief -- Добавление сообщений в БД.
	*	\param msg_list -- сообщения;
	*	\return -- флаг успешности добавления.
	*/
	bool insertMKOData(const std::vector<MsgFromMonitorDB>& msg_list);
private:
	/// -- Интерфейс связи с СУБД.
	std::shared_ptr<MongoMonitorDBInterface_v3> MonitorDBInterface;
};

#endif // MONGOMONITORDBCONTROLLER_H