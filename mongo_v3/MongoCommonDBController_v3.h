#ifndef MONGOCOMMONDBCONTROLLERV3_H
#define MONGOCOMMONDBCONTROLLERV3_H

#include "../CommonDBController.h"
#include "mongo_controller_v3.h"
#include <qtimer.h>
#include <qsettings.h>
#include <QDateTime>

/**
*!\class MongoCommonDBController
*	\brief -- Общий интерфейс доступа к БД на основе MongoDB.
*
*/
class MongoCommonDBController_v3 : public MongoController_v3, public CommonDBInterface
{
	Q_OBJECT
public:

	/**
	*	\brief -- Конструктор класса MongoCommonDBController;
	*	\param _settings_path -- путь до файла с настройками;
	*	\param _db_name -- имя базы;
	*	\param _db_rusname -- русское имя базы;
	*	\param parent -- родительский объект.
	*/
	MongoCommonDBController_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent = 0);
	/**
	*	\brief -- Установка начальной позиции чтения;
	*	\param col_name -- имя текущей коллекции(таблицы).
	*/
	void setStartPosition(const QString& col_name);
	/// -- Получение числа записей;
	int getCount();
	/**
	*	\brief -- Преобразование списка условий в строку запроса.
	*	\param queryList -- список условий для поиска;
	*	\return -- строка запроса.
	*/
	QString getQueryString(QList<QMap<QString, QVariant> > *list);
	/// -- Список коллекицй;
	std::list<std::string> collectionList;
	/// -- Получение списка коллекций;
	std::list<std::string> getCollList();
	/// -- Установка соединения с удаленной базой по ip;
	bool setIpMon(QString ip_str);
	/// -- Обработчик чтения данных;
	void readDataFromDB();
	/**
	*	\brief -- Получение блока данных;
	*	\return -- последний блок данных.
	*/
	std::vector<QVariant> getDataBlock();
	virtual std::vector<QVariant> convertDataBlock();
	/// -- Вектор данных;
	std::vector<QVariant> msgVector;
	/**
	*	\brief -- Конвертация из объекта базы в структуру;
	*	\param obj -- объект из базы;
	*	\return -- сообщение в виде структуры.
	*/
	virtual QVariant convertMapToMsg(const QVariantMap& obj) = 0;
	/**
	*	\brief -- Установка соединения с базой;
	*	\param col_name -- имя коллекции;
	*	\param db_ip -- ip адрес базы для подключения;
	*	\param create -- создавать коллекцию с именем #col_name при ее отсутствии.
	*/
	virtual bool tryConnect(QString col_name = "", QString db_ip = "", bool create = false);


	/**
	*	\brief -- Чтение поля из БД МЦА;
	*	\param query -- запрос;
	*	\param field -- Имя запрашиваемого поля;
	*	\param orderBy -- Имя поля, по которому происходит сортировка;
	*	\param sort -- Сортировка. 1 - ASC(по возрастанию), -1 - DESC(по убыванию), 0 - игнорировать сортировку;
	*	\return -- значение поля.
	*/
	virtual QVariant selectFieldOrderBy(QString query, QString field = "value", QString orderBy = "dt", int sort = 1);

	/**
	*	\brief -- Чтение строки из БД МЦА;
	*	\param query -- запрос;
	*	\param field -- Имя запрашиваемого поля;
	*	\param orderBy -- Имя поля, по которому происходит сортировка;
	*	\param sort -- Сортировка. 1 - ASC(по возрастанию), -1 - DESC(по убыванию), 0 - игнорировать сортировку;
	*	\return -- значения полей.
	*/
	virtual QVariantMap selectMapOrderBy(QString query, QString field = "value", QString orderBy = "dt", int sort = 1);

	/// -- Запрос состояния соединения с БД;
	virtual bool isConnected();
	/// -- Мьютекс на запросы;
	QMutex mtx;
	/// -- Мьютекс на соединение;
	QMutex conn_mtx;
	QMutex data_mtx;
	/// -- Номер первого элемента при возврате блока;
	int start_pos;
	/// -- Размер запрашиваемого блока данных;
	int block_size;
	/// -- Таймер подсчета времени запроса;
	QTime testTime;
	/**
	*	\brief -- Преобразование запроса из структуры в строку;
	*	\param queryList -- запрос в виде структуры;
	*	\return -- запрос в виде строки.
	*/
	virtual QString msgToQuery(QList<QMap<QString, QVariant> > *queryList) = 0;
	/// -- Флаг, отвечающий за дозапрос БВ;
	bool useBV;
	/// -- Имя БД;
	QString db_name;
	/// -- Русское имя БД;
	QString db_rus_name;
	/// -- Режим  режим работы с _id для скроллинга. 0 - не работаем, 1 - скроллинг вниз.
	int rezhim;
public slots:
	/// -- Слот запроса и обработки новых сообщений(вызывается по таймеру);
	void readDataFromDBQuery(QList<QMap<QString, QVariant> > *queryList);
	/**
	*	brief -- Слот запроса и обработки сообщений;
	*	\param _start_pos -- стартовая позиция блока;
	*	\param _block_size -- количество блоков;
	*	\param queryList -- структура с данными для запроса;
	*	\param needCount -- признак необходимости запроса числа записей;
	*	\param rezh -- режим работы с _id для скроллинга. 0 - не работаем, 1 - скроллинг вниз.
	*/
	void readBlockData(int _start_pos, int _block_size, QList<QMap<QString, QVariant> > *queryList, bool needCount, int rezh = 0);
	/**
	*	\brief -- Слот запроса количесва записей;
	*	\param queryList -- список условий для поиска.
	*/
	void getCountRec(QList<QMap<QString, QVariant> > *queryList);
	/// -- Слот запроса коллекций;
	void getCollectionNames();
	/// -- Сохраняет информации о последней прочитанной записи.
	void saveLastData();
	///Синхронизация с запросом
	void synchronize(QList<QMap<QString, QVariant> > *queryList);
signals:
	/// -- Сигнал о завершении чтения блока;
	void finishedReadDataBlock();
	/// -- Сигнал для изменения строки состояния;
	void changeProgressMsg(int value, QString msg);
	/// -- Сигнал окончания синхронизации.
	void synchronizeFinished(int count);
};
#include "../BVDBController.hpp"

/**
*!\class MongoViewerDBController
*	\brief Общий интерфейс доступа к БД для просмотра в виде таблиц
*
*/
class MongoViewerDBController_v3 : public MongoCommonDBController_v3
{
public:

	/**
	*	\brief  -- Конструктор класса MongoViewerDBController;
	*	\param _settings_path -- путь до файла с настройками;
	*	\param _db_name -- имя базы;
	*	\param _db_rusname -- русское имя базы;
	*	\param parent -- родительский объект.
	*/
	MongoViewerDBController_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent = 0);
	/// -- Деструктор;
	~MongoViewerDBController_v3();
	/**
	*	\brief -- Установка соединения с базой.
	*	\param col_name -- имя коллекции;
	*	\param db_ip -- ip адрес базы для подключения;
	*	\param create -- создавать коллекцию с именем #col_name при ее отсутствии.
	*/
	virtual bool tryConnect(QString col_name = "", QString db_ip = "", bool create = false);

	/**
	*	\brief -- Получение блока данных;
	*	\return -- последний блок данных.
	*/
	virtual std::vector<QVariant> convertDataBlock();

protected:
	/// -- Указатель на контроллер доступа к базе БВ.
	std::shared_ptr<BVDBController> BVController;

	std::vector<MsgFromBVDB> bv_vector;

};
#endif // MONGOCOMMONDBCONTROLLER_H