#ifndef MONGOPROTOCOLDBCONTROLLERV3_H
#define MONGOPROTOCOLDBCONTROLLERV3_H


#include "mongo_controller_v3.h"
#include "MongoCommonDBController_v3.h"
#include "../ProtocolDBController.hpp"
#include "../BVDBController.hpp"
#include <qtimer.h>
#include <qsettings.h>
#include <QTime>
#include <QTcpSocket>

/**
*!\class MongoProtocolDBInterface
*	\brief -- Интерфейс связи с БД протокола.
*
*/
class MongoProtocolDBInterface_v3 : public MongoViewerDBController_v3
{
	Q_OBJECT
public:
	/**
	*	\brief -- Конструктор класса MongoProtocolDBInterface
	*	\param _settings_path -- путь к файлу с настройками;
	*	\param _db_name -- имя БД;
	*	\param _db_rusname -- русское имя БД;
	*	\param parent -- родительский объект.
	*/
	MongoProtocolDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent = 0);
	/**
	*	\brief -- Конвертация из объекта базы в структуру;
	*	\param obj -- объект из базы;
	*	\return -- сообщение в виде структуры.
	*/
	QVariant convertMapToMsg(const QVariantMap& obj);
	/**
	*	\brief -- Добавление новых данных;
	*	\param _msg -- сообщение;
	*	\param dt -- метка времени;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertProtocolData(QString _msg, qint64 dt = 0);
	/**
	*	\bried -- Добавление массива новых данных;
	*	\param msg_list -- массив новых записей;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertProtocolDataArr(const std::vector<MsgFromProtocolDB> msg_list);
	/**
	*	\brief -- Преобразование запроса из структуры в строку.
	*	\param queryList -- запрос в виде структуры;
	*	\return -- запрос в виде строки.
	*/
	QString msgToQuery(QList<QMap<QString, QVariant> > *queryList);
	virtual bool tryConnect(QString col_name, QString db_ip, bool create);
private:
	/**
	*	\brief -- Конвертация сообщения в объект BSON;
	*	\param msg -- сообщение для конвертации;
	*	\return -- объект BSON, полученный после конвертации.
	*/
	bsoncxx::document::view_or_value convertToObj(const MsgFromProtocolDB& msg) const;
	/// -- Максимальный размер посылки в БД(кол-во сообщений).
	const int max_gr_insert;
};

/**
*!\class MongoProtocolDBController
*	\brief -- Управление БД протокола.
*
*/
class MongoProtocolDBController_v3 : public ProtocolDBController
{
	Q_OBJECT
public:
	/// -- Имя БД;
	static const QString mon_db_name;
	/**
	*	\brief -- Конструктор класса MongoProtocolDBController;
	*	\param parent -- родительский объект.
	*/
	MongoProtocolDBController_v3(QObject* parent = 0);
	/**
	*	\brief -- Деструктор класса MongoProtocolDBController;
	*/
	~MongoProtocolDBController_v3();
	/**
	*	\brief -- Добавление новых данных;
	*	\param _msg -- сообщение;
	*	\param dt -- метка времени;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertProtocolData(QString _msg, qint64 dt = 0);
	/**
	*	\bried -- Добавление массива новых данных.
	*	\param msg_list -- массив новых записей;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertProtocolDataArr(const std::vector<MsgFromProtocolDB> msg_list);
private:
	/// -- Интерфейс связи с СУБД.
	std::shared_ptr<MongoProtocolDBInterface_v3> ProtocolDBInterface;
};

#endif // MONGOPROTOCOLDBCONTROLLER_H