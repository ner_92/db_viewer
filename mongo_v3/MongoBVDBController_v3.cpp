#include "MongoBVDBController_v3.h"
#include <qiodevice.h>
#include <qdatastream.h>
#include <qcoreapplication.h>
#include <qfile.h>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

const QString MongoBVDBController_v3::mon_db_name = "bv_db";


MongoBVDBInterface_v3::MongoBVDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent)
: MongoCommonDBController_v3(_settings_path, _db_name, _db_rusname, parent)
{

}

MongoBVDBController_v3::MongoBVDBController_v3(QObject* parent)
: BVDBController(MongoController_v3::read_delay)
{
	BVDBInterface = std::shared_ptr<MongoBVDBInterface_v3>(new MongoBVDBInterface_v3(QCoreApplication::applicationDirPath() + "/bvdb_set.conf", MongoBVDBController_v3::mon_db_name, "БВ", parent));
	DBInterface = std::dynamic_pointer_cast<CommonDBInterface>(BVDBInterface);

	connect(BVDBInterface.get(), &MongoCommonDBController_v3::finishedReadDataBlock, this, &CommonDBController::finishedReadDataBlock);
	connect(BVDBInterface.get(), &MongoCommonDBController_v3::changeProgressMsg, this, &CommonDBController::changeProgressMsg);
	connect(BVDBInterface.get(), &MongoController_v3::error, this, &CommonDBController::error);
}



QVariant MongoBVDBInterface_v3::convertMapToMsg(const QVariantMap& obj)
{
	return QVariant::fromValue(convertObjToMsgBV(obj));
}

MsgFromBVDB MongoBVDBInterface_v3::convertObjToMsgBV(const QVariantMap& obj)
{
	//QMutexLocker lock(&mtx);
	MsgFromBVDB msg;
	msg.time_stamp = obj["dt"].toLongLong();
	msg.bv = obj["bv"].toInt();
	msg.utc = obj["utc"].toLongLong();
	return msg;
}

QString MongoBVDBInterface_v3::msgToQuery(QList<QMap<QString, QVariant> > *queryList)
{
	QString tmp_query = "{}";

	if (queryList->at(0).contains("fullquery"))
		tmp_query = queryList->at(0)["fullquery"].toString();
	return tmp_query;
}

MsgFromBVDB MongoBVDBInterface_v3::getBVbyDT(long long dt)
{
	QMutexLocker lock(&mtx);
	MsgFromBVDB tmp{ 0, 0, 0 };
	last_block.clear();
	//count = getCountRecords("{}");
	//getNRecords(last_block, 0, 1, QString("{dt:{$lte:%1}}").arg(dt));
	//getNRecordsSort(last_block, 0, 1, -1, QString("{dt:{$lte:%1}}").arg(dt));
	QVariantMap obj;
	get1Record(obj, 0, QString("{\"dt\":{\"$lte\":%1}}").arg(dt), -1, "dt");
	return convertObjToMsgBV(obj);
}


std::vector<MsgFromBVDB> MongoBVDBInterface_v3::getBVbyDT_12(long long dt1, long long dt2)
{
	QMutexLocker lock(&mtx);

	std::vector<MsgFromBVDB> tmp_vector;


	QString query_str = QString("{\"$and\":[{\"dt\":{\"$gte\":%1}},{\"dt\":{\"$lte\":%2}}]}").arg(dt1).arg(dt2);

	count = getCountRecords(query_str);

	last_block.clear();
	//getNRecords(last_block, 0, -1, query_str, 0);

	getNRecords(last_block, 0, -1, QString(query_str), -1, "dt");

	lock.unlock();

	foreach(const auto& obj, last_block)
	{
		tmp_vector.push_back(convertObjToMsgBV(obj));
	}
	tmp_vector.push_back(getBVbyDT(dt1));
	return tmp_vector;
}

QStringList MongoBVDBInterface_v3::getDTbyBV(QString query)
{
	QMutexLocker lock(&mtx);
	QStringList listQuery;
	listQuery.clear();
	last_block.clear();
	count = getCountRecords(query);
	getNRecords(last_block, 0, count, QString(query));

	MsgFromBVDB msg;
	foreach(const  QVariant& obj, last_block)
	{
		msg = qvariant_cast<MsgFromBVDB>(obj);
		listQuery.push_back(QString::number(msg.time_stamp));
	}
	//return_query.remove(return_query.size() - 1, 1); // del ","
	//return_query = QString("{$or:[%1]}").arg(return_query);
	return listQuery;
}

QStringList MongoBVDBInterface_v3::getDTbyBV(int BV1, int BV2)
{
	QMutexLocker lock(&mtx);
	QStringList listQuery;
	last_block.clear();
	QString bv_query = QString("{\"$and\":[{\"bv\":{\"$gte\":%1}, \"bv\":{\"$lte\":%2}}]}").arg(QString::number(BV1)).arg(QString::number(BV2));//{bv:{$gte:%1}, bv:{$lte:%2}}
	count = getCountRecords(bv_query);
	
	getNRecords(last_block, 0, count, bv_query);
	MsgFromBVDB msg;
	bool leftBV = true;		// БВ от
	bool rightBV = false;	// БВ до
	bool go = true;			// Защита для "БВ от". Смысл: т.к. "БВ от" ищется >=, то после нахождения "БВ до" сработает сразу на следующем дискрете. А надо дождаться дискрета <= "БВ от"
	bool find = false;		// Поиск go
	/// Для ситуации, когда бортовое время обнуляетя, не достигнув BV2
	long long  second_dt;	
	int second_BV2;		
	///
	foreach(const  QVariant& obj, last_block)
	{
		msg = qvariant_cast<MsgFromBVDB>(obj);
		
		if (find)
			if (msg.bv <= BV1 || msg.bv == 1)
				go = true;

		if (go)
		{

			if (leftBV)
			{
				if (msg.bv >= BV1)
				{
					
					listQuery.push_back(QString::number(msg.time_stamp));
					leftBV = false;
					rightBV = true;
					find = false;
					second_BV2 = msg.bv;
				}
			}

			if (rightBV)
			{
				if (msg.bv >= BV2)
				{
					
					listQuery.push_back(QString::number(msg.time_stamp));
					leftBV = true;
					rightBV = false;

					go = false;
					find = true;
				}
				else
				{
					if (msg.bv > second_BV2)
					{
						second_BV2 = msg.bv;
						second_dt = msg.time_stamp;
					}
					// Если бортовое время обнулилось, не достигнув BV2 - берём в качесте пары к первому БВ предыдущий дискрет(second_dt)
					if (msg.bv < second_BV2)
					{
						listQuery.push_back(QString::number(second_dt));
						//listQuery.push_back(QString::number(msg.time_stamp));
						leftBV = true;
						rightBV = false;
						go = false;
						find = true;
					}
				}
			}
		}	//go

	}
	// Если для БВ1 не нашлось пары, берём наибольший дискрет после него
	if (listQuery.size() % 2 != 0)
		listQuery.push_back(QString::number(second_dt));
	return listQuery;
}

bool MongoBVDBInterface_v3::insertBVData(long long time_stamp, unsigned short bv, long long utc)
{
	bsoncxx::document::value obj = make_document(
		kvp("dt", (int64_t)time_stamp),
		kvp("bv", bv),
		kvp("utc", (int64_t)utc)
	);
	return insertData(obj.view());
}

bool MongoBVDBController_v3::insertBVData(long long time_stamp, unsigned short bv, long long utc)
{
	return BVDBInterface->insertBVData(time_stamp, bv, utc);
}
// Возвращает MSG с bv и utc по МСК.
MsgFromBVDB MongoBVDBController_v3::getBVbyDT(long long dt)
{
	return BVDBInterface->getBVbyDT(dt);
}
QStringList MongoBVDBController_v3::getDTbyBV(QString query)
{
	return BVDBInterface->getDTbyBV(query);
}
QStringList MongoBVDBController_v3::getDTbyBV(int BV1, int BV2)
{
	return BVDBInterface->getDTbyBV(BV1, BV2);
}

std::vector<MsgFromBVDB> MongoBVDBController_v3::getBVbyDT_12(long long dt1, long long dt2)
{
	return BVDBInterface->getBVbyDT_12(dt1, dt2);
}
