#include "MongoCommonDBController_v3.h"
#include "MongoBVDBController_v3.h"
#include <qiodevice.h>
#include <qdatastream.h>
#include <qcoreapplication.h>
#include <qfile.h>
#include <qdebug.h>
#include "../instruments.h"


MongoCommonDBController_v3::MongoCommonDBController_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent)
: MongoController_v3(_settings_path, parent), db_name(_db_name), db_rus_name(_db_rusname), useBV(true)
{
	QFile::remove(settings.fileName());
	
}


MongoViewerDBController_v3::MongoViewerDBController_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent)
: MongoCommonDBController_v3(_settings_path, _db_name, _db_rusname, parent)
{
	BVController = std::dynamic_pointer_cast<BVDBController>(std::shared_ptr<MongoBVDBController_v3>(new MongoBVDBController_v3()));
}

MongoViewerDBController_v3::~MongoViewerDBController_v3()
{
	BVController.reset();
}


bool MongoCommonDBController_v3::isConnected()
{
	return isStillConnected();
}

bool MongoCommonDBController_v3::tryConnect(QString col_name, QString db_ip, bool create)
{
	QMutexLocker lock(&conn_mtx);
	bool res = false;
	emit changeProgressMsg(10, "Подключаюсь к БД " + db_rus_name);
	if (connectToDB(db_name, db_ip))
	{
		int pos_col_list = 0;
		std::list<std::string> col_list = getCollectionList(db_name);

		if (col_list.empty()&&!create)
			return false;

		if (col_name.isEmpty())
		{
			if (create)
			{
				col_name = genColNameForCurDB();
			}
			else
			{
				col_name = QString::fromStdString(col_list.back());
				if (col_name == "system.js")
				{
					col_list.pop_back();
					if (col_list.size() > 0)
					{

						col_name = QString::fromStdString(col_list.back());
					}
					else return false;
				}
			}
		}

		qDebug() << db_name << " " << col_name;
		res = setCurrentCollection(col_name);

		setStartPosition(col_name);
	}
	if (!res)
		emit changeProgressMsg(0, "Ошибка подключения к БД " + db_rus_name);
	return res;
}

bool MongoViewerDBController_v3::tryConnect(QString col_name, QString db_ip, bool create)
{
	QMutexLocker lock(&conn_mtx);
	bool res = false;
	emit changeProgressMsg(10, "Подключаюсь к БД " + db_rus_name);
	qDebug() << db_name << " "<< db_ip;
	if (connectToDB(db_name, db_ip))
	{
		int pos_col_list = 0;
		std::list<std::string> col_list = getCollectionList(db_name);

		if (col_list.empty()&&!create)
			return false;

		if (col_name.isEmpty())
		{
			if (create)
			{
				col_name = genColNameForCurDB();
			}
			else
			{
				col_name = QString::fromStdString(col_list.back());
				if (col_name == "system.js")
				{
					col_list.pop_back();
					if (col_list.size() > 0)
					{

						col_name = QString::fromStdString(col_list.back());
					}
					else return false;
				}
			}
		}
		qDebug() << db_name << " " << col_name;
		res = setCurrentCollection(col_name);

		setStartPosition(col_name);
		QStringList ind_list = getIndexesList();

		bool contains_dt = false;

		for (auto& _ind : ind_list)
		{
			if (_ind.contains("dt"))
				contains_dt = true;
			qDebug() << _ind;
		}

		if (!contains_dt)
			createIndex(bsoncxx::from_json("{\"dt\":1}"));

		QString tmp_srvr_ip;
		QSettings ipSettings("Cometa", "СПО АИК");
		tmp_srvr_ip = ipSettings.value("BV_db", "localhost").toString();

		emit changeProgressMsg(20, "Подключаюсь к БД БВ");
		if (BVController->tryConnect(col_name, tmp_srvr_ip))
		{
			emit changeProgressMsg(0, "Завершено");
			return res;
		}



	}
	if (!res)
		emit changeProgressMsg(0, "Ошибка подключения к БД " + db_rus_name);
	else
		emit changeProgressMsg(0, "Ошибка подключения к БД БВ");
	return false;
}

void MongoCommonDBController_v3::readDataFromDBQuery(QList<QMap<QString, QVariant> > *queryList)
{
	try
	{
		QMutexLocker lock(&mtx);
		last_block.clear();
		QString query_str = msgToQuery(queryList);
		getNewRecords(last_block, query_str, INTERVAL_DATA_SIZE);
		if (last_block.empty())
			return;
		
		addIntervalDataBlock(convertDataBlock());
		saveLastData();

		 emit finishedReadDataBlock(); 
	}
	catch (std::exception& e)
	{
		emit error(e.what());
	}
}

void MongoCommonDBController_v3::saveLastData()
{
	settings.setValue(last_id_key, lastReadID());
	settings.sync();
}

void MongoCommonDBController_v3::setStartPosition(const QString& col_name)
{
	if (col_name == settings.value(col_name_key).toString())
		setLastID(settings.value(last_id_key).toString());

	///Сохраняем имя коллекции(таблицы) с которой работаем(нужно если упадет)
	settings.setValue(col_name_key, col_name);
	settings.sync();

}

void MongoCommonDBController_v3::readBlockData(int _start_pos, int _block_size, QList<QMap<QString, QVariant> > *queryList, bool needCount, int rezh)
{
	QMutexLocker lock(&mtx);
	
	rezhim = rezh;
	QString query_str = msgToQuery(queryList);

	if (needCount)
	{
		emit changeProgressMsg(30, "Запрашиваю количество данных из БД " + db_rus_name);
		count = getCountRecords(query_str);
	}

	emit changeProgressMsg(35, "Запрашиваю данные из БД "+db_rus_name);
	
	start_pos = _start_pos;
	block_size = _block_size;
	last_block.clear();
	if (!block_size)
		return;

	testTime.start();
	//	QString query_str = msgToQuery(queryList);
	getNRecords(last_block, (rezh == 1) ? 0 : start_pos, block_size, query_str);
	lock.unlock();
	emit finishedReadDataBlock();
}

int MongoCommonDBController_v3::getCount()
{
	QMutexLocker lock(&mtx);
	return count;
}

QString MongoCommonDBController_v3::getQueryString(QList<QMap<QString, QVariant> > *list)
{
	QMutexLocker lock(&mtx);
	return msgToQuery(list);
	
}

void MongoCommonDBController_v3::getCountRec(QList<QMap<QString, QVariant> > *queryList)
{
	emit changeProgressMsg(30, "Запрашиваю количество данных из БД " + db_rus_name);
	QMutexLocker lock(&mtx);
	QString query_str = msgToQuery(queryList);
	count = getCountRecords(query_str);

}

void MongoCommonDBController_v3::synchronize(QList<QMap<QString, QVariant> > *queryList)
{
	QMutexLocker lock(&mtx);
	QString query_str = msgToQuery(queryList);
	int tmp_count = getCountRecords(query_str);
	emit synchronizeFinished(tmp_count);
}

void MongoCommonDBController_v3::getCollectionNames()
{
	QMutexLocker lock(&mtx);
	collectionList = getCollectionList(db_name);
}

std::list<std::string> MongoCommonDBController_v3::getCollList()
{
	//QMutexLocker lock(&mtx);
	return collectionList;
}

bool MongoCommonDBController_v3::setIpMon(QString ip_str)
{
	return true;
}

std::vector<QVariant> MongoCommonDBController_v3::getDataBlock()
{
	QMutexLocker lock(&mtx);
	return convertDataBlock();
}

std::vector<QVariant> MongoCommonDBController_v3::convertDataBlock()
{
	msgVector.clear();
	msgVector.resize(last_block.size());
	int ind = 0;
	for(auto& obj : last_block)
	{
		msgVector[ind] = convertMapToMsg(obj);
		ind++;
		//QCoreApplication::processEvents();
	}

	qDebug("Time readBlockData : %d ms", testTime.elapsed());
	return msgVector;
}

std::vector<QVariant> MongoViewerDBController_v3::convertDataBlock()
{
	std::vector<QVariant> msgVector;
	if (last_block.empty())
		return msgVector;
	long long dt1 = last_block[0]["dt"].toLongLong();
	long long dt2 = last_block.back()["dt"].toLongLong();
	if (db_name == "mon_db")
	{
		dt1 /= 1000;
		dt2 /= 1000;
	}
	bv_vector = BVController->getBVbyDT_12(dt1, dt2);
	foreach(auto& obj, last_block)
	{
		msgVector.push_back(convertMapToMsg(obj));
	}
	qDebug("Time readBlockData : %d ms", testTime.elapsed());
	return msgVector;
}

void MongoCommonDBController_v3::readDataFromDB()
{
	try
	{
		std::vector<QVariantMap> ret_vect;
		getNewRecords(ret_vect, "{}", 2000);/////////////FIX!!!
		if (ret_vect.empty())
			return;
		std::vector<QVariant> msg_vect;

		for (auto& _msg : ret_vect)
			msg_vect.push_back(convertMapToMsg(_msg));

		process(msg_vect);

		saveLastData();
	}
	catch (std::exception& e)
	{
		emit error(e.what());
	}
}

QVariant MongoCommonDBController_v3::selectFieldOrderBy(QString query, QString field, QString orderBy, int sort)
{
	QVariantMap obj;
	get1Record(obj, 0, query, sort, orderBy);
	return obj[field];
}

QVariantMap MongoCommonDBController_v3::selectMapOrderBy(QString query, QString field, QString orderBy, int sort)
{
	QVariantMap obj;
	get1Record(obj, 0, query, sort, orderBy);
	return obj;
}