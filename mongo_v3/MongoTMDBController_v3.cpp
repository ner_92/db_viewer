#include "MongoTMDBController_v3.h"
#include <qiodevice.h>
#include <qdatastream.h>
#include <qcoreapplication.h>
#include <qfile.h>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::sub_array;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

MongoTMDBInterface_v3::MongoTMDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent)
: MongoViewerDBController_v3(_settings_path, _db_name, _db_rusname, parent), max_gr_insert(100)
{

}

MongoTMDBController_v3::MongoTMDBController_v3(const QString& _db_name, const QString& _db_rus_name, QObject* parent)
: TMDBController(MongoController_v3::read_delay)
{
	db_name = _db_name;
	TMDBInterface = std::shared_ptr<MongoTMDBInterface_v3>(new MongoTMDBInterface_v3(QCoreApplication::applicationDirPath() + "/"+ _db_name+"_set.conf", _db_name, _db_rus_name, parent));
	DBInterface = std::dynamic_pointer_cast<CommonDBInterface>(TMDBInterface);

	connect(TMDBInterface.get(), &MongoCommonDBController_v3::finishedReadDataBlock, this, &CommonDBController::finishedReadDataBlock);
	connect(TMDBInterface.get(), &MongoCommonDBController_v3::changeProgressMsg, this, &CommonDBController::changeProgressMsg);
	connect(TMDBInterface.get(), &MongoController_v3::error, this, &CommonDBController::error);
}

MongoTMDBController_v3::~MongoTMDBController_v3()
{

}

QVariant MongoTMDBInterface_v3::convertMapToMsg(const QVariantMap& obj)
{
	QTime testTime2;
	testTime2.start();

	MsgFromTMDB msg;
	MsgFromBVDB msg_BV;
	msg.dt = obj["dt"].toLongLong();
	msg.tm_code = obj["tm_code"].toInt();
	msg.value = obj["value"];

	if (useBV)
	{

		std::vector<MsgFromBVDB>::iterator bv_itr = std::find_if(bv_vector.begin(), bv_vector.end(), [msg](MsgFromBVDB bv_msg)
		{return bv_msg.time_stamp <= msg.dt; });

		if (bv_itr == bv_vector.end())
		{
			msg.bv = 0;
		}
		else
		{
			msg.bv = bv_itr->bv;
		}
	}

	return QVariant::fromValue(msg);
}

QString MongoTMDBInterface_v3::msgToQuery(QList<QMap<QString, QVariant> > *queryList)
{
	QString stackQuery;
	QString andQuery;
	QString query;
	for (int i = 0; i < queryList->size(); i++)
	{
		stackQuery.clear();

		if (queryList->at(i).contains("BT1") || queryList->at(i).contains("BT2"))
		{
			QTime c = QTime::fromString(queryList->at(i)["BT1"].toString(), "hh:mm:ss");
			int BVDiscret1 = c.second() + c.minute() * 60 + c.hour() * 3600;
			queryList->at(i).contains("BT1") ? BVDiscret1 /= 4 : BVDiscret1 = 0;
			c = QTime::fromString(queryList->at(i)["BT2"].toString(), "hh:mm:ss");
			int BVDiscret2 = c.second() + c.minute() * 60 + c.hour() * 3600;
			queryList->at(i).contains("BT2") ? BVDiscret2 /= 4 : BVDiscret2 = 21600;
			BVDiscret2 += 1;	// на дискрет больше, чтобы отображать все обмены введёного дискрета, а не один
			QStringList dtList = BVController->getDTbyBV(BVDiscret1, BVDiscret2);

			QString tmp_string = "";
			QString and_string = "";
			bool and_flag = false;	// Если в листе нечётное количество, последний dt останется без {$and}
			if (!dtList.isEmpty())
			{
				for (int i = 0; i <= dtList.size() - 1; i++)
				{
					and_flag = false;
					if (i % 2 == 0)
						tmp_string += QString("{\"dt\":{\"$gte\":%1}},").arg(dtList.at(i));
					else
					{
						and_flag = true;
						tmp_string += QString("{\"dt\":{\"$lte\":%1}},").arg(dtList.at(i));
						tmp_string.remove(tmp_string.size() - 1, 1);
						and_string += QString("{\"$and\":[%1]},").arg(tmp_string);
						tmp_string.clear();
					}

				}
				if (!and_flag)
				{
					tmp_string.remove(tmp_string.size() - 1, 1);
					and_string += QString("{\"$and\":[%1]},").arg(tmp_string);
				}
				and_string.remove(and_string.size() - 1, 1);
				stackQuery += QString("{\"$or\":[%1]},").arg(and_string);
			}
			else
			{
				stackQuery += QString("{\"dt\":{\"$eq\": 0}},"); // Ломаю поиск, если БВ не нашёлся
			}
		}
		if (queryList->at(i).contains("MSK1"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK1"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			//tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$gte\": %1}},").arg(QString::number(tmp_time));
		}
		if (queryList->at(i).contains("MSK2"))
		{
			qint64 tmp_time = QDateTime::fromString(queryList->at(i)["MSK2"].toString(), "dd.MM.yyyy hh:mm:ss.zzz").toMSecsSinceEpoch();
			//tmp_time *= 1000;
			stackQuery += QString("{\"dt\":{\"$lte\": %1}},").arg(QString::number(tmp_time));
		}
		if (queryList->at(i).contains("signalTM1"))
		{
			stackQuery += QString("{\"tm_code\":{\"$gte\":%1}},").arg(queryList->at(i)["signalTM1"].toString());
		}
		if (queryList->at(i).contains("signalTM2"))
		{
			stackQuery += QString("{\"tm_code\":{\"$lte\":%1}},").arg(queryList->at(i)["signalTM2"].toString());
		}
		if (queryList->at(i).contains("signalValue1"))
		{
			stackQuery += QString("{\"value\":{\"$gte\":%1}},").arg(queryList->at(i)["signalValue1"].toString());
		}
		if (queryList->at(i).contains("signalValue2"))
		{
			stackQuery += QString("{\"value\":{\"$lte\":%1}},").arg(queryList->at(i)["signalValue2"].toString());
		}

		QString last_id = getLastId();

		switch (rezhim)
		{
		case 1:
			if (last_id.isEmpty())
				last_id = "ObjectId(\"000000000000000000000000\")";
			stackQuery += QString("{\"_id\":{\"$gt\": {\"$oid\" : \"%1\"}}}").arg(last_id);
			stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
			andQuery += stackQuery;
			break;
		case 2:
			// Замедляет работу
			//if (last_id.isEmpty())
			//	last_id = "ObjectId(\"FFFFFFFFFFFFFFFFFFFFFFFF\")";
			//stackQuery += QString("{_id:{$lt:%1}}").arg(last_id);
			//stackQuery = QString("{$and:[%1]},").arg(stackQuery);
			//andQuery += stackQuery;
			break;
		default:
			if (!stackQuery.isEmpty())
			{
				stackQuery.remove(stackQuery.size() - 1, 1); //удалить последнюю запятую
				stackQuery = QString("{\"$and\":[%1]},").arg(stackQuery);
				andQuery += stackQuery;
			}
		}

	}
	if (!andQuery.isEmpty())
	{
		andQuery.remove(andQuery.size() - 1, 1); //удалить последнюю запятую
		query = QString("\"$or\":[%1]").arg(andQuery);
	}
	query = QString("{%1}").arg(query);
	//mon_th->setQueryString(query);
	qDebug(query.toStdString().c_str());
	return query;
}


bool MongoTMDBInterface_v3::insertTMData(int tm_code, const QVariant& value)
{
	MsgFromTMDB tmp_msg;
	tmp_msg.dt = QDateTime::currentMSecsSinceEpoch();
	tmp_msg.tm_code = tm_code;
	tmp_msg.value = value;

	return insertData(convertToObj(tmp_msg));
}

bool MongoTMDBInterface_v3::insertTMData(const std::vector<MsgFromTMDB>& msg_list)
{
	bool res = false;

	try
	{
		if (!isConnected())
			return res;

		const int n = msg_list.size();
		auto start_itr = msg_list.begin(), end_itr = msg_list.begin();
		std::vector<bsoncxx::document::view_or_value> vect;
		int size = 0, cur_ind = 0;
		while (end_itr != msg_list.end())
		{
			size = ((cur_ind += max_gr_insert)  < n) ? max_gr_insert
				: (n - cur_ind + max_gr_insert);
			end_itr = std::next(start_itr, size);
			if (vect.size() != size)
				vect.resize(size);

			std::transform(start_itr, end_itr, vect.begin(),
				std::bind(&MongoTMDBInterface_v3::convertToObj, this, std::placeholders::_1));

			if (!vect.empty())
				res = insertData(vect);

			start_itr = end_itr;
		}
		return res;
	}
	catch (std::exception& exp)
	{
		emit error(QString("Ошибка добавления данных сообщений MSGLIST") + exp.what());
	}
	return false;
}

bsoncxx::document::view_or_value MongoTMDBInterface_v3::convertToObj(const MsgFromTMDB& msg) const
{

	bsoncxx::builder::basic::document doc{};
	doc.append(kvp("dt", (int64_t)msg.dt));

	doc.append(kvp("tm_code", msg.tm_code));
	switch (msg.value.type())
	{
	case QVariant::Double: doc.append(kvp("value", msg.value.toDouble())); break;
	case QVariant::Int: doc.append(kvp("value", msg.value.toInt())); break;
	case QVariant::UInt: doc.append(kvp("value", (int)(msg.value.toUInt()))); break;
	case QVariant::ULongLong:
	case QVariant::LongLong: doc.append(kvp("value", (int64_t)(msg.value.toLongLong()))); break;
	case QVariant::String: doc.append(kvp("value", msg.value.toString().toStdString())); break;
	case QVariant::Bool: doc.append(kvp("value", msg.value.toBool())); break;

	case QVariant::List:
	{
		const QVariantList& tmp_list(msg.value.toList());
		doc.append(kvp("value", [&tmp_list](sub_array child) {
			foreach(const auto& w, tmp_list)
			{
				switch (w.type())
				{
					case QVariant::Double: child.append(w.toDouble()); break;
					case QVariant::Int: child.append(w.toInt()); break;
					case QVariant::UInt: child.append((int)(w.toUInt())); break;
					case QVariant::ULongLong:
					case QVariant::LongLong: child.append((int64_t)(w.toLongLong())); break;
					case QVariant::String: child.append(w.toString().toStdString()); break;
					case QVariant::Bool: child.append(w.toBool()); break;
				};
				
			}
		}));

		break;
	}

	default:
	{
	}
	}

	return doc.extract();
}

QVariant MongoTMDBInterface_v3::getTMData(int tm_code)
{
	QVariantMap obj;
	get1Record(obj, 0, QString("{\"tm_code\":{\"$eq\":%1}}").arg(tm_code), -1, "dt");
	return obj["value"];
}

bool MongoTMDBController_v3::insertTMData(int tm_code, const QVariant& value)
{
	return TMDBInterface->insertTMData(tm_code, value);
}

bool MongoTMDBController_v3::insertTMData(const std::vector<MsgFromTMDB>& msg_list)
{
	return TMDBInterface->insertTMData(msg_list);
}


QVariant MongoTMDBController_v3::getTMData(int tm_code)
{
	return TMDBInterface->getTMData(tm_code);
}

