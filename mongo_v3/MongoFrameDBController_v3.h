#ifndef MONGOFRAMEDBCONTROLLERV3_H
#define MONGOFRAMEDBCONTROLLERV3_H


#include "mongo_controller_v3.h"
#include "MongoCommonDBController_v3.h"
#include "../FrameDBController.hpp"
#include "../BVDBController.hpp"
#include <qtimer.h>
#include <qsettings.h>
#include <QTime>
#include <QTcpSocket>

/**
*!\class MonitorDBReader
*	\brief Управление БД монитора
*
*/
class MongoFrameDBInterface_v3 : public MongoViewerDBController_v3
{
	Q_OBJECT
public:
	/**
	*	\brief Конструктор класса MonitorDBReader
	*	\param parent - родительский объект
	*/
	MongoFrameDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent = 0);
	/**
	*	\brief -- Конвертация из объекта базы в структуру;
	*	\param obj -- объект из базы;
	*	\return -- сообщение в виде структуры.
	*/
	QVariant convertMapToMsg(const QVariantMap& obj);
	/**
	*	\brief -- Добавление новых данных в БД ИК;
	*	\param _msg -- данные для добавления в виде структуры типа MsgFromFrameDB;
	*	\param dt -- метка времени;
	*	\return -- флаг успешности добавления новых данных.
	*/
	virtual bool insertFrameData(MsgFromFrameDB _msg, qint64 dt = 0);
	/**
	*	\brief -- Преобразование запроса из структуры в строку.
	*	\param queryList -- запрос в виде структуры;
	*	\return -- запрос в виде строки.
	*/
	QString msgToQuery(QList<QMap<QString, QVariant> > *queryList);

};

/**
*!\class MongoFrameDBController
*	\brief Управление БД монитора
*
*/
class MongoFrameDBController_v3 : public FrameDBController
{
	Q_OBJECT
public:
	/// -- Имя БД;
	static const QString mon_db_name;
	/**
	*	\brief -- Конструктор класса MongoFrameDBController;
	*	\param parent -- родительский объект.
	*/
	MongoFrameDBController_v3(QObject* parent = 0);
	/**
	*	\brief -- Деструктор класса MongoFrameDBController;
	*/
	~MongoFrameDBController_v3();
	/**
	*	\brief -- Добавление новых данных в БД ИК.
	*	\param _msg -- данные для добавления в виде структуры типа MsgFromFrameDB;
	*	\param dt -- метка времени;
	*	\return -- флаг успешности добавления новых данных.
	*/
	virtual bool insertFrameData(MsgFromFrameDB _msg, qint64 dt = 0);

private:
	/// -- Интерфейс связи с СУБД.
	std::shared_ptr<MongoFrameDBInterface_v3> FrameDBInterface;
};

#endif // MONGOFRAMEDBCONTROLLER_H