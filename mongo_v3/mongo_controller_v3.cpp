#include "mongo_controller_v3.h"

#include <boost/exception/diagnostic_information.hpp>
#include <boost/exception/exception.hpp>

#include <boost/optional.hpp>

#include <bsoncxx/builder/stream/array.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/types.hpp>

#include <qdatetime.h>
#include <exception>
#include <QDebug>
#include <QTimer>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::sub_array;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;

const int MongoController_v3::read_delay = 500;

const QString MongoController_v3::last_id_key = "last_read_id";
const QString MongoController_v3::col_name_key = "last_col_name";



MongoSingleton_v3::~MongoSingleton_v3()
{

}


MongoController_v3::MongoController_v3(QString _settings_path, QObject* parent)
	: client_connect(NULL), last_id(""),
	new_records_count(0), cur_rec_readed(0),
	cur_server(""), settings(_settings_path, QSettings::IniFormat)
{

}

MongoController_v3::~MongoController_v3()
{
	disconnectFromDB();
	//mongo::client::shutdown();
}

void MongoController_v3::disconnectFromDB()
{
	if (client_connect != NULL)
	{
		delete client_connect;
		client_connect = nullptr;
		db_connect = mongocxx::database{};
		cur_col = mongocxx::collection{};
	}
	new_records_count = 0;
}

std::string MongoController_v3::fullColName(const QString& col_name)
{
	return col_name.toStdString();
	//return QString("%1.%2").arg(db_name).arg(col_name).toStdString();
}

bool MongoController_v3::connectToDB(const QString& database_name, const QString& socket)
{
	QString err_msg = "Ошибка подключения к БД: ";
	try
	{
		//system("taskkill /IM mongod.exe /F");
		db_name = database_name;
		disconnectFromDB();
		cur_server = "mongodb://" + (socket.isEmpty() ? "localhost" : socket.toStdString());
		bsoncxx::string::view_or_value _cur_server(cur_server);
		mongocxx::uri uri(_cur_server);
		client_connect = new mongocxx::client(uri);

		auto db_cursor = client_connect->list_databases();
		for (auto& db : db_cursor)
			qDebug() << db["name"].get_utf8().value.to_string().c_str();

		db_connect = client_connect->database(database_name.toStdString());
		return true;

	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + QString(boost::diagnostic_information(exp).c_str()));
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + QString(exp.what()));
	}
	catch (...)
	{
		emit error(err_msg + QString("Неизвестное исключение"));
	}

	return false;
}

bool MongoController_v3::copyDB(const QString& db_name, const QString& from_host, const QString& new_db_name)
{
	QString err_msg = "Ошибка копирования коллекции: ";
	try
	{

	//bsoncxx::document::value res = db_connect.run_command(bsoncxx::from_json(QString("{\"copydb\": 1, \"fromhost\": \"%1\", \"fromdb\": \"%2\", \"todb\": \"%3\"}").arg(from_host).arg(db_name).arg(new_db_name).toStdString()));
		//std::string command = QString("{\"copydb\": 1, \"fromhost\": \"%1\", \"fromdb\": \"%2\", \"todb\": \"%3\"}").arg(from_host).arg(db_name).arg(new_db_name).toStdString();
		std::string command = QString("{\"clone\": \"%1\"}").arg(from_host).toStdString();
		bsoncxx::document::value res = db_connect.run_command(bsoncxx::from_json(command));

		return true;
	}
	///TODO FIX!!!
	//mongo::BSONObj info;
	//bool res = db_connect->copyDatabase(db_name.toStdString(), new_db_name.toStdString(),
	//									from_host.toStdString(), "DEFAULT", "", "", &info);
	//if (!res)
	//	emit error(QString("Не удалось скопировать БД %1( с адресом %2) в %3:\n%4")
	//			.arg(db_name).arg(from_host).arg(new_db_name).arg(info.toString().c_str()));
	//return res;
	
	catch (boost::exception& exp)
	{
		emit error(err_msg + QString(boost::diagnostic_information(exp).c_str()));
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + QString(exp.what()));
	}
	return false;
}

void MongoController_v3::shutdownServer()
{
	try
	{
		//mongo::BSONObj info;
		db_connect.run_command(bsoncxx::from_json("{\"shutdown\":1}"));
	}
	catch (const std::exception& e)
	{
		disconnectFromDB();
	}
}

bool MongoController_v3::copyCollection(const QString& col_name, const QString& db_name, const QString& from_host)
{
	QString err_msg = "Ошибка копирования коллекции: ";
	try
	{
		if (hasCollection(col_name))
		{
			emit error(QString("Ошибка копирования. Коллекция %1 уже существует.").arg(col_name));
			return false;
		}
	
		bsoncxx::document::value res = db_connect.run_command(bsoncxx::from_json(QString("{\"cloneCollection\":\"%1.%2\", \"from\": \"%3\"}").arg(db_name).arg(col_name).arg(from_host).toStdString()));

		return true;
		//if (!res)
		//{
		//	emit error(QString("Не удалось скопировать Коллекцию %1 из БД %2( с адресом %3): %4")
		//		.arg(col_name).arg(db_name).arg(from_host).arg(res.view()));
		//	
		//	return false;
		//}
		//	
		//return res;
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + QString(boost::diagnostic_information(exp).c_str()));
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + QString(exp.what()));
	}
	return false;
}

bool MongoController_v3::hasCollection(const QString& col_name)
{
	try
	{
		if (!db_connect)
			throw std::runtime_error("Ошибка: отсутствует подключение к БД");

		return db_connect.has_collection(fullColName(col_name));
	}
	catch (boost::exception& exp)
	{
		emit error(boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(exp.what());
	}
	return false;
}

std::list<std::string> MongoController_v3::getCollectionList(const QString& name)
{
	QMutexLocker lock(&mtx);
	try
	{
		if (!db_connect || (db_connect.name().to_string() != name.toStdString()))
			db_connect = client_connect->database(name.toStdString());
		if (!db_connect)
			throw std::runtime_error("Ошибка: отсутствует подключение к БД");

		std::list<std::string> collectionList;

		mongocxx::cursor col_cursor  = db_connect.list_collections();
		for (auto col : col_cursor)
			collectionList.push_back(col["name"].get_utf8().value.to_string());
		collectionList.sort();
		return collectionList;
	}
	catch (const std::exception& e)
	{
	}
	return std::list<std::string>();
}

std::list<std::string> MongoController_v3::getDatabasesList() const
{
	try
	{
		if (client_connect == NULL)
			throw std::runtime_error("Ошибка: отсутствует подключение к БД");
		mongocxx::cursor db_cursor = client_connect->list_databases();
		std::list<std::string> db_list;
		for (auto db : db_cursor)
			db_list.push_back(db["name"].get_utf8().value.to_string());
		return db_list;
	}
	catch (const std::exception& e)
	{
	}
	return std::list<std::string>();
}

mongocxx::collection MongoController_v3::createCollection(QString col_name)
{
	QString err_msg = "Ошибка создания таблицы БД: ";
	try
	{
		if (!hasCollection(col_name))
		{
			auto col = db_connect.create_collection(fullColName(col_name));
			return col;
		}
		else
			return db_connect[fullColName(col_name)];
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return mongocxx::collection();
}

bool MongoController_v3::removeCollection(QString col_name)
{
	QString err_msg = "Ошибка удаления таблицы БД: ";
	try
	{
		if (hasCollection(col_name))
			db_connect[fullColName(col_name)].drop();

		return true;
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;
}

bool MongoController_v3::setCurrentCollection(const QString& col_name)
{
	bool res = true;
	if (cur_col && (cur_col.name().to_string() == fullColName(col_name)))
		return res;

	if (!hasCollection(col_name))
		cur_col = createCollection(col_name);
	else
		cur_col = db_connect[fullColName(col_name)];
	QMutexLocker lock(&mtx);
	if (db_connect)
	{
		if (last_id.empty())
			new_records_count = cur_col.count_documents({});
	}
	return res;
}

bool MongoController_v3::insertData(const bsoncxx::document::view_or_value& data)
{
	QMutexLocker lock(&mtx);
	QString err_msg = QString("Ошибка добавления данных в таблицу");
	try
	{
		if (cur_col)
		{
			QString err_msg = QString("Ошибка добавления данных в таблицу \"%1\": ").arg(cur_col.name().to_string().c_str());
			cur_col.insert_one(std::move(data));
			
			++new_records_count;
			return true;
		}
		else
			throw std::runtime_error("\nТаблица не задана");
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;
}

bool MongoController_v3::insertData(const std::vector<bsoncxx::document::view_or_value>& data)
{
	QMutexLocker lock(&mtx);
	QString err_msg = QString("Ошибка добавления данных в таблицу");
	try
	{
		if (cur_col)
		{
			QString err_msg = QString("Ошибка добавления данных в таблицу \"%1\": ").arg(cur_col.name().to_string().c_str());

			QVariantMap tmp_msg = convertObjToMap(data.front());

			cur_col.insert_many(std::move(data));

			++new_records_count;
			return true;
		}
		else
			throw std::runtime_error("\nТаблица не задана");
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;
}

bool MongoController_v3::insertData(const QString& data)
{
	QMutexLocker lock(&mtx);
	QString err_msg = QString("Ошибка добавления данных в таблицу");
	try
	{
		if (cur_col)
		{
			QString err_msg = QString("Ошибка добавления данных в таблицу \"%1\": ").arg(cur_col.name().to_string().c_str());
			cur_col.insert_one(bsoncxx::from_json(data.toStdString()));

			++new_records_count;
			return true;
		}
		else
			throw std::runtime_error("\nТаблица не задана");
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;
}

bool MongoController_v3::updateData(const bsoncxx::document::view_or_value& obj, QString query_str, int flags, const mongocxx::write_concern* wc)
{
	QMutexLocker lock(&mtx);
	QString err_msg = QString("Ошибка обновления данных в таблице");
	try
	{
		if (cur_col)
		{
			QString err_msg = QString("Ошибка обновления данных в таблице \"%1\": ").arg(cur_col.name().to_string().c_str());
			cur_col.replace_one(bsoncxx::from_json(query_str.toStdString()), obj);
			return true;
		}
		else
			throw std::runtime_error("\nТаблица не задана");
	}

	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;
}

bool MongoController_v3::removeData(QString query_str, int flags, const mongocxx::write_concern* wc)
{
	QMutexLocker lock(&mtx);
	QString err_msg = QString("Ошибка удаления данных из таблицы");
	try
	{
		if (cur_col)
		{
			QString err_msg = QString("Ошибка удаления данных из таблицы \"%1\": ").arg(cur_col.name().to_string().c_str());
			cur_col.delete_one(bsoncxx::from_json(query_str.toStdString()));

			return true;
		}
		else
			throw std::runtime_error("\nТаблица не задана");
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;
}

QVariantMap MongoController_v3::convertObjToMap(const bsoncxx::document::view& obj)
{
	QVariantMap tmp_map;
	for (auto& el : obj)
	{
		QString tmp_key = el.key().to_string().c_str();

		switch (el.type())
		{
		case bsoncxx::type::k_oid:
			tmp_map.insert(tmp_key, el.get_oid().value.to_string().c_str());
			break;
		case bsoncxx::type::k_int32:
			tmp_map.insert(tmp_key, el.get_int32().value);
			break;
		case bsoncxx::type::k_int64:
			tmp_map.insert(tmp_key, qint64(el.get_int64().value));
			break;
		case bsoncxx::type::k_bool:
			tmp_map.insert(tmp_key, el.get_bool().value);
			break;
		case bsoncxx::type::k_utf8:
			tmp_map.insert(tmp_key, el.get_utf8().value.to_string().c_str());
			break;
		case bsoncxx::type::k_double:
			tmp_map.insert(tmp_key, el.get_double().value);
			break;


		case bsoncxx::type::k_document:
			tmp_map.insert(tmp_key, convertObjToMap(el.get_document().view()));
			break;
		case bsoncxx::type::k_array:
		{
			QVariantList tmp_arr;
			for (auto& arr_el : el.get_array().value)
			{
				switch (arr_el.type())
				{
				case bsoncxx::type::k_int32:
					tmp_arr << arr_el.get_int32().value;
					break;
				case bsoncxx::type::k_int64:
					tmp_arr << qint64(arr_el.get_int64().value);
					break;
				case bsoncxx::type::k_bool:
					tmp_arr << arr_el.get_bool().value;
					break;
				case bsoncxx::type::k_utf8:
					tmp_arr << arr_el.get_utf8().value.to_string().c_str();
					break;
				case bsoncxx::type::k_double:
					tmp_arr << arr_el.get_double().value;
					break;
				case bsoncxx::type::k_document:
					tmp_arr << convertObjToMap(arr_el.get_document().view());
					break;
				default:
					break;
				}
			}

			tmp_map.insert(tmp_key, tmp_arr);

			break;
		}

		default:
			break;
		}
	}

	return tmp_map;
}

bool MongoController_v3::query(const bsoncxx::document::view_or_value& query, std::vector<QVariantMap>& ret_vect)
{


	QString err_msg = QString("Ошибка запроса данных из таблицы");
	try
	{
		ret_vect.clear();
		if (cur_col)
		{
			QString err_msg = QString("Ошибка запроса данных из таблицы \"%1\": ").arg(cur_col.name().to_string().c_str());
			mongocxx::cursor cursor = cur_col.find(query);

			for (auto obj : cursor)
				ret_vect.push_back(convertObjToMap(obj));

			return true;
		}
		else
			throw std::runtime_error("\n\nТаблица не задана");
	}

	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;
}

bool MongoController_v3::getAggregateRecords(std::vector<QVariantMap>& ret_vect, QList<QPair<int, QString>> querys)
{

	QString err_msg = QString("Ошибка запроса данных из таблицы");
	try
	{

		ret_vect.clear();
		if (cur_col)
		{
			QString err_msg = QString("Ошибка запроса данных из таблицы \"%1\": ").arg(cur_col.name().to_string().c_str());
			mongocxx::pipeline p{};

			for (auto & query : querys)
			{
				switch (query.first)
				{
				case AGG_MATCH:
					p.match(bsoncxx::from_json(query.second.toStdString()));
					break;
				case AGG_SORT:
					p.sort(bsoncxx::from_json(query.second.toStdString()));
					break;
				case AGG_GROUP:
					p.group(bsoncxx::from_json(query.second.toStdString()));
					break;
				default:
					;
				}
			}

			mongocxx::cursor cursor = cur_col.aggregate(p);

			for (auto obj : cursor)
				ret_vect.push_back(convertObjToMap(obj));

			return true;

		}
		else
			throw std::runtime_error("\nТаблица не задана");

	}

	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;

	//QString err_msg = QString("Ошибка запроса данных из таблицы \"%1\": ").arg(cur_col);
	//try
	//{
	//	if (!cur_col.isEmpty())
	//	{
	//		if (!hasCollection(cur_col) && !createCollection(cur_col))
	//			return false;

	//		ret_vect.clear(); 
	//		mongo::BSONArrayBuilder arrayBuilder;
	//		for (QStringList::iterator itr = querys.begin(); itr != querys.end(); ++itr)
	//		{
	//			arrayBuilder.append(mongo::fromjson(itr->toStdString()));
	//		}
	//		auto pipeline = arrayBuilder.obj();
	//		mongo::BSONObj aggregateOptions;// = BSON("explain" << true);
	//		std::auto_ptr<mongo::DBClientCursor> cursor = db_connect->aggregate(ns, pipeline, &aggregateOptions);

	//		while (cursor->more())
	//			ret_vect.push_back(cursor->next().getOwned());

	//		return true;
	//	}
	//	else
	//		throw std::runtime_error(QString("Таблица %1 не задана")
	//		.arg(cur_col).toStdString().c_str());
	//}
	//catch (boost::exception& exp)
	//{
	//	emit error(err_msg + boost::diagnostic_information(exp).c_str());
	//}
	//catch (std::exception& exp)
	//{
	//	emit error(err_msg + exp.what());
	//}
	//return false;
}

bool MongoController_v3::get1Record(QVariantMap &res,
	const int start_pos, QString query_str, int sort, QString sort_field)
{
	QString err_msg = QString("Ошибка запроса данных из таблицы");
	try
	{
		if (cur_col)
		{
			QString err_msg = QString("Ошибка запроса данных из таблицы \"%1\": ").arg(cur_col.name().to_string().c_str());
			mongocxx::options::find find_options;
			if (sort != 0)
				find_options = find_options.sort(make_document(kvp(sort_field.toStdString(), sort)));
			if (start_pos != 0)
				find_options = find_options.skip(start_pos);

			auto opt_ret = cur_col.find_one(bsoncxx::from_json(query_str.toStdString()), find_options);
			if (opt_ret)
				res = convertObjToMap(*opt_ret);

			return true;
		}
		else
			throw std::runtime_error("\nТаблица не задана");
	}

	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	return false;

	//QString err_msg = QString("Ошибка запроса данных из таблицы \"%1\": ").arg(cur_col);
	//try
	//{
	//	if (!cur_col.isEmpty())
	//	{
	//		if (!hasCollection(cur_col) && !createCollection(cur_col))
	//			return false;

	//		mongo::BSONArrayBuilder arrayBuilder;
	//		arrayBuilder.append(BSON("$match" << mongo::fromjson(query_str.toStdString())));
	//		arrayBuilder.append(BSON("$sort" << BSON("dt" << sort)));
	//		arrayBuilder.append(BSON("$limit" << 1));
	//		auto pipeline = arrayBuilder.obj();
	//		mongo::BSONObj aggregateOptions;// = BSON("explain" << true);
	//		std::auto_ptr<mongo::DBClientCursor> cursor = db_connect->aggregate(ns, pipeline, &aggregateOptions);

	//		if (cursor->more())
	//			res = cursor->next().getOwned();

	//		return true;
	//	}
	//	else
	//		throw std::runtime_error(QString("Таблица %1 не задана")
	//		.arg(cur_col).toStdString().c_str());
	//}
	//catch (boost::exception& exp)
	//{
	//	emit error(err_msg + boost::diagnostic_information(exp).c_str());
	//}
	//catch (std::exception& exp)
	//{
	//	emit error(err_msg + exp.what());
	//}
	//return false;
}

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::sub_array;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

bool MongoController_v3::getNRecords(std::vector<QVariantMap>& ret_vect,
	const int start_pos, const int n, QString query_str, int sort, QString sort_field)
{
	QString err_msg = QString("Ошибка запроса данных из таблицы");
	try
	{
		if (cur_col)
		{
			ret_vect.clear();
			err_msg = QString("Ошибка запроса данных из таблицы \"%1\": ").arg(cur_col.name().to_string().c_str());
			mongocxx::options::find find_options;
			if (sort != 0)
				find_options = find_options.sort(make_document(kvp(sort_field.toStdString(), sort)));
			if (start_pos != 0)
				find_options = find_options.skip(start_pos);
			if (n != -1)
				find_options = find_options.limit(n);

			mongocxx::cursor cursor = cur_col.find(bsoncxx::from_json(query_str.toStdString()), find_options);
			//QString tmp_query = "{\"saddr\":{\"$lte\":5}}";

			//mongocxx::cursor cursor = cur_col.find(bsoncxx::from_json(tmp_query.toStdString()), find_options);
			for (auto obj : cursor)
				ret_vect.push_back(convertObjToMap(obj));
			if (!ret_vect.empty())
			{
				last_id = ret_vect.back()["_id"].toString().toStdString();
//				last_id.erase(last_id.begin(), last_id.begin() + 5);
			}
			return true;
		}
		else
			throw std::runtime_error("\nТаблица не задана");
	}

	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	ret_vect.clear();
	return false;
/*
	QString err_msg = QString("Ошибка запроса данных из таблицы \"%1\": ").arg(cur_col);
	try
	{
		int _start_pos = start_pos;
		if (rezh == 1)
		{
			_start_pos = 0;
		}

		mongo::Query query;
		qDebug() << query_str;
		query = mongo::Query(mongo::fromjson(query_str.toStdString()));

		if (!cur_col.isEmpty())
		{
			if (!hasCollection(cur_col) && !createCollection(cur_col))
				return false;

			db_connect->findN(ret_vect, ns, query, (n == -1) ? 2147483647 : n, _start_pos);
			if (!ret_vect.empty())
			{
				last_id = ret_vect.back().getField("_id").toString();
				last_id.erase(last_id.begin(), last_id.begin() + 5);
			}
			return true;
		}
		else
			throw std::runtime_error(QString("Таблица %1 не задана, либо отсутствует подключение к БД")
			.arg(cur_col).toStdString().c_str());
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	ret_vect.clear();
	return false;*/
}

bool MongoController_v3::getNewRecords(std::vector<QVariantMap>& ret_vect, QString query_str, int chop)
{
	bool res = false;
	QString err_msg = QString("Ошибка запроса данных из таблицы");
	try
	{
		QMutexLocker lock(&mtx);
		if (!cur_col)
			return false;

		if (!last_id.empty())
		{
			QString id_query;
			bsoncxx::builder::stream::document filter;

			bsoncxx::oid tmp_oid(last_id);

			filter << "_id" << open_document << "$gt" << bsoncxx::types::b_oid{ tmp_oid } << close_document;

			id_query = bsoncxx::to_json(filter.extract()).c_str();
			if (query_str == "{}")
			{
				//query_str = QString("{\"_id\": {\"$gt\": \"%1\"}}").arg(last_id.c_str());
				query_str = id_query;
			}
			else
			{
				QString tmp_str = query_str;
				query_str = QString("{\"$and\":[%1, %2]}").arg(id_query).arg(tmp_str);
			}
		}

		QTime count_timer;
		count_timer.start();
		new_records_count = cur_col.count_documents(bsoncxx::from_json(query_str.toStdString()));
		qDebug() << "count_documents " << count_timer.elapsed() << " " << new_records_count;
		if (!new_records_count)
			return true;
		int tmp_records_count = new_records_count;
		if ((chop) && (tmp_records_count > chop))
			tmp_records_count = chop;


		qDebug() << "NLastRecords " << db_name << " " << query_str;

		res = getNRecords(ret_vect, new_records_count-tmp_records_count, tmp_records_count, query_str);

		if (res)
			cur_rec_readed += new_records_count;
	}
	catch (boost::exception& exp)
	{
		emit error(err_msg + boost::diagnostic_information(exp).c_str());
	}
	catch (std::exception& exp)
	{
		emit error(err_msg + exp.what());
	}
	if (!res)
		ret_vect.clear();
	return res;
}

int MongoController_v3::getCountRecords(QString query_str)
{
	try
	{

		QMutexLocker lock(&mtx);
		if (!cur_col)
			return 0;

		//QString tmp_query = "{\"saddr\":{\"$lte\":5}}";
		//bsoncxx::document::view_or_value bson_query = bsoncxx::from_json(tmp_query.toStdString());

		bsoncxx::document::view_or_value bson_query = bsoncxx::from_json(query_str.toStdString());
		return cur_col.count_documents(bson_query);
		
	}
	catch (const std::exception& err)
	{

	}
	catch (...)
	{
	}
	return 0;
}

bool MongoController_v3::isConnected() const
{
	///TODO FIX!!
	return (bool)db_connect;
}

bool MongoController_v3::isStillConnected() const
{
	///TODO FIX!!
	return (bool)db_connect;
}

mongocxx::client* MongoController_v3::getConnectPtr() const
{
	return client_connect;
}

QString MongoController_v3::genColNameForCurDB()
{
	if (!db_connect)
		return "";

	int size = 	getCollectionList(db_name).size();
	QString index = QString("%1").arg(size, 6, 10, QChar('0'));
	QString cur_dt = QDateTime::currentDateTime().toString("hh_mm_ss_dd_MM_yyyy");

	return QString("col_%1_%2").arg(index).arg(cur_dt);
}

QString MongoController_v3::lastReadID()
{
	QMutexLocker lock(&mtx);
	return QString::fromStdString(last_id);
}

void MongoController_v3::setLastID(const QString& lastID)
{
	QMutexLocker lock(&mtx);
	last_id = lastID.toStdString();
}

QString MongoController_v3::getDBPath()
{
	bsoncxx::document::value res = db_connect.run_command(bsoncxx::from_json("{\"getCmdLineOpts\": 1}"));

	QVariantMap res_map = convertObjToMap(res.view());

	QString db_path = res_map["parsed"].toMap()["storage"].toMap()["dbPath"].toString();
	qDebug() << db_path;
	return db_path;
	//mongo::BSONObj info;
	//mongo::BSONElement ret;
	//bool res = db_connect->eval("admin", "db.serverCmdLineOpts().parsed.storage.dbPath", info, ret);
	//return res ? ret.String().c_str() : "";
}

QString MongoController_v3::getLastId()
{
	return QString::fromStdString(last_id);
}


QStringList MongoController_v3::getIndexesList()
{
	mongocxx::cursor indexes_cursor = cur_col.indexes().list();
	QStringList indexes_list;
	for (auto& _ind : indexes_cursor)
		indexes_list.push_back(_ind["name"].get_utf8().value.to_string().c_str());
	return indexes_list;
}

void MongoController_v3::createIndex(const bsoncxx::document::view_or_value& keys)
{
	cur_col.create_index(keys);

}

