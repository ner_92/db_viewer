#ifndef MONGOTMDBCONTROLLERV3_H
#define MONGOTMDBCONTROLLERV3_H


#include "mongo_controller_v3.h"
#include "MongoCommonDBController_v3.h"
#include "../TMDBController.hpp"
#include "../BVDBController.hpp"
#include <qtimer.h>
#include <qsettings.h>
#include <QTime>
#include <QTcpSocket>


/**
*!\class MongoMCADBInterface
*	\brief -- Интерфейс связи с БД ТМ МЦА.
*
*/
class MongoTMDBInterface_v3 : public MongoViewerDBController_v3
{
	Q_OBJECT
public:
	/**
	*	\brief Конструктор класса MongoMCADBInterface
	*	\param _settings_path -- путь к файлу с настройками;
	*	\param _db_name -- имя БД;
	*	\param _db_rusname -- русское имя БД;
	*	\param parent -- родительский объект.
	*/
	MongoTMDBInterface_v3(QString _settings_path, QString _db_name, QString _db_rusname, QObject* parent = 0);
	/**
	*	\brief -- Конвертация из объекта базы в структуру;
	*	\param obj -- объект из базы;
	*	\return -- сообщение в виде структуры.
	*/
	QVariant convertMapToMsg(const QVariantMap& obj);

	/**
	*	\brief -- Запись значения ТМ в базу;
	*	\param tm_code -- код сигнала;
	*	\param value -- значение;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertTMData(int tm_code, const QVariant& value);
	/**
	*	\brief -- Запись значения ТМ в базу;
	*	\param msgs -- вектор сообщений;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertTMData(const std::vector<MsgFromTMDB>& msg_list);
	/**
	*	\brief -- Чтение ТМ сигнала из БД АИК;
	*	\param tm_code -- код сигнала;
	*	\return -- значение ТМ.
	*/
	virtual QVariant getTMData(int tm_code);

	/**
	*	\brief -- Преобразование запроса из структуры в строку.
	*	\param queryList -- запрос в виде структуры;
	*	\return -- запрос в виде строки.
	*/
	QString msgToQuery(QList<QMap<QString, QVariant> > *queryList);
private:
	/// -- Максимальный размер посылки в БД(кол-во сообщений).
	const int max_gr_insert;
	/**
	*	\brief -- Конвертация сообщения в объект BSON;
	*	\param msg -- сообщение для конвертации;
	*	\return -- объект BSON, полученный после конвертации.
	*/
	bsoncxx::document::view_or_value convertToObj(const MsgFromTMDB& msg) const;
};

/**
*!\class MongoMCADBController
*	\brief -- Контроллер БД ТМ МЦА.
*
*/
class MongoTMDBController_v3 : public TMDBController
{
	Q_OBJECT
public:
	/**
	*	\brief -- Конструктор класса MongoMCADBController;
	*	\param parent -- родительский объект.
	*/
	MongoTMDBController_v3(const QString& _db_name, const QString& _db_rus_name, QObject* parent = 0);
	/**
	*	\brief -- Деструктор класса MongoMCADBController;
	*/
	~MongoTMDBController_v3();
	/**
	*	\brief -- Запись значения ТМ в базу;
	*	\param tm_code -- код сигнала;
	*	\param value -- значение;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertTMData(int tm_code, const QVariant& value);
	/**
	*	\brief -- Запись значения ТМ в базу;
	*	\param msgs -- вектор сообщений;
	*	\return -- признак успешности добавления данных.
	*/
	virtual bool insertTMData(const std::vector<MsgFromTMDB>& msg_list);
	/**
	*	\brief -- Чтение ТМ сигнала из БД АИК.
	*	\param tm_code -- код сигнала;
	*	\return -- значение ТМ.
	*/
	virtual QVariant getTMData(int tm_code);

private:
	/// -- Интерфейс связи с СУБД.
	std::shared_ptr<MongoTMDBInterface_v3> TMDBInterface;

	QString db_name;
	QString db_rus_name;
};

#endif // MONGOTMDBCONTROLLERV3_H