#include "XMLReader.h"


XMLReader::XMLReader()
{
}

QList<OK> XMLReader::read(QIODevice *device, QString _addr, QString _paddr, QString _SD, int _SD_count)
{
	xml.setDevice(device);
	addr = _addr;
	saddr = _paddr;
	SD = _SD;
	SD_count = _SD_count;

	if (xml.readNextStartElement()) 
	{
		if (xml.name() == "OKData" || xml.name() == "CBKData")// && xml.attributes().value("version") == "1.0")
			readXML();
		else
			xml.raiseError(QObject::tr("The file is not an XML version 1.0 file."));
	}

	//return !xml.error();
	return okList;
}

QString XMLReader::errorString() const
{
	return QObject::tr("%1\nLine %2, column %3")
		.arg(xml.errorString())
		.arg(xml.lineNumber())
		.arg(xml.columnNumber());
}

void XMLReader::readXML()
{
	Q_ASSERT(xml.isStartElement() && (xml.name() == "OKData" || xml.name() == "CBKData"));

	while (xml.readNextStartElement()) 
	{
		if (xml.name() == "device")
			readDevice();
		else
			xml.skipCurrentElement();
	}
}


void XMLReader::readDevice()
{
	Q_ASSERT(xml.isStartElement() && xml.name() == "device");

	QString curDeviceName = xml.attributes().value("name").toString();
		
	while (xml.readNextStartElement()) 
	{
		QString tmp = xml.name().toString();
		if (xml.name() == "ok")
			readOk(curDeviceName);
		else
			xml.skipCurrentElement();
	}
	QString tmp = xml.name().toString();
}

void XMLReader::readOk(QString curDeviceName)
{
	Q_ASSERT(xml.isStartElement() && xml.name() == "ok");
	QXmlStreamAttributes xmlAttributes = xml.attributes();
	QString ok_id = xmlAttributes.value("id").toString();
	QString ok_name = xmlAttributes.value("name").toString();
	QString ok_descr = xmlAttributes.value("descr").toString();
	QString ok_mkoAddr = xmlAttributes.value("mkoAddr").toString();
	QString ok_mkoSAddr = xmlAttributes.value("mkoSAddr").toString();
	QString ok_mkoWord = xmlAttributes.value("mkoWord").toString();
	QString ok_mkoBitStart = xmlAttributes.value("mkoBitStart").toString();
	QString ok_mkoBitCount = xmlAttributes.value("mkoBitCount").toString();
	QString ok_cbkMsg = xmlAttributes.value("cbkMsg").toString();
	QString ok_cbkWordNum = xmlAttributes.value("cbkWordNum").toString();
	QString ok_cbkWordNumStart = xmlAttributes.value("cbkWordNumStart").toString();
	QString ok_cbkWordNumCount = xmlAttributes.value("cbkWordNumCount").toString();
	QList<okState> ok_state;
	okFormula okForm = { 0 };
	QString analiz_ok_id = "";
	QList<analiz_OK> analiz_ok;

	bool skipIf = false;
	if (((addr == ok_mkoAddr) && (saddr == ok_mkoSAddr) && (ok_mkoWord.toInt()<=SD_count) ) || (addr == "0" && saddr == "0"))
	{
		while (xml.readNextStartElement()) 
		{
			QString tmp = xml.name().toString();
			if (xml.name() == "state")
			{
				ok_state.push_back(readState());
			}
			else if(xml.name() == "formula")
			{
				okForm = readFormula();
			}
			else if (xml.name() == "analiz_ok")
			{
				analiz_ok_id = readAnaliz_ok_id();
			}
			else if (xml.name() == "if")
			{
				analiz_ok.push_back(readAnaliz_ok_if());
				skipIf = true;
			}
			else
				xml.skipCurrentElement();
		}
		QString tmp = xml.name().toString();
		if (skipIf)
			xml.skipCurrentElement();
		OK ok_struckt(ok_id, ok_name, ok_descr, ok_mkoAddr, ok_mkoSAddr, ok_mkoWord, ok_mkoBitStart, ok_mkoBitCount, ok_cbkMsg, ok_cbkWordNum, ok_state, okForm,
			analiz_ok_id, analiz_ok, curDeviceName, ok_cbkWordNumStart, ok_cbkWordNumCount);
		okList.push_back(ok_struckt);
	}
	else
		xml.skipCurrentElement();
	
}

okState XMLReader::readState()
{
	Q_ASSERT(xml.isStartElement() && (xml.name() == "state" || xml.name() == "const_poluch" || xml.name() == "const_res"));

	QXmlStreamAttributes xmlAttributes = xml.attributes();
	QString value = xmlAttributes.value("value").toString();
	QString descr = xmlAttributes.value("descr").toString();
	okState ret(value, descr);
	xml.readNextStartElement();
	return ret;
}

okFormula XMLReader::readFormula()
{
	Q_ASSERT(xml.isStartElement() && xml.name() == "formula");

	QXmlStreamAttributes xmlAttributes = xml.attributes();
	bool available = true;
	QString form = xmlAttributes.value("form").toString();
	QString ci = xmlAttributes.value("ci").toString();
	QString cc = xmlAttributes.value("cc").toString();
	QString ogran_poluch_min = "";							// Ограничение полученного значения снизу
	QString ogran_poluch_max = "";							// Ограничение полученного значения сверху
	QString ogran_res_min = "";								// Ограничение результата снизу
	QString ogran_res_max = "";								// Ограничение результата сверху
	QList<okState> const_poluch_list = QList<okState>();	// Константа полученного значения
	QList<okState> const_res_list = QList<okState>();		// Константа полученного результата

	while (xml.readNextStartElement()) {
		if (xml.name() == "ogran_poluch")
			readOgran_poluch(ogran_poluch_min, ogran_poluch_max);
		else if (xml.name() == "ogran_res")
			readOgran_res(ogran_res_min, ogran_res_max);
		else if (xml.name() == "const_poluch")
			const_poluch_list.push_back(readState());
		else if (xml.name() == "const_res")
			const_res_list.push_back(readState());
		else
			xml.skipCurrentElement();
	}
	
	okFormula ret(available, form, ci, cc, ogran_poluch_min, ogran_poluch_max, ogran_res_min, ogran_res_max, const_poluch_list, const_res_list);
	//xml.readNextStartElement();
	return ret;
}

void XMLReader::readOgran_poluch(QString &poluch_min, QString &mpoluch_max)
{
	Q_ASSERT(xml.isStartElement() && xml.name() == "ogran_poluch");

	QXmlStreamAttributes xmlAttributes = xml.attributes();
	poluch_min = xmlAttributes.value("min").toString();
	mpoluch_max = xmlAttributes.value("max").toString();
	xml.readNextStartElement();
}

void XMLReader::readOgran_res(QString &res_min, QString &res_max)
{
	Q_ASSERT(xml.isStartElement() && xml.name() == "ogran_res");

	QXmlStreamAttributes xmlAttributes = xml.attributes();
	res_min = xmlAttributes.value("min").toString();
	res_max = xmlAttributes.value("max").toString();
	xml.readNextStartElement();
}

QString XMLReader::readAnaliz_ok_id()
{
	Q_ASSERT(xml.isStartElement() && xml.name() == "analiz_ok");
	QString res;
	QXmlStreamAttributes xmlAttributes = xml.attributes();
	res = xmlAttributes.value("id").toString();

	return res;
}

analiz_OK XMLReader::readAnaliz_ok_if()
{
	analiz_OK res = { 0 };
	QXmlStreamAttributes xmlAttributes = xml.attributes();
	res.value = xmlAttributes.value("value").toInt();
	while (xml.readNextStartElement())
	{
		QString tmp = xml.name().toString();
		if (xml.name() == "state")
		{
			res.state.push_back(readState());
		}
	}
	return res;
}

XMLReader::~XMLReader()
{
}
