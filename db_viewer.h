﻿#ifndef DBView_H
#define DBView_H
#include <qheaderview.h>
#include "DBThread.h"
//#include "BVDBThread.h"
#include "multyFindWidget.h"
#include "multyFindForm.h"
#include "GrathWidg.h"
#include "NarabotkaWidg.h"
#include "TM_name_widget.h"
#include "XMLReader.h"
//#include "XMLMDReader.h"
#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include <QListWidget>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QString>
#include <QLabel>
#include <QComboBox>
#include <QTableView>
#include <QStandardItemModel>
#include <QScrollBar>
#include <QWheelEvent>
#include <QCoreApplication>
#include <QToolTip>
#include <QMenu>
#include <QMenuBar>
#include <QStringListModel>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QGroupBox>
#include <QCheckBox>
#include <QSettings>
#include <QFileDialog>
#include <QList>
#include <QStackedWidget>
#include <QDateTime>
#include <QTimer>
#include <QProgressBar>
#include <QPrinter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QPrintPreviewWidget>
#include <QScriptEngine>
#include "instruments.h"

class Image : public QWidget
{
	Q_OBJECT
public:
	Image();
	void form_image(QList<QList<unsigned char> >& data);
private:
	QImage Qim;
	int rows;
	int cols;
protected:
	void paintEvent(QPaintEvent *);
};


/**
*	\class MyModel
*	\brief -- класс MyModel;
*/
class MyModel : public QStandardItemModel
{
	Q_OBJECT
public:
	/// -- Конструктор;
	MyModel(QWidget *parent = 0){}
	
	/**
	*	\brief -- Назначить БД;
	*	\param db -- База данных.
	*/
	void set_db(QString db)
	{
		cur_db = db;
	}

	/**
	*	\brief -- Назначить модель;
	*	\param model -- модель.
	*/
	void set_model(QString model)
	{
		cur_model = model;
	}
	
	/**
	*	\brief -- Флаги;
	*/
	virtual Qt::ItemFlags flags(const QModelIndex& index) const override	//read only
	{
		Qt::ItemFlags result = QStandardItemModel::flags(index);
		result &= ~Qt::ItemIsEditable;
		return result;
	}
protected:

	/**
	*	\brief -- Выравнивание СД в БД МКО;
	*/
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const
	{
		//10 - столбик с СД
		if (cur_db == "MKO" && index.column() != 9)
		{
			//Роль - выравнивание
			if (role == Qt::TextAlignmentRole)
			{
				return Qt::AlignCenter;
			}
		}
		return QStandardItemModel::data(index, role);
	}
private:
	/// -- Выбранная БД;
	QString cur_db;
	/// -- Выбранная модель.
	QString cur_model;
};

/**
*	\class DBView
*	\brief -- класс DBView;
*/
class DBView : public QMainWindow
{
	Q_OBJECT

public:
	
	/**
	*	\brief Конструктор;
	*	\param parent -- Родитель.
	*/
	DBView(QWidget *parent = 0);
	
	/**
	*	\brief -- Деструктор;
	*/
	~DBView();

private:
	/// -- указатель класса DBView;
	QWidget* widg;
	//QWidget* grathWidg;
	/// -- указатель класса grathWidg;
	GrathWidg * grathWidg;
	/// -- указатель класса narabotkaWidg;
	NarabotkaWidg *narabotkaWidg;
	/// -- Виджет мультипоиска;
	QWidget *widgMultyFind;
	/// -- Виджет стандартного поиска;
	QWidget *widgStandartFind;
	/// -- Виджет БД ИК;
	QWidget *DB_IKwidg;
	/// -- Главная таблица;
	QTableView *mainTable;
	/// -- Модель главной таблицы;
	MyModel *table_model;
	/// -- Контейнер указателей потокв;
	QMap<QString, DBReaderThread*> thr_map;
	/// -- Контейнер ip баз;
	QMap<QString, QString> ip_map;
	/// -- Выбранный поток;
	DBReaderThread* cur_thr;
	//MonReaderDBThread* mon_th;
	//DBViewerBVReaderDBThread* bv_th;
	/// -- Лайаут;
	QVBoxLayout *v_layout;
	/// -- Указатель класса multyForm;
	multyFindForm *multyForm;
	/// -- Переключатель виджетов;
	QStackedWidget *stackWidget;
	/// -- Переключатель виджетов;
	QStackedWidget *mainStackWidget;
	/// -- Гридлайаут;
	QGridLayout *multyLayout;
	
	/// -- Кнопка поиска;
	QPushButton *findButton;
	/// -- Кнопка поиска с интервалом;
	QPushButton *findIntervalButton;
	/// -- Кнопка мультипоиска;
	QPushButton *multyFindButton;
	/// -- Кнопка поиска;
	QPushButton *findQueryButton;
	/// -- Кнопка конвертации в q10;
	QPushButton *translateToQ10Button;
	/// -- Кнопка просмотра ИК;
	QPushButton *IKViewButton;

	/// -- Кнопка БД МКО;
	QPushButton *DB_MKO_button;
	/// -- Кнопка БД ИК;
	QPushButton *DB_IK_button;
	/// -- Кнопка ОКов;
	QPushButton *OK_open_Button;

	/// -- Кнопка формат ИК;
	QPushButton *FormatIK15;
	/// -- Кнопка формат ИК;
	QPushButton *FormatIK8;
	/// -- Кнопка формат ИК;
	QPushButton *FormatIKWTF8;

	/// -- Контейнер с кнопками ТМ
	QList<QPushButton*> *TM_buttons;

	/// -- Поле МСК1;
	QLineEdit *MSK1Edit;
	/// -- Поле МСК2;
	QLineEdit *MSK2Edit;
	/// -- Поле адрес;
	QLineEdit *ADREdit;
	/// -- Поле подадрес;
	QLineEdit *PADREdit;
	/// -- Поле количество СД;
	QLineEdit *CountSDEdit;
	/// -- Поле СД;
	QTextEdit *SDEdit;
	/// -- Поле ботвого времени;
	QLineEdit *BT1Edit;
	/// -- Поле бортового времени;
	QLineEdit *BT2Edit;
	/// -- Поле ОК;
	QLineEdit *OKEdit;
	/// -- Поле КС;
	QLineEdit *CWEdit;
	/// -- Поле ОС;
	QLineEdit *AWEdit;
	/// -- Поле КУ СПОБУЕ;
	QLineEdit *KU_SPOBUEdit;
	/// -- Поле КС КПИ2;
	QLineEdit *KS_KPI2Edit;
	/// -- Поле МД ЦБК;
	QLineEdit *MD_CBKEdit;
	/// -- Поле ЮТС;
	QLineEdit *UTCEdit;
	/// -- Поле ЮТС;
	QLineEdit *UTCEdit2;
	/// -- Поле расхождение БВ;
	QLineEdit *diffBTEdit;
	/// -- Поле расхождение БВ;
	QLineEdit *diffBTEdit2;
	/// -- Поле сигнал ТМ;
	QLineEdit *signalTMEdit1;
	/// -- Поле сигнал ТМ;
	QLineEdit *signalTMEdit2;
	/// -- Поле наименование ТМ;
	QLineEdit *nameTMEdit;
	/// -- Поле значение ТМ;
	QLineEdit *valueTMEdit1;
	/// -- Поле значение ТМ;
	QLineEdit *valueTMEdit2;
	/// -- Поле номер ТМ;
	QLineEdit *numberTMEdit1;
	/// -- Поле номер ТМ;
	QLineEdit *numberTMEdit2;
	/// -- Поле измеренного значения;
	QLineEdit *measuredValueEdit1;
	/// Поле измеренного значения;
	QLineEdit *measuredValueEdit2;
	//QLineEdit *calculatedValueEdit1;
	//QLineEdit *calculatedValueEdit2;
	/// -- Поле метки;
	QLineEdit *metkaEdit;
	/// -- Поле номера строки;
	QLineEdit *nStartFrameEdit;
	/// -- Поле номера строки;
	QLineEdit *nEndFrameEdit;
	/// -- Поле пути к файлу;
	QLineEdit *filePathEdit;
	/// -- Поле КфК;
	QLineEdit *KFKEdit1;
	/// -- Поле КФК;
	QLineEdit *KFKEdit2;

	/// -- Метка МСК;
	QLabel *MSKLabel;
	/// -- Метка адрес;
	QLabel *ADRLabel;
	/// -- Метка подадрес;
	QLabel *PADRLabel;
	/// -- Метка количество СД;
	QLabel *CountSDLabel;
	/// -- Метка СД;
	QLabel *SDLabel;
	/// Метка БВ;
	QLabel *BTLabel;
	/// -- Метка ОК;
	QLabel *OKLabel;
	/// -- Метка КС;
	QLabel *CWLabel;
	/// -- Метка ОС;
	QLabel *AWLabel;
	/// -- Метка КУ СПОБУ;
	QLabel *KU_SPOBULabel;
	/// -- Метка КПИ2;
	QLabel *KS_KPI2Label;
	/// -- Метка ЦБК;
	QLabel *MD_CBKLabel;
	/// -- Метка ЮТС;
	QLabel *UTCLabel;
	///--  Метка расхождение БВ;
	QLabel *diffBTLabel;
	/// -- Метка сигнал ТМ;
	QLabel *signalTMLabel;
	///--  Метка наименование ТМ;
	QLabel *nameTMLabel;
	/// -- Метка значние;
	QLabel *valueTMLabel;
	/// -- Метка номер ТМ;
	QLabel *numberTMLabel;
	/// -- Метка измеренное значение;
	QLabel *measuredValueLabel;
	//QLabel *calculatedValueLabel;
	/// -- Метка метка;
	QLabel *metkaLabel;
	/// -- Метка номер ИК;
	QLabel *nomerIKLabel;
	/// -- Метка путь к файлу;
	QLabel *filePathLabel;
	/// -- Метка МД ЦБК;
	QLabel *MD_CBK_Label;
	/// -- Метка ДТК;
	QLabel *DTK_Label;
	/// -- Метка квитанции;
	QLabel *Kvitancii_Label;
	/// -- Метка КФК;
	QLabel *KFKlabel;

	/// -- Комбобокс МД ЦБК;
	QComboBox *MD_CBK_ComboBox;
	/// -- Комбобокс ДТК;
	QComboBox *DTK_ComboBox;
	/// -- Комбобокс квитанции;
	QComboBox *Kvitancii_ComboBox;

	/// -- Групбокс МКО;
	QGroupBox *MMKOGroupBox;
	/// -- Групбокс МКО;
	QGroupBox *LMKOGroupBox;
	/// -- Групбокс формат;
	QGroupBox *FormatGroupBox;
	/// -- Групбокс режим;
	QGroupBox *modeGroupBox;

	/// -- Групбокс фильтр МКО;
	QGroupBox *filter1MKOGroupBox;
	/// -- Групбокс фильтр МКО;
	QGroupBox *filter2MKOGroupBox;
	/// -- Групбокс фильтр МКО;
	QGroupBox *filter3MKOGroupBox;
	/// -- Групбокс фильтр МКО;
	QGroupBox *filter4MKOGroupBox;
	/// -- Групбокс фильтр МКО;
	QGroupBox *filter5MKOGroupBox;
	/// -- Групбокс фильтр МКО;
	QGroupBox *filter6MKOGroupBox;
	/// -- Групбокс фильтр МКО;
	QGroupBox *filter7MKOGroupBox;
	/// -- Компановка;
	QHBoxLayout *h_filterMKOLayout;

	/// -- Групбокс фильтр ИК;
	QGroupBox *filter1IkGroupBox;
	/// -- Групбокс фильтр ИК;
	QGroupBox *filter2IkGroupBox;
	/// -- Групбокс фильтр ИК;
	QGroupBox *filter3IkGroupBox;
	/// -- Компановка;
	QHBoxLayout *h_filterIKLayout;

	/// -- Компановка;
	QHBoxLayout *modeLayout;
	/// -- Компановка;
	QGridLayout *mdLayout;

	/// -- Чекбокс;
	QCheckBox *MMKOCheckBox1;
	/// -- Чекбокс;
	QCheckBox *MMKOCheckBox2;
	/// -- Чекбокс;
	QCheckBox *LMKOCheckBox1;
	/// -- Чекбокс;
	QCheckBox *LMKOCheckBox2;
	/// -- Чекбокс;
	QCheckBox *FormatCheckBox1;
	/// -- Чекбокс;
	QCheckBox *FormatCheckBox2;
	/// -- Чекбокс;
	QCheckBox *FormatCheckBox3;
	//QCheckBox *modeCheckBox1;
	//QCheckBox *modeCheckBox2;
	//QCheckBox *modeCheckBox3;

	/// -- Компановка;
	QGridLayout *filtrLayout;


	/// -- Компановка;
	QGridLayout *DB_MKOmini1GridLayout;
	/// -- Компановка;
	QGridLayout *DB_MKOmini2GridLayout;
	/// -- Компановка;
	QGridLayout *DB_MKOmini3GridLayout;
	/// -- Компановка;
	QGridLayout *DB_MKOmini4GridLayout;
	/// -- Компановка;
	QGridLayout *DB_MKOmini5GridLayout;
	/// -- Компановка;
	QGridLayout *DB_MKOmini6GridLayout;
	/// -- Компановка;
	QGridLayout *DB_MKOmini7GridLayout;

	/// -- Компановка;
	QGridLayout *DB_IKmini1GridLayout;
	/// -- Компановка;
	QGridLayout *DB_IKmini2GridLayout;
	/// -- Компановка;
	QGridLayout *DB_IKmini3GridLayout;

	/// -- Контейнер указателей на мультиформ;
	QList<multyFindForm*> *multiFormList;
	/// -- Контейнер значений;
	QList<QMap<QString, QVariant> > formValueList;
	/// -- Контейнер значений;
	QList<QMap<QString, QVariant> > standartFindList;

	/// -- Контейнер с содержанием инишника баз ТМ
	QList<TMDBs> db_names;

	QMap<int, QString> tm_aik_names;

	QMap<int, QString> tm_aik_meas;


	QMap<int, QString> tm_mca_names;

	QMap<int, QString> tm_mca_meas;

	/// -- Индекс форм;
	int currentFormIndex;
	/// -- Скроллбар;
	QScrollBar *vbar;

	/// -- Меню;
	QMenu *subdMenu;

	/// -- Меню;
	QMenu *settingsMenuMKO;
	/// -- Меню;
	QMenu *settingsMenuIK;
	/// -- Меню;
	QMenu *viewMenu;
	/// -- Меню;
	QMenu *helpMenu;

	QList<QMenu*> *TM_menus;

	/// -- Действие;
	QAction *foundManagerAct;
	/// -- Действие;
	QAction *grathicManagerAct;
	/// -- Действие;
	QAction *narabotkiManagerAct;
	/// -- Действие;
	QAction *settingSUBDAct;

	/// -- Действие;
	QAction *defaultFiltersAct;
	/// -- Действие;
	QAction *allFiltersAct;
	/// -- Действие;
	QAction *clearAct;
	/// -- Действие;
	QAction *openFiltersAct;
	/// -- Действие;
	QAction *openLastFiltersAct;
	/// -- Действие;
	QAction *saveFiltersAct;
	/// -- Действие;
	QAction *saveFiltersAsAct;
	/// -- Действие;
	QAction *printAct;
	/// -- Действие;
	QAction *ipMonAct;

	/// -- Действие;
	QAction *aboutAct;

	/// -- Действие;
	QAction *showHideColumnAct;
	/// -- Действие;
	QAction *zebraAct;
	/// -- Действие;
	QAction *countRecAct;

	/// -- Действие;
	QAction *copyAct;
	
	/// -- Запрос строкой;
	QString query_str;

	/**
	*	\brief -- Получить количество записей;
	*	\return Количество записей.
	*/
	int getCount();

	//int getBVCount();
	/**
	*	\brief -- Создание заголовков таблицы;
	*	\return.
	*/
	void createTableHeaders();
	/// -- Колисетво записй снизу;
	int numRecordsDown;
	/// -- Количество записей сверху;
	int numRecordsUp;
	// numRecordsUp - delta = строка в таблице, с которой срабатывает запрос новых строк при скроллинге вниз
	/// -- коэфициент;
	int delta;
	/// -- значение курсора;
	int vbarCursor = 0;
	/// -- значение курсора;
	int barCursor = 0;
	/// -- Флаг игнорирования сигналов;
	bool ignor;
	/// -- Нижний блок;
	int blockDown;
	/// -- Верхний блок;
	int blockUp;
	/// -- Количество строк;
	int countStrings;
	/// -- Флаг положения курсора;
	bool cursorDown;

	/// -- Флаг в работе;
	bool inWork;
	/// -- Позиция по X;
	int posX;
	/// -- Позиция по  Y;
	int posY;

	/**
	*	\brief -- Создание меню;
	*/
	void createMenus();

	/**
	*	\brief -- Создание действий меню;
	*/
	void createActions();

	/// -- Просмотор коллекций;
	QListView *collectionList;
	/// -- Модель;
	QStringListModel *model;
	/// -- Ячейка;
	QString collName;
	/// -- Указатель класса диалог;
	QDialog *dlg;
	/// -- Флаг режима поиска с интервалом;
	bool interval;
	/// -- Флаг первого поиск с интервалом. Для очистки таблицы;
	bool firstInterval;
	/// --путь к DB_settings.ini;
	QString settings_dir_path;
	/// --путь к DBFrame_settings.ini;
	QString frame_settings_dir_path;
	/// --настройки кадра
	QSettings *frame_settings;
	///
	QString remote_frame_path;
	/// -- Флаг отображения ВСЕХ фильтров(когда false - фильтры из раздела "все" игнорируются при запросе);
	bool allFilters;
	/// -- Позиция формы по X;
	int findFormPosX;
	/// -- Позиция формы по Y;
	int findFormPosY;

	/**
	*	\brief -- Изменение индекса при работе с мультиформами;
	*	\param currentIndex -- настоящий индекс.
	*/
	void changeIndex(int currentIndex);

	/**
	*	\brief -- Изменение позиции в формлисте;
	*/
	void changePosFormList();

	/// --индекс формы, вызвавшей диалог;
	int indexForm;
	multyFindWidget *multyDialog;
	/// -- флаг мультипоиска;
	bool multyFindOn;
	
	/**
	*	\brief -- Перенос текста из листа значений формочек на ТекстЭдит формочек;
	*/
	void textFromlistToForm(int index);
	//std::vector<MsgFromBVDB> BVvector;
	/// -- Таймер;
	QTime testTime;
	
	/**
	*	\brief -- Получить структуру запроса;
	*	\return Структура запроса.
	*/
	QList<QMap<QString, QVariant> > getQueryStruct();

	/// -- IP- адоресс монитора;
	QString mon_ip = "localhost";
	/// -- Прогрессбар;
	QProgressBar *progressBar;
	/// -- Лейб прогресс бара;
	QLabel *progressLabel;
	/// -- Список скрываемых столбцов;
	QList<int> *hideColumnList;
	/// -- количество запрашиваемых записей;
	int countRecord = 200;
	/// -- Лист ТМ виджетов
	QList<TM_name_widget*> *TM_widgets;
	/**
	*	\brief -- Валидация введённого ;
	*/
	void SDTextChange();

	/**
	*	\brief -- Отображение ОКа;
	*	\param adr -- адрес;
	*	\param padr -- подадрес;
	*	\param SD -- слово данных;
	*	\param countSD -- Количество СД;
	*	\param whoIs -- устройство.
	*/
	void OK_dlg(QString adr, QString padr, QString SD, QString countSD, QString whoIs = "");

	/**
	*	\brief -- Отображение массива;
	*	\param SD -- слово данных;
	*/
	void Any_dlg(QString SD);
	
	/**
	*	\brief -- Анализ ОК;
	*	\param ok -- ОК;
	*	\param SD - СД;
	*	\param okList -- Лист ОКов;
	*	\param color -- Цвет выделения;
	*	\param MDCBK -- Флаг работы с массивом;
	*	\param ifState -- тег значения.
	*	\return Состояние ОКа.
	*/
	QString checkOkState(OK ok, QString SD, QList<OK> okList, int &color, bool MDCBK, bool ifState = false);
	bool show = false;
signals:
	/**
	*	\brief -- Сигнал подтверждения;
	*/
	void acc();
public slots:

	/**
	*	\brief -- Вывод ошибки;
	*	\param msg -- Сообщение ошибки;
	*/
	void error(QString msg);

	/**
	*	\brief -- Вывод результата поиска;
	*/
	void showData();

	/**
	*	\brief -- Начать поиск;
	*/
	void startSearch();

	/**
	*	\brief -- Начать поиск с интервалом;
	*/
	void startIntervalSearch();

	/**
	*	\brief -- Движение скроллбара таблицы;
	*	\param n -- значение скроллбара.
	*/
	void slideBarIsMoved(int n);

	/**
	*	\brief -- Движение бара;
	*	\param n -- позиция.
	*/
	void slideVBarIsMoved(int n);

	/**
	*	\brief -- Обновить курсор скроллбара;
	*/
	void cursurUpdate()
	{
		mainTable->verticalScrollBar()->setValue(countStrings - (delta - 1));
		mainTable->verticalScrollBar()->setValue(countStrings - delta);		//fix белого экрана
		//mainTable->scrollToBottom();
		ignor = false;
		inWork = false;
	}

	/**
	*	\brief -- Игнорирование новых сигналов;
	*/
	void ignorTimer()
	{
		ignor = false;
		inWork = false;
	}

	/**
	*	\brief -- Игнорирование сигналов скроллбара во зажатия;
	*/
	void slideVBarPressed()
	{
		ignor = true;
	}

	/**
	*	\brief -- Отпускание скроллбара после зажатия;
	*/
	void slideVBarReleased()
	{
		ignor = false;
		slideVBarIsMoved(vbar->value());
	}

	/**
	*	\brief -- Окно настроек СУБД;
	*	\param show -- отображение окна.
	*/
	void settingSUBD(bool show = true);

	/**
	*	\brief -- Выбор коллекции;
	*/
	void useCollection()
	{
		if (!model->rowCount() > 0)
		{
			emit acc();
			return;
		}
		QModelIndex index = collectionList->currentIndex();
		//collName = collectionList->Data(index).;
		collName = model->data(index, Qt::DisplayRole).toString();
		//cur_thr->connectWithCollection(collName, mon_ip);
		for (QMap<QString, DBReaderThread*>::iterator itr = thr_map.begin(); itr != thr_map.end(); itr++)
		{
			QString ip = ip_map.value(itr.key());
			QString db = itr.key();
			(*itr)->connectWithCollection(collName, ip);
			if (db != "Frame")
			{
				grathWidg->connectToDB(db);
				grathWidg->connectWithCollection(collName, ip);
			}
		}
		/*if (stackWidget->currentWidget() != grathWidg)
			startSearch();*/
		//grathWidg->connectWithCollection(collName, mon_ip);
		//emit needReconectGrath(collName, mon_ip);
		emit acc();
	}

	/**
	*	\brief -- Использование последней коллекции;
	*/
	void useLastCollection()
	{
		if (!model->rowCount() > 0)
		{
			emit acc();
			return;
		}
		QModelIndex index = model->index(model->rowCount() - 1); //системная
		collName = model->data(index, Qt::DisplayRole).toString();
		if (collName == "system.js")
		{
			index = model->index(model->rowCount() - 2); //системная
			collName = model->data(index, Qt::DisplayRole).toString();
		}
		//cur_thr->connectWithCollection(collName, mon_ip);
		for (QMap<QString, DBReaderThread*>::iterator itr = thr_map.begin(); itr != thr_map.end(); itr++)
		{
			QString ip = ip_map.value(itr.key());
			QString db = itr.key();
			(*itr)->connectWithCollection(collName, ip);
			if (db != "Frame")
			{
				grathWidg->connectToDB(db);
				grathWidg->connectWithCollection(collName, ip);
			}
		}
		/*if (stackWidget->currentWidget() != grathWidg)
			startSearch();*/
		
		//emit needReconectGrath(collName, mon_ip);
		emit acc();
	}
	
	/**
	*	\brief -- Выбор коллекции;
	*/
	void collectionViewDoubleClicked()
	{
		useCollection();
	}

	/**
	*	\brief -- Стандартные фильтры;
	*/
	void setDefaultFilters();
	
	/**
	*	\brief -- Все фильтры;
	*/
	void setAllFilters();

	/**
	*	\brief -- Выбор режима мультипоиска;
	*/
	void multyFind();

	/**
	*	\brief -- Сохранить фильтры;
	*/
	void saveFilters();

	/**
	*	\brief -- Загрузить фильтры;
	*/
	void loadFilters();

	/**
	*	\brief -- Сохранить фильтры как;
	*/
	void saveFiltersAs();

	/**
	*	\brief -- Загрузить фильтры как;
	*/
	void loadFiltersAs();

	/**
	*	\brief -- Добавить новую форму в мультипоиске;
	*/
	void addNewFindForm();

	/**
	*	\brief -- Удалить форму в мультипоиске;
	*/
	void delFindForm(int curIndex);

	/**
	*	\brief -- Вывод очередного набора в режиме мультипоиск;
	*	\param curIndex -- текущий индекс;
	*/
	void showMultyDialog(int curIndex);

	/**
	*	\brief -- Собрать текст фильтров в мультипоиске;
	*/
	void setMultyQueryString();
	// Получение данных из базы БВ;
	//void getBVtime();

	/**
	*	\brief -- Окно ввода ip-адресса монитора;
	*/
	void ipForMon();

	/**
	*	\brief -- Изменение значение прогресс бара;
	*	\param value -- Значение ;
	*	\param msg -- Сообщение.
	*/
	void changeProgressLabel(int value, QString msg);

	/**
	*	\brief -- Вывод менеджера графиков;
	*/
	void grathicManager();

	/**
	*	\brief -- Вывод менеджера поиска ;
	*/
	void foundManager();

	/**
	*	\brief -- Вывод менеджера наработки;
	*/
	void narabotkaManager();


	/**
	*	\brief -- Настройка графики для БД ИК;
	*/
	void DB_IKWidgConstructor();


	/**
	*	\brief -- Выбрать БД ТМ;
	*/
	void setDB_TM();

	/**
	*	\brief -- Выбрать БД МКО;
	*/
	void setDB_MKO();

	/**
	*	\brief -- Выбрать БД ИК;
	*/
	void setDB_IK();
	
	/**
	*	\brief -- Привести графику в исходное;
	*/
	void uncheckedMenuDB();

	/**
	*	\brief -- Клик по таблице;
	*	\param index -- индекс.
	*/
	void onTableClicked(const QModelIndex &index);
	
	/**
	*	\brief --  Скрыть/показать столбцы таблицы;
	*/
	void showHideColumnDlg();

	/**
	*	\brief -- Скрыть столбцы таблицы;
	*	\param check_list -- Чек лист.
	*/
	void hideColumn(QList<int> check_list);

	/**
	*	\brief -- Раскрасска таблицы в корпоративные цвета;
	*/
	void showZebra();

	/**
	*	\brief -- копирование ячеек;
	*/
	void copy_cell();

	/**
	*	\brief -- Печать;
	*/
	void print_doc();

	/**
	*	\brief -- Очистка полей;
	*/
	void clearFields();

	/**
	*	\brief -- Валидация фильтра времени;
	*	\param str -- валидируемая строка.
	*/
	void MSKTextCheck(QString str);

	/**
	*	\brief --  Окно настройки количества запрашиваемых записей.
	*/
	void dlgCountRecords();

	/**
	*	\brief -- Настойка количества запрашиваемых записепй;
	*	\param count -- Количество запрашиваемых записей.
	*/
	void setCountRecord(int count);

	/**
	*	\brief -- Клик по таблице МД ЦБК;
	*	\param index -- Индекс.
	*/
	void MDCBKClicked(const QModelIndex &index);
	
	/**
	*	\brief -- Просмотр данных ИК;
	*/
	void IKView();

	/**
	*	\brief -- Просмотр данных q10 в графическом виде.
	*/
	void Q10View();

	/**
	*	\brief -- Получить строку запроса;
	*/
	void getQueryString();

	/**
	*	\brief -- Назначить формат ИК;
	*/
	void setFormatIKButton();

	void writeLog(QString text)
	{
		QFile mFile("db_viewer_log.log");
		if (mFile.open(QFile::Append))
		{
			QTextStream out(&mFile);
			out << QDateTime::currentDateTime().toString("yyyy.MM.dd_hh.mm.ss") << " " << text << "\r\n";
			mFile.close();
		}
		return ;
	}
};

#endif // DBView_H
