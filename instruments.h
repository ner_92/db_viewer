#ifndef INSTRUMENTS_H
#define INSTRUMENTS_H

#include <qvariant.h>
#include <qscriptengine.h>
#include <qfont.h>
#include <qcoreapplication.h>
#include <QDirIterator>

struct TMDBs
{
	QString DBType;
	QString rusName;
	QString engName;
};

namespace instr
{
	/**
	*	\brief Отображение строки
	*	\param str - исходная строка
	*	\param hexformat - флаг отображения целый чисел в шестнадцатиричном виде(по умолчанию отключено)
	*	\return Строку в изменненном формате
	*/
	QString displayString(const QString& str, bool hexformat = true);
	/**
	*	\brief Преобразование данных в строку
	*	\param val - данные
	*	\param hexformat - флаг отображения целый чисел в шестнадцатиричном виде
	*	\return Строка с данными
	*/
	QString variantToString(const QVariant& val, bool hexformat = false);
	/**
	*	\brief Преобразование массива в строку
	*	\param list - массив данных
	*	\param hexformat - флаг отображения целый чисел в шестнадцатиричном виде
														(по умолчанию отключено)
	*	\return Строка с данными массива
	*/
	QString variantListToString(const QVariantList& list, bool hexformat = false);
	/**
	*	\brief Преобразование строки в QVariant
	*	\param str - строка
	*	\param type - тип переменной
	*	\param eng - исполнитель скриптов
	*	\return QVariant
	*/
	QVariant convertToVariant(const QString& str, 
							  const QVariant::Type type = QVariant::UserType, 
							  QScriptEngine* eng = 0);

	/**
	*	\brief Чтение версии программы из файла(Git hex)
	*	\return QVariant
	*/
	QString readVersion();
	/**
	*	\brief Вычисление контрольной суммы программы
	*	\param path - путь к файлу(включая имя файла)
	*	\return Контрольная сумма
	*/
	QString CalcHash(const QString& path);
	/**
	*	\brief Вычисление контрольной суммы папки
	*	\param path - путь к папке
	*	\return Контрольная сумма
	*/
	QString CalcFolderHash(const QString& path);

	/**
	*	\brief Перевод данных из QVariant в формат скрипта
	*	\param val - данные
	*	\param eng - МИИП
	*	\return Данные в формате скрипта
	*/
	QScriptValue QVariantToScriptValue(QVariant val, QScriptEngine* eng);
	/**
	*	\brief Перевод данных из QScriptValue в QVariant
	*	\param val - данные
	*	\param eng - МИИП
	*	\return Данные в формате QVariant
	*/
	QVariant QScriptValueToVariant(QScriptValue val, QScriptEngine* eng);
	

	QString GetPathFromSettings(QString applname);
	QString GetIpFromSettings(QString srver);


	QFont mainFont();

	QString get_table_style();

	QString get_aik_colour(int);

	bool showSimpleMsg(const QString& title, const QString& text);
	int showSimpleMsgButtons(const QString& title, const QString& text, const QStringList& btns = QStringList());


	QList<TMDBs> getTMDBs();

	bool checkTemplate(QByteArray data, QString templ);

	QVariantList arrToList(const QByteArray& _arr);
	QVariantList arrToShortList(const QByteArray& _arr);
	QByteArray listToArr(const QVariantList& _list);
	QByteArray shortListToArr(const QVariantList& _list);

	std::vector<bool> byteToBits(const QByteArray ba);
	QByteArray bitsToByte(std::vector<bool> bits);
	quint16 KSM_msg(QByteArray new_msg);
	bool controlUnParity(const quint16 spo);	// true, если нечётное количество 1

	int gen_crc5(std::vector<bool> data);
	QByteArray gen_crc6(std::vector<bool> data);
	int gen_crc16(const QByteArray data);
}

#endif