#ifndef GRATHWIDG_H
#define GRATHWIDG_H

#include "DBThread.h"
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QBoxLayout>
#include <QGridLayout>
#include <QTextEdit>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsTextItem>
#include <QDateTime>
#include <QStandardItem>
#include <QPoint>
#include <QEvent>
#include <QMouseEvent>
#include <QProgressBar>
#include <QTimer>
#include <QComboBox>
#include "XMLReader.h"
#include <QMessageBox>
#include <bitset>

#include "SearchBox.h"

/// -- Структура координат;
struct grathValues
{
	grathValues(float _oldX = 0, float _oldY = 0, long long _newX = 0, float _newY = 0)
	: oldX(_oldX), oldY(_oldY), newX(_newX), newY(_newY)
	{}
	/// -- Старый X;
	float oldX = 0;
	/// -- Старый Y;
	float oldY = 0;
	/// -- Новый X;
	long long newX = 0;
	/// --  Новый Y;
	float newY = 0;
	/// -- Ручка.
	QPen pen;
};

/// -- Структура координат точки;
struct PointData
{
	PointData(quint64 dt, int x, double y)
	{
		this->dt = dt;
		this->x = x;
		this->y = y;
	}
	/// -- Дата;
	quint64 dt= 0;
	/// -- X;
	int x= 0;
	/// -- Y.
	double y= 0;
};

/// -- Структура значения ОКов;
struct okValues
{
	okValues(int _addr = 0, int _paddr = 0, int _word = 0, int _bit = 0)
	: addr(_addr), paddr(_paddr), word(_word), mask(0)
	{}
	/// -- Адрес;
	int addr  = 0;
	/// -- Подадрес;
	int paddr = 0;
	/// -- Слово;
	int word  = 0;
	/// -- Маска;
	std::bitset<16> mask;
	/// -- Ручка;
	QPen pen;
	/// -- Графическое значение.
	QGraphicsTextItem *textValue;
};

/// -- Структура сообщений для БД ТМ АИК.
struct TMValues
{
	/**
	*	\brief -- Конструктор;
	*	\param _tm_code -- код сигнала;
	*	\param _value -- значение;
	*/
	TMValues(int _tm_code = 0, double _value = 0.0)
	: tm_code(_tm_code), value(_value)
	{}

	/// -- Код сигнала;
	int tm_code = 0;
	/// -- Значение;
	double value = 0.0;
	/// -- Ручка;
	QPen pen;
	/// -- Графическое значение.
	QGraphicsTextItem *textValue;

};


/**
*	\class GrathWidg
*	\brief -- класс GrathWidg;
*/

class GrathWidg: public QWidget
{
	Q_OBJECT
public:

	/**
	*	\brief -- Конструктор;
	*	\param parent -- Родитель.
	*/
	GrathWidg(QWidget *parent = 0);
	
	/**
	*	\brief -- Деструктор;
	*/
	~GrathWidg();
private:
	/// -- Контейнер потоков;
	QMap<QString, DBReaderThread*> thr_map;
	/// -- Выбранный поток;
	DBReaderThread* cur_thr;

	/// -- Метка МСК;
	QLabel *MSKLabel;
	/// -- Поле МСК;
	QLineEdit *MSK1Edit;
	/// -- Поле ;
	QLineEdit *MSK2Edit;
	/// -- Метка БВ;
	QLabel *BTLabel;
	/// -- Поле БВ;
	QLineEdit *BT1Edit;
	/// -- Поле БВ;
	QLineEdit *BT2Edit;
	/// -- Метка сигнал;
	QLabel *TLSignanLabel;
	/// -- Поле сигнал;
	SearchBox *TLSignanEdit;
	/// -- График;
	QGraphicsView *view;
	/// -- График;
	QGraphicsScene *scene;
	/// -- График;
	QGraphicsView *leftView;
	/// -- График;
	QGraphicsScene *leftScene;
	/// -- График;
	QGraphicsView *botView;
	/// -- График;
	QGraphicsScene *botScene;
	/// -- Графический объект;
	QGraphicsTextItem *text;
	/// -- Графический объект;
	QGraphicsTextItem *leftText;
	/// -- Графический объект;
	QGraphicsLineItem *leftLine;
	/// -- Графический объект;
	QGraphicsTextItem *botText;
	/// -- Графический объект;
	QGraphicsLineItem *botLine;
	/// -- Графический объект;
	QGraphicsLineItem *sceneLine;
	/// -- Кнопка построить график;
	QPushButton *buildButton;
	/// -- Контейнер значений;
	QList<QMap<QString, QVariant> > *queryValueList;
	/// -- Графический объект;
	grathValues *grathStructValues;
	/// -- Значение ОКов
	okValues *okStruckValues;
	/// -- Значение TM
	TMValues *TMStruckValues;

	/// -- Кнопка приблизить;
	QPushButton *zoomInButton;
	/// -- Кнопка отдалить;
	QPushButton *zoomOutButton;
	/// -- Кнопка сместить влево;
	QPushButton *zoomLeftButton;
	/// -- Кнопка сместить вправо;
	QPushButton *zoomRightButton;

	/// -- Вектор при построении графика;
	std::vector<QVariant> globalVect;	 
	/// -- Ширина view;
	float x;
	/// -- Высота view;
	float y;	

	/**
	*	\brief -- Добавить к запросу;
	*	\param okStruckValues - значения ОКов.
	*/
	void appendQueryMKO(okValues *okStruckValues);

	/**
	*	\brief -- Добавить к запросу;
	*	\param okStruckValues - значения TMов.
	*/
	void appendQueryAIK(TMValues *TMStruckValues);

	/// -- Контейнер векторов со значениями ОКов;
	QList<QList<PointData>> *valuesList;
	/// -- Контейнер значений;
	std::vector<grathValues*> *vec;
	/// -- Контейнер с параметрами ОКов;
	QList<okValues*> *okList;
	/// -- Контейнер с параметрами TMов;
	QList<TMValues*> *TMList;
	/// -- Контейнер точек;
	QList<PointData> *points;
	
	/// -- Константа сдвига по X;
	const int shift = /*80*/10;
	/// -- Константа сдвига по Y;
	const int shiftY = 10;

	/**
	*	\brief -- eventFilter;
	*	\param object -- object;
	*	\param event -- event.
	*	\return eventFilter.
	*/
	bool eventFilter(QObject* object, QEvent* event);
	/// -- Флаг отрисовки;
	bool draw = true;
	/// -- Минимальное значение;
	int grathMinY;
	/// -- Минимальное значение;
	int grathMaxY;
	/// -- ND;
	long long ND;
	/// -- KD;
	long long KD;
	/// -- norm;
	long long norm;
	/// -- Выбранная БД;
	QString currentDB = "";
	/// -- БД последнего запроса. Нужно для графики;
	QString queryDB = "";
	/// -- Прогрессбар;
	QProgressBar *progressBar = nullptr;
	/// -- Метка прогресса;
	QLabel *progressLabel;
	/// -- Левое смещение вектора ;
	int vectLeftValue;
	/// -- Правое смещение вектора ;
	int vectRightValue;	
	/// Лист ОКов из xml
	QList<OK> ok_list;
public slots:
	
	/**
	*	\brief -- Отрисовка задника: оси и их значения;
	*	\param maxY -- Максимальный X;
	*	\param minY -- Минимальный Y.
	*/
	void drawBack(double maxY, double minY);

	/**
	*	\brief -- Отрисовка графика;
	*	\param vect -- Вектор;
	*	\param maxY -- Максимальный X.
	*/
	void drawGrath(std::vector<QVariant> vect, double maxY);
	/**
	*	\brief -- Слот приёма даных;
	*/
	void receiptData();
	
	/**
	*	\brief -- Анализ данных;
	*	\param vect -- Вектор.
	*/
	void data(std::vector<QVariant> vect);

	/**
	*	\brief -- Запрос;
	*/
	void query();
	
	/**
	*	\brief -- Отрисовка левой линии;
	*	\param x -- x;
	*	\param y -- y.
	*/
	void drawLeftBotLine(int x, int y);

	/**
	*	\brief -- Стереть линии;
	*/
	void removeLeftBotLine();

	/**
	*	\brief -- Отрисовка знчения;
	*	\param x -- x.
	*/
	void drawGrathValue(int x);

	/**
	*	\brief -- Стереть значение;
	*/
	void removeGrathValue();

	/**
	*	\brief -- Удалённый монитор;
	*	\param collName - имя коллекции;
	*	\param mon_ip -- IP монитора.
	*/
	void connectWithCollection(QString collName, QString mon_ip);
	/**
	*	\brief -- Подключени к бд;
	*	\param DB -- Выбранная БД;
	*/
	void connectToDB(QString DB);

	/**
	*	\brief -- Прогресс в прогрессбаре;
	*	\param value -- значение;
	*	\param msg -- Сообщение.
	*/
	void changeProgressLabel(int value, QString msg);

	/**
	*	\brief -- Приблизить;
	*/
	void zoomIn();

	/**
	*	\brief -- Отдалить;
	*/
	void zoomOut();

	/**
	*	\brief -- Сдвинуть влево;
	*/
	void zoomLeft();
	
	/**
	*	\brief -- Сдвинуть вправо;
	*/
	void zoomRight();
};
#endif // GRATHWIDG_H