#ifndef MYXMLREADER_H
#define MYXMLREADER_H
#include <qstring.h>
#include <boost/property_tree/ptree.hpp>
#include <qscriptengine.h>
#include "Operator.hpp"

/**
*!\class OperatorsXmlReader
*	\brief ������ ����������
*
*/
class OperatorsXmlReader
{
public:
	/**
	*	\brief ��������� XML ������ � ��������� ����������
	*	\param text - ����� XML
	*	\return ������ ����������
	*/
	void parse(const QString& text, QList<const Operator*>& op_list, QList<const OpGroup*>& group_list);
private:
	/**
	*	\brief �������������� �������� ���� � ��� QVariant
	*	\param name - �������� ����
	*	\return ��� � QVariant
	*/
	QVariant::Type getTypeFromName(const QString& name);
	/**
	*	\brief ��������� �������� ���������
	*	\param param - �����(�������� ���������)
	*	\return ��������
	*/
	Param* parseParam(const boost::property_tree::wptree::value_type& param);
	/**
	*	\brief ��������� �������� ������ ����������
	*	\param param - �����(�������� ������)
	*	\return ������ ����������
	*/
	OpGroup* parseGroup(const boost::property_tree::wptree::value_type& group, OpGroup* parent);
private:
	///����������� ��������
	QScriptEngine eng;
	///���������� XML
	boost::property_tree::wptree pt;
};


#endif // MYXMLREADER_H
