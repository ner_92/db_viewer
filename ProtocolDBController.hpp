#ifndef PROTOCOLDBCONTROLLER_H
#define PROTOCOLDBCONTROLLER_H

#include <QString>
#include <QMutexLocker>
#include <QObject>
#include <qvariant.h>
#include <exception>
#include <list>
#include <string>
#include <functional>
#include "CommonDBController.h"

/// -- ��������� ��������� ��� �� ���������.
struct MsgFromProtocolDB
{
	/**
	*	\brief -- �����������;
	*	\param _dt -- ����� �������;
	*	\param _msg -- ���������;
	*	\param _bv -- �������� �����.
	*/
	MsgFromProtocolDB(qint64 _dt = 0, QString _msg = "", unsigned short _bv = 0)
	:dt(_dt), msg(_msg), bv(_bv)
	{}
	/// -- ����� �������;
	qint64 dt = 0;
	/// -- ��������� ���������;
	QString msg = "";
	/// -- �������� �����.
	unsigned short bv = 0;
};
Q_DECLARE_METATYPE(MsgFromProtocolDB);

/**
*!\class MainSrvDBCtrl
*	\brief ���������� �� ���������.
*
*/
class ProtocolDBController : public CommonDBController
{
public:
	/**
	*	\brief -- �����������;
	*	\param _read_delay -- ������ ������.
	*/
	ProtocolDBController(int _read_delay) : CommonDBController(_read_delay) {}
	/**
	*	\brief -- ���������� ����� ������;
	*	\param _msg -- ���������;
	*	\param dt -- ����� �������;
	*	\return -- ������� ���������� ���������� ������.
	*/
	virtual bool insertProtocolData(QString _msg, qint64 dt = 0) = 0;
	/**
	*	\bried -- ���������� ������� ����� ������.
	*	\param msg_list -- ������ ����� �������;
	*	\return -- ������� ���������� ���������� ������.
	*/
	virtual bool insertProtocolDataArr(const std::vector<MsgFromProtocolDB> msg_list) = 0;
};


#include "mongo_v3/MongoProtocolDBController_v3.h"

#endif // PROTOCOLDBCONTROLLER_H