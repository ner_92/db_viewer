#ifndef TM_NAME_WIDGET_H
#define TM_NAME_WIDGET_H
#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include <QListWidget>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QString>
#include <QLabel>
#include <QComboBox>
#include <QTableView>
#include <QStandardItemModel>
//#include "DBThread.h"
#include <QScrollBar>
#include <QWheelEvent>
#include <QCoreApplication>
#include <QToolTip>
#include <QMenu>
#include <QMenuBar>
#include <QStringListModel>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QGroupBox>
#include <QCheckBox>
#include <QSettings>
#include <QFileDialog>
#include <QDialog>
#ifdef WIN32
#include <Windows.h>
#endif
#include <QDateTime>

/**
*	\class TM_name_widget
*	\brief -- класс TM_name_widget;
*/
class TM_name_widget : public QWidget
{
	Q_OBJECT

public:
	/**
	*	\brief -- Конструктор;
	*	\param DB_name -- Имя БД ТМ.
	*	\param parent -- Родитель.
	*/
	TM_name_widget(QString DB_name, QWidget *parent = 0);

	/**
	*	\brief -- Деструктор;
	*/
	~TM_name_widget();

	QList<QMap<QString, QVariant> > getQueryStruct();

	void clearFields();

private:
	/// -- Виджет;
	QWidget* widg;

	/// -- Кнопка ТМ;
	QPushButton *TM_name_button;

	/// -- Поле МСК;
	QLineEdit *MSK1Edit;
	/// -- Поле МСК;
	QLineEdit *MSK2Edit;
	/// -- Поле БВ;
	QLineEdit *BT1Edit;
	/// -- Поле БВ;
	QLineEdit *BT2Edit;
	/// -- Поле номер ТМ;
	QLineEdit *numberTMEdit1;
	/// -- Поле номер ТМ;
	QLineEdit *numberTMEdit2;
	/// -- Поле наименование ТМ;
	QLineEdit *nameTMEdit;
	/// -- Поле измеренного значения;
	QLineEdit *measuredValueEdit1;
	/// Поле измеренного значения;
	QLineEdit *measuredValueEdit2;
	/// -- Поле метка;
	QLineEdit *metkaEdit;

	/// -- Метка МСК;
	QLabel *MSKLabel;
	/// -- Метка метка;
	QLabel *metkaLabel;
	/// -- Метка номер ТМ;
	QLabel *numberTMLabel;
	///--  Метка наименование ТМ;
	QLabel *nameTMLabel;
	/// -- Метка измеренное значение;
	QLabel *measuredValueLabel;
	/// -- Метка БВ;
	QLabel *BTLabel;

	/// -- Групбокс фильтр ТМ;
	QGroupBox *filter1_TM_name_GroupBox;
	/// -- Групбокс фильтр ТМ;
	QGroupBox *filter2_TM_name_GroupBox;
	/// -- Групбокс фильтр ТМ;
	QGroupBox *filter3_TM_name_GroupBox;
	/// -- Групбокс фильтр ТМ;
	QGroupBox *filter4_TM_name_GroupBox;
	/// -- Компановка;
	QHBoxLayout *h_filter_TM_name_Layout;
	/// -- Компановка;
	QGridLayout *TM_name_GridLayout;
	/// -- Компановка;
	QGridLayout *TM_name_mini1GridLayout;
	/// -- Компановка;
	QGridLayout *TM_name_mini2GridLayout;
	/// -- Компановка;
	QGridLayout *TM_name_mini3GridLayout;
	/// -- Компановка;
	QGridLayout *TM_name_mini4GridLayout;
	/// -- Меню;
	QMenu *settingsMenu_TM_name_;

	/// -- Групбокс фильтр ТМ МЦА;
	QGroupBox *filter1TMMCAGroupBox;
	/// -- Групбокс фильтр ТМ МЦА;
	QGroupBox *filter2TMMCAGroupBox;
	/// -- Групбокс фильтр ТМ МЦА;
	QGroupBox *filter3TMMCAGroupBox;
	/// -- Групбокс фильтр ТМ МЦА;
	QGroupBox *filter4TMMCAGroupBox;
	/// -- Компановка;
	QHBoxLayout *h_filterTMMCALayout;

	/// -- Компановка;
	QGridLayout *DB_TM_MCAmini1GridLayout;
	/// -- Компановка;
	QGridLayout *DB_TM_MCAmini2GridLayout;
	/// -- Компановка;
	QGridLayout *DB_TM_MCAmini3GridLayout;
	/// -- Компановка;
	QGridLayout *DB_TM_MCAmini4GridLayout;

};
#endif // TM_NAME_WIDGET_H