#ifndef TMDBCONTROLLER_H
#define TMDBCONTROLLER_H

#include <QString>
#include <QMutexLocker>
#include <QObject>
#include <qvariant.h>
#include <exception>
#include <list>
#include <string>
#include <functional>
#include "CommonDBController.h"

/// -- Структура сообщений для БД ТМ МЦА.
struct MsgFromTMDB
{
	/**
	*	\brief -- Конструктор;
	*	\param _dt -- метка времени;
	*	\param _tm_code -- код сигнала;
	*	\param _value -- значение;
	*	\param _bv -- бортовое время.
	*/
	MsgFromTMDB(long long _dt = 0, int _tm_code = 0, QVariant _value = QVariant(), unsigned short _bv = 0)
	: dt(_dt), tm_code(_tm_code), value(_value), bv(_bv)
	{}
	/// -- Метка времени;
	long long dt = 0;
	/// -- Код сигнала;
	int tm_code = 0;
	/// -- Значение;
	QVariant value = QVariant();
	/// -- Бортовое время.
	unsigned short bv = 0;
};
Q_DECLARE_METATYPE(MsgFromTMDB);

/**
*!\class MainSrvDBCtrl
*	\brief -- Управление БД ТМ МЦА.
*
*/
class TMDBController : public CommonDBController
{
public:
	/**
	*	\brief -- Конструктор класса MCADBController;
	*	\param _read_delay -- задержка чтения.
	*/
	TMDBController(int _read_delay) : CommonDBController(_read_delay) {}

	/**
	*	\brief -- Запись значения ТМ в базу;
	*	\param tm_code -- код сигнала;
	*	\param value -- значение.
	*/
	virtual bool insertTMData(int tm_code, const QVariant& value) = 0;
	/**
	*	\brief -- Запись значения ТМ в базу;
	*	\param msg_list -- вектор сообшений.
	*/
	virtual bool insertTMData(const std::vector<MsgFromTMDB>& msg_list) = 0;
	/**
	*	\brief -- Чтение ТМ сигнала из БД АИК.
	*	\param tm_code -- код сигнала;
	*	\return -- значение.
	*/
	virtual QVariant getTMData(int tm_code) = 0;

};


#include "mongo_v3/MongoTMDBController_v3.h"

#endif // TMDBCONTROLLER_H