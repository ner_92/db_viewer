
#include "CommonDBController.h"


/**
*	\brief Добавление функции обратного вызова в список
*	\param func - функция обратного вызова
*/
void CommonDBInterface::addCallBack(DBCallback& func)
{
	QMutexLocker lock(&callback_mtx);
	callback_list.push_back(func);
}
///Очистка списка функций обратного вызова
void CommonDBInterface::clearCallBackList()
{
	QMutexLocker lock(&callback_mtx);
	callback_list.clear();
}

///Обработка списка сообщений
void CommonDBInterface::process(const std::vector<QVariant>& msg_list)
{
	QMutexLocker lock(&callback_mtx);
	foreach(DBCallback func, callback_list)
		func(msg_list);
}

/**
*	\brief Добавление функции обратного вызова в список
*	\param func - функция обратного вызова
*/
void CommonDBController::addCallBack(DBCallback& func)
{
	DBInterface->addCallBack(func);
}
///Очистка списка функций обратного вызова
void CommonDBController::clearCallBackList()
{
	DBInterface->clearCallBackList();
}

void CommonDBController::startRead()
{
	emit startIntervalSignal(db_read_delay);
}

void CommonDBController::stopRead()
{
	emit stopIntervalSignal();
}


void CommonDBController::readDataFromDB()
{
	DBInterface->readDataFromDB();
}

bool CommonDBController::tryConnect(QString col_name, QString db_ip, bool create)
{
	return DBInterface->tryConnect(col_name, db_ip, create);
}
///Получение блока данных
std::vector<QVariant> CommonDBController::getDataBlock()
{
	return DBInterface->getDataBlock();
}

std::vector<QVariant> CommonDBController::getIntervalDataBlock()
{
	return DBInterface->getIntervalDataBlock();
}

std::vector<QVariant> CommonDBInterface::getIntervalDataBlock()
{
	std::vector<QVariant> tmp_list;
	QMutexLocker lock(&interval_list_mtx);
	tmp_list.swap(interval_msg_list);
	interval_msg_list.clear();
	return tmp_list;
}

void CommonDBInterface::addIntervalDataBlock(std::vector<QVariant> add_list)
{
	//QMutexLocker lock(&interval_list_mtx);
	//if (add_list.empty())
	//	return;
	//if ((add_list.size() == INTERVAL_DATA_SIZE) || interval_msg_list.empty())
	//{
	//	interval_msg_list.swap(add_list);
	//	return;
	//}

	//int msgs_to_pop = interval_msg_list.size() + add_list.size() - INTERVAL_DATA_SIZE;
	//std::vector<QVariant> tmp_vector;
	//if (msgs_to_pop < 0)
	//{
	//	msgs_to_pop = 0;
	//}

	//std::copy(interval_msg_list.begin() + msgs_to_pop, interval_msg_list.end(), std::back_inserter(tmp_vector));

	//std::copy(add_list.begin(), add_list.end(), std::back_inserter(tmp_vector));

	//interval_msg_list.swap(tmp_vector);

	interval_msg_list.swap(add_list);
}

///Слот запроса и обработки новых сообщений(вызывается по таймеру)
void CommonDBController::readDataFromDBQuery(QList<QMap<QString, QVariant> > *queryList)
{
	DBInterface->readDataFromDBQuery(queryList);
}

/**
*	brief Слот запроса и обработки сообщений
*	\param _start_pos - стартовая позиция блока
*	\param _block_size - количество блоков
*	\param queryList - структура с данными для запроса
*	\param rezh - режим работы с _id для скроллинга. 0 - не работаем, 1 - скроллинг вниз
*	\param BV -
*  \return
*/
void CommonDBController::readBlockData(int _start_pos, int _block_size, QList<QMap<QString, QVariant> > *queryList, bool needCount, int rezh)
{
	DBInterface->readBlockData(_start_pos, _block_size, queryList, needCount, rezh);
}

//Слот запроса количесва записей
//void getCountRec(QString query_str);
void CommonDBController::getCountRec(QList<QMap<QString, QVariant> > *queryList)
{
	DBInterface->getCountRec(queryList);
}

void CommonDBController::synchronize(QList<QMap<QString, QVariant> > *queryList)
{
	DBInterface->synchronize(queryList);
}

///Слот запроса коллекций
void CommonDBController::getCollectionNames()
{
	DBInterface->getCollectionNames();
}

///Сохраняет информации о последней прочитанной записи
void CommonDBController::saveLastData()
{
	DBInterface->saveLastData();
}


std::list<std::string> CommonDBController::getCollList()
{
	return DBInterface->getCollList();
}

int CommonDBController::getCount()
{
	return DBInterface->getCount();
}

QString CommonDBController::getQueryString(QList<QMap<QString, QVariant> > *list)
{
	return DBInterface->getQueryString(list);
}


bool CommonDBController::isConnected()
{
	return DBInterface->isConnected();
}



QVariant CommonDBController::selectFieldOrderBy(QString query, QString field, QString orderBy, int sort)
{
	return DBInterface->selectFieldOrderBy(query, field, orderBy, sort);
}

QVariantMap CommonDBController::selectMapOrderBy(QString query, QString field, QString orderBy, int sort)
{
	return DBInterface->selectMapOrderBy(query, field, orderBy, sort);
}